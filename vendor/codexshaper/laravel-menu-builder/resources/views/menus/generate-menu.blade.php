@foreach ($menuItems as $menu)
        @include(
        	'menu::menus.recursive',
        	[
        		'menu'=>$menu,
        		'settings'=>$settings,
        		'i' => 0
        	])
    @endforeach

