
@php $detect = new Mobile_Detect;@endphp

@if($detect->isMobile())
    @if (count($menu->children) > 0)
        <li data-id="{{$menu->id}}" class="menu-item menu-item-has-children has-child">
            <a href="{{ ($menu->url != '') ? $menu->url : '#' }}" class="nav-top-link {{$menu->custom_class}}">
                <span class="menu-icon"></span>{!! $menu->icon !!}
                <span class="menu-title">{{ $menu->title }}</span>
            </a>
    @else
        <li data-id="{{$menu->id}}" class="menu-item">
            <a href="{{ ($menu->url != '') ? $menu->url : '#' }}" class="nav-top-link {{$menu->custom_class}}">
                <span class="menu-icon"></span>{!! $menu->icon !!}
                <span class="menu-title">{{ $menu->title }}</span>
            </a>
    @endif

    @if (count($menu->children) > 0)
        <ul class="children {{ $settings['levels']['root']['style'] }}">
            @foreach($menu->children as $menu)
                @include(
                    'menu::menus.recursive',
                    [
                        'menu'=>$menu,
                        'settings'=>$settings,
                        'i' => $i
                    ])
            @endforeach
        </ul>
    @endif
</li>
@else
    @if (count($menu->children) > 0)
        <li data-id="{{$menu->id}}" class="menu-item has-dropdown">
            <a href="{{ ($menu->url != '') ? $menu->url : '#' }}" class="nav-top-link {{$menu->custom_class}}">
                <span class="menu-icon"></span>{!! $menu->icon !!}
                <span class="menu-title">{{ $menu->title }}</span>
                <i class="icon-angle-down"></i>
            </a>
    @else
        <li data-id="{{$menu->id}}" class="menu-item">
            <a href="{{ ($menu->url != '') ? $menu->url : '#' }}" class="nav-top-link {{$menu->custom_class}}">
                <span class="menu-icon"></span>{!! $menu->icon !!}
                <span class="menu-title">{{ $menu->title }}</span>
            </a>
            @endif

            @if (count($menu->children) > 0)
                <ul class="nav-dropdown nav-dropdown-simple dropdown-uppercase
         {{ $settings['levels']['root']['style'] }}">
                    @foreach($menu->children as $menu)
                        @include(
                            'menu::menus.recursive',
                            [
                                'menu'=>$menu,
                                'settings'=>$settings,
                                'i' => $i
                            ])
                    @endforeach
                </ul>
            @endif
        </li>
@endif
