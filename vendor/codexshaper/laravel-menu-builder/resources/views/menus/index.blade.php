@extends('layouts.dboard')

@section('page-header')
<div class="row mb-2">
    <div class="col-sm-6">
        <h1 class="m-0 text-dark">Menus</h1>
    </div>
</div>
@endsection

@section('content')
<link href="{{ menu_asset('css/menu.css') }}" rel="stylesheet">
<link href="{{ menu_asset('css/style.css') }}" rel="stylesheet">

<div id="app">
    <nest-menu prefix="{{ menu_prefix() }}"></nest-menu>
</div>

<!-- Scripts -->
<script src="{{ menu_asset('js/app.js') }}"></script>
<script src="{{ menu_asset('js/menu.js') }}"></script>
@endsection