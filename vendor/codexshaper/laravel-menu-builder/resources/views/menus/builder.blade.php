@extends('layouts.dboard')

@section('page-header')
<div class="row mb-2">
    <div class="col-sm-6">
        <h1 class="m-0 text-dark">Menu Builder</h1>
    </div>
</div>
@endsection

@section('content')

<link href="{{ menu_asset('css/menu.css') }}" rel="stylesheet">
<link href="{{ menu_asset('css/style.css') }}" rel="stylesheet">


<div id="app">
    <div class="cx-main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <draggable-menu :menu="{{ $menu }}" prefix="{{ menu_prefix() }}"></draggable-menu>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Scripts -->
<script src="{{ menu_asset('js/app.js') }}"></script>
<script src="{{ menu_asset('js/menu.js') }}"></script>
@endsection