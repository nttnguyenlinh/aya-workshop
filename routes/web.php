<?php
use Illuminate\Support\Facades\Artisan;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Route::get('/faq', array('uses' => 'FAQController@index', 'as' => 'faq.index'));
| Route::get('/faq', ['uses' => 'FAQController@index', 'as' => 'faq.index']);
| Route::get('/faq', 'FAQController@index')->name('faq.index');
|
php artisan cache:clear
php artisan route:clear
php artisan config:clear
php artisan config:cache
php artisan view:clear

|--------------------------------------------------------------------------
 */

Route::get('menu', function () {
    return view('home.menu');
});

/* Clear all cache */
Route::get('clear', function () {
    Artisan::call('data:clear');
    return redirect()->route('home');
});

/* Get php info */
Route::get('info', function () {
    return view('phpinfo');
})->name('info');

/* Home */
Route::get('/forget-session/{session}', 'HomeController@forgetSession')->name('forgetSession');

/* Home */
Route::get('/', 'HomeController@index')->name('home');

/* Products */
Route::get('products', 'HomeController@products')->name('products');
Route::get('product-category/{slug}', 'HomeController@productCategory')->name('product-category');
Route::get('products/{slug}', 'HomeController@productDetails')->name('productDetails');
Route::get('products/images/{product_id}', 'HomeController@productImages');

/* Posts */
Route::get('posts', 'HomeController@posts')->name('posts');
Route::get('post-category/{slug}', 'HomeController@postCategory')->name('post-category');
Route::get('posts/{slug}', 'HomeController@postDetails')->name('postDetails');

/* FAQ */
Route::get('faq', 'FAQController@index')->name('faq.index');
Route::post('faq', 'FAQController@store')->name('faq.store');

/* Contact */
Route::get('contact', 'ContactController@create')->name('contact.create');
Route::post('contact', 'ContactController@store')->name('contact.store');

/* Track order */
Route::get('track-order', 'HomeController@trackOrder')->name('track.order');

/* Cart */
Route::group(['prefix' => 'cart'], function () {
    Route::get('/', 'HomeController@cart')->name('cart');
    Route::post('add', 'HomeController@addCart')->name('addCart');
    Route::get('update/{rowId}', 'HomeController@updateCart')->name('updateCart');
    Route::get('remove/{rowId}', 'HomeController@removeCart')->name('removeCart');
    Route::get('destroy', 'HomeController@destroyCart')->name('destroyCart');
});

/* Checkout */
Route::get('checkout', 'HomeController@checkout')->name('checkout');
Route::post('checkout', 'HomeController@postCheckout')->name('postCheckout');
Route::get('checkout/order-received', 'HomeController@orderReceived')->name('orderReceived');
Route::post('coupon/apply', 'HomeController@applyCoupon')->name('applyCoupon');

/* My Account Group*/

Route::group(['prefix' => 'my-account'], function () {
    Route::get('register', 'AccountController@getRegister')->name('account.getRegister');
    Route::post('register', 'AccountController@register')->name('account.register');
    Route::get('login', 'AccountController@getLogin')->name('account.getLogin');
    Route::post('login', 'AccountController@login')->name('account.login');
    Route::get('logout', 'AccountController@logout')->name('account.logout');

    Route::group(['middleware' => 'checkaccountlogin'], function () {
        Route::group(['middleware' => 'verified'], function () {
            Route::get('/', 'AccountController@index')->name('myaccount');
            Route::get('edit-account', 'AccountController@editAccount')->name('editAccount');
            Route::post('edit-account', 'AccountController@editAccountSubmit')->name('editAccountSubmit');
            Route::get('orders', 'AccountController@orders')->name('account.orders');
            Route::get('view-order/{bill_id}', 'AccountController@viewOrder')->name('account.viewOrder');
        });
    });
});

/* Dashboard Group*/
Route::group(['prefix' => 'dashboard'], function () {
    Route::get('login', 'AuthController@getLogin')->name('dashboard.getLogin');
    Route::post('login', 'AuthController@login')->name('dashboard.login');
    Route::get('logout', 'AuthController@logout')->name('dashboard.logout');

    Route::group(['middleware' => 'checkadminlogin'], function () {
        Route::get('/', 'DashboardController@index')->name('dashboard');
        Route::get('users', 'DashboardController@users')->name('dashboard.users');
        Route::get('anyUsers', 'DashboardController@anyUsers')->name('dashboard.anyUsers');
        Route::get('findCustomer/{id}', 'DashboardController@findCustomer')->name('dashboard.findCustomer');
        Route::post('users/add', 'DashboardController@postAddUsers')->name('dashboard.postAddUsers');
        Route::get('users/UserByID/{id}', 'DashboardController@getUserByID')->name('dashboard.getUserByID');
        Route::post('users/edit', 'DashboardController@postEditUsers')->name('dashboard.postEditUsers');

        Route::get('posts', 'DashboardController@posts')->name('dashboard.posts');
        Route::get('posts/list', 'DashboardController@posts_list')->name('dashboard.posts.list');
        Route::delete('post/delete', 'DashboardController@post_delete')->name('dashboard.post.delete');
        Route::post('post/trash', 'DashboardController@post_trash')->name('dashboard.post.trash');
        Route::get('post/edit/{id}', 'DashboardController@post_edit')->name('dashboard.post.edit');
        Route::post('post/edit', 'DashboardController@post_update')->name('dashboard.post.update');
        Route::get('post/create', 'DashboardController@post_create')->name('dashboard.post.create');
        Route::post('post/create', 'DashboardController@post_store')->name('dashboard.post.store');
        Route::get('post/category', 'DashboardController@post_category')->name('dashboard.post.category');
        Route::get('post/category/list', 'DashboardController@post_category_list')->name('dashboard.post.category.list');
        Route::get('post/tags', 'DashboardController@post_tag')->name('dashboard.post.tags');
        Route::get('post/tag/list', 'DashboardController@post_tag_list')->name('dashboard.post.tag.list');

        Route::get('category/{id}', 'DashboardController@category_show')->name('dashboard.category.show');
        Route::post('category/store', 'DashboardController@category_store')->name('dashboard.category.store');
        Route::get('category/edit/{id}', 'DashboardController@category_edit')->name('dashboard.category.edit');
        Route::post('category/update', 'DashboardController@category_update')->name('dashboard.category.update');
        Route::delete('category/delete', 'DashboardController@category_delete')->name('dashboard.category.delete');

        Route::get('post-tag/autocomplete', 'DashboardController@tag_autocomplete')->name('dashboard.post.tag.autocomplete');
        Route::delete('post-tag/delete', 'DashboardController@post_tag_delete')->name('dashboard.post.tag.delete');
        Route::post('tag/store', 'DashboardController@tag_store')->name('dashboard.tag.store');
        Route::delete('tag/delete', 'DashboardController@tag_delete')->name('dashboard.tag.delete');

        Route::group(['prefix' => 'products'], function () {
            Route::get('', 'DashboardController@products')->name('dashboard.products');
            Route::get('list', 'DashboardController@products_list')->name('dashboard.products.list');
        });

        Route::group(['prefix' => 'product'], function () {
            Route::get('category', 'DashboardController@product_category')->name('dashboard.product.category');
            Route::get('category/list', 'DashboardController@product_category_list')->name('dashboard.product.category.list');
            Route::get('tags', 'DashboardController@product_tag')->name('dashboard.product.tags');
            Route::delete('tag/delete', 'DashboardController@product_tag_delete')->name('dashboard.product.tag.delete');
            Route::get('tag/list', 'DashboardController@product_tag_list')->name('dashboard.product.tag.list');
            Route::get('tag/autocomplete', 'DashboardController@product_tag_autocomplete')->name('dashboard.product.tag.autocomplete');

            Route::get('create', 'DashboardController@product_create')->name('dashboard.product.create');
            Route::post('store', 'DashboardController@product_store')->name('dashboard.product.store');
            Route::post('create-draft', 'DashboardController@product_create_draft')->name('dashboard.product.create.draft');
            Route::get('edit/{id}', 'DashboardController@product_edit')->name('dashboard.product.edit');
            Route::post('update', 'DashboardController@product_update')->name('dashboard.product.update');
            Route::delete('delete', 'DashboardController@product_delete')->name('dashboard.product.delete');
            Route::post('trash', 'DashboardController@product_trash')->name('dashboard.product.trash');
            Route::post('publish', 'DashboardController@product_publish')->name('dashboard.product.publish');
            Route::get('featured/{id}', 'DashboardController@product_featured')->name('dashboard.product.featured');

            Route::get('attributes-load', 'DashboardController@product_attributes_load')->name('dashboard.product.attributes.load');
            Route::post('attribute/add', 'DashboardController@product_attribute_add')->name('dashboard.product.attribute.add');
            Route::get('variant-get', 'DashboardController@product_variant_get')->name('dashboard.product.variant.get');
            Route::post('variant-load', 'DashboardController@product_variant_load')->name('dashboard.product.variant.load');
            Route::post('variant-update', 'DashboardController@product_variant_update')->name('dashboard.product.variant.update');
            Route::delete('simple-variant-delete', 'DashboardController@product_simple_variant_delete')->name('dashboard.product.simple.variant.delete');

            Route::post('images', 'DashboardController@product_images')->name('dashboard.product.images');
            Route::post('images/remove', 'DashboardController@product_images_remove')->name('dashboard.product.images.remove');
            Route::get('edit/images/load', 'DashboardController@LoadImages')->name('dashboard.images.load');
            Route::post('images/upload', 'DashboardController@product_images_upload')->name('dashboard.product.images.upload');
        });


        Route::get('attributes', 'DashboardController@attributes')->name('dashboard.attributes');
        Route::post('attributes', 'DashboardController@storeAttributes')->name('dashboard.attributes.store');
        Route::get('attribute/edit/{id}', 'DashboardController@editAttribute')->name('dashboard.attribute.edit');
        Route::post('attribute/edit', 'DashboardController@updateAttribute')->name('dashboard.attribute.update');
        Route::delete('attribute/delete', 'DashboardController@deleteAttribute')->name('dashboard.attribute.delete');

        Route::get('attribute/{id}', 'DashboardController@terms')->name('dashboard.attribute.terms');
        Route::post('terms', 'DashboardController@storeTerms')->name('dashboard.terms.store');
        Route::get('term/{id}', 'DashboardController@term_detail')->name('dashboard.term.detail');
        Route::get('term/edit/{id}', 'DashboardController@editTerm')->name('dashboard.term.edit');
        Route::post('term', 'DashboardController@updateTerm')->name('dashboard.term.update');
        Route::get('terms/list/{id}', 'DashboardController@listTerms')->name('dashboard.terms.list');
        Route::delete('terms/delete', 'DashboardController@deleteTerms')->name('dashboard.terms.delete');

        Route::get('media', 'DashboardController@media')->name('dashboard.media');
        Route::get('media/file-explorer.php', function () {
            return view('dashboard.media.explorer');
        });

        Route::get('orders', 'DashboardController@orders')->name('dashboard.orders');
        Route::get('anyOrders', 'DashboardController@anyOrders')->name('dashboard.anyOrders');
        Route::post('order/change', 'DashboardController@orderChange')->name('dashboard.orderChange');

        Route::get('anyStatus', 'DashboardController@anyStatus')->name('dashboard.anyStatus');
        Route::get('order/getCustomer/{bill_id}', 'DashboardController@getCustomerByBill')->name('dashboard.getCustomerByBill');
        Route::get('order/getDetails/{bill_id}', 'DashboardController@getDetailsByBill')->name('dashboard.getDetailsByBill');

        Route::get('coupons', 'DashboardController@coupons')->name('dashboard.coupons');
        Route::post('coupons', 'DashboardController@postCoupons')->name('dashboard.postCoupons');
        Route::get('anyCoupons', 'DashboardController@anyCoupons')->name('dashboard.anyCoupons');
        Route::delete('coupons/delete', 'DashboardController@deleteCoupon')->name('dashboard.deleteCoupon');

        Route::get('options', 'DashboardController@options')->name('dashboard.options');
        Route::post('options', 'DashboardController@storeOptions')->name('dashboard.options.store');
        Route::delete('options/delete', 'DashboardController@deleteOptions')->name('dashboard.options.delete');

    });
});

/* Forgot Password*/

/* Auth Config*/
Auth::routes([
    'verify' => true,
    'reset' => true,
]);

Route::match(['get', 'post'], 'register', function () {
    abort(404);
});

/* ------------------------------------------------------------*/

/* Test Error 403 404 500*/

// Route::get('403', function () {
//     abort(403);
// });

// Route::get('404', function () {
//     abort(404);
// });

// Route::get('500', function () {
//     abort(500);
// });

/* ------------------------------------------------------------*/

/* Test Model Customers Factory*/

// Route::get('customers-factory', function () {
//     $faker = Faker\Factory::create('vi_VN');
//     $customers = [];
//     for ($i = 0; $i < 1000; $i++) {
//         $customers[$i] = [
//             'Họ và tên'     => $faker->name,
//             'Email'         => $faker->unique()->email,
//             'Số điện thoại' => $faker->unique()->phoneNumber,
//             'Website'       => $faker->domainName,
//             'Tuổi'          => $faker->numberBetween(20, 80),
//             'Địa chỉ'       => $faker->address,
//             'Thành Phố'     => $faker->city,
//             'Credit Card'  => $faker->creditCardNumber,
//             'Mô tả'         => $faker->text(200),
//         ];
//     }
//     return response()->json($customers);
// });

/* ------------------------------------------------------------*/

/* Test Storage*/

// Route::get('storage/put', function () {
//     Storage::disk('local')->put('file.txt', 'Contents');
//     Storage::disk('public')->put('file.txt', 'Contents');
// });

// Route::get('loadimages/{filename}', function ($filename) {
//     $path = storage_path('app\public\\') . $filename;
//     if (!File::exists($path)) {
//         abort(404);
//     }

//     $file = File::get($path);
//     $type = File::mimeType($path);

//     $response = Response::make($file, 200);
//     $response->header("Content-Type", $type);

//     return $response;
// });

Route::get('loadimages', 'DashboardController@loadimages');

Route::get('locale/{locale}', 'HomeController@Locale')->name('locale');

Route::group(['prefix' => config('menu.prefix')], function () {
    MenuBuilder::routes();
});
