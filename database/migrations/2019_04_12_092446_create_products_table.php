<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->unique();
            $table->string('slug')->unique();
            $table->string('sku')->nullable();
            $table->string('type')->nullable();
            $table->decimal('min_price', 8, 2)->nullable()->default(0);
            $table->decimal('max_price', 8, 2)->nullable()->default(0);
            $table->tinyInteger('onsale')->nullable()->default(0);
            $table->dateTime('sale_start')->nullable();
            $table->dateTime('sale_end')->nullable();
            $table->tinyInteger('in_stock')->nullable()->default(1);
            $table->integer('stock_quantity')->nullable()->default(0);
            $table->text('description')->nullable();
            $table->longText('contents')->nullable();
            $table->integer('total_sales')->nullable()->default(0);
            $table->boolean('featured')->default(false);
            $table->string('thumbnail')->nullable();
            $table->string('seo_title')->nullable();
            $table->string('seo_keyword')->nullable();
            $table->string('meta_description')->nullable();
            $table->string('facebook_image')->nullable();
            $table->tinyInteger('visibility')->nullable()->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
