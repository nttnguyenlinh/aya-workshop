<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 50);
            $table->string('slug', 50);
            $table->string('type')->nullable();
            $table->string('description', 50)->nullable();
            $table->longText('top_content')->nullable();
            $table->longText('bottom_content')->nullable();
            $table->bigInteger('parent')->default(0);
            $table->string('thumbnail')->nullable();
            $table->integer('count')->nullable()->default(0);
            $table->integer('position')->nullable()->default(0);
            $table->tinyInteger('isDelete')->nullable()->default(1);
            $table->tinyInteger('visibility')->nullable()->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
