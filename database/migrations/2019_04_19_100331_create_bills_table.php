<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bills', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('customer_id')->unsigned();
            $table->foreign('customer_id')->references('id')->on('customers');
            $table->string('coupon_code')->nullable();
            $table->integer('coupon_amount')->nullable();
            $table->double('subtotal_amount')->nullable();
            $table->double('total_amounts')->nullable();
            $table->string('notes')->nullable();
            $table->string('shipping_address')->nullable();
            $table->tinyInteger('payment')->default(0)->nullable(false);
            $table->text('payment_info')->nullable();
            $table->bigInteger('shipping_status')->unsigned()->default(1);
            $table->foreign('shipping_status')->references('id')->on('shipping_status');
            $table->string('phone', 15)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bills');
    }
}
