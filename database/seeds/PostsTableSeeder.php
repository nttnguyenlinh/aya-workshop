<?php

use Illuminate\Database\Seeder;
use App\Post;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $posts = [
            ['KZ ZS5 No Mic', 'kz-zs5-no-mic', 'pgjGq_02.png'],
            ['iBasso IT01', 'ibasso-it01', 'pgjGq_02.png'],
            ['KZ AS6', 'kz-as6', 'pgjGq_02.png'],
            ['True Wireless Sabbat E12', 'true-wireless-sabbat-e12', 'pgjGq_02.png'],
            ['True Wireless Audio-Technica ATH-CKR7TW', 'true-wireless-audio-technica-ath-ckr7tw', 'pgjGq_02.png'],
            ['Soranik x Satin SP3 LTD', 'soranik-x-satin-sp3-ltd', 'pgjGq_02.png'],
            ['Silver SA-01X', 'silver-sa-01x', 'pgjGq_02.png'],
            ['Silver SA-01', 'silver-sa-01', 'pgjGq_02.png'],
            ['Sony IER-Z1R', 'sony-ier-z1r', 'pgjGq_02.png'],
            ['64 Audio A10 Custom IEM', '64-audio-a10-custom-iem', 'pgjGq_02.png'],
        ];

        foreach ($posts as $item) {
            $post = new Post();
            $post->name = $item[0];
            $post->slug = $item[1];
            $post->author = 1;
            $post->excerpt = $item[0];
            $post->contents = $item[0];
            $post->thumbnail = "images/".$item[2];
            $post->facebook_image = "images/".$item[2];
            $post->seo_title = $item[0];
            $post->meta_description = $item[0];
            $post->save();
        }
    }
}
