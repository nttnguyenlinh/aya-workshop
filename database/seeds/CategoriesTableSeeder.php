<?php

use Illuminate\Database\Seeder;
use App\Category;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            ['News', 'news', 'post', 0, 100, 0],
            ['Uncategorized', 'uncategorized', 'product', 0, 100, 0],
            ['Reviews', 'reviews', 'post', 0, 0, 0],
            ['Earphones', 'earphones', 'product', 0, 0, 0],
            ['Accessories', 'accessories', 'product', 0, 0, 0],

        ];

        foreach ($categories as $item) {
            $category = new Category();
            $category->name = $item[0];
            $category->slug = $item[1];
            $category->type = $item[2];
            $category->description = $item[0];
            $category->parent = $item[3];
            $category->position = $item[4];
            $category->isDelete = $item[5];
            $category->save();
        }
    }
}
