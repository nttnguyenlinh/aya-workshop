<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UsersTableSeeder::class,
            CustomersTableSeeder::class,
            CategoriesTableSeeder::class,
            ProductsTableSeeder::class,
            PostsTableSeeder::class,
            ShippingStatusTableSeeder::class,
            FAQTableSeeder::class,
            MenuSettingsSeeder::class
        ]);
    }
}
