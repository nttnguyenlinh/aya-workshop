<?php

use Illuminate\Database\Seeder;
use App\ShippingStatus;

class ShippingStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $status = [
            ['Processing'],
            ['Pending payment'],
            ['Pending refund'],
            ['Successfully'],
            ['Failed'],
            ['On hold'],
            ['Refunded'],
        ];

        foreach ($status as $item) {
            $status = new ShippingStatus();
            $status->title = $item[0];
            $status->save();
        }
    }
}
