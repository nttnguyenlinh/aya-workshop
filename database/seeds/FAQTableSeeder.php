<?php

use Illuminate\Database\Seeder;
use App\FAQ;

class FAQTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faqs = [
            ['delivery', '<p data-animate="fadeIn"> <img class="aligncenter size-full wp-image-946" src="https://ayaworkshop.com/wp-content/uploads/2019/03/Capture-1.png" width="1354" height="700"></p><h3 style="text-align: center; color:#A7D6D1;" data-animate="fadeIn"> <strong>Contact us by clicking “SUPPORT” for your more information.</strong></h3><p data-animate="fadeIn"> <img class="wp-image-948 aligncenter" src="https://ayaworkshop.com/wp-content/uploads/2019/03/chuyển-phát-nhanh-hỏa-tốc-từ-Hà-Nội-đi-Quảng-Ninh.png" width="625" height="208"></p>'],
            ['warranty', '<img class="size-full wp-image-973 aligncenter" src="https://ayaworkshop.com/wp-content/uploads/2019/03/W2.png" width="382" height="683"> <img class="size-full wp-image-973 aligncenter" src="https://ayaworkshop.com/wp-content/uploads/2019/03/3.png" width="382" height="683"> <img class="size-full wp-image-973 aligncenter" src="https://ayaworkshop.com/wp-content/uploads/2019/03/5.png" width="382" height="683"> <img class="size-full wp-image-647 aligncenter" src="/uploads/icons/LOGO.png" width="401" height="161">'],
            ['quality', '<img class="size-full wp-image-978 aligncenter" src="https://ayaworkshop.com/wp-content/uploads/2019/03/Quality-Banner.png" width="755" height="475"> <img class="size-full wp-image-647 aligncenter" src="/uploads/icons/LOGO.png" width="401" height="161">'],

        ];

        foreach ($faqs as $item) {
            $faq = new FAQ();
            $faq->title = $item[0];
            $faq->contents = $item[1];
            $faq->save();
        }
    }
}