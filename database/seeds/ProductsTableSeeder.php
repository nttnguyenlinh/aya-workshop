<?php

use Illuminate\Database\Seeder;
use App\Product;
class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = [
            ['KZ ZS5 No Mic', 'kz-zs5-no-mic', '399', 's0bxO_DSC03089.jpg'],
            ['iBasso IT01', 'ibasso-it01', '112', 's0bxO_DSC03089.jpg'],
            ['KZ AS6', 'kz-as6', '100', 's0bxO_DSC03089.jpg'],
            ['True Wireless Sabbat E12', 'true-wireless-sabbat-e12', '80', 's0bxO_DSC03089.jpg'],
            ['True Wireless Audio-Technica ATH-CKR7TW', 'true-wireless-audio-technica-ath-ckr7tw', '289', 's0bxO_DSC03089.jpg'],
            ['Soranik x Satin SP3 LTD', 'soranik-x-satin-sp3-ltd', '164', 's0bxO_DSC03089.jpg'],
            ['Silver SA-01X', 'silver-sa-01x', '399', 's0bxO_DSC03089.jpg'],
            ['Silver SA-01', 'silver-sa-01', '350', 's0bxO_DSC03089.jpg'],
            ['Sony IER-Z1R', 'sony-ier-z1r', '1050', 's0bxO_DSC03089.jpg'],
            ['64 Audio A10 Custom IEM', '64-audio-a10-custom-iem', '1785', 's0bxO_DSC03089.jpg'],
        ];

        foreach ($products as $item) {
            $product = new Product();
            $product->name = $item[0];
            $product->slug = $item[1];
            $product->type = "product";
            $product->min_price = $item[2];
            $product->max_price = $item[2];
            $product->description = $item[0];
            $product->contents = $item[0];
            $product->thumbnail = "images/".$item[3];
            $product->facebook_image = "images/".$item[3];
            $product->seo_title = $item[0];
            $product->meta_description = $item[0];
            $product->save();
        }
    }
}
