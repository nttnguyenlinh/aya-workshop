<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        //Schema::disableForeignKeyConstraints();
        //User::truncate();

        $users = [
            ['admin', 'Phương Duy', 'support@ayaworkshop.com', now(), bcrypt('admin'), 1, 1],
            ['nguyenlinh', 'Nguyễn Linh', 'nguyenlinh2396@gmail.com', now(), bcrypt('admin'), 1, 1],
        ];

        foreach ($users as $item) {
            $user = new User();
            $user->username = $item[0];
            $user->nicename = $item[1];
            $user->email = $item[2];
            $user->email_verified_at = $item[3];
            $user->password = $item[4];
            $user->level = $item[5];
            $user->status = $item[6];
            $user->save();
        }

        //Schema::enableForeignKeyConstraints();

        //or
        //factory(User::class, 100)->create();
    }
}
