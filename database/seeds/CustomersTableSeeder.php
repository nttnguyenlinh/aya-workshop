<?php

use Illuminate\Database\Seeder;
use App\Customer;

class CustomersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $customers = [
            [1, 'VN', 'Phan Xich Long', '70000', 'Ho Chi Minh', 'Phu Nhuan', '0834444444'],
            [2, 'VN', 'Phước Lại', '850000', 'Long An', 'Cần Giuộc', '0906754290'],
        ];

        foreach ($customers as $item) {
            $customer = new Customer();
            $customer->user_id = $item[0];
            $customer->country = $item[1];
            $customer->address = $item[2];
            $customer->zipcode = $item[3];
            $customer->city = $item[4];
            $customer->county = $item[5];
            $customer->phone = $item[6];
            $customer->save();
        }
    }
}
