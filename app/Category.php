<?php

namespace App;

use App\Post;
use Illuminate\Database\Eloquent\Model;
use DataTables;
use Helpers;
use App\Post_Category;
use App\Product;
class Category extends Model
{
    protected $table = 'categories';

    protected $fillable = [
        'name', 'slug', 'type', 'description', 'parent', 'thumbnail', 'count', 'position', 'isDelete', 'visibility'
    ];

    public function posts()
    {
        return $this->belongsToMany(Post::class, 'post_category');
    }

    public function products()
    {
        return $this->belongsToMany(Product::class, 'product_category');
    }
//    public static function product_category()
//    {
//        $data = Category::where('parent', 0)
//                        ->select('id', 'name', 'slug', 'type', 'description', 'parent')
//                        ->get();
//
//        return Datatables::of($data)
//            ->editColumn('id', function ($data) {
//                return '<button id="product_child_view" value="'.$data->id.'" class="btn badge badge-info">'.$data->id.'</button>';
//            })
//            ->editColumn('type', function ($data) {
//                return $data->type . '<button id="delete" value="'.$data->id.'" class="btn badge badge-danger float-right"><i class="fal fa-trash-alt"></i> Remove</button>';
//            })
//            ->rawColumns(['id', 'type'])
//            ->make(true);
//    }
//
    public static function post_category_list()
    {
        $data = Category::where('type', 'post')->select('id', 'name', 'slug', 'description', 'parent', 'count', 'position', 'isDelete')->orderBy('position', 'desc')->get();
        return Datatables::of($data)
            ->editColumn('name', function ($data) {
                $parent_name = Helpers::get_category_name_by_id($data->parent);;

                if($data->parent > 0)
                    $parent = '<span style="color: #0664af;"> — Parent: ' . $parent_name . '</span>';
                else
                    $parent = '';
                if($data->isDelete == 0)
                    return '
                        <strong><a href="' . route('dashboard.category.edit', $data->id) . '">' . $data->name . '</a></strong> ' . $parent . '
                        <div class="row-actions">
                            <span class="edit">
                                <a href="' . route('dashboard.category.edit', $data->id) . '">Edit</a>
                            </span> |
    
                            <span class="quick-edit">
                                <a href="javascript:void(0)" id="quickEdit" data-id="' . $data->id .'">Quick Edit</a>
                            </span>
                        </div>
                    ';
                else
                    return '
                        <strong><a href="' . route('dashboard.category.edit', $data->id) . '">' . $data->name . '</a></strong> ' . $parent . '
                        <div class="row-actions">
                            <span class="edit">
                                <a href="' . route('dashboard.category.edit', $data->id) . '">Edit</a>
                            </span> |
    
                            <span class="quick-edit">
                                <a href="javascript:void(0)" id="quickEdit" data-id="' . $data->id .'">Quick Edit</a>
                            </span> |
    
                            <span class="delete">
                                <a class="delete" href="javascript:void(0)" id="delete" data-id="' . $data->id .'">Delete</a>
                            </span>
                        </div>
                    ';

            })

            ->editColumn('description', function ($data) {
                if(empty($data->description))
                    return '—';
                else
                    return $data->description;
            })
            ->rawColumns(['name', 'description'])
            ->make(true);
    }

    public static function product_category_list()
    {
        $data = Category::where('type', 'product')->select('id', 'name', 'slug', 'description', 'parent', 'top_content', 'bottom_content', 'thumbnail', 'count', 'position', 'isDelete')->orderBy('position', 'desc')->get();
        return Datatables::of($data)

            ->editColumn('thumbnail', function ($data) {
                if(empty($data->thumbnail))
                    return '<img src="' . asset('storage/placeholder.png'). '" alt="' . $data->name. '" class="wp_cat_thumb">';
                else
                    return '<img src="' . asset('storage/' . $data->thumbnail). '" alt="' . $data->name. '" class="wp_cat_thumb">';
            })
            ->editColumn('name', function ($data) {
                $parent_name = Helpers::get_category_name_by_id($data->parent);;

                if($data->parent > 0)
                    $parent = '<span style="color: #0664af;"> — Parent: ' . $parent_name . '</span>';
                else
                    $parent = '';
                if($data->isDelete == 0)
                    return '
                        <strong><a href="' . route('dashboard.category.edit', $data->id) . '">' . $data->name . '</a></strong> ' . $parent . '
                        <div class="row-actions">
                            <span class="edit">
                                <a href="' . route('dashboard.category.edit', $data->id) . '">Edit</a>
                            </span> |
    
                            <span class="quick-edit">
                                <a href="javascript:void(0)" id="quickEdit" data-id="' . $data->id .'">Quick Edit</a>
                            </span>
                        </div>
                    ';
                else
                    return '
                        <strong><a href="' . route('dashboard.category.edit', $data->id) . '">' . $data->name . '</a></strong> ' . $parent . '
                        <div class="row-actions">
                            <span class="edit">
                                <a href="' . route('dashboard.category.edit', $data->id) . '">Edit</a>
                            </span> |
    
                            <span class="quick-edit">
                                <a href="javascript:void(0)" id="quickEdit" data-id="' . $data->id .'">Quick Edit</a>
                            </span> |
    
                            <span class="delete">
                                <a class="delete" href="javascript:void(0)" id="delete" data-id="' . $data->id .'">Delete</a>
                            </span>
                        </div>
                    ';

            })
            ->editColumn('description', function ($data) {
                if(empty($data->description))
                    return '—';
                else
                    return $data->description;
            })
            ->rawColumns(['thumbnail', 'name', 'description'])
            ->make(true);
    }

    public static function get_product_child_category()
    {
        $data = Category::where('type', '=', 'product', 'and')
            ->where('parent', '!=', 0)
            ->select('id', 'name', 'slug')
            ->get();
        return $data;
    }

    public static function get_post_child_category()
    {
        $data = Category::where('type', '=', 'post', 'and')
            ->where('parent', '!=', 0)
            ->select('id', 'name', 'slug')
            ->get();
        return $data;
    }
    //use for product details
    //get category via ID
    //get category parent when parent = cate_id

    public static function get_parent_category($parent)
    {
        $data = Category::where('id', '=', $parent)
            ->select('id', 'name', 'slug')
            ->get()
            ->first();
        return $data;
    }
}
