<?php

namespace App;

use DataTables;
use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    protected $table = 'coupons';

    protected $fillable = [
        'coupon_code', 'coupon_description', 'coupon_amount', 'coupon_discount_type', 'coupon_expiry_date', 'coupon_usage', 'coupon_limit',
    ];

    public static function anyCoupons()
    {
        $coupons = Coupon::query()->orderBy('created_at', 'DESC');

        return Datatables::of($coupons)

            ->editColumn('coupon_usage', function ($data) {
                return $data->coupon_usage . ' / ' . $data->coupon_limit;
            })
            ->editColumn('coupon_expiry_date', function ($data) {
                return date('d-m-Y', strtotime($data->coupon_expiry_date));
            })
            ->addColumn('action', function ($data) {
                return '<button id="delete" value="' . $data->id . '" class="btn btn-xs btn-danger" style="margin-top:20px;"><i class="fal fa-trash-alt"></i> Remove</button>
                ';
            })
            ->rawColumns(['coupon_usage', 'coupon_expiry_date', 'action'])
            ->make(true);
    }

    public static function findByCode($code)
    {
        return self::where('coupon_code', $code)->first();
    }

    public static function discount($code, $total)
    {
        $total = str_replace(',', '', $total);
        $coupon = self::where('coupon_code', $code)->first();

        if ($coupon->coupon_discount_type == "Fixed") {
            return $coupon->coupon_amount;
        } elseif ($coupon->coupon_discount_type == "Percentage") {
            return ($coupon->coupon_amount * 100) / $total;
        } else {
            return 0;
        }
    }

    public static function updateUsage($code)
    {
        $coupon = self::where('coupon_code', $code)->first();
        $coupon->coupon_usage = $coupon->coupon_usage + 1;
        $coupon->save();
    }
}