<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FAQ extends Model
{
    protected $table = 'faq';

    protected $fillable = ['title', 'contents'];

    public static function getAll()
    {
        $data = FAQ::all();
        return $data;
    }
}