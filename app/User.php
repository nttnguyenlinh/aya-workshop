<?php

namespace App;

use DataTables;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    protected $table = 'users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'nicename', 'email', 'password', 'level', 'status',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function customers()
    {
        return $this->hasOne('App\Customer');
    }

    public static function anyUsers()
    {
        $users = User::query()->orderBy('created_at', 'DESC');

        return Datatables::of($users)
            ->editColumn('email_verified_at', function ($user) {
                if ($user->email_verified_at != null) {
                    return '<center><span class="badge badge-success">YES</span></center>';
                } else {
                    return '<center><span class="badge badge-danger">NO</span></center>';
                }
            })
            ->editColumn('level', function ($user) {
                if ($user->level == 1) {
                    return '<center><span class="badge badge-info">Admin</span></center>';
                } else {
                    return '<center><span class="badge badge-warning" style="color:white;">Customer</span></center>';
                }
            })

            ->editColumn('status', function ($user) {
                if ($user->status == 1) {
                    return '<center><span class="badge badge-success">Active</span></center>';
                } else {
                    return '<center><span class="badge badge-danger">Locked</span></center>';
                }
            })
            ->addColumn('action', function ($user) {
                return '
                <button id="view" value="' . $user->id . '" class="btn btn-xs btn-success"><i class="fal fa-eye"></i> View</button>
                <button id="edit" value="' . $user->id . '" class="btn btn-xs btn-warning"><i class="fal fa-edit"></i> Edit</button>
                ';
            })
            ->rawColumns(['email_verified_at', 'level', 'status', 'action'])
            ->make(true);
    }

    public static function findCustomer($user_id)
    {
        $data = User::join('customers', 'users.id', '=', 'customers.user_id')
            ->where('users.id', '=', $user_id)
            ->select(['customers.id', 'customers.address', 'customers.country', 'customers.zipcode', 'customers.city', 'customers.county', 'customers.phone'])
            ->get();

        return Datatables::of($data)
            ->make(true);
    }

    public static function hasEmailExists($email)
    {
        $data = User::where('email', '=', $email)->get()->first();
        return $data;
    }

    public static function getUserByID($id)
    {
        $data = User::findOrFail($id);
        return response()->json($data);
    }
}
