<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BillDetail extends Model
{
    protected $table = 'bill_detail';

    protected $fillable = [
        'bill_id', 'product_id', 'quantity', 'price'
    ];

    public function products()
    {
        return $this->belongsTo('App\Product');
    }

    public function bills()
    {
        return $this->belongsTo('App\Bill');
    }

    public static function DetailByBillID($bill_id)
    {
        $data = BillDetail::leftJoin('products', 'bill_detail.product_id', '=', 'products.id')
            ->where('bill_id', '=', $bill_id)
            ->select('bill_detail.id', 'bill_detail.bill_id', 'bill_detail.product_id', 'products.name', 'products.slug', 'bill_detail.quantity', 'bill_detail.price')
            ->get();
        return $data;
    }
}
