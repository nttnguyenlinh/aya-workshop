<?php

namespace App;

use App\Category;
use App\Post_Category;
use App\User;
use Illuminate\Database\Eloquent\Model;
use DataTables;
use App\Tag;
use App\Post_Tag;

class Post extends Model
{
    protected $table = 'posts';

    protected $fillable = [
        'name', 'slug', 'author', 'excerpt', 'contents', 'thumbnail', 'seo_title', 'seo_keyword', 'meta_description', 'facebook_image', 'status'
    ];

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'post_category');
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'post_tag');
    }

    public static function posts_list($status = 100)
    {
        define("STATUS_F", $status);
        if(STATUS_F == 100 || STATUS_F == "")
            $data = self::with('categories')->with('tags')->leftJoin('users', 'posts.author', '=', 'users.id')
                    ->select(['users.id AS author_id', 'users.nicename AS author_nicename', 'posts.*'])
                    ->orderBy('posts.updated_at', 'desc')
                    ->get();
        else
            $data = self::with('categories')->with('tags')->leftJoin('users', 'posts.author', '=', 'users.id')
                ->where('posts.status', STATUS_F)
                ->select(['users.id AS author_id', 'users.nicename AS author_nicename', 'posts.*'])
                ->orderBy('posts.updated_at', 'desc')
                ->get();

        return Datatables::of($data)
            ->editColumn('name', function ($data) {
                if(STATUS_F != "")
                    $status_info = '';
                else
                    switch ($data->status)
                    {
                        case -1:
                            $status_info = ' — Draft';
                            break;
                        case 0:
                            $status_info = ' — Pending';
                            break;
                        default:
                            $status_info = '';
                    }

                return '
                    <strong>
                        <a href="' . route('dashboard.post.edit', $data->id) . '">' . $data->name . ' <span class="span-status">' . $status_info . '</span></a>
                    </strong>
                    <div class="row-actions">
                        <span class="edit">
                            <a href="' . route('dashboard.post.edit', $data->id) . '">Edit</a>
                        </span> |
                        <span class="delete">
                            <a class="delete" href="javascript:void(0)" id="trash" data-id="' . $data->id .'">Trash</a>
                        </span>
                    </div>
                    ';
            })
            ->editColumn('author', function ($data) {
                return '
                    <a href="javascript:void(0);">' . $data->author_nicename. '</a>
                    ';
            })
            ->addColumn('categories', function ($data) {
                $list = '';
                foreach ($data->categories as $item)
                    $list .= '<a href="javascript:void(0);"><span class="badge badge-info" style="margin:0 1px;">' . $item->name. '</span></a>';
                return $list;
            })

            ->addColumn('tags', function ($data) {
                $list = '';
                foreach ($data->tags as $item)
                    $list .= '<a href="javascript:void(0);"><span class="badge badge-info" style="margin:0 1px;">' . $item->name. '</span></a>';
                return $list;
            })
            ->addColumn('date', function ($data) {
                if($data->created_at === $data->updated_at)
                {
                    $date_info = "Published";
                    $date_time = $data->created_at;
                }
                else
                {
                    $date_info = "Last Modified";
                    $date_time = $data->updated_at;
                }
                return '
                        <p class="tbl_date_info">' . $date_info . '</p>
                        <p class="tbl_date_time">' . $date_time . '</p>
                    ';
            })
            ->rawColumns(['name', 'author', 'categories', 'tags', 'date'])
            ->make(true);
    }
    public static function posts()
    {
        $posts = Post::join('categories', 'posts.category_id', '=', 'categories.id')
            ->where('posts.status', 1)
            ->select(['categories.name AS categories_name', 'categories.slug AS categories_slug', 'posts.id', 'posts.name', 'posts.slug', 'posts.category_id', 'posts.excerpt', 'posts.thumbnail', 'posts.status', 'posts.created_at'])
            ->orderBy('posts.id', 'DESC')
            ->paginate(5);

        return $posts;
    }

    public static function postsCategory($categories_slug)
    {
        $posts = Post::join('categories', 'posts.category_id', '=', 'categories.id')
            ->where('posts.status', 1, 'and')
            ->where('categories.slug', '=', $categories_slug)
            ->select(['categories.name AS categories_name', 'categories.slug AS categories_slug', 'posts.id', 'posts.name', 'posts.slug', 'posts.category_id', 'posts.excerpt', 'posts.thumbnail', 'posts.status', 'posts.created_at'])
            ->orderBy('posts.id', 'DESC')
            ->paginate(5);

        return $posts;
    }

    public static function postsList()
    {
        $posts = self::with('categories')->with('tags')->leftJoin('users', 'posts.author', '=', 'users.id')
            ->select(['users.id AS author_id', 'users.nicename AS author_nicename', 'posts.*'])
            ->orderBy('posts.id', 'desc')
            ->take(5)
            ->get();
        return $posts;
    }

    public static function postDetails($slug)
    {
        $post = Post::join('categories', 'posts.category_id', '=', 'categories.id')
            ->where('posts.slug', 'like', $slug)
            ->select(['posts.category_id', 'categories.name AS category_name', 'categories.slug AS category_slug', 'categories.parent AS category_parent', 'posts.id', 'posts.name', 'posts.slug', 'posts.excerpt', 'posts.contents', 'posts.thumbnail', 'posts.status', 'posts.created_at'])
            ->get()
            ->first();

        return $post;
    }

    public static function latestPosts()
    {
        $posts = Post::join('categories', 'posts.category_id', '=', 'categories.id')
	    ->select(['categories.name AS categories_name', 'posts.id', 'posts.name', 'posts.slug', 'posts.thumbnail', 'posts.created_at'])
            ->where('posts.status', 1)
            ->orderBy('posts.id', 'DESC')
            ->take(5)
            ->get();
        return $posts;
    }

    public static function anyPosts()
    {
        $posts = Post::join('categories', 'posts.category_id', '=', 'categories.id')
            ->select(['categories.name AS categories_name', 'posts.id', 'posts.name', 'posts.category_id', 'posts.excerpt', 'posts.status', 'posts.created_at'])
            ->orderBy('posts.id', 'DESC')
            ->get();

        return Datatables::of($posts)
            ->editColumn('category_id', function ($post) {
                return '<center><span class="badge badge-info" style="margin-top:20px;">'. $post->categories_name . '</span></center>';
            })
            ->editColumn('created_at', function ($post) {
                if ($post->status == 1) {
                    return '<center style="margin-top:20px;">' . Date_format($post->created_at, "Y-m-d") . '<br><span class="badge badge-success">Published</span></center>';
                } else {
                    return '<center style="margin-top:20px;">' . Date_format($post->created_at, "Y-m-d") . '<br><span class="badge badge-warning">Pending Review</span></center>';
                }
            })
            ->addColumn('action', function ($post) {
                return '
                <a href="posts/edit/'. $post->id .'" class="btn btn-xs btn-warning" style="margin-top:20px; margin-right:5px;"><i class="fal fa-edit"></i> Edit</a>  
                <button id="delete" value="' . $post->id . '" class="btn btn-xs btn-danger" style="margin-top:20px;"><i class="fal fa-trash-alt"></i> Remove</button>      
                ';
            })
            ->rawColumns(['category_id', 'created_at', 'action'])
            ->make(true);
    }
}
