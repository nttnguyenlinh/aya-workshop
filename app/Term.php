<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DataTables;

class Term extends Model
{
    protected $table = 'terms';

    protected $fillable = ['attribute_id', 'name', 'slug', 'description', 'visibility'];

    public function attributes()
    {
        return $this->belongsTo(Attribute::class);
    }

    public static function get_list($attribute_id)
    {
        $data = Term::where('attribute_id', $attribute_id)
            ->select(['terms.*']);

        return Datatables::of($data)
                ->editColumn('name', function ($data) {
                    return '
                        <strong>
                            <a href="">' . $data->name . '</a>
                            </strong>
                                <div class="row-actions">
                                    <span class="edit">
                                        <a href="' . route('dashboard.term.edit', $data->id) . '">Edit</a>
                                    </span> |

                                    <span class="quick-edit">
                                        <a href="javascript:void(0)" id="quickEdit" data-id="' . $data->id .'">Quick Edit</a>
                                    </span> |

                                    <span class="delete">
                                        <a class="delete" href="javascript:void(0)" id="delete" data-id="' . $data->id .'">Delete</a>
                                    </span>
                                </div>
                    ';
                })
            ->rawColumns(['name'])
            ->make(true);
    }
}
