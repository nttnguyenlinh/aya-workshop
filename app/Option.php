<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    protected $table = 'options';

    protected $fillable = ['key', 'value', 'type'];

    public static function getOption($key)
    {
        $option = self::where('key', $key)->first();
        if ($option) {
            return html_entity_decode($option->value);
        } else {
            return false;
        }

    }

    public static function findByKey($key)
    {
        return self::where('key', $key)->first();
    }

    public static function findKeyUpdate($key)
    {
        return self::where('key', $key);
    }
}