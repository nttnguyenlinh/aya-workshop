<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table = 'customers';

    protected $fillable = [
        'user_id', 'country', 'address', 'zipcode', 'city', 'county', 'phone',
    ];

    public function users()
    {
        return $this->belongsTo('App\User');
    }

    public function bills()
    {
        return $this->hasMany('App\Bill');
    }
    // public static function anyCustomers()
    // {
    //     $customers = Customers::query();

    //     return Datatables::of($customers)
    //         ->addColumn('action', function ($user) {
    //             return '<a href="#edit-'.$user->id.'" class="btn btn-xs btn-warning"><i class="fal fa-edit"></i> Edit</a>';
    //         })
    //     ->make(true);
    // }

    public static function getCustomerByUserID($id)
    {
        $data = Customer::where('user_id', '=', $id)
            ->select('*')->get()->first();
        return $data;

    }
}