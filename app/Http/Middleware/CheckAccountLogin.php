<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class CheckAccountLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check() || Auth::viaRemember()){
            $user = Auth::user();
            if($user->status == 1)
                return $next($request);
            else
                return redirect()->route('account.getLogin');
        }
        else
            return redirect()->route('account.getLogin');
    }
}