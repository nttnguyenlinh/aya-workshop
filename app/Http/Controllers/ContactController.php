<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ContactFormRequest;
use Mail;
use App\Mail\ContactEmail;

class ContactController extends Controller
{
    public function create()
    {
        return view('home.contact');
    }

    public function store(ContactFormRequest $request)
    {
        // reCAPTCHA
        $recaptcha_api = config('app.recaptcha_api');
        $secret_key = config('app.recaptcha_secret_key');

        if (!empty($_POST['g-recaptcha-response'])) {
            $captcha = $_POST['g-recaptcha-response'];

            //GET IP
            if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
                $remoteip = $_SERVER['HTTP_CLIENT_IP'];
            } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                $remoteip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } else {
                $remoteip = $_SERVER['REMOTE_ADDR'];
            }

            //Request API
            $recaptcha_api = $recaptcha_api.'?secret='.$secret_key.'&response='.$captcha.'&remoteip='.$remoteip;

            //GET result API
            $response = file_get_contents($recaptcha_api);
            //Decode
            $response = json_decode($response);

            if (isset($response->success) && $response->success == true) {
                $data = $request->all();
                Mail::to('support@ayaworkshop.com', 'AYA WORKSHOP')->send(new ContactEmail($data));
                return redirect()->back()->with(['flag'=>'success','message'=>'Your message has been sent!'])->withInput();
            }
        } else {
            return redirect()->back()->with(['flag'=>'error','message'=>'Your message has not be sent!'])->withInput();
        }
    }
}