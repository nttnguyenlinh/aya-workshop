<?php

namespace App\Http\Controllers;

use App\Attribute;
use App\Tag;
use App\Term;
use App\Bill;
use App\Category;
use App\Coupon;
use App\Post_Category;
use App\Product_Category;
use App\Product_Term;
use App\Product_Attribute;
use App\Product_Variant;
use App\Product_Option;
use App\Post_Tag;
use App\Product_Tag;
use App\Http\Requests\PostRequest;
use App\Http\Requests\RegisterRequest;
use App\Image;
use App\Option;
use App\Post;
use App\Product;
use App\ShippingStatus;
use App\User;
use Carbon\Carbon;
use Hash;
use DB;
use DataTables;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Helpers;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('verified');
    }

    public function index()
    {
        return view('dashboard.index');
    }
    public function users()
    {
        return view('dashboard.users');
    }
    public function anyUsers()
    {
        $data = User::anyUsers();
        return $data;
    }
    public function getUserByID(Request $request)
    {
        $data = User::getUserByID($request->id);
        return $data;
    }
    public function findCustomer(Request $request)
    {
        $data = User::findCustomer($request->id);
        return $data;
    }
    public function hasEmailExists(Request $request)
    {
        $data = User::hasEmailExists($request->email);
        return $data;
    }
    public function postAddUsers(RegisterRequest $request)
    {
        $user = new User;

        $username = explode('@', $request->email);
        $user->username = $username[0];
        $user->nicename = $request->name;

        $user->email = $request->email;
        $user->email_verified_at = Carbon::now();
        $user->password = Hash::make($request->password);
        $user->level = 1;
        $user->status = 1;

        if ($user->save()) {
            $notification = ['alert' => 'success', 'message' => 'Account has been added!'];
            return redirect()->back()->with($notification);
        } else {
            $notification = ['alert' => 'error', 'message' => 'Unable to add!'];
            return redirect()->back()->with($notification);
        }
    }
    public function postEditUsers(Request $request)
    {
        $id = $request->id;
        $user = User::findOrFail($id);

        $user->level = $request->level;
        $user->status = $request->status;

        if ($user->save()) {
            $notification = ['alert' => 'success', 'message' => 'Account has been updated!'];
            return redirect()->back()->with($notification);
        } else {
            $notification = ['alert' => 'error', 'message' => 'Unable to update!'];
            return redirect()->back()->with($notification);
        }
    }
    public function posts()
    {
        return view('dashboard.posts.index');
    }
    public function posts_list(Request $request)
    {
        $posts = Post::posts_list($request->status);
        return $posts;
    }

    public function post_category_list()
    {
        $data = Category::post_category_list();
        return $data;
    }
    public function post_tag_list()
    {
        $data = Tag::post_tag_list();
        return $data;
    }
    public function post_create()
    {
        $categories = Category::where('type', 'post')->get();
        $users = User::where('level', 1)->select('id', 'nicename')->get();
        return view('dashboard.posts.create', compact('categories', 'users'));
    }
    public function post_store(PostRequest $request)
    {
        $slug = Str::slug(trim($request->input('title')));
        $count = Post::where('slug', $slug)->count();
        if($count > 0)
            return redirect()->back()->with(['alert' => 'error', 'message' => 'The Post already exists!']);
        else{
            $post = new Post();
            $post->name = trim($request->input('title'));
            $post->slug = $slug;
            $post->author = $request->input('author');
            $post->excerpt = $request->input('excerpt');
            $post->contents = htmlentities($request->input('contents'));

            if(empty($request->input('thumbnail')) || $request->input('thumbnail') == null)
            {
                $post->thumbnail = "";
            }
            else
                $post->thumbnail = explode("/storage/", $request->input('thumbnail'))[1];

            if(empty($request->input('seo_title')))
                $post->seo_title = trim($request->input('title'));
            else
                $post->seo_title = trim($request->input('seo_title'));

            if(empty($request->input('meta_description')))
                $post->meta_description = Helpers::truncate($request->input('excerpt'), 150, "");
            else
                $post->meta_description = Helpers::truncate($request->input('meta_description'), 150, "");

            $post->seo_keyword = trim($request->input('seo_keyword'));

            if($request->input('facebook_image') == "https://dummyimage.com/1920x768/000/fff" || $request->input('facebook_image') == null)
            {
                $post->facebook_image = "";
            }
            else
                $post->facebook_image = explode("/storage/", $request->input('facebook_image'))[1];

            $post->status = $request->input('status');

            if ($post->save()) {
                $post_id = Post::select('id')->where('slug', $slug)->first();
                $post_id = $post_id->id;
                $tags = explode(",", $request->input('post_tag'));
                $categories = $request->input('categories');
                foreach ($tags as $item)
                {
                    //Add record for Post_Tag
                    $pt= new Post_Tag();
                    $pt->tag_id = $item;
                    $pt->post_id = $post_id;
                    $pt->timestamps = false;
                    $pt->save();

                    //Update count for Tag
                    $t = Tag::findOrFail($item);
                    $t->count += 1;
                    $t->save();
                }

                foreach ($categories as $cat)
                {
                    //Add record for Post_Category
                    $pc= new Post_Category();
                    $pc->category_id = $cat;
                    $pc->post_id = $post_id;
                    $pc->timestamps = false;
                    $pc->save();

                    //Update count for Category
                    $c = Category::findOrFail($cat);
                    $c->count += 1;
                    $c->save();
                }
                return redirect()->route('dashboard.posts')->with(['alert' => 'success', 'message' => 'The Post has been added!']);
            } else {
                return redirect()->back()->with(['alert' => 'error', 'message' => 'Unable to add!']);
            }
        }
    }
    public function post_edit(Request $request)
    {
        $post = Post::findOrFail($request->id);
        $users = User::where('level', 1)->select('id', 'nicename')->get();
        $tags = Tag::select('tags.id', 'tags.name')
                        ->leftJoin('post_tag', 'tags.id', '=', 'post_tag.tag_id')
                        ->where('post_tag.post_id', $request->id)->get()->toArray();

        $categories = Category::where('type', 'post')->get();
        $categories_currents = array();
        if($categories)
        {
            $post_category = Post_Category::select('category_id')->where('post_id', $request->id)->get();
            foreach($post_category as $item)
                $categories_currents[] = $item->category_id;
        }

        return view('dashboard.posts.edit', compact('post', 'categories', 'categories_currents', 'tags', 'users'));
    }
    public function post_update(PostRequest $request)
    {
        $slug = Str::slug(trim($request->input('title')));
        $count = Post::where('id', '!=', $request->id)->where('slug', $slug)->count();
        if($count > 0)
            return redirect()->back()->with(['alert' => 'error', 'message' => 'The Post already exists!']);
        else {
            $post = Post::findOrFail($request->id);
            $post->name = trim($request->input('title'));
            $post->slug = $slug;
            $post->author = $request->input('author');
            $post->excerpt = $request->input('excerpt');
            $post->contents = htmlentities($request->input('contents'));

            if(empty($request->input('thumbnail')) || $request->input('thumbnail') == null)
            {
                $post->thumbnail = "";
            }
            else
                $post->thumbnail = explode("/storage/", $request->input('thumbnail'))[1];

            if(empty($request->input('seo_title')))
                $post->seo_title = trim($request->input('title'));
            else
                $post->seo_title = trim($request->input('seo_title'));

            if(empty($request->input('meta_description')))
                $post->meta_description = Helpers::truncate($request->input('excerpt'), 150, "");
            else
                $post->meta_description = Helpers::truncate($request->input('meta_description'), 150, "");

            $post->seo_keyword = trim($request->input('seo_keyword'));

            if($request->input('facebook_image') == "https://dummyimage.com/1920x768/000/fff" || $request->input('facebook_image') == null)
            {
                $post->facebook_image = "";
            }
            else
                $post->facebook_image = explode("/storage/", $request->input('facebook_image'))[1];

            $post->status = $request->input('status');

            if ($post->save()) {
                $post_id = Post::select('id')->where('slug', $slug)->first();
                $post_id = $post_id->id;
                $tags = explode(",", $request->input('post_tag'));
                $categories = $request->input('categories');

                //Scan and remove all tags have in post_tag when post_id = current post id
                //After remove then update count for an tag in Tag
                $tags_can_update = array();
                $post_tag_remove = Post_Tag::where('post_id', $post_id);

                //Remove and update count
                foreach ($post_tag_remove->get() as $itags)
                    array_push($tags_can_update, $itags->tag_id);
                $post_tag_remove->delete();

                //Update count for tag
                foreach ($tags_can_update as $item) {
                    //Update count for Tag
                    $t = Tag::findOrFail($item);
                    $t->count -= 1;
                    $t->save();
                }

                //Scan and remove all categories have in Post_Category when post_id = current post id
                //After remove then update count for an category in Category
                $cates_can_update = array();
                $post_cates_remove = Post_Category::where('post_id', $post_id);

                //Remove and update count
                foreach ($post_cates_remove->get() as $icate)
                    array_push($cates_can_update, $icate->category_id);
                $post_cates_remove->delete();

                //Update count for Category
                foreach ($cates_can_update as $item) {
                    //Update count for Tag
                    $c = Category::findOrFail($item);
                    $c->count -= 1;
                    $c->save();
                }

                //Add new - sync
                foreach ($tags as $item)
                {
                    //Add record for Post_Tag
                    $pt= new Post_Tag();
                    $pt->tag_id = $item;
                    $pt->post_id = $post_id;
                    $pt->timestamps = false;
                    $pt->save();

                    //Update count for Tag
                    $t = Tag::findOrFail($item);
                    $t->count += 1;
                    $t->save();
                }

                foreach ($categories as $cat)
                {
                    //Add record for Post_Category
                    $pc= new Post_Category();
                    $pc->category_id = $cat;
                    $pc->post_id = $post_id;
                    $pc->timestamps = false;
                    $pc->save();

                    //Update count for Category
                    $c = Category::findOrFail($cat);
                    $c->count += 1;
                    $c->save();
                }
                return redirect()->route('dashboard.posts')->with(['alert' => 'success', 'message' => 'Post has been updated!']);
            } else {
                return redirect()->back()->with(['alert' => 'error', 'message' => 'Unable to update!']);
            }
        }
    }
    public function post_delete(Request $request)
    {
        $list_id = $request->posts_id;
        $success = 0;
        $errors = 0;
        foreach($list_id as $id)
        {
            $post = Post::findOrFail($id);
            if($post->delete())
                $success++;
            else
                $errors++;
        }
        $return = ['success' => $success, 'errors' => $errors];
        return response()->json($return);
    }
    public function post_trash(Request $request)
    {
        $list_id = $request->posts_id;
        $success = 0;
        $errors = 0;
        foreach($list_id as $id)
        {
            $post = Post::findOrFail($id);
            $post->status = -1;
            if($post->save())
                $success++;
            else
                $errors++;
        }

        $return = ['success' => $success, 'errors' => $errors];
        return response()->json($return);
    }
    public function attributes()
    {
        $attributes = Attribute::all();
        return view('dashboard.attributes.index', compact('attributes'));
    }
    public function terms(Request $request)
    {
        $attribute = Attribute::findOrFail($request->id);
        return view('dashboard.terms.index', compact('attribute'));
    }
    public function listTerms($attribute_id)
    {
        $data = Term::get_list($attribute_id);
        return $data;
    }
    public function term_detail($term_id)
    {
        $data = Term::findOrFail($term_id);
        return response()->json($data);
    }
    public function editTerm($term_id)
    {
        $term = Term::findOrFail($term_id);
        return view('dashboard.terms.edit', compact('term'));
    }
    public function updateTerm(Request $request)
    {
        $this->validate($request,
            [
                'term_name' => 'required|max:28',
                'term_slug' => 'max:28',
            ],
            [
                'term_name.required' => 'Please, provide an term name.',
                'term_name.max' => 'Name for the term must be no more than 28 characters.',
                'term_slug.max' => 'Slug for the term must be no more than 28 characters.',
            ]
        );

        $count = Term::where('id', '!=', $request->term_id)->where('name', $request->term_name)->where('slug', $request->term_slug)->count();

        if ($count != 0) {
            if($request->edit_type == "quick")
                $return = "unique";
            else
                $notification = ['alert' => 'error', 'message' => 'The Term is already in use. Change it, please.'];
        } else {
            $term = Term::findOrFail($request->term_id);
            $term->name = trim($request->term_name);
            if (empty($request->term_slug)) {
                $term->slug = Str::slug(trim($request->term_name));
            } else {
                $term->slug = Str::slug(trim($request->term_slug));
            }

            $term->description = trim($request->term_description);
            $term->timestamps = false;
            if ($term->save()) {
                if($request->edit_type == "quick")
                    $return = "success";
                else
                    $notification = ['alert' => 'success', 'message' => 'The Term has been updated!'];
            } else {
                if($request->edit_type == "quick")
                    $return = "error";
                else
                    $notification = ['alert' => 'error', 'message' => 'The Term unable to update!'];
            }
        }

        if($request->edit_type == "quick")
            return response()->json($return);
        else
            return redirect()->back()->with($notification);
    }
    public function storeTerms(Request $request)
    {
        $this->validate($request,
            [
                'term_name' => 'required|max:28|unique:terms,name',
                'term_slug' => 'max:28|unique:terms,slug',
            ],
            [
                'term_name.required' => 'Please, provide an term name.',
                'term_name.max' => 'Name for the term must be no more than 28 characters.',
                'term_name.unique' => 'The Term is already in use. Change it, please.',
                'term_slug.max' => 'Slug for the term must be no more than 28 characters.',
                'term_slug.unique' => 'The Term is already in use. Change it, please.',
            ]
        );

        $term = new Term();
        $term->attribute_id = $request->input('attribute_id');
        $term->name = trim($request->input('term_name'));
        if (empty($request->input('term_slug'))) {
            $term->slug = Str::slug(trim($request->input('term_name')));
        } else {
            $term->slug = Str::slug(trim($request->input('term_slug')));
        }

        $term->description = trim($request->input('term_description'));
        $term->timestamps = false;

        if ($term->save()) {
            $notification = ['alert' => 'success', 'message' => 'The Term has been added!'];
        } else {
            $notification = ['alert' => 'error', 'message' => 'The Term unable to add!'];
        }

        return redirect()->back()->with($notification);
    }
    public function deleteTerms(Request $request)
    {
        $list_id = $request->terms_id;
        $success = 0;
        $errors = 0;
        foreach($list_id as $id)
        {
            $term = Term::findOrFail($id);
            if($term->delete())
                $success++;
            else
                $errors++;
        }

        $return = ['success' => $success, 'errors' => $errors];
        return response()->json($return);
    }
    public function storeAttributes(Request $request)
    {
        $this->validate($request,
            [
                'attribute_name' => 'required|max:28|unique:attributes,name',
                'attribute_slug' => 'max:28|unique:attributes,slug',
            ],
            [
                'attribute_name.required' => 'Please, provide an attribute name.',
                'attribute_name.max' => 'Name for the attribute must be no more than 28 characters.',
                'attribute_name.unique' => 'The Attribute is already in use. Change it, please.',
                'attribute_slug.max' => 'Slug for the attribute must be no more than 28 characters.',
                'attribute_slug.unique' => 'The Attribute is already in use. Change it, please.',
            ]
        );

        $attribute = new Attribute();
        $attribute->name = trim($request->input('attribute_name'));
        if (empty($request->input('attribute_slug'))) {
            $attribute->slug = Str::slug(trim($request->input('attribute_name')));
        } else {
            $attribute->slug = Str::slug(trim($request->input('attribute_slug')));
        }

        $attribute->position = trim($request->input('attribute_position'));
        $attribute->timestamps = false;

        if ($attribute->save()) {
            $notification = ['alert' => 'success', 'message' => 'The Attribute has been added!'];
        } else {
            $notification = ['alert' => 'error', 'message' => 'The Attribute unable to add!'];
        }

        return redirect()->back()->with($notification);
    }
    public function editAttribute(Request $request)
    {
        $attribute = Attribute::findOrFail($request->id);
        return view('dashboard.attributes.edit', compact('attribute'));
    }
    public function updateAttribute(Request $request)
    {
        $this->validate($request,
            [
                'attribute_name' => 'required|max:28',
                'attribute_slug' => 'max:28',
            ],
            [
                'attribute_name.required' => 'Please, provide an attribute name.',
                'attribute_name.max' => 'Name for the attribute must be no more than 28 characters.',
                'attribute_slug.max' => 'Slug for the attribute must be no more than 28 characters.',
            ]
        );

        $count = Attribute::where('id', '!=', $request->attribute_id)->where('name', $request->attribute_name)->where('slug', $request->attribute_slug)->count();

        if ($count != 0) {
            $notification = ['alert' => 'error', 'message' => 'The Attribute is already in use. Change it, please.'];
        } else {
            $attribute = Attribute::findOrFail($request->attribute_id);
            $attribute->name = trim($request->input('attribute_name'));
            if (empty($request->input('attribute_slug'))) {
                $attribute->slug = Str::slug(trim($request->input('attribute_name')));
            } else {
                $attribute->slug = Str::slug(trim($request->input('attribute_slug')));
            }

            $attribute->position = trim($request->input('attribute_position'));
            $attribute->timestamps = false;

            if ($attribute->save()) {
                $notification = ['alert' => 'success', 'message' => 'The Attribute has been updated!'];
            } else {
                $notification = ['alert' => 'error', 'message' => 'The Attribute unable to update!'];
            }
        }
        return redirect()->route('dashboard.attributes')->with($notification);
    }
    public function deleteAttribute(Request $request)
    {
        $attribute = Attribute::findOrFail($request->id);
        if ($attribute->delete()) {
            $return = "success";
        } else {
            $return = "error";
        }
        return response()->json($return);
    }
    public function products()
    {
        return view('dashboard.products.index');
    }

    public function products_list(Request $request)
    {
        $products = Product::products_list($request->status);
        return $products;
    }

    public function product_category_list()
    {
        $data = Category::product_category_list();
        return $data;
    }
    public function product_tag_list()
    {
        $data = Tag::product_tag_list();
        return $data;
    }
    public function product_create()
    {
        $categories = Category::where('type', 'product')->get();
        return view('dashboard.products.create', compact('categories'));
    }

    public function product_create_draft(Request $request)
    {
        $slug = Str::slug(trim($request->input('title')));
        $count = Product::where('slug', $slug)->count();
        if($count > 0)
           $return = "error";
        else {
            $product = new Product();
            $product->name = trim($request->input('title'));
            $product->slug = $slug;
            $product->featured = 0;
            $product->status = -1;
            if ($product->save())
                $return = "success";
            else
                $return = "error";
        }
        return response()->json($return);
    }

    public function product_store(Request $request)
    {
        $product_slug = Str::slug(trim($request->title));
        $product = Product::select('id')->where('slug', $product_slug)->get()->first();
        $product_id = $product->id;
        $product = Product::findOrFail($product_id);
        $product->name = trim($request->input('title'));
        $product->slug = Helpers::slugify(trim($request->input('title')));
        $product->sku = trim($request->input('sku'));
        $product->type = trim($request->input('product_type'));
        $product->description = trim($request->input('description'));
        $product->contents = htmlentities(trim($request->input('contents')));

        if($product->type == "variable")
        {
            $product_item = Product_Variant::select('min_price', 'max_price', 'stock_quantity')->where('product_id', $product_id);
            $stock_quantity = $product_item->sum('stock_quantity');
            $max_price = $product_item->max('max_price');
            $min_price =  $product_item->where('min_price', '>', 0)->min('min_price');
            $product->min_price = $min_price;
            $product->max_price = $max_price;
            $product->stock_quantity = $stock_quantity;
        }
        else
        {
            if(!empty($request->input('max_price')))
                $product->max_price = str_replace(',', '', trim($request->input('max_price')));

            if(!empty($request->input('min_price')))
            {
                $product->min_price = str_replace(',', '', trim($request->input('min_price')));
                $product->onsale = 1;
                $discount_time = explode(' - ', $request->input(discounttime));
                $product->sale_start = $discount_time[0];
                $product->sale_end = $discount_time[1];
            }

            $product->stock_quantity = (int)$request->input(stock_quantity);
        }

        if(empty($request->input('thumbnail')) || $request->input('thumbnail') == null)
        {
            $product->thumbnail = "";
        }
        else
            $product->thumbnail = explode("/storage/", $request->input('thumbnail'))[1];

        if(empty($request->input('seo_title')))
            $product->seo_title = trim($request->input('title'));
        else
            $product->seo_title = trim($request->input('seo_title'));

        if(empty($request->input('meta_description')))
            $product->meta_description = Helpers::truncate($request->input('excerpt'), 150, "");
        else
            $product->meta_description = Helpers::truncate($request->input('meta_description'), 150, "");

        $product->seo_keyword = trim($request->input('seo_keyword'));

        if($request->input('facebook_image') == "https://dummyimage.com/1920x768/000/fff" || $request->input('facebook_image') == null)
            $product->facebook_image = "";
        else
            $product->facebook_image = explode("/storage/", $request->input('facebook_image'))[1];

        $product->featured = $request->input('featured');
        $product->in_stock = $request->input('stock');
        $product->visibility = $request->input('visibility');

        if ($product->save()) {
            $tags = explode(",", $request->input('product_tag'));
            $categories = $request->input('categories');
            foreach ($tags as $item)
            {
                //Add record for Post_Tag
                $pt= new Product_Tag();
                $pt->tag_id = $item;
                $pt->product_id = $product_id;
                $pt->timestamps = false;
                $pt->save();

                //Update count for Tag
                $t = Tag::findOrFail($item);
                $t->count += 1;
                $t->save();
            }

            foreach ($categories as $cat)
            {
                //Add record for Post_Category
                $pc= new Product_Category();
                $pc->category_id = $cat;
                $pc->product_id = $product_id;
                $pc->timestamps = false;
                $pc->save();

                //Update count for Category
                $c = Category::findOrFail($cat);
                $c->count += 1;
                $c->save();
            }

            return redirect()->route("dashboard.products")->with(["alert" => "success", "message" => "The product has been added!"]);
        } else {
            return redirect()->back()->with(["alert" => "error", "message" => "Unable to add!"]);
        }

        // if ($request->hasFile('thumbnail')) {
        //     $file = $request->file('thumbnail');

        //     $file_name = $file->getClientOriginalName(); // Get Name
        //     $file_extension = $file->getClientOriginalExtension(); // Get Extension
        //     $file_size = $file->getClientSize(); // Get Size

        //     if ($file_extension != 'png' && $file_extension != 'jpg' && $file_extension != 'jpeg') {
        //         return redirect()->back()->with(['alert' => 'error', 'message' => 'The image format is not allowed (png, jpg, jpeg) only.']);
        //     }
        //     if ($file_size > 2000000) { //2 MB (size is also in bytes)
        //         return redirect()->back()->with(['alert' => 'error', 'message' => 'The file size is too big (2MB) max).']);
        //     }

        //     $newfile = Str::random(5) . '_' . $file_name;
        //     $product->thumbnail = $newfile;
        // }


    }
    public function product_images_remove(Request $request)
    {
        $return = '';
        $data = Image::getByPath($request->name);
        if (!empty($data)) {
            $path = $data->path;
            if ($data->delete()) {
                //unlink(public_path() . "/" . $path);
                Storage::delete($path);
                $return = 'success';
            } else {
                $return = 'error';
            }
        } else {
            $return = '';
        }
        return $return;
    }

    public function loadimages()
    {
        $allFiles = Storage::files();
        $files = array();
        foreach ($allFiles as $file) {
            $files[] = $this->fileInfo($file);
        }
        return view('dashboard.media.images', compact('files'));
    }

    public function product_images(Request $request)
    {
        if($request->type == "create")
        {
            $product_slug = Str::slug(trim($request->product_title));
            $product = Product::select('id')->where('slug', $product_slug)->get()->first();
            $product_id = $product->id;
            $images = Image::getProductImages($product_id);
        }
        else
            $images = Image::getProductImages($request->product_id);

        return response()->json($images);
    }

    public function product_images_upload(Request $request)
    {
        $product_id = "";
        if($request->type == "create")
        {
            $product_slug = Str::slug(trim($request->product_title));
            $product = Product::select('id')->where('slug', $product_slug)->get()->first();
            $product_id = $product->id;
        }
        else
            $product_id = $request->product_id;

        //Uploads multiple images
        if ($request->hasFile('file')) {
            foreach ($request->file('file') as $files) {
                $newName = Helpers::generateRandomString(5) . '_' . $files->getClientOriginalName();
                $image = new Image();
                $image->product_id = $product_id;
                $image->title = $files->getClientOriginalName();
                $image->path = $newName;
                $image->size = Helpers::formatBytes($files->getClientSize());
                $image->bytes_size = $files->getClientSize();
                if ($image->save()) {
                    Storage::disk()->putFileAs('', $files, $newName);
                }
            }
        }
    }
    public function product_edit(Request $request)
    {
        $product = Product::getProductByID($request->id);
        $images = Image::getProductImages($request->id);
        $category = Category::get_product_child_category();
        return view('dashboard.products.edit', compact('product', 'images', 'category'));
    }
    public function product_update(Request $request)
    {
        $product = Product::findOrFail($request->product_id);
        $product->category_id = $request->category;
        $product->name = $request->title;
        $product->slug = $this->slugify($request->title);
        $product->description = $request->description;
        $product->details = $request->details;
        $product->price = str_replace(',', '', trim($request->price));
        if (!empty($request->sale)) { ////$ 0.00
            $product->sale = str_replace(',', '', trim($request->sale));
            //discounttime = "2019-04-15 06:00:00 - 2019-04-17 02:00:00"
            $discounttime = explode(' - ', $request->discounttime);
            $product->sale_start = $discounttime[0];
            $product->sale_end = $discounttime[1];
        } else {
            $product->sale = null;
            $product->sale_start = null;
            $product->sale_end = null;
        }

        // if ($request->hasFile('thumbnail')) {
        //     $file = $request->file('thumbnail');

        //     $file_name = $file->getClientOriginalName(); // Get Name
        //     $file_extension = $file->getClientOriginalExtension(); // Get Extension
        //     $file_size = $file->getClientSize(); // Get Size

        //     if ($file_extension != 'png' && $file_extension != 'jpg' && $file_extension != 'jpeg') {
        //         return redirect()->back()->with(['alert'=>'error','message'=>'The image format is not allowed.']);
        //     }
        //     if ($file_size > 2000000) {
        //         return redirect()->back()->with(['alert'=>'error','message'=>'The file size is too big 2MB max.']);
        //     }

        //     $newfile = Str::random(5) . '_' . $file_name;
        //     $product->thumbnail = $newfile;
        // } else {
        //     $product->thumbnail = $product->thumbnail;
        // }
        $product->thumbnail = (empty($request->thumbnail) ? 'no_image.jpg' : explode("/storage/", $request->thumbnail)[1]);
        $product->featured = $request->featured;
        $product->stock = $request->stock;

        $product->status = $request->status;

        if ($product->save()) {
            // if ($request->hasFile('thumbnail')) {
            //     Storage::disk()->putFileAs('', $file, $newfile);
            // }

            return redirect()->route("dashboard.products")->with(["alert" => "success", "message" => "Product has been updated!"]);
        } else {
            return redirect()->back()->with(["alert" => "error", "message" => "Unable to update!"]);
        }
    }
    public function product_delete(Request $request)
    {
        $list_id = $request->products_id;
        $success = 0;
        $errors = 0;
        foreach($list_id as $id)
        {
            $product = Product::findOrFail($id);
            if($product->delete())
                $success++;
            else
                $errors++;
        }
        $return = ['success' => $success, 'errors' => $errors];
        return response()->json($return);
    }
    public function product_trash(Request $request)
    {
        $list_id = $request->products_id;
        $success = 0;
        $errors = 0;
        foreach($list_id as $id)
        {
            $product = Product::findOrFail($id);
            $product->status = -1;
            if($product->save())
                $success++;
            else
                $errors++;
        }

        $return = ['success' => $success, 'errors' => $errors];
        return response()->json($return);
    }
    public function product_publish(Request $request)
    {
        $list_id = $request->products_id;
        $success = 0;
        $errors = 0;
        foreach($list_id as $id)
        {
            $product = Product::findOrFail($id);
            $product->status = 1;
            if($product->save())
                $success++;
            else
                $errors++;
        }

        $return = ['success' => $success, 'errors' => $errors];
        return response()->json($return);
    }
    public function product_featured(Request $request)
    {
        $product = Product::findOrFail($request->id);
        if ($product->featured == 0)
            $product->featured = 1;
        else
            $product->featured = 0;

        if ($product->save()) {
            $return = "success";
        } else {
            $return = "error";
        }
        return $return;
    }
    public function media()
    {
        return view("dashboard.media.index");
    }
    public function product_category()
    {
        $categories = Category::where('type', 'product')->get();
        return view('dashboard.products.category', compact('categories'));
    }
    public function product_tag()
    {
        return view('dashboard.products.tag');
    }
    public function post_category()
    {
        $categories = Category::where('type', 'post')->get();
        return view('dashboard.posts.category', compact('categories'));
    }
    public function post_tag()
    {
        return view('dashboard.posts.tag');
    }
    public function orders()
    {
        return view('dashboard.orders.index');
    }
    public function anyOrders()
    {
        $data = Bill::anyBills();
        return $data;
    }
    public function anyStatus()
    {
        $data = ShippingStatus::anyStatus();
        return $data;
    }
    public function orderChange(Request $request)
    {
        $bill = Bill::getBill($request->orderID);
        $bill->shipping_status = $request->status;
        if ($bill->save()) {
            $notification = ['alert ' => ' success', 'message' => 'Order has been changed!'];
            return redirect()->back()->with($notification);
        } else {
            $notification = ['alert' => 'error', ' message' => 'Unable to change!'];
            return redirect()->back()->with($notification);
        }
    }
    public function getCustomerByBill(Request $request)
    {
        $data = Bill::getCustomerByBill($request->bill_id);
        return $data;
    }
    public function getDetailsByBill(Request $request)
    {
        $data = Bill::getDetailsByBill($request->bill_id);
        return $data;
    }
    public function coupons()
    {
        return view('dashboard.coupons');
    }
    public function postCoupons(Request $request)
    {
        $coupon = new Coupon;
        $coupon->coupon_code = $request->coupon_code;
        $coupon->coupon_description = $request->coupon_description;
        $coupon->coupon_amount = $request->coupon_amount;
        $coupon->coupon_discount_type = $request->discount_type;
        $time = strtotime($request->expiry_date);
        $coupon->coupon_expiry_date = date('Y-m-d', $time);
        $coupon->coupon_limit = $request->coupon_limit;

        if ($coupon->save()) {
            $notification = ['alert' => 'success', 'message' => 'Coupon has been added!'];
            return redirect()->back()->with($notification);
        } else {
            $notification = ['alert' => 'error', 'message' => 'Unable to add!'];
            return redirect()->back()->with($notification);
        }
    }
    public function anyCoupons()
    {
        $data = Coupon::anyCoupons();
        return $data;
    }
    public function deleteCoupon(Request $request)
    {
        $return = '';
        $data = Coupon::findOrFail($request->id);
        if ($data->delete()) {
            $return = 'success';
        } else {
            $return = 'error';
        }
        return response()->json($return);
    }
    public function options()
    {
        $options = Option::orderBy('key', 'asc')->get();
        return view('dashboard.options', compact('options'));
    }
    public function storeOptions(Request $request)
    {
        $keys = $request->input("key");
        $values = $request->input("value");
        $types = $request->input("type");

        foreach ($keys as $k => $value) {
            $data = Option::findByKey($value);

            if ($data) {
                $option = Option::findOrFail($data->id);
                if ($option) {
                    $option->key = $value;
                    $option->value = htmlentities($values[$k]);
                    if ($types[$k] == "single") {
                        $option->type = 0;
                    } else {
                        $option->type = 1;
                    }
                    $option->timestamps = false;
                    $option->save();
                }

            } else {
                $option = new Option();
                $option->key = $value;
                $option->value = htmlentities($values[$k]);
                if ($types[$k] == "single") {
                    $option->type = 0;
                } else {
                    $option->type = 1;
                }
                $option->timestamps = false;
                $option->save();
            }
        }
        $notification = ['alert' => 'success', 'message' => 'The options has been updated!'];
        return redirect()->back()->with($notification);
    }
    public function deleteOptions(Request $request)
    {
        $option = Option::findByKey($request->key);
        if ($option) {
            $return = "true";
            $option->delete();
        }
        $return = "false";
        return response()->json($return);
    }
    public function category_show(Request $request)
    {
        $data = Category::findOrFail($request->id);
        return $data;
    }
    public function category_edit(Request $request)
    {
        $category = Category::findOrFail($request->id);
        if($category->type == "post")
            $categories = Category::where('type', 'post')->get();
        else
            $categories = Category::where('type', 'product')->get();
        return view('dashboard.category', compact('category', 'categories'));
    }
    public function category_update(Request $request)
    {
        $this->validate($request,
            [
                'category_name' => 'required|max:50',
                'category_slug' => 'max:50',
            ],
            [
                'category_name.required' => 'Please, provide an category name.',
                'category_name.max' => 'Name for the category must be no more than 50 characters.',
                'category_slug.max' => 'Slug for the category must be no more than 50 characters.',
            ]
        );

        $count = Category::where('id', '!=', $request->category_id)->where('name', $request->category_name)->where('slug', $request->category_slug)->count();

        if ($count != 0) {
            if($request->edit_type == "quick")
                $return = "unique";
            else
                $notification = ['alert' => 'error', 'message' => 'The Category is already in use. Change it, please.'];
        } else {
            $category = Category::findOrFail($request->category_id);
            $category->name = trim($request->category_name);

            if (empty($request->category_slug)) {
                $category->slug = Str::slug(trim($request->category_name));
            } else {
                $category->slug = Str::slug(trim($request->category_slug));
            }

            $category->position = $request->position;

            if ($request->edit_type == "edit"){
                $category->description = trim($request->category_description);
                $category->parent = trim($request->category_parent);
            }

            if($request->category_type == "product") {
                if ($request->edit_type == "edit"){
                    $category->top_content = htmlentities($request->input('category_top_content'));
                    $category->bottom_content = htmlentities($request->input('category_bottom_content'));
                    $category->thumbnail = (empty($request->input('category_thumbnail')) ? 'placeholder.png' : explode("/storage/", $request->input('category_thumbnail'))[1]);
                }
            }

            if ($category->save()) {
                if($request->edit_type == "quick")
                    $return = "success";
                else
                    $notification = ['alert' => 'success', 'message' => 'The Category has been updated!'];
            } else {
                if($request->edit_type == "quick")
                    $return = "error";
                else
                    $notification = ['alert' => 'error', 'message' => 'The Category unable to update!'];
            }
        }

        if($request->edit_type == "quick")
            return response()->json($return);
        else
        {
            $route = 'dashboard.' . $request->category_type . '.category';
            return redirect()->route($route)->with($notification);
        }
    }
    public function category_store(Request $request)
    {
        $this->validate($request,
            [
                'category_name' => 'required|max:50|unique:categories,name',
                'category_slug' => 'max:50|unique:categories,slug',
            ],
            [
                'category_name.required' => 'Please, provide an category name.',
                'category_name.max' => 'Name for the category must be no more than 50 characters.',
                'category_name.unique' => 'The Category is already in use. Change it, please.',
                'category_slug.max' => 'Slug for the category must be no more than 50 characters.',
                'category_slug.unique' => 'The Category is already in use. Change it, please.',
            ]
        );

        $category = new Category();

        $category->name = $request->input('category_name');

        if(empty($request->input('category_slug')))
            $category->slug = Str::slug($request->input('category_name'));
        else
            $category->slug = Str::slug($request->input('category_slug'));
        $category->type = $request->input('category_type');
        $category->description = $request->input('category_description');
        $category->parent = $request->input('category_parent');
        $category->position = $request->input('position');

        if($request->input('category_type') == "product")
        {
            $category->thumbnail = (empty($request->input('category_thumbnail')) ? 'placeholder.png' : explode("/storage/", $request->input('category_thumbnail'))[1]);
        }

        if ($category->save()) {
            $notification = ['alert' => 'success', 'message' => 'The category has been added!'];
            return redirect()->back()->with($notification);
        } else {
            $notification = ['alert' => 'error', 'message' => 'Unable to add!'];
            return redirect()->back()->with($notification);
        }
    }
    public function category_delete(Request $request)
    {
        $list_id = $request->categories_id;
        $success = 0;
        $errors = 0;
        foreach($list_id as $id)
        {
            $category = Category::findOrFail($id);
            if($category->delete())
                $success++;
            else
                $errors++;
        }

        $return = ['success' => $success, 'errors' => $errors];
        return response()->json($return);
    }
    public function tag_autocomplete(Request $request)
    {
        $data = Tag::select("id", "name")->where("type", "post")->where("name", "LIKE", "%{$request->input('query')}%")->get();
        if($data->count() == 0)
        {
            $tag = new Tag();
            $tag->name = trim($request->input('query'));
            $tag->slug = Str::slug(trim($request->input('query')));
            $tag->type = 'post';
            $tag->save();
            $data = Tag::select("id", "name")->where("type", "post")->where("name", "LIKE", "%{$request->input('query')}%")->get();
            return response()->json($data);
        }
        return response()->json($data);
    }

    public function product_tag_autocomplete(Request $request)
    {
        $data = Tag::select("id", "name")->where("type", "product")->where("name", "LIKE", "%{$request->input('query')}%")->get();
        if($data->count() == 0)
        {
            $tag = new Tag();
            $tag->name = trim($request->input('query'));
            $tag->slug = Str::slug(trim($request->input('query')));
            $tag->type = 'product';
            $tag->save();
            $data = Tag::select("id", "name")->where("type", "product")->where("name", "LIKE", "%{$request->input('query')}%")->get();
            return response()->json($data);
        }
        return response()->json($data);
    }

    public function product_variant_get(Request $request)
    {
        $attr = $request->input('attribute');
        $attr = explode("pa_", $attr)[1];
        $data = Term::select("id", "name")->where("attribute_id", $attr)->get();
        return response()->json($data);
    }

    public function product_variant_load(Request $request)
    {
        $product_slug = Str::slug(trim($request->title));
        $product = Product::select('id')->where('slug', $product_slug)->get()->first();
        $product_id = $product->id;

        $product_term = Product_Term::where('product_term.product_id', $product_id)
                    ->leftJoin('terms', 'product_term.term_id', '=', 'terms.id')
                    ->select('product_term.*', 'terms.*')
                    ->get()->toJson();

        $attributes = Product_Attribute::where('product_attribute.product_id', $product_id)
                    ->leftJoin('attributes', 'product_attribute.attribute_id', '=', 'attributes.id')
                    ->select('attributes.id', 'attributes.name');

        $attr_count = $attributes->count();
        $attrs = $attributes->get()->toJson();

        //Đệ quy tính số biến thể product
        for($c = 0; $c < $attr_count; $c++)
        {
            $data = array();
            foreach(json_decode($attrs, true) as $k =>  $attr)
            {
                $temps = array();
                foreach(json_decode($product_term, true) as $i => $item) {
                    if($item['attribute_id'] === $attr['id'])
                        array_push($temps, $item['name']);
                }
                array_push($data, $temps);
            }
        }

        //Số biến thể
        $combos =  $this->possible_combos($data);

        //Scan and remove all product variant
        $product_variant = Product_Variant::where('product_id', $product->id);
        $product_variant->delete();

        //add new product variant
        foreach ($combos as $row)
        {
            $product_variant = new Product_Variant();
            $product_variant->product_id = $product_id;
            $product_variant->variant_code = Helpers::slugify(trim($row));
            $product_variant->timestamps = false;
            $product_variant->save();
        }

        //GET all product_variant
        $product_variant = Product_Variant::where('product_id', $product_id)->get()->toJson();

        //Truyền sang ajax
        $return = '{"product_variant":' . $product_variant . ',"product_term":' . $product_term . ',"attr_count":' . $attr_count . ',"attributes":' . $attrs . '}';
       return $return;
    }

    public function product_variant_update(Request $request){
        $product_slug = Str::slug(trim($request->product_title));
        $product = Product::select('id')->where('slug', $product_slug)->get()->first();
        $product_id = $product->id;

        $variant_id = array();
        $pa_term = array();
        $variant_thumbnail = array();
        $variant_sku = array();
        $variant_max_price = array();
        $variant_min_price = array();
        $variant_discounttime = array();
        $variant_stock = array();
        $variant_stock_quantity = array();
        $terms = array();

        //Phân loại data
        foreach ($request->frmData as $frmData)
        {
            switch ($frmData['name']){
                case 'variant_id':
                    array_push($variant_id, $frmData['value']);
                    break;
                case 'pa_term':
                    array_push($pa_term, $frmData['value']);
                    break;
                case 'thumbnail_variant':
                    array_push($variant_thumbnail, $frmData['value']);
                    break;
                case 'variant_sku':
                    array_push($variant_sku, $frmData['value']);
                    break;
                case 'variant_max_price':
                    array_push($variant_max_price, $frmData['value']);
                    break;
                case 'variant_min_price':
                    array_push($variant_min_price, $frmData['value']);
                    break;
                case 'variant_discounttime':
                    array_push($variant_discounttime, $frmData['value']);
                    break;
                case 'variant_stock':
                    array_push($variant_stock, $frmData['value']);
                    break;
                case 'variant_stock_quantity':
                    array_push($variant_stock_quantity, $frmData['value']);
                    break;
            };
        }

        //phân loại pa_term
        foreach ($pa_term as $pa)
        {
            $term = Term::with('attributes')->where('id', $pa)->select('terms.id', 'terms.name', 'terms.attribute_id')->get()->first();
            array_push($terms, array("id" => $term->id, "name" => $term->name, "attribute_id" => $term->attribute_id));
        }

        $attributes = Product_Attribute::where('product_attribute.product_id', $product_id)
            ->leftJoin('attributes', 'product_attribute.attribute_id', '=', 'attributes.id')
            ->select('attributes.id', 'attributes.name');

        $attr_count = $attributes->count();
        $attrs = $attributes->get();

        //Đệ quy tính số biến thể product
        for($c = 0; $c < $attr_count; $c++)
        {
            $data = array();
            foreach($attrs as $k =>  $attr)
            {
                $temps = array();
                foreach($terms as $i => $item) {
                    if($item['attribute_id'] === $attr->id)
                        array_push($temps, $item['name']);
                }
                array_push($data, array_unique($temps, SORT_REGULAR));
            }
        }

        //Số biến thể
        $combos =  $this->possible_combos($data);

        $success = 0;
        $errors = 0;

        //save update product_variant
        for($i = 0; $i < count($variant_id); $i++)
        {
            $product_variant = Product_Variant::findOrFail($variant_id[$i]);
            $product_variant->product_id = $product_id;
            $product_variant->variant_code = Helpers::slugify(trim($combos[$i]));
            $product_variant->sku = $variant_sku[$i];

            if (!empty($variant_max_price[$i]))
                $product_variant->max_price = str_replace(',', '', trim($variant_max_price[$i]));

            if (!empty($variant_min_price[$i])) {
                $product_variant->onsale = 1;
                $product_variant->min_price = str_replace(',', '', trim($variant_min_price[$i]));
                //discounttime = "2019-04-15 06:00:00 - 2019-04-17 02:00:00"
                $discount_time = explode(' - ', $variant_discounttime[$i]);
                $product_variant->sale_start = $discount_time[0];
                $product_variant->sale_end = $discount_time[1];
            }

            $product_variant->stock_status = $variant_stock[$i];
            $product_variant->stock_quantity = $variant_stock_quantity[$i];
            $product_variant->thumbnail = (empty($variant_thumbnail[$i]) ? '' : explode("/storage/", $variant_thumbnail[$i])[1]);
            $product_variant->timestamps = false;
            if ($product_variant->save())
                $success++;
            else
                $errors++;
        }

        $return = ['success' => $success, 'errors' => $errors];
        return response()->json($return);
    }

    //calculate all the possible comobos creatable from a given choices array
    public function possible_combos($groups, $prefix = ' ') {
        $result = array();
        $group = array_shift($groups);
        foreach($group as $selected) {
            if($groups) {
                $result = array_merge($result, $this->possible_combos($groups, $prefix . $selected. ' '));
            } else {
                $result[] = $prefix . $selected;
            }
        }
        return $result;
    }

    public function product_simple_variant_delete(Request $request){
        $product_slug = Str::slug(trim($request->product_name));
        $product = Product::select('id')->where('slug', $product_slug)->get()->first();

        //Scan and remove all product_term with product_id
        $product_attribute_remove = Product_Attribute::where('product_id', $product->id);
        $product_attribute_remove->delete();

        //Scan and remove all product_term with product_id
        $product_term_remove = Product_Term::where('product_id', $product->id);
        $product_term_remove->delete();

        //Scan and remove all
        $product_variant_remove = Product_Variant::where('product_id', $product->id);
        $product_variant_remove->delete();

        return response()->json('Success');
    }

    public function product_attributes_load(Request $request)
    {
        $data = Attribute::select("id", "name")->get();
        return response()->json($data);
    }

    public function product_attribute_add(Request $request)
    {
        $product_slug = Str::slug(trim($request->product_name));
        $product = Product::select('id')->where('slug', $product_slug)->get()->first();
        $attributes = array();
        $terms = array();
        $product_variant = array();

        foreach ($request->frmData as $frmData)
        {
            if($frmData['name'] == "attributes")
                if(!empty($frmData['value']))
                    array_push($attributes, explode("pa_", $frmData['value'])[1]);
            if($frmData['name'] == "terms")
                if(!empty($frmData['value']))
                    array_push($terms, $frmData['value']);
        }

        //Scan and remove all product_term with product_id
        $product_attribute_remove = Product_Attribute::where('product_id', $product->id);
        $product_attribute_remove->delete();

        //Scan and remove all product_term with product_id
        $product_term_remove = Product_Term::where('product_id', $product->id);
        $product_term_remove->delete();

        //Scan and remove all
        $product_variant_remove = Product_Variant::where('product_id', $product->id);
        $product_variant_remove->delete();

        //Add to product attribute
        foreach ($attributes as $attr)
        {
            //Add record for product variant
            $attribute = new Product_Attribute();
            $attribute->product_id = $product->id;
            $attribute->attribute_id = $attr;
            $attribute->timestamps = false;
            $attribute->save();
        }

        //Add to product variant with default values
        foreach ($terms as $i)
        {
            //Add record for product variant
//            $product_variant = new Product_Variant();
//            $product_variant->product_id = $product->id;
//            $product_variant->term_id = $i;
//            $product_variant->timestamps = false;
//            $product_variant->save();

            $product_term = new Product_Term();
            $product_term->product_id = $product->id;
            $product_term->term_id = $i;
            $product_term->timestamps = false;
            $product_term->save();

            //array_push($product_variant, $product->id);
        }

        return response()->json("success");
    }

    public function tag_store(Request $request)
    {
        $this->validate($request,
            [
                'tag_name' => 'required|max:50|unique:tags,name',
                'tag_slug' => 'max:50|unique:tags,slug',
            ],
            [
                'tag_name.required' => 'Please, provide an tag name.',
                'tag_name.max' => 'Name for the tag must be no more than 50 characters.',
                'tag_name.unique' => 'The Tag is already in use. Change it, please.',
                'tag_slug.max' => 'Slug for the tag must be no more than 50 characters.',
                'tag_slug.unique' => 'The Tag is already in use. Change it, please.',
            ]
        );

        $tag = new Tag();

        $tag->name = $request->input('tag_name');

        if(empty($request->input('tag_slug')))
            $tag->slug = Str::slug($request->input('tag_name'));
        else
            $tag->slug = Str::slug($request->input('tag_slug'));
        $tag->type = $request->input('tag_type');


        if ($tag->save()) {
            $notification = ['alert' => 'success', 'message' => 'The tag has been added!'];
            $route = 'dashboard.' . $request->tag_type . '.tags';
            return redirect()->back()->with($notification);
        } else {
            $notification = ['alert' => 'error', 'message' => 'Unable to add!'];
            return redirect()->back()->with($notification);
        }
    }
    public function tag_delete(Request $request)
    {
        $list_id = $request->tags_id;
        $success = 0;
        $errors = 0;
        foreach($list_id as $id)
        {
            $tag = Tag::findOrFail($id);
            if($tag->delete())
                $success++;
            else
                $errors++;
        }

        $return = ['success' => $success, 'errors' => $errors];
        return response()->json($return);
    }

    public function post_tag_delete(Request $request)
    {
        $list_id = $request->tags_id;
        $data = "";
        foreach($list_id as $id)
        {
            $tag = Post_Tag::where('tag_id', $id);
            if($tag->count() > 0)
            {
                $tag->delete();
                $data = "success";
            }

        }
        return response()->json($data);
    }

    public function product_tag_delete(Request $request)
    {
        $list_id = $request->tags_id;
        $data = "";
        foreach($list_id as $id)
        {
            $tag = Product_Tag::where('tag_id', $id);
            if($tag->count() > 0)
            {
                $tag->delete();
                $data = "success";
            }

        }
        return response()->json($data);
    }
}
