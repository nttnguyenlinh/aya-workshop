<?php

namespace App\Http\Controllers;

use App;
use App\Bill;
use App\BillDetail;
use App\Category;
use App\Coupon;
use App\Customer;
use App\FAQ;
use App\Http\Requests\OrderRequest;
use App\Image;
use App\Post;
use App\Product;
use App\User;
use Auth;
use Carbon\Carbon;
use Cart;
use Hash;
use Illuminate\Http\Request;
use Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $products = Product::productsList();
        $featured = Product::featuredProducts();
        $posts = Post::postsList();
        $faq = FAQ::getAll();
        return view('home.index', compact('products', 'posts', 'featured', 'faq'));
    }

    public function forgetSession(Request $request)
    {
        if (Session::has($request->session))
            Session::forget($request->session);
    }

    public function cart()
    {
        if (!empty(Cart::content())) {
            $cart = Cart::content();
            return view('home.cart', compact('cart'));
        } else {
            return view('home.cart');
        }
    }

    public function addCart(Request $request)
    {
        $product = Product::getProductByID($request->product_id);

        if (!empty($product)) {
            $quantity = $request->quantity;

            if (!empty($product->min_price) && $product->sale_start <= Carbon::now() && Carbon::now() <= $product->sale_end) {
                $price = $product->min_price;
            } else {
                $price = $product->max_price;
            }

            Cart::add(['id' => $request->product_id, 'name' => $product->name, 'qty' => $quantity, 'price' => $price, 'weight' => 0, 'options' => ['slug' => $product->slug, 'thumbnail' => $product->thumbnail]]);

            return redirect()->back()->with(['cart-message' => $product->name . ' has been added to your cart.']);
        } else {
            return abort(404);
        }
    }

    public function updateCart(Request $request)
    {
        Cart::update($request->rowId, $request->qty);
        $return = 'success';
        return $return;
    }

    public function removeCart(Request $request)
    {
        Cart::remove($request->rowId);
        $return = 'success';
        return $return;
    }

    public function destroyCart(Request $request)
    {
        Cart::destroy();
        return redirect()->route('home');
    }

    public function checkout()
    {
        if (Auth::check()) {
            $customer = Customer::getCustomerByUserID(Auth::user()->id);
            return view('home.checkout', compact('customer'));
        }

        return view('home.checkout');
    }

    public function applyCoupon(Request $request)
    {
        //Session::forget('Coupon');
        //dd(Session::get('Coupon'));

        $return = '';
        $coupon = Coupon::findByCode($request->coupon_code);
        if (!$coupon) {
            $return = ['alert' => 'error', 'message' => 'Invalid coupon code. Please try again.'];
        } else {

            date_default_timezone_set('Asia/Ho_Chi_Minh');
            $today = date('Y-m-d');
            $expiry_date = $coupon->coupon_expiry_date;
            $diff = date_diff(date_create($today), date_create($expiry_date))->format('%R%a');

            if ($diff > 0) {
                if ($coupon->coupon_usage < $coupon->coupon_limit) {
                    if (!Session::has('Coupon')) {
                        Coupon::updateUsage($coupon->coupon_code);

                        $discount = Coupon::discount($coupon->coupon_code, Cart::subtotal());
                        $total = str_replace(',', '', Cart::subtotal()) - $discount;

                        $sesionCoupon = [
                            'coupon_code' => $coupon->coupon_code,
                            'coupon_description' => $coupon->coupon_description,
                            'coupon_amount' => $coupon->coupon_amount,
                            'coupon_discount_type' => $coupon->coupon_discount_type,
                            'discount' => $discount,
                            'total' => $total,
                        ];

                        Session::push('Coupon', $sesionCoupon);

                        $cart_content = '
                            <tr class="cart-subtotal">
                                <th>Subtotal</th>
                                <td>
                                    <span class="woocommerce-Price-amount amount">
                                    <span class="woocommerce-Price-currencySymbol">$</span>' . Cart::subtotal() . '</span>
                                </td>
                            </tr>

                            <tr class="cart-discount coupon-' . $coupon->coupon_code . '">
                                <th>Coupon: ' . $coupon->coupon_code . '</th>
                                <td>-<span class="woocommerce-Price-amount amount">
                                    <span class="woocommerce-Price-currencySymbol">$</span>' . $discount . '</span>
                                </td>
                            </tr>

                            <tr class="order-total">
                                <th>Total</th>
                                <td>
                                    <strong>
                                        <span class="woocommerce-Price-amount amount">
                                            <span class="woocommerce-Price-currencySymbol">$</span>' . number_format($total, 2) . '
                                        </span>
                                    </strong>
                                </td>
                            </tr>
                        ';

                        $return = ['alert' => 'success', 'message' => 'Coupon code applied successfully.', 'cart_content' => $cart_content];
                    } else {
                        $return = ['alert' => 'error', 'message' => 'Sorry! You have already applied a discount code.'];
                    }
                } else {
                    $return = ['alert' => 'error', 'message' => 'Discount code is no longer valid.'];
                }
            } else {
                $return = ['alert' => 'error', 'message' => 'Discount code is no longer valid.'];
            }
        }

        return response()->json($return);
    }

    public function postCheckout(OrderRequest $request)
    {
        // reCAPTCHA
        $recaptcha_api = config('app.recaptcha_api');
        $secret_key = config('app.recaptcha_secret_key');

        if (!empty($_POST['g-recaptcha-response'])) {
            $captcha = $_POST['g-recaptcha-response'];

            //GET IP
            if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
                $remoteip = $_SERVER['HTTP_CLIENT_IP'];
            } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                $remoteip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } else {
                $remoteip = $_SERVER['REMOTE_ADDR'];
            }

            //Request API
            $recaptcha_api = $recaptcha_api . '?secret=' . $secret_key . '&response=' . $captcha . '&remoteip=' . $remoteip;

            //GET result API
            $response = file_get_contents($recaptcha_api);
            //Decode
            $response = json_decode($response);

            if (isset($response->success) && $response->success == true) {
                if (!Auth::check()) {
                    $user = User::hasEmailExists($request->email_address);

                    if ($user) {
                        return redirect()->back()->with(['flag' => 'info', 'message' => 'This email already exists, please login to continue.']);
                    } else {
                        $user = new User();

                        $username = explode('@', $request->email_address);

                        $user->username = $username[0];
                        $user->nicename = $request->fullname;
                        $user->email = $request->email_address;
                        $password = $user->username . '123';
                        $user->password = Hash::make($password);
                        $user->level = 0;
                        $user->status = 1;

                        if ($user->save()) {
                            $getuser = User::hasEmailExists($request->email_address);
                            $customer = new Customer();
                            $customer->user_id = $getuser->id;
                            $customer->country = $request->country;
                            $customer->address = $request->address;
                            $customer->city = $request->city;
                            $customer->county = $request->county;
                            $customer->zipcode = $request->zipcode;
                            $customer->phone = $request->phone;
                            $customer->save();

                            $credentials = [
                                'email' => $request->email_address,
                                'password' => $password,
                                'status' => 1,
                            ];

                            Auth::attempt($credentials);
                            Auth::user()->sendEmailVerificationNotification();

                            $customer = Customer::getCustomerByUserID(Auth::user()->id);
                            $email = $customer->email;
                            $bill = new Bill();
                            $bill->customer_id = $customer->id;

                            if (Session::has('Coupon')) {
                                $coupon_Data = Session::get('Coupon');
                                $bill->coupon_code = $coupon_Data[0]['coupon_code'];
                                $bill->coupon_amount = $coupon_Data[0]['discount'];
                                $bill->subtotal_amount = (float) str_replace(',', '', Cart::subtotal());
                                $bill->total_amounts = (float) str_replace(',', '', Cart::subtotal()) - $coupon_Data[0]['discount'];
                            } else {
                                $bill->total_amounts = (float) str_replace(',', '', Cart::subtotal());
                            }

                            $bill->shipping_address = $customer->address . ' - ' . $customer->city . ' - ' . $customer->country;
                            $bill->phone = $customer->phone;

                            $bill->payment = $request->input('payment-method');
                            $bill->payment_info = "";

                            $bill->notes = $request->notes;
                            $bill->shipping_status = 1;
                            $bill->save();

                            $bill_info = Bill::getBillByCustomerID($customer->id);
                            $carts = Cart::content();

                            foreach ($carts as $item) {
                                $bill_detail = new BillDetail();
                                $bill_detail->bill_id = $bill_info->id;
                                $bill_detail->product_id = $item->id;
                                $bill_detail->quantity = $item->qty;
                                $bill_detail->price = $item->price;
                                $bill_detail->save();
                            }

                            if (Session::has('Coupon')) {
                                $coupon_Data = Session::get('Coupon');
                                $sesion = [
                                    'bill_id' => $bill_info->id,
                                    'payment' => $request->input('payment-method'),
                                    'cart' => Cart::content(),
                                    'coupon_code' => $coupon_Data[0]['coupon_code'],
                                    'coupon_amount' => $coupon_Data[0]['discount'],
                                    'subtotal_amount' => str_replace(',', '', Cart::subtotal()),
                                    'total' => str_replace(',', '', Cart::subtotal()) - $coupon_Data[0]['discount'],
                                    'info' => [
                                        'name' => $request->fullname,
                                        'email' => $request->email_address,
                                        'address' => $request->address . ' - ' . $request->city . ' - ' . $request->country,
                                        'phone' => $request->phone,
                                    ],
                                    'timeout' => time(),
                                ];
                            } else {
                                $sesion = [
                                    'bill_id' => $bill_info->id,
                                    'payment' => $request->input('payment-method'),
                                    'cart' => Cart::content(),
                                    'coupon_code' => '',
                                    'coupon_amount' => 0,
                                    'subtotal_amount' => 0,
                                    'total' => Cart::subtotal(),
                                    'info' => [
                                        'name' => $request->fullname,
                                        'email' => $request->email_address,
                                        'address' => $request->address . ' - ' . $request->city . ' - ' . $request->country,
                                        'phone' => $request->phone,
                                    ],
                                    'timeout' => time(),
                                ];
                            }

                            if (Session::has('received')) {
                                Session::forget('received');
                            }
                            Session::push('received', $sesion);

                            if ($request->input('payment-method') == 0) {
                                Cart::destroy();
                                return redirect('checkout/order-received');
                            }

                            //VNPAY
                            else {

                                if (Session::has('Coupon')) {
                                    $coupon_Data = Session::get('Coupon');
                                    $Amount = (str_replace(',', '', Cart::subtotal()) - $coupon_Data[0]['discount']) * 23175;
                                } else {
                                    $Amount = str_replace(',', '', Cart::subtotal()) * 23175;
                                }

                                $TxnRef = $bill_info->id;
                                $OrderInfo = "Thanh toan cho hoa don #" . $TxnRef . " . So tien la: " . $Amount;
                                $Locale = "vn";
                                $Returnurl = route('orderReceived');
                                Cart::destroy();
                                return Bill::vnpay($TxnRef, $Amount, $OrderInfo, $Locale, $Returnurl);
                            }
                        }
                    }
                } else {
                    $getuser = User::query()->where('email', '=', $request->email_address)->first();

                    $customer = Customer::getCustomerByUserID(Auth::user()->id);

                    $bill = new Bill();
                    $bill->customer_id = $customer->id;
                    if (Session::has('Coupon')) {
                        $coupon_Data = Session::get('Coupon');
                        $bill->coupon_code = $coupon_Data[0]['coupon_code'];
                        $bill->coupon_amount = $coupon_Data[0]['discount'];
                        $bill->subtotal_amount = (float) str_replace(',', '', Cart::subtotal());
                        $bill->total_amounts = (float) str_replace(',', '', Cart::subtotal()) - $coupon_Data[0]['discount'];
                    } else {
                        $bill->total_amounts = (float) str_replace(',', '', Cart::subtotal());
                    }

                    $bill->shipping_address = $customer->address . ' - ' . $customer->city . ' - ' . $customer->country;
                    $bill->phone = $customer->phone;
                    $bill->payment = $request->input('payment-method');
                    $bill->payment_info = "";
                    $bill->notes = $request->notes;
                    $bill->shipping_status = 1;
                    $bill->save();

                    $bill_info = Bill::getBillByCustomerID($customer->id);
                    $carts = Cart::content();

                    foreach ($carts as $item) {
                        $bill_detail = new BillDetail();
                        $bill_detail->bill_id = $bill_info->id;
                        $bill_detail->product_id = $item->id;
                        $bill_detail->quantity = $item->qty;
                        $bill_detail->price = $item->price;
                        $bill_detail->save();
                    }

                    if (Session::has('Coupon')) {
                        $coupon_Data = Session::get('Coupon');
                        $sesion = [
                            'bill_id' => $bill_info->id,
                            'payment' => $request->input('payment-method'),
                            'cart' => Cart::content(),
                            'coupon_code' => $coupon_Data[0]['coupon_code'],
                            'coupon_amount' => $coupon_Data[0]['discount'],
                            'subtotal_amount' => str_replace(',', '', Cart::subtotal()),
                            'total' => str_replace(',', '', Cart::subtotal()) - $coupon_Data[0]['discount'],
                            'info' => [
                                'name' => $getuser->nicename,
                                'email' => $getuser->email,
                                'address' => $customer->address . ' - ' . $customer->city . ' - ' . $customer->country,
                                'phone' => $customer->phone,
                            ],
                            'timeout' => time(),
                        ];
                    } else {
                        $sesion = [
                            'bill_id' => $bill_info->id,
                            'payment' => $request->input('payment-method'),
                            'cart' => Cart::content(),
                            'coupon_code' => '',
                            'coupon_amount' => 0,
                            'subtotal_amount' => 0,
                            'total' => Cart::subtotal(),
                            'info' => [
                                'name' => $getuser->nicename,
                                'email' => $getuser->email,
                                'address' => $customer->address . ' - ' . $customer->city . ' - ' . $customer->country,
                                'phone' => $customer->phone,
                            ],
                            'timeout' => time(),
                        ];
                    }

                    if (Session::has('received')) {
                        Session::forget('received');
                    }
                    Session::push('received', $sesion);

                    if ($request->input('payment-method') == 0) {
                        Cart::destroy();
                        return redirect('checkout/order-received');
                    }

                    //VNPAY
                    else {

                        if (Session::has('Coupon')) {
                            $coupon_Data = Session::get('Coupon');
                            $Amount = (str_replace(',', '', Cart::subtotal()) - $coupon_Data[0]['discount']) * 23175;
                        } else {
                            $Amount = str_replace(',', '', Cart::subtotal()) * 23175;
                        }

                        $TxnRef = $bill_info->id;
                        $OrderInfo = "Thanh toan cho hoa don #" . $TxnRef . " . So tien la: " . $Amount;
                        $Locale = "vn";
                        $Returnurl = route('orderReceived');
                        Cart::destroy();
                        return Bill::vnpay($TxnRef, $Amount, $OrderInfo, $Locale, $Returnurl);
                    }
                }
            } else {
                $notification = ['flag' => 'error', 'message' => 'Could not verify you. Please try again!'];
                return redirect()->back()->with($notification)->withInput();
            }
        } else {
            $notification = ['flag' => 'error', 'message' => 'Could not verify you. Please try again!'];
            return redirect()->back()->with($notification)->withInput();
        }
    }

    public function orderReceived(Request $request)
    {
        // if ((time() - Session::get('received')[0]['timeout']) < 10) {

        // }

        //Session::forget('received');

        if (Session::get('received')[0]['payment'] > 0) {
            $vnp_SecureHash = $request->vnp_SecureHash;
            $inputData = array();

            $req = (array) $request->all();

            foreach ($req as $key => $value) {
                $inputData[$key] = $value;
            }

            unset($inputData['vnp_SecureHashType']);
            unset($inputData['vnp_SecureHash']);
            ksort($inputData);
            $i = 0;
            $hashData = "";

            foreach ($inputData as $key => $value) {
                if ($i == 1) {
                    $hashData = $hashData . '&' . $key . "=" . $value;
                } else {
                    $hashData = $hashData . $key . "=" . $value;
                    $i = 1;
                }
            }

            $secureHash = hash('sha256', config('app.vnp_HashSecret') . $hashData);

            $payment_code = -1;
            $payment_message = "";
            if ($secureHash == $vnp_SecureHash) {
                if ($request->vnp_ResponseCode == '00') {
                    $payment_code = 0;
                    $payment_message = "Giao dịch thành công!";
                } else {
                    $payment_code = 1;
                    $payment_message = "Giao dịch thất bại!";
                }
            } else {
                $payment_code = -1;
                $payment_message = "Lỗi! Chữ ký không hợp lệ.";
            }

            $bill = Bill::getBill(Session::get('received')[0]['bill_id']);
            $bill->payment_info = $payment_message;
            $bill->save();

            $payment_info = [
                'payment_code' => $payment_code,
                'payment_message' => $payment_message,
                'timeout' => time(),
            ];

            if (Session::has('paymentInfo')) {
                Session::forget('paymentInfo');
            }
            Session::push('paymentInfo', $payment_info);

            return view('home.order-received');

        } else {
            return view('home.order-received');
        }
    }

    public function products()
    {
        $products = Product::products();
        $categories = Category::get_product_child_category();
        return view('home.products', compact('products', 'categories'));
    }

    public function posts()
    {
        $posts = Post::posts();
        $categories = Category::get_post_child_category();
        $latestPosts = Post::latestPosts();
        return view('home.posts', compact('posts', 'categories', 'latestPosts'));
    }

    public function productDetails(Request $request)
    {
        $product = Product::productDetails($request->slug);
        $related = Product::relatedProducts($product->category_id);
        $categories = Category::get_product_child_category();
        $parent_category = Category::get_parent_category($product->category_parent);
        return view('home.productdetais', compact('product', 'parent_category', 'categories', 'related'));
    }

    public function productImages(Request $request)
    {
        $images = Image::getProductImages($request->product_id);
        return response()->json($images);
    }

    public function postDetails(Request $request)
    {
        $post = Post::postDetails($request->slug);
        $latestPosts = Post::latestPosts();
        $categories = Category::get_post_child_category();
        return view('home.postdetais', compact('post', 'latestPosts', 'categories'));
    }

    public function postCategory(Request $request)
    {
        $posts = Post::postsCategory($request->slug);
        $categories = Category::get_post_child_category();
        $latestPosts = Post::latestPosts();
        return view('home.postsCategory', compact('posts', 'categories', 'latestPosts'));
    }

    public function productCategory(Request $request)
    {
        $products = Product::productsCategory($request->slug);
        $category_name = Category::select('name')->where('slug', '=', $request->slug)->get()->first();
        $categories = Category::get_product_child_category();
        return view('home.productsCategory', compact('products', 'categories', 'category_name'));
    }

    public function trackOrder()
    {
        return view('home.trackOrder');
    }

    public function Locale($locale)
    {
        App::setLocale($locale);
        Session::put('locale', $locale);
        return redirect()->back();
    }
}
