<?php

namespace App\Http\Controllers;

use App\Bill;
use App\BillDetail;
use App\Customer;
use App\Http\Controllers\Controller;
use App\Http\Requests\AccountRequest;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\User;
use Hash;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class AccountController extends Controller
{

    public function index()
    {
        return view('account.index');
    }

    public function orders()
    {
        $customer = Customer::getCustomerByUserID(Auth::user()->id);

        $orders = Bill::getListByCustomer($customer->id);
        return view('account.orders', compact('orders'));
    }

    public function viewOrder(Request $request)
    {
        $user = User::findOrFail(Auth::user()->id);
        $customer = Customer::getCustomerByUserID(Auth::user()->id);
        $bill = Bill::getBillByIdCustomer($customer->id, $request->bill_id);

        if (!empty($bill)) {
            $bill_details = BillDetail::DetailByBillID($bill->id);
            return view('account.view-order', compact('user', 'customer', 'bill', 'bill_details'));
        } else {
            abort(404);
        }
    }

    public function editAccount()
    {
        if (Auth::check()) {
            $customer = Customer::getCustomerByUserID(Auth::user()->id);

            return view('account.edit-account', compact('customer'));
        }
        return redirect()->route('account.getLogin');
    }

    public function editAccountSubmit(AccountRequest $request)
    {
        $user = User::findOrFail($request->id);

        $password = $user->password;

        if ($user) {
            $exists = User::where('email', '=', $request->email_address, 'and')->where('id', '!=', $user->id)->get()->first();
            if ($exists) {
                return redirect()->back()->with(['flag' => 'error', 'message' => 'Email already exists!']);
            } else {
                if (!empty($request->password_current)) {
                    if (!Hash::check($request->password_current, $user->password)) {
                        return redirect()->back()->with(['flag' => 'error', 'message' => 'Password does not match!']);
                    } else {
                        $this->validate(
                            $request,
                            [
                                'password_1' => 'required|regex:/(?!^[0-9]*$)(?!^[a-zA-Z]*$)(?!^[0-9]{1})^([a-zA-Z0-9]{6,})$/',
                                'password_2' => 'same:password_1',
                            ],
                            [
                                'password_1.required' => 'Please provide a password new.',
                                'password_1.regex' => 'The password should be at least six characters long. To make it stronger, use upper and lower case letters and numbers.',
                                'password_2.same' => 'A new password does not match.',
                            ]
                        );
                        $password = Hash::make($request->password_1);
                    }
                }
                $user->nicename = $request->nicename;
                $user->email = $request->email_address;
                $user->password = $password;

                $customer = Customer::findOrFail($request->customer_id);
                $customer->country = $request->country;
                $customer->address = $request->address;
                $customer->city = $request->city;
                $customer->county = $request->county;
                $customer->zipcode = $request->zipcode;
                $customer->phone = $request->phone;

                if ($user->save()) {
                    if ($customer->save()) {
                        return redirect()->back()->with(['flag' => 'success', 'message' => 'Account details have been changed!']);
                    } else {
                        return redirect()->back()->with(['flag' => 'error', 'message' => 'Please check billing address!']);
                    }
                } else {
                    return redirect()->back()->with(['flag' => 'error', 'message' => 'Please check account details!']);
                }
            }
        }
    }

    use AuthenticatesUsers;

    protected $maxAttempts = 5; // Default is 5
    protected $decayMinutes = 30; // Default is 1

    protected function hasTooManyLoginAttempts(LoginRequest $request)
    {
        return $this->limiter()->tooManyAttempts($this->throttleKey($request), $this->maxAttempts, $this->decayMinutes);
    }

    public function getRegister()
    {
        return view('account.login');
    }

    public function register(RegisterRequest $request)
    {
        // reCAPTCHA
        //Config
        $recaptcha_api = config('app.recaptcha_api');
        $secret_key = config('app.recaptcha_secret_key');

        if (!empty($_POST['g-recaptcha-response'])) {
            $captcha = $_POST['g-recaptcha-response'];

            //GET IP
            if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
                $remoteip = $_SERVER['HTTP_CLIENT_IP'];
            } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                $remoteip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } else {
                $remoteip = $_SERVER['REMOTE_ADDR'];
            }

            //Request API
            $recaptcha_api = $recaptcha_api . '?secret=' . $secret_key . '&response=' . $captcha . '&remoteip=' . $remoteip;

            //GET result API
            $response = file_get_contents($recaptcha_api);
            //Decode
            $response = json_decode($response);

            if (isset($response->success) && $response->success == true) {
                $user = new User();

                $username = explode('@', $request->email);

                $user->username = $username[0];
                $user->nicename = $request->name;
                $user->email = $request->email;
                $user->password = Hash::make($request->password);
                $user->level = 0;
                $user->status = 1;

                if ($user->save()) {
                    $getuser = User::query()->where('email', '=', $request->email)->first();

                    $customer = new Customer();
                    $customer->user_id = $getuser->id;
                    $customer->save();

                    $credentials = [
                        'email' => $request->email,
                        'password' => $request->password,
                        'status' => 1,
                    ];

                    if (Auth::attempt($credentials)) {
                        Auth::user()->sendEmailVerificationNotification();
                        $notification = ['flag' => 'success', 'message' => 'Thank you to us! <br>Please check your email for a verification.<br>Wish you a good day!'];
                        return redirect('/')->with($notification);
                    }
                }
            } else {
                $notification = ['flag' => 'error', 'message' => 'Could not verify you. Please try again!'];
                return redirect()->back()->with($notification)->withInput();
            }
        } else {
            $notification = ['flag' => 'error', 'message' => 'Could not verify you. Please try again!'];
            return redirect()->back()->with($notification)->withInput();
        }
    }

    public function getLogin()
    {
        if (Auth::check() || Auth::viaRemember()) {
            return redirect()->route('myaccount');
        }
        return view('account.login');
    }

    public function login(LoginRequest $request)
    {
        // reCAPTCHA
        //Config
        $recaptcha_api = config('app.recaptcha_api');
        $secret_key = config('app.recaptcha_secret_key');

        if (!empty($_POST['g-recaptcha-response'])) {
            $captcha = $_POST['g-recaptcha-response'];

            //GET IP
            if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
                $remoteip = $_SERVER['HTTP_CLIENT_IP'];
            } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                $remoteip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } else {
                $remoteip = $_SERVER['REMOTE_ADDR'];
            }

            //Request API
            $recaptcha_api = $recaptcha_api . '?secret=' . $secret_key . '&response=' . $captcha . '&remoteip=' . $remoteip;

            //GET result API
            $response = file_get_contents($recaptcha_api);
            //Decode
            $response = json_decode($response);

            if (isset($response->success) && $response->success == true) {
                /** This line should be in the start of method */
                if ($this->hasTooManyLoginAttempts($request)) {
                    $this->fireLockoutEvent($request);
                    return $this->sendLockoutResponse($request);
                }

                $remember = (Input::has('remember')) ? true : false;

                $credentials = [
                    'email' => $request->email,
                    'password' => $request->password,
                    'status' => 1,
                ];

                if (Auth::attempt($credentials, $remember)) {
                    $this->clearLoginAttempts($request);

                    $user = Auth::user();

                    $notification = ['flag' => 'success', 'message' => 'Welcome back, ' . mb_strtoupper($user->nicename, 'UTF-8')];

                    return redirect('my-account')->with($notification);
                } else {
                    $this->incrementLoginAttempts($request);
                    $notification = ['flag' => 'error', 'message' => 'Please check again, maybe:<br>-Email\Password is incorrect<br>-Email is locked'];
                    return redirect()->back()->with($notification)->withInput();
                }
            } else {
                $notification = ['flag' => 'error', 'message' => 'Could not verify you. Please try again!'];
                return redirect()->back()->with($notification)->withInput();
            }
        } else {
            $notification = ['flag' => 'error', 'message' => 'Could not verify you. Please try again!'];
            return redirect()->back()->with($notification)->withInput();
        }
    }

    public function logout()
    {
        if (Auth::check()) {
            Auth::logout();
        }
        return redirect()->route('account.getLogin');
    }
}
