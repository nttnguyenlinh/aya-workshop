<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use Illuminate\Support\Facades\Auth;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use User;

class AuthController extends Controller
{
    /*Integrating Throttle in Custom Login*/

    use AuthenticatesUsers;

    protected $maxAttempts = 5; // Default is 5
    protected $decayMinutes = 30; // Default is 1

    protected function hasTooManyLoginAttempts(LoginRequest $request)
    {
        return $this->limiter()->tooManyAttempts($this->throttleKey($request), $this->maxAttempts, $this->decayMinutes);
    }

    public function getLogin()
    {
        if(Auth::check() || Auth::viaRemember())
            return redirect()->route('dashboard');
        return view('dashboard.login');
    }

    public function login(LoginRequest $request)
    {
        // reCAPTCHA
        //Config
        $recaptcha_api = config('app.recaptcha_api');
        $secret_key = config('app.recaptcha_secret_key');

        if (!empty($_POST['g-recaptcha-response']))
        {
            $captcha = $_POST['g-recaptcha-response'];

            //GET IP
            if (!empty($_SERVER['HTTP_CLIENT_IP']))
                $remoteip = $_SERVER['HTTP_CLIENT_IP'];
            elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
                $remoteip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            else
                $remoteip = $_SERVER['REMOTE_ADDR'];

            //Request API
            $recaptcha_api = $recaptcha_api.'?secret='.$secret_key.'&response='.$captcha.'&remoteip='.$remoteip;

            //GET result API
            $response = file_get_contents($recaptcha_api);
            //Decode 
            $response = json_decode($response);

            if(isset($response->success) && $response->success == true)
            {
                /** This line should be in the start of method */
                if ($this->hasTooManyLoginAttempts($request)) {
                    $this->fireLockoutEvent($request);
                    return $this->sendLockoutResponse($request);
                }

                $remember = (Input::has('remember')) ? true : false;

                $credentials = [
                    'email' => $request->email,
                    'password' => $request->password,
                    'level' => 1,
                    'status' => 1
                ];

                if (Auth::attempt($credentials, $remember))
                {
                    $this->clearLoginAttempts($request);

                    $user = Auth::user();

                    $notification = ['alert' =>'success', 'message' => 'Welcome back, ' . mb_strtoupper($user->nicename, 'UTF-8') . ' to the dashboard'];

                    return redirect('dashboard')->with($notification);
                }
                    
                else
                {
                    $this->incrementLoginAttempts($request);
                    $notification = ['alert' =>'error', 'message' => 'Please check again, maybe: <br>-Email\Password is incorrect <br>-Email is locked <br>-Not an Admin account'];
                    return redirect()->back()->with($notification)->withInput();
                }    
            }
            else
            {
                $notification = ['alert' =>'error', 'message' => 'Could not verify you. Please try again!'];
                return redirect()->back()->with($notification)->withInput();
            }    
        }
        else
        {
            $notification = ['alert' =>'error', 'message' => 'Could not verify you. Please try again!'];
            return redirect()->back()->with($notification)->withInput();
        }          
    }

    public function logout()
    {
        if( Auth::check())
            Auth::logout();
        return redirect()->route('dashboard.getLogin');
    }
}

