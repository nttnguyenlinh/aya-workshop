<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContactFormRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'your_name' => 'required|max:50',
            'your_email' => 'required|email|max:50',
            'your_message' => 'required|max:150',
            'g-recaptcha-response' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'your_name.required' => __('required', ['attribute' => 'name']),
            'your_name.max' => __('max.string', ['attribute' => 'name', 'max' => 50]),
            'your_email.required' => __('required', ['attribute' => 'email']),
            'your_email.email' => __('email_validation'),
            'your_email.max' => __('max.string', ['attribute' => 'email', 'max' => 50]),
            'your_message.required' => __('required', ['attribute' => 'message']),
            'your_message.max' => __('max.string', ['attribute' => 'message', 'max' => 150]),
            'g-recaptcha-response.required' => __('required', ['attribute' => 'recaptcha'])
        ];
    }
}