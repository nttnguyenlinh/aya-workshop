<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fullname' => 'required|string|max:50',
            'email_address' => 'required|email|max:50',
            'country' => 'required|max:2',
            'address' => 'required',
            'city' => 'required',
            'phone' => 'required|max:15',
        ];
    }

    public function messages()
    {
        return [
            'fullname.required' => 'Please enter your full name',
            'fullname.max' => 'Full name has not more than 50 chars',
            'email_address.required' => 'Please enter a valid email address',
            'email_address.email' => 'Please enter a valid email address',
            'email_address.max' => 'Email has not more than 50 chars',
            'country.required' => 'Please enter country',
            'country.max' => 'Invalid country',
            'address.required' => 'Please enter address',
            'city.required' => 'Please enter city',
            'phone.required' => 'Please enter phone',
            'phone.max' => 'Invalid phone',
        ];
    }
}
