<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email',
            'password' => 'required',
            'g-recaptcha-response' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'email.required' => __('required', ['attribute' => 'email']),
            'email.email' => __('email_validation'),
            'password.required' => __('required', ['attribute' => 'password']),
            'g-recaptcha-response.required' => __('required', ['attribute' => 'recaptcha'])
        ];
    }
}