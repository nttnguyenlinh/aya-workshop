<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:50',
            'email' => 'required|email|max:50|unique:users,email',
            'password' => [
                'required', 
                'string', 
                'min:6', 
                'regex:/(?!^[0-9]*$)(?!^[a-zA-Z]*$)(?!^[0-9]{1})^([a-zA-Z0-9]{6,})$/'   
            ],
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Please enter your name',
            'name.string' => 'Name must be a string',
            'name.max' => 'Name has not more than 50 chars',
            'email.required' => 'Please enter a valid email address',
            'email.email' => 'Please enter a valid email address',
            'email.max' => 'Email has not more than 50 chars',
            'email.unique' => 'Email already exists',
            'password.required' => 'Please provide a password',
            'password.min' => 'The password should be at least six characters long. To make it stronger, use upper and lower case letters and numbers.',
            'password.regex' => 'The password should be at least six characters long. To make it stronger, use upper and lower case letters and numbers.',
        ];
    }
}
