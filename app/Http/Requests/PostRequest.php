<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'contents' => 'required',
            'excerpt' => 'required',
            'post_tag' => 'required',
            'categories' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Please enter title',
            'contents.required' => 'Please enter contents',
            'excerpt.required' => 'Please enter excerpt',
            'post_tag.required' => 'Please select tag',
            'categories.required' => 'Please select category'
        ];
    }
}
