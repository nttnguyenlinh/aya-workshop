<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AccountRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nicename' => 'required|string|max:50',
            'email_address' => 'required|email|max:50',
            'country' => 'required|max:2',
            'address' => 'required',
            'city' => 'required',
            'phone' => 'required|max:15',
        ];
    }

    public function messages()
    {
        return [
            'nicename.required' => __('required', ['attribute' => 'name']),
            'nicename.string' => __('string', ['attribute' => 'name']),
            'nicename.max' => __('max.string', ['attribute' => 'name', 'max' => 50]),
            'email_address.required' => __('required', ['attribute' => 'email']),
            'email_address.email' => __('email_validation'),
            'email_address.max' => __('max.string', ['attribute' => 'email', 'max' => 50]),
            'country.required' => __('required', ['attribute' => 'country']),
            'country.max' => __('not_in', ['attribute' => 'country']),
            'address.required' => __('required', ['attribute' => 'address']),
            'city.required' => __('required', ['attribute' => 'city']),
            'phone.required' => __('required', ['attribute' => 'phone']),
            'phone.max' => __('max.string', ['attribute' => 'phone', 'max' => 15]),
        ];
    }
}