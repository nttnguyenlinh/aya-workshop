<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        //Custom 404 error
        if ($e instanceof ModelNotFoundException) {
            return response()->view('errors.404', [], 404);
        }

        //Custom 500 error any debug
        // if ($e instanceof \ErrorException) {
        //     $response = response()->view('errors.500', [], 500);
        //     return  $this->toIlluminateResponse($response,$e);
        // }

        return parent::render($request, $e);

    }

    //Custom 403 error
    public function forbiddenResponse()
    {
        return response(view('errors.403'), 403);
    }

    //Custom 500 error when debug false
    // protected function convertExceptionToResponse(Exception $e)
    // {
    //     if (!config('app.debug')) {
    //         return parent::convertExceptionToResponse($e);
    //     }
    //     ob_clean();
    //     return response()->view('errors.500', [], 500);
    // }
}
