<?php

namespace App;

use App\Bill_Detail;
use Carbon\Carbon;
use DataTables;
use DB;
use Illuminate\Database\Eloquent\Model;

class Bill extends Model
{
    protected $table = 'bills';

    protected $fillable = [
        'customer_id', 'coupon_code', 'coupon_amount', 'subtotal_amount', 'total_amounts', 'notes', 'payment', 'payment_info', 'shipping_status', 'shipping_address', 'phone',
    ];

    public function shipping_status()
    {
        return $this->belongsTo('App\ShippingStatus');
    }

    public function bill_detail()
    {
        return $this->hasMany('App\BillDetail');
    }

    public function customers()
    {
        return $this->belongsTo('App\Customer');
    }

    public static function getBill($id)
    {
        $bills = Bill::where('id', '=', $id)
            ->select('*')
            ->get()
            ->first();
        return $bills;
    }

    public static function anyBills()
    {
        $bills = Bill::leftJoin('shipping_status as status', 'bills.shipping_status', '=', 'status.id')
            ->leftJoin('customers', 'bills.customer_id', '=', 'customers.id')
            ->leftJoin('users', 'customers.user_id', '=', 'users.id')
            ->select('customers.id as customer_id', 'users.nicename as user_nicename', 'bills.id', 'bills.payment', 'bills.payment_info', 'bills.created_at', 'bills.shipping_status as status', 'status.title', 'total_amounts')
            ->orderBy('bills.id', 'DESC')
            ->get();

        return Datatables::of($bills)
            ->editColumn('id', function ($bill) {
                return '<span style="font-weight:bold;"># ' . $bill->id . ' ' . $bill->user_nicename . ' </span><button class="btn btn-outline-primary" id="view" data-bill="' . $bill->id . '" title="Preview" style="float:right"><i class="fal fa-eye"></i></button>';
            })
            ->editColumn('created_at', function ($bill) {
                return '<center style="margin-top:10px;" title="' . $bill->created_at . '"><span>' . Carbon::parse($bill->created_at)->diffForHumans() . '</span><br>' . $bill->created_at . '</center>';
            })
            ->editColumn('title', function ($bill) {
                switch ($bill->status) {
                    case 1:
                        return '<button type="button" class="btn btn-block btn-success" id="change" data-status="' . $bill->status . '" data-bill="' . $bill->id . '" style="margin-top:20px; text-align:center; background: #c6e1c6; color: #5b841b; border-radius: 4px;">' . $bill->title . '</button>';
                        break;

                    case 2:
                        return '<button type="button" class="btn btn btn-block btn-default" id="change" data-status="' . $bill->status . '" data-bill="' . $bill->id . '" style="margin-top:20px; text-align:center; color: #777; background: #e5e5e5; border-radius: 4px;">' . $bill->title . '</button>';
                        break;
                    case 3:
                        return '<button type="button" class="btn btn-block btn-default" id="change" data-status="' . $bill->status . '" data-bill="' . $bill->id . '" style="margin-top:20px; text-align:center; color: #777; background: #e5e5e5; border-radius: 4px;">' . $bill->title . '</button>';
                        break;
                    case 4:
                        return '<button type="button" class="btn btn-block btn-success" id="change" data-status="' . $bill->status . '" data-bill="' . $bill->id . '" style="margin-top:20px; text-align:center; border-radius: 4px;">' . $bill->title . '</button>';
                        break;
                    case 5:
                        return '<button type="button" class="btn btn-block btn-danger" id="change" data-status="' . $bill->status . '" data-bill="' . $bill->id . '" style="margin-top:20px; text-align:center; color: #761919; background: #eba3a3; border-radius: 4px;">' . $bill->title . '</button>';
                        break;
                    case 6:
                        return '<button type="button" class="btn btn-block btn-warning" id="change" data-status="' . $bill->status . '" data-bill="' . $bill->id . '" style="margin-top:20px; text-align:center; color: #94660c; background: #f8dda7;border-radius: 4px;">' . $bill->title . '</button>';
                        break;
                    case 7:
                        return '<button type="button" class="btn btn-block btn-info" id="change" data-status="' . $bill->status . '" data-bill="' . $bill->id . '" style="margin-top:20px; text-align:center; border-radius: 4px;">' . $bill->title . '</button>';
                        break;
                }
            })
            ->editColumn('payment', function ($bill) {
                if ($bill->payment > 0) {
                    return '<center style="margin-top:10px;" title="VNPAY"><span>VNPAY</span><br>' . $bill->payment_info . '</center>';
                } else {
                    return '<center style="margin-top:10px;" title="Cash on delivery"><span>Cash on delivery</span><br>' . $bill->payment_info . '</center>';
                }

            })
            ->editColumn('total_amounts', function ($bill) {
                return '<p style="margin-top:20px; text-align:center;">$' . ((floor($bill->total_amounts) == round($bill->total_amounts, 2)) ? number_format($bill->total_amounts) : number_format($bill->total_amounts, 2)) . '</p>';
            })
            ->rawColumns(['id', 'created_at', 'title', 'payment', 'total_amounts'])
            ->make(true);
    }

    public static function getBillByCustomerID($id)
    {
        $data = Bill::where('customer_id', '=', $id)
            ->select('*')
            ->orderBy('id', 'DESC')
            ->get()
            ->first();
        return $data;
    }

    public static function getListByCustomer($customer_id)
    {
        $data = Bill::where('customer_id', '=', $customer_id)
            ->leftJoin('bill_detail', 'bills.id', '=', 'bill_detail.bill_id')
            ->leftJoin('shipping_status as status', 'bills.shipping_status', '=', 'status.id')
            ->select('bills.id', 'bills.payment', 'bills.payment_info', 'total_amounts', 'bills.shipping_status as status', 'status.title', 'bills.created_at', DB::raw('count(bill_detail.bill_id) as total_item'))
            ->groupBy('bills.id')
            ->get();
        return $data;
    }

    public static function getBillByIdCustomer($customer_id, $bill_id)
    {
        $data = Bill::where('customer_id', '=', $customer_id, 'and')
            ->where('bills.id', '=', $bill_id)
            ->leftJoin('shipping_status as status', 'bills.shipping_status', '=', 'status.id')
            ->select('bills.id', 'bills.payment', 'bills.payment_info', 'total_amounts', 'bills.shipping_status as status', 'status.title', 'bills.created_at')
            ->get()
            ->first();
        return $data;
    }

    public static function getCustomerByBill($bill_id)
    {
        $customer = Bill::where('bills.id', '=', $bill_id)
            ->leftJoin('customers', 'bills.customer_id', '=', 'customers.id')
            ->leftJoin('users', 'customers.user_id', '=', 'users.id')
            ->select('users.nicename', 'users.email', 'customers.address', 'customers.county', 'customers.city', 'customers.country', 'customers.phone', 'bills.notes', 'bills.payment', 'bills.payment_info')
            ->get()
            ->first();

        return $customer;
    }

    public static function getDetailsByBill($bill_id)
    {
        $details = Bill::where('bills.id', '=', $bill_id)
            ->leftJoin('bill_detail as detail', 'bills.id', '=', 'detail.bill_id')
            ->leftJoin('products', 'detail.product_id', '=', 'products.id')
            ->select('products.name', 'products.slug', 'detail.quantity', 'detail.price')
            ->get();

        return $details;
    }

    public static function vnpay(int $TxnRef, int $Amount, string $OrderInfo, string $Locale, string $Returnurl)
    {
        session(['url_prev' => url()->previous()]);
        $vnp_TmnCode = config('app.vnp_TmnCode'); //Mã website tại VNPAY
        $vnp_HashSecret = config('app.vnp_HashSecret'); //Chuỗi bí mật
        $vnp_Url = config('app.vnp_Url');
        $vnp_Returnurl = $Returnurl;

        $vnp_TxnRef = $TxnRef; //Mã đơn hàng.
        $vnp_OrderInfo = $OrderInfo; //ghi chú thanh toán
        $vnp_OrderType = 'billpayment';
        $vnp_Amount = $Amount * 100;
        $vnp_Locale = $Locale;
        $vnp_BankCode = "";
        $vnp_IpAddr = $_SERVER['REMOTE_ADDR'];

        $inputData = array(
            "vnp_Version" => "2.0.0",
            "vnp_TmnCode" => $vnp_TmnCode,
            "vnp_Amount" => $vnp_Amount,
            "vnp_Command" => "pay",
            "vnp_CreateDate" => date('YmdHis'),
            "vnp_CurrCode" => "VND",
            "vnp_IpAddr" => $vnp_IpAddr,
            "vnp_Locale" => $vnp_Locale,
            "vnp_OrderInfo" => $vnp_OrderInfo,
            "vnp_OrderType" => $vnp_OrderType,
            "vnp_ReturnUrl" => $vnp_Returnurl,
            "vnp_TxnRef" => $vnp_TxnRef,
        );

        if (isset($vnp_BankCode) && $vnp_BankCode != "") {
            $inputData['vnp_BankCode'] = $vnp_BankCode;
        }

        ksort($inputData);
        $query = "";
        $i = 0;
        $hashdata = "";
        foreach ($inputData as $key => $value) {
            if ($i == 1) {
                $hashdata .= '&' . $key . "=" . $value;
            } else {
                $hashdata .= $key . "=" . $value;
                $i = 1;
            }
            $query .= urlencode($key) . "=" . urlencode($value) . '&';
        }

        $vnp_Url = $vnp_Url . "?" . $query;
        if (isset($vnp_HashSecret)) {
            $vnpSecureHash = hash('sha256', $vnp_HashSecret . $hashdata);
            $vnp_Url .= 'vnp_SecureHashType=SHA256&vnp_SecureHash=' . $vnpSecureHash;
        }

        return redirect($vnp_Url);
        //return response()->json($vnp_Url);
    }
}