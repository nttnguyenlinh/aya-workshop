<?php

namespace App;

use DataTables;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';

    protected $fillable = [
        'name', 'slug', 'sku', 'type', 'min_price', 'max_price', 'onsale', 'sale_start', 'sale_end',
        'in_stock', 'stock_quantity', 'description', 'contents', 'total_sales', 'featured', 'thumbnail',
        'seo_title', 'seo_keyword', 'meta_description', 'facebook_image', 'visibility',
    ];

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'product_category');
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'product_tag');
    }

    public function bill_detail()
    {
        return $this->hasMany('App\BillDetail');
    }

    public function images()
    {
        return $this->hasMany('App\Image');
    }

    public static function products()
    {
        $products = self::with('categories')->with('tags')
            ->where('products.visibility', 1)
            ->select(['products.*'])
            ->orderBy('products.id', 'DESC')
            ->paginate(8);
        return $products;
    }

    public static function productsCategory($categories_slug)
    {
        $products = self::with(["categories" => function($c, $categories_slug){
            $c->where('categories.slug', $categories_slug);
        }])->with('tags')
            ->where('products.visibility', 1)
            ->select(['products.*'])
            ->orderBy('products.id', 'DESC')
            ->paginate(8);
        return $products;
    }

    public static function productsList()
    {
        $products = self::with('categories')->with('tags')
            ->where('products.visibility', 1)
            ->select(['products.*'])
            ->orderBy('products.id', 'DESC')
            ->take(5)
            ->get();
        return $products;
    }

    public static function productDetails($slug)
    {
        $product = self::with('categories')->with('tags')
        ->select(['products.*'])
        ->where('products.slug', 'like', $slug)
        ->get()
        ->first();
        return $product;
    }

    public static function featuredProducts()
    {
        $featured = self::with('categories')->with('tags')
            ->where('products.visibility', 1)
            ->where('products.featured', 1)
            ->select(['products.*'])
            ->orderBy('products.id', 'DESC')
            ->take(5)
            ->get();
        return $featured;
    }

    public static function relatedProducts($category_id)
    {
        $related = self::with(["categories" => function($c) use($category_id){
            $c->where('categories.id', $category_id);
        }])->with('tags')
            ->where('products.visibility', 1)
            ->select(['products.*'])
            ->orderBy('products.id', 'DESC')
            ->take(5)
            ->get();
        return $related;
    }

    public static function products_list($visibility = 100)
    {
        define("visibility_F", $visibility);
        if(visibility_F == 100 || visibility_F == "")
            $data = self::with('categories')->with('tags')
                ->select(['products.*'])
                ->orderBy('products.updated_at', 'desc')
                ->get();
        else
            $data = self::with('categories')->with('tags')
                ->where('products.visibility', visibility_F)
                ->select(['products.*'])
                ->orderBy('products.updated_at', 'desc')
                ->get();

        return Datatables::of($data)
            ->editColumn('thumbnail', function ($data) {
                if(empty($data->thumbnail))
                    return '<img src="' . asset('storage/placeholder.png'). '" alt="' . $data->name. '" class="wp_cat_thumb">';
                else
                    return '<img src="' . asset('storage/' . $data->thumbnail). '" alt="' . $data->name. '" class="wp_cat_thumb">';
            })
            ->editColumn('name', function ($data) {
                if(visibility_F != "")
                    $visibility_info = '';
                else
                    switch ($data->visibility)
                    {
                        case -1:
                            $visibility_info = ' — Draft';
                            $span_delete = '<span class="text-success"><a class="text-success" href="javascript:void(0)" id="publish" data-id="' . $data->id .'">Public</a></span>';
                            break;
                        case 0:
                            $visibility_info = ' — Pending';
                            $span_delete = '<span class="delete"><a class="delete" href="javascript:void(0)" id="trash" data-id="' . $data->id .'">Trash</a></span>';
                            break;
                        default:
                            $visibility_info = '';
                            $span_delete = '<span class="delete"><a class="delete" href="javascript:void(0)" id="trash" data-id="' . $data->id .'">Trash</a></span>';
                    }

                return '
                    <strong>
                        <a href="' . route('dashboard.product.edit', $data->id) . '">' . $data->name . ' <span class="span-visibility">' . $visibility_info . '</span></a>
                    </strong>
                    <div class="row-actions">
                        <span style="color:#999;">ID: ' . $data->id . '</span> | 
                        <span class="edit">
                            <a href="' . route('dashboard.product.edit', $data->id) . '">Edit</a>
                        </span> |
                        ' . $span_delete . '
                    </div>
                    ';
            })
            ->editColumn('sku', function ($data) {
                if(empty($data->sku))
                    return '<center style="margin-top:30px;color: #3399ff;">—</center>';
                else
                    return '<center style="margin-top:30px;color: #3399ff;">' . $data->sku . '</center>';
            })
            ->addColumn('price', function ($data) {
                if($data->min_price === $data->max_price)
                    return '<center style="margin-top:30px;color: #3399ff;"><span>$' . number_format($data->min_price, 2) . '</span></center>';
                else
                    return '<center style="margin-top:30px;color: #3399ff;"><span>$' . number_format($data->min_price, 2) . '</span> – <span>$' . number_format($data->max_price, 2) . '</span></center>';
//                    return '
//                    <center style="margin-top:20px;color: #3399ff;"><span style="text-decoration: line-through dotted Black; color: #999999;">$' . number_format($product->price, 2) . '</span>
//                        <br><span title="Sale time: ' . $product->sale_start . ' - ' . $product->sale_end . '">$' . number_format($product->sale, 2) . '</span>
//                        </center>
//                    ';
            })
            ->addColumn('categories', function ($data) {
                $list = '';
                foreach ($data->categories as $item)
                    $list .= '<a href="javascript:void(0);"><span class="badge badge-info" style="margin:0 1px;">' . $item->name. '</span></a>';
                return $list;
            })
            ->addColumn('tags', function ($data) {
                $list = '';
                foreach ($data->tags as $item)
                    $list .= '<a href="javascript:void(0);"><span class="badge badge-info" style="margin:0 1px;">' . $item->name. '</span></a>';
                return $list;
            })
            ->editColumn('featured', function ($data) {
                if ($data->featured == 1) {
                    return '<center><button class="btn btn-sm" id="featured" data-id="' . $data->id . '" style="margin-top:30px; background-color: Transparent; background-repeat:no-repeat; border: none; cursor:pointer; overflow: hidden;">
                            <span class="text-sm text-warning" title="Featured: Yes"><i class="fas fa-star"></i></span></button></center>
                    ';
                } else {
                    return '<center><button class="btn btn-sm" id="featured" data-id="' . $data->id . '" style="margin-top:30px; background-color: Transparent; background-repeat:no-repeat; border: none; cursor:pointer; overflow: hidden;">
                        <span class="text-sm text-muted" title="Featured: No"><i class="fal fa-star"></i></span></button></center>
                    ';
                }
            })
            ->addColumn('date', function ($data) {
                if($data->created_at === $data->updated_at)
                {
                    $date_info = "Published";
                    $date_time = $data->created_at;
                }
                else
                {
                    $date_info = "Last Modified";
                    $date_time = $data->updated_at;
                }
                return '
                        <p class="tbl_date_info">' . $date_info . '</p>
                        <p class="tbl_date_time">' . $date_time . '</p>
                    ';
            })
            ->rawColumns(['thumbnail', 'name', 'sku', 'price', 'categories', 'tags', 'featured', 'date'])
            ->make(true);
    }

    public static function getProductByID($id)
    {
        $data = Product::where('id', 'like', $id)->first();
        return $data;
    }
}
