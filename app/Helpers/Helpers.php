<?php

namespace App\Helpers;

use App\Option;
use App\Category;
use App\Post;
use App\Product;
use Illuminate\Support\Facades\Storage;

class Helpers
{
    public static function generateRandomString($length = 5) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public static function siteURL()
    {
        $protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
        $domainName = $_SERVER['HTTP_HOST'];
        return $protocol . $domainName;
    }

    public static function getOption($key)
    {
        return Option::getOption($key);
    }

    public static function get_count_post_via_status($status)
    {
        if($status == 100)
            return Post::count();
        else
            return Post::where('status', $status)->count();
    }

    public static function get_count_product_via_visibility($status)
    {
        if($status == 100)
            return Product::count();
        else
            return Product::where('visibility', $status)->count();
    }

    public static function slugify($text)
    {
        //TRANSLIT
        $text = preg_replace("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/", "a", $text);
        $text = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/", "e", $text);
        $text = preg_replace("/(ì|í|ị|ỉ|ĩ)/", "i", $text);
        $text = preg_replace("/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/", "o", $text);
        $text = preg_replace("/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/", "u", $text);
        $text = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ)/", "y", $text);
        $text = preg_replace("/(đ)/", "d", $text);
        $text = preg_replace("/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/", "A", $text);
        $text = preg_replace("/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/", "E", $text);
        $text = preg_replace("/(Ì|Í|Ị|Ỉ|Ĩ)/", "I", $text);
        $text = preg_replace("/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/", "O", $text);
        $text = preg_replace("/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/", "U", $text);
        $text = preg_replace("/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/", "Y", $text);
        $text = preg_replace("/(Đ)/", "D", $text);
        // replace non letter or digits by -
        $text = preg_replace("~[^\\pL\d]+~u", " -", $text);
        // trim
        $text = trim($text, "-");
        // lowercase
        $text = strtolower($text);
        // remove unwanted characters
        $text = preg_replace("~[^-\w]+~", "", $text);
        return $text;
    }

    public static function bbcode2html($text) {
        //Tìm BBcode
        $find = array(
            '~\\n~s',
            '~\[hr\](.*?)\[/hr\]~s',
            '~\[b\](.*?)\[/b\]~s',
            '~\[i\](.*?)\[/i\]~s',
            '~\[u\](.*?)\[/u\]~s',
            '~\[quote\](.*?)\[/quote\]~s',
            '~\[quote=(.*?)\](.*?)\[/quote\]~s',
            '~\[address\](.*?)\[/address\]~s',
            '~\[code\](.*?)\[/code\]~s',
            '~\[size=(.*?)\](.*?)\[/size\]~s',
            '~\[color=(.*?)\](.*?)\[/color\]~s',
            '~\[url\]((?:ftp|http|https?)://.*?)\[/url\]~s',
            '~\[url=((?:ftp|http|https?)://.*?)\](.*?)\[/url\]~s',
            '~\[email\](.*?)\[/email\]~s',
            '~\[tel=(.*?)\](.*?)\[/tel\]~s',
            '~\[img=(.*?\.(?:jpg|jpeg|gif|png|bmp))\](.*?)\[/img\]~s',
            '~\[img\](.*?\.(?:jpg|jpeg|gif|png|bmp))\[/img\]~s',
            '~\[youtube=(.*?)\](.*?)\[/youtube\]~s',
        );

        // HTML tags thay thế BBcode
        $replace = array(
            '<br/>',
            '<hr style="border-color:$1;"/>',
            '<strong>$1</strong>',
            '<em>$1</em>',
            '<u>$1</u>',
            '<q>$1</'.'q>',
            '<blockquote cite="$1">$2</blockquote>',
            '<address>$1</'.'address>',
            '<code>$1</'.'code>',
            '<span style="font-size:$1px;">$2</span>',
            '<span style="color:$1;">$2</span>',
            '<a href="$1">$1</a>',
            '<a href="$1">$2</a>',
            '<a href="mailto:$1">$1</a>',
            '<a href="tel:$1">$2</a>',
            '<img src="$1" alt="$2" />',
            '<img src="$1" alt="$1" />',
            '<iframe width="420" height="345" src="https://www.youtube.com/embed/$1$2"></iframe>'
        );

        // Thay thế và trả về
        return preg_replace($find,$replace,$text);
    }

    public static function fileInfo($filePath)
    {
        $file = array();
        $file['name'] = $filePath;
        $file['extension'] = pathinfo(storage_path() . $filePath, PATHINFO_EXTENSION);

        $file['size'] = self::formatBytes(Storage::size($filePath));
        $file['path'] = Storage::url($filePath);
        return $file;
    }

    public static function formatBytes($bytes, $precision = 2)
    {
        $units = array('B', 'KB', 'MB', 'GB', 'TB');

        $bytes = max($bytes, 0);
        $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
        $pow = min($pow, count($units) - 1);

        $bytes /= pow(1024, $pow);

        return round($bytes, $precision) . ' ' . $units[$pow];
    }

    public static function filesize_formatted($file)
    {
        $bytes = $file->getClientSize();
        if ($bytes >= 1073741824) {
            return number_format($bytes / 1073741824, 2) . ' GB';
        } elseif ($bytes >= 1048576) {
            return number_format($bytes / 1048576, 2) . ' MB';
        } elseif ($bytes >= 1024) {
            return number_format($bytes / 1024, 2) . ' KB';
        } elseif ($bytes > 1) {
            return $bytes . ' bytes';
        } elseif ($bytes == 1) {
            return '1 byte';
        } else {
            return '0 bytes';
        }
    }

    //$categories, $current, $category->parent, $lv, 'edit'
    public static function select_categories_hierarchy($categories, $current = 0, $parent = 0, $lv = 0, $method = ''){
        // Biến lưu danh mục lặp ở bước đệ quy này
        $cate_tmp = array();
        $options = '';
        //Lọc danh sách các danh mục theo parent_id truyền vào
        foreach ($categories as $key => $item) {
            // Nếu có parent_id bằng với parent_id hiện tại
            if ($item['parent'] == $parent) {
                // Thêm vào biến lưu trữ menu ở bước lặp
                $cate_tmp[] = $item;
                // Sau khi thêm vào biến lưu trữ danh mục ở bước lặp
                // thì unset nó ra khỏi danh sách danh mục ở các bước tiếp theo
                unset($categories[$key]);
            }
        }

        if ($cate_tmp) {
            foreach ($cate_tmp as $item) {
                if($item['id'] !=''){
                    if($method == 'edit')
                    {
                        if($item['id'] == $current)
                            $options .= '<option class="level-' . $lv . '" value="' . $item['id'] .'" selected>' .  str_repeat("&nbsp;&nbsp;&nbsp;&nbsp;", $lv) . $item['name'] . '</option>';
                        else
                            $options .= '<option class="level-' . $lv . '" value="' . $item['id'] .'">' .  str_repeat("&nbsp;&nbsp;&nbsp;&nbsp;", $lv) . $item['name'] . '</option>';
                    }
                    else
                    {
                        $options .= '<option class="level-' . $lv . '" value="' . $item['id'] .'">' .  str_repeat("&nbsp;&nbsp;&nbsp;&nbsp;", $lv) . $item['name'] . '</option>';
                    }
                    // Truyền vào danh sách danh mục chưa lặp và parent_id của danh mục hiện tại
                    $options .= self::select_categories_hierarchy($categories, $current, $item['id'], $lv+1, $method);
                }
            }
        }
        //Trả về ul li menu
        return $options;
    }

    //$categories, $current, $category->parent, 'edit'
    public static function checkbox_categories_hierarchy($categories, $current, $parent = 0, $method = ''){
        // Biến lưu danh mục lặp ở bước đệ quy này
        $cate_tmp = array();
        $options = '';
        //Lọc danh sách các danh mục theo parent_id truyền vào
        foreach ($categories as $key => $item) {
            // Nếu có parent_id bằng với parent_id hiện tại
            if ($item['parent'] == $parent) {
                // Thêm vào biến lưu trữ menu ở bước lặp
                $cate_tmp[] = $item;
                // Sau khi thêm vào biến lưu trữ danh mục ở bước lặp
                // thì unset nó ra khỏi danh sách danh mục ở các bước tiếp theo
                unset($categories[$key]);
            }
        }

        if ($cate_tmp) {
            $options .= '<ul class="list_categories_choice clear">';
            foreach ($cate_tmp as $item) {
                if($item['id'] !=''){
                    if($method == 'edit')
                    {
                        if(in_array($item['id'], $current))
                        {
                            $options .= '<li><input type="checkbox" name="categories[]" checked id="checkbox_'.$item['id'] .'" value="'.$item['id'].'"/><label for="checkbox_'.$item['id'] .'">' . $item['name'] . '</label>';
                            $options .= self::checkbox_categories_hierarchy($categories, $current, $item['id'], $method);
                            $options .= '</li>';
                        }
                        else
                        {
                            $options .= '<li><input type="checkbox" name="categories[]" id="checkbox_'.$item['id'] .'" value="'.$item['id'].'"/><label for="checkbox_'.$item['id'] .'">' . $item['name'] . '</label>';
                            $options .= self::checkbox_categories_hierarchy($categories, $current, $item['id'], $method);
                            $options .= '</li>';
                        }
                    }
                    else
                    {
                        $options .= '<li><input type="checkbox" name="categories[]" id="checkbox_'.$item['id'] .'" value="'.$item['id'].'"/><label for="checkbox_'.$item['id'] .'">' . $item['name'] . '</label>';
                        $options .= self::checkbox_categories_hierarchy($categories, $current, $item['id'], $method);
                        $options .= '</li>';
                    }
                }
            }
            $options .= '</ul>';
        }
        //Trả về ul li menu
        return $options;
    }

    public static function get_category_name_by_id($id){
        $data = Category::where('id', $id)->pluck('name')->first();
        return $data;
    }

    public static function truncate($string, $length, $dots = "") {
        $charset = 'UTF-8';
        return (mb_strlen($string, $charset) > $length) ? mb_substr($string, 0, $length - strlen($dots), $charset) . $dots : $string;
    }
}
