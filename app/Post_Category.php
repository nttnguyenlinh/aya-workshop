<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post_Category extends Model
{
    protected $table = 'post_category';

    protected $fillable = [
        'post_id', 'category_id'
    ];
}
