<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;


class ContactEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $request;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //$data = $this->$data;

        return $this ->view('email.emailContact')
                    ->from(config('mail.from.address'), config('mail.from.name'))
                    ->subject('AYA WORKSHOP - Contact')
                    ->with([
                        'your_name' => $this->request['your_name'],
                        'your_email' => $this->request['your_email'],
                        'your_message' => $this->request['your_message'],
                        ]);
    }
}