<?php

namespace App;

use App\Product;
use App\Post_Tag;
use Illuminate\Database\Eloquent\Model;
use DataTables;

class Tag extends Model
{
    protected $table = 'tags';

    protected $fillable = [
        'name', 'slug', 'type', 'count'
    ];

    public function posts()
    {
        return $this->belongsToMany(Post::class, 'post_tag');
    }

    public function products()
    {
        return $this->belongsToMany(Product::class, 'product_category');
    }

    public static function post_tag_list()
    {
        $data = Tag::where('type', 'post')->select('id', 'name', 'slug', 'count')->get();
        return Datatables::of($data)->make(true);
    }

    public static function product_tag_list()
    {
        $data = Tag::where('type', 'product')->select('id', 'name', 'slug', 'count')->get();
        return Datatables::of($data)->make(true);
    }
}
