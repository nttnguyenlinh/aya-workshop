<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product_Variant extends Model
{
    protected $table = 'product_variant';

    protected $fillable = [
        'product_id', 'variant_code', 'sku', 'min_price', 'max_price', 'onsale', 'sale_start', 'sale_end',
        'in_stock', 'stock_quantity', 'total_sales', 'thumbnail', 'visibility'
    ];
}
