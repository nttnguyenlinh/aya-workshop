<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Tag;
use App\Post;

class Post_Tag extends Model
{
    protected $table = 'post_tag';

    protected $fillable = [
        'post_id', 'tag_id'
    ];
}
