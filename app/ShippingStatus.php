<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShippingStatus extends Model
{
    protected $table = 'shipping_status';

    protected $fillable = [
        'title'
    ];

    public function bills()
    {
        return $this->hasMany('App\Bill');
    }

    public static function anyStatus()
    {
        return ShippingStatus::all();
    }
}
