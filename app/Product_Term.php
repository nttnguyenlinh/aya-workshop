<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product_Term extends Model
{
    protected $table = 'product_term';

    protected $fillable = [
        'product_id', 'term_id'
    ];
}
