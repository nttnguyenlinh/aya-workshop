<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $table = 'images';

    protected $fillable = ['product_id', 'title', 'path', 'size', 'bytes_size'];

    public function products()
    {
        return $this->belongsTo('App\Product');
    }

    public static function getProductImages($product_id)
    {
        $images = Image::where('product_id', 'like', $product_id)->get();
        return $images;
    }

    public static function getByTitle($title)
    {
        $image = Image::where('title', 'like', $title)->get()->first();
        return $image;
    }

    public static function getByPath($path)
    {
        $image = Image::where('path', 'like', $path)->get()->first();
        return $image;
    }
}