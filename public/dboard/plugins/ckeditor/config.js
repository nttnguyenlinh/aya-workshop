CKEDITOR.editorConfig = function(config) {
    config.defaultLanguage = "en";
    config.language = "en";
    //config.uiColor = "#C8F6EE";
    config.skin = "office2013";
    config.width = "100%";
    config.height = 450;
    config.resize_dir = "vertical";
    config.toolbarCanCollapse = true;
    config.removePlugins = "easyimage, cloudservices, image";
    config.toolbarLocation = 'top';
    config.toolbar = [
        {
            name: "styles",
            items: [
                "Format",
                "TextColor",
                "BGColor"
            ]
        },
        {
            name: "basicstyles",
            items: [
                "Bold",
                "Italic",
                "Underline",
                "-",
                "CopyFormatting",
                "RemoveFormat"
            ]
        },
        {
            name: "paragraph",
            items: [
                "-",
                "NumberedList",
                "BulletedList",
                "-",
                "Outdent",
                "Indent",
                "-",
                "Blockquote",
                "Table"
            ]
        },
        {
            name: "paragraph",
            items: [
                "JustifyLeft",
                "JustifyCenter",
                "JustifyRight",
                "JustifyBlock",
            ]
        },
        {
            name: "insert",
            items: [
                "Image",
                "HorizontalRule",
                "PageBreak",
                "Youtube"
            ]
        },
        {
            name: "document",
            items: ["Source", "Maximize", "Preview"]
        }
    ];

    config.filebrowserBrowseUrl = "/dashboard/media/file-explorer.php";

    config.filebrowserImageBrowseUrl =
        "/dashboard/media/file-explorer.php?type=Images";

    config.filebrowserFlashBrowseUrl =
        "/dashboard/media/file-explorer.php?type=Flash";

    config.filebrowserUploadUrl =
        "/dboard/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files";

    config.filebrowserImageUploadUrl =
        "/dboard/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images";

    config.filebrowserFlashUploadUrl =
        "/dboard/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash";

    config.extraPlugins =
        "youtube, image2, tableresize, wordcount, pastebase64";
    config.youtube_width = "640";
    config.youtube_height = "480";
    config.youtube_responsive = true;
    config.youtube_related = false;
    config.youtube_older = false;
    config.youtube_privacy = true;
    config.youtube_autoplay = false;
    config.youtube_controls = true;
};
