<?php

return [

    'password' => 'Mật khẩu phải có ít nhất 8 ký tự và khớp với xác nhận.',
    'reset' => 'Mật khẩu của bạn đã đưược thiết lập lại!',
    'sent' => 'Chúng tôi đã gửi email tới bạn liên kết đặt lại mật khẩu!',
    'token' => 'Mã đặt lại mật khẩu này không hợp lệ.',
    'user' => "Chúng tôi không thể tìm hồ sơ người dùng với địa chỉ email.",

];
