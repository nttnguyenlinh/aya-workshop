<?php

return [
    'accepted' => 'Trường :attribute phải được chấp nhận.',
    'active_url' => 'Trường :attribute không phải là một URL hợp lệ.',
    'after' => 'Trường :attribute phải là một ngày sau :date.',
    'after_or_equal' => 'Trường :attribute phải là một ngày sau hoặc bằng :date.',
    'alpha' => 'Trường :attribute chỉ có thể chứa các chữ cái.',
    'alpha_dash' => 'Trường :attribute chỉ có thể chứa chữ cái, số, dấu gạch ngang và dấu gạch dưới.',
    'alpha_num' => 'Trường :attribute chỉ có thể chứa chữ cái và số.',
    'array' => 'Trường :attribute phải là một mảng.',
    'before' => 'Trường :attribute phải là một ngày trước :date.',
    'before_or_equal' => 'Trường :attribute phải là một ngày trước hoặc bằng :date.',
    'between' => [
        'numeric' => 'Trường :attribute phải nằm trong khoảng :min và: max.',
        'file' => 'Trường :attribute phải nằm trong khoảng :min và :max kilobytes.',
        'string' => 'Trường :attribute phải nằm trong khoảng :min và :max kí tự.',
        'array' => 'Trường :attribute phải nằm trong khoảng :min và :max items.',
    ],
    'boolean' => 'Trường :attribute lĩnh vực phải đúng hoặc sai.',
    'confirmed' => 'Trường :attribute xác nhận không khớp.',
    'date' => 'Trường :attribute Không phải là ngày hợp lệ.',
    'date_equals' => 'Trường :attribute phải là một ngày bằng :date.',
    'date_format' => 'Trường :attribute không phù hợp với định dạng :format.',
    'different' => 'Trường :attribute và :other phải khác nhau.',
    'digits' => 'Trường :attribute phải là :digits chữ số.',
    'digits_between' => 'Trường :attribute phải nằm trong khoảng :min và :max chữ số.',
    'dimensions' => 'Trường :attribute có kích thước hình ảnh không hợp lệ.',
    'distinct' => 'Trường :attribute có một giá trị trùng lặp.',
    'email' => 'Trường :attribute Phải la một địa chỉ email hợp lệ.',
    'exists' => 'The :attribute đã chọn không hợp lệ.',
    'file' => 'Trường :attribute phải là một tập tin.',
    'filled' => 'Trường :attribute phải là một giá trị.',
    'gt' => [
        'numeric' => 'Trường :attribute phải lớn hơn :value.',
        'file' => 'Trường :attribute phải lớn hơn :value kilobytes.',
        'string' => 'Trường :attribute phải lớn hơn :value kí tự.',
        'array' => 'Trường :attribute phải lớn hơn :value items.',
    ],
    'gte' => [
        'numeric' => 'Trường :attribute phải lớn hơn hoặc bằng :value.',
        'file' => 'Trường :attribute phải lớn hơn hoặc bằng :value kilobytes.',
        'string' => 'Trường :attribute phải lớn hơn hoặc bằng :value kí tự.',
        'array' => 'Trường :attribute phải có :value items hoặc hơn.',
    ],
    'image' => 'Trường :attribute phải là hình ảnh.',
    'in' => 'The selected :attribute không hợp lệ.',
    'in_array' => 'Trường :attribute không tồn tại trong :other.',
    'integer' => 'Trường :attribute phải là một số nguyên.',
    'ip' => 'Trường :attribute phải là một địa chỉ IP hợp lệ.',
    'ipv4' => 'Trường :attribute phải là một địa chỉ IPV4 hợp lệ.',
    'ipv6' => 'Trường :attribute phải là một địa chỉ IPV6 hợp lệ.',
    'json' => 'Trường :attribute phải là một chuỗi JSON.',
    'lt' => [
        'numeric' => 'Trường :attribute phải nhỏ hơn :value.',
        'file' => 'Trường :attribute phải nhỏ hơn :value kilobytes.',
        'string' => 'Trường :attribute phải nhỏ hơn :value kí tự.',
        'array' => 'Trường :attribute phải nhỏ hơn :value items.',
    ],
    'lte' => [
        'numeric' => 'Trường :attribute phải nhỏ hơn hoặc bằng :value.',
        'file' => 'Trường :attribute phải nhỏ hơn hoặc bằng :value kilobytes.',
        'string' => 'Trường :attribute phải nhỏ hơn hoặc bằng :value kí tự.',
        'array' => 'Trường :attribute phải nhỏ hơn hoặc bằng :value items.',
    ],
    'max' => [
        'numeric' => 'Trường :attribute có thể không lớn hơn :max.',
        'file' => 'Trường :attribute có thể không lớn hơn :max kilobytes.',
        'string' => 'Trường :attribute có thể không lớn hơn :max kí tự.',
        'array' => 'Trường :attribute có thể không lớn hơn :max items.',
    ],
    'mimes' => 'Trường :attribute phải là một tập tin loại: :values.',
    'mimetypes' => 'Trường :attribute phải là một tập tin loại: :values.',
    'min' => [
        'numeric' => 'Trường :attribute ít nhất phải :min.',
        'file' => 'Trường :attribute ít nhất phải :min kilobytes.',
        'string' => 'Trường :attribute ít nhất phải :min kí tự.',
        'array' => 'Trường :attribute ít nhất phải :min items.',
    ],
    'not_in' => 'Trường :attribute được chọn không hợp lệ.',
    'not_regex' => 'Trường :attribute định dạng không hợp lệ.',
    'numeric' => 'Trường :attribute phải là một số.',
    'present' => 'Trường :attribute phải có sẵn.',
    'regex' => 'Trường :attribute định dạng không hợp lệ.',
    'required' => 'Trường :attribute là bắt buộc.',
    'required_if' => 'Trường :attribute bắt buộc khi :other là :value.',
    'required_unless' => 'Trường :attribute bắt buộc trừ khi :other có trong :values.',
    'required_with' => 'Trường :attribute bắt buộc khi :values có sẵn.',
    'required_with_all' => 'Trường :attribute bắt buộc khi :values có sẵn.',
    'required_without' => 'Trường :attribute bắt buộc khi :values không có sẵn.',
    'required_without_all' => 'Trường :attribute bắt buộc khi không có :values có sẵn.',
    'same' => 'Trường :attribute và :other phải khớp nhau.',
    'size' => [
        'numeric' => 'Trường :attribute phải chứa :size.',
        'file' => 'Trường :attribute phải chứa :size kilobytes.',
        'string' => 'Trường :attribute phải chứa :size kí tự.',
        'array' => 'Trường :attribute phải chứa :size items.',
    ],
    'starts_with' => 'Trường :attribute phải bắt đầu với một trong những điều sau đây: :values',
    'string' => 'Trường :attribute phải là một chuỗi.',
    'timezone' => 'Trường :attribute phải là một khu vực hợp lệ.',
    'unique' => 'Trường :attribute đã được thực hiện.',
    'uploaded' => 'Trường :attribute không tải lên được.',
    'url' => 'Trường :attribute định dạng không hợp lệ.',
    'uuid' => 'Trường :attribute phải là một UUID hợp lệ.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [],

];
