<?php

return [
    'failed' => 'Thông tin không chính xác.',
    'throttle' => 'Quá nhiều lần thử đăng nhập. Vui lòng thử lại sau :seconds giây.',

];
