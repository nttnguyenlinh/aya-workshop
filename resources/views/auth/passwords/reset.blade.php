@extends('layouts.home')

@section('title', 'Reset Password - AYA WORKSHOP')

@section('content')
<main id="main" class="">
    <div class="my-account-header page-title normal-title">
        <div class="page-title-inner flex-row  container">
            <div class="flex-col flex-grow medium-text-center">
                <div class="text-center social-login">
                    <h1 class="uppercase mb-0">{{ __('Reset Password') }}</h1>
                </div>
            </div><!-- .flex-left -->
        </div><!-- flex-row -->
    </div><!-- .page-title -->
    <div class="page-wrapper my-account mb">
        <div class="container" role="main">
            <div class="woocommerce">
                <div class="woocommerce-notices-wrapper"></div>
                <form method="post" class="woocommerce-ResetPassword lost_reset_password" acction="{{route('password.reset') }}">
                    @csrf
                    <input type="hidden" name="token" value="{{ $token }}">
                    <p class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first">
                        <label for="email">{{ __('E-Mail Address') }}</label>
                        @if ($errors->has('email'))
                            <input style="border: 1px solid #f00;" id="email" type="email" class="form-control" name="email" value="{{ $email ?? old('email') }}" required autofocus>
                            <div class="message-container container alert-color medium-text-center"><strong>{{ $errors->first('email') }}</strong></div>
                        @else
                            <input id="email" type="email" class="form-control" name="email" value="{{ $email ?? old('email') }}" required autofocus>
                        @endif
                    </p>
                    <div class="clear"></div>

                    <p class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first">
                        <label for="password">{{ __('Password') }}</label>
                        @if ($errors->has('password'))
                            <input style="border: 1px solid #f00;" id="password" type="password" class="form-control" name="password" required>
                            <div class="message-container container alert-color medium-text-center"><strong>{{ $errors->first('password') }}</strong></div>
                        @else
                            <input id="password" type="password" class="form-control" name="password" required>
                        @endif
                    </p>
                    <div class="clear"></div>

                    <p class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first">
                        <label for="password-confirm">{{ __('Confirm Password') }}</label>
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                    </p>
                    <div class="clear"></div>

                    <p>
                            <div class="g-recaptcha" data-sitekey="{{config('app.recaptcha_site_key')}}"></div>
                        <br />
                    </p>

                    <p class="woocommerce-form-row form-row">
                        <button type="submit" class="woocommerce-Button button">{{ __('Reset Password') }}</button>
                    </p>
                </form>
            </div>
        </div><!-- .container -->
    </div><!-- .page-wrapper.my-account  -->
</main><!-- #main -->
@endsection