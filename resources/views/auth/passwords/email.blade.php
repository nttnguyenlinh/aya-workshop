@extends('layouts.home')

@section('title', 'Forgot Password - AYA WORKSHOP')

@section('content')
<main id="main" class="">
    <div class="my-account-header page-title normal-title">
        <div class="page-title-inner flex-row  container">
            <div class="flex-col flex-grow medium-text-center">
                <div class="text-center social-login">
                    <h1 class="uppercase mb-0">{{ __('Forgot Password') }}</h1>
                </div>
            </div><!-- .flex-left -->
        </div><!-- flex-row -->
    </div><!-- .page-title -->
    <div class="page-wrapper my-account mb">
        <div class="container" role="main">
            <div class="woocommerce">
                <div class="woocommerce-notices-wrapper"></div>
                @if (session('status'))
                <div class="message-container container medium-text-center" style="color:#00b220;"><strong>{{ session('status') }}</strong></div>
                @endif
                <form method="post" class="woocommerce-ResetPassword lost_reset_password" acction="{{route('password.email') }}">
                    @csrf
                    <p>{{ __('Lost your password? Please enter your username or email address. You will receive a link to create a new password via email.')}}</p>
                    <p class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first">
                        <label for="email">{{ __('E-Mail Address') }}</label>
                        @if ($errors->has('email'))
                        <input style="border: 1px solid #f00;" id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                        <div class="message-container container alert-color medium-text-center"><strong>{{ $errors->first('email') }}</strong></div>
                        @else
                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
                        @endif
                    </p>
                    <div class="clear"></div>

                    <p>
                            <div class="g-recaptcha" data-sitekey="{{config('app.recaptcha_site_key')}}"></div>
                        <br />
                    </p>

                    <p class="woocommerce-form-row form-row">
                        <button type="submit" class="woocommerce-Button button">{{ __('Send Password Reset Link') }}</button>
                    </p>
                </form>
            </div>
        </div><!-- .container -->
    </div><!-- .page-wrapper.my-account  -->
</main><!-- #main -->
@endsection