<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Dashboard - {{config('app.name')}}</title>

    <link rel="icon" href="{{asset('dboard/images/icons/ICO-32x32.png')}}" sizes="32x32" />
    <link rel="icon" href="{{asset('dboard/images/icons/ICO-192x192.png')}}" sizes="192x192" />
    <link rel="apple-touch-icon-precomposed" href="{{asset('dboard/images/icons/ICO-180x180.png')}}" />
    <meta name="msapplication-TileImage" content="{{asset('dboard/images/icons/ICO-270x270.png')}}" />

    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="{{asset('dboard/plugins/font-awesome/css/all.min.css')}}">

    <!-- DataTables bootstrap 4-->
    <link rel="stylesheet" href="{{asset('dboard/plugins/datatables/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('dboard/plugins/datatables/css/select.dataTables.min.css')}}">
    <link type="text/css" href="{{asset('dboard/plugins/datatables/css/dataTables.checkboxes.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('dboard/css/adminlte.min.css')}}">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

    <!-- sweetalert2 js -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@8.8.5/dist/sweetalert2.min.css">

    <!-- Toastr css -->
    <link rel="stylesheet" href="{{asset('dboard/plugins/toastr/toastr.min.css')}}">

    <!-- Date time range picker JS -->
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

    <!-- jQuery -->
    <!-- <script src="{{asset('dboard/js/jquery.min.js')}}"></script> -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>

    <!-- Google API -->
    <script src="https://www.google.com/recaptcha/api.js?hl={{Session::get('locale')}}"></script>

    <!-- dropify js upload -->
    <link rel="stylesheet" href="{{asset('dboard/plugins/dropify/dist/css/dropify.css')}}">

    <!-- Ckeditor js -->
{{--    <script src="{{asset('dboard/plugins/ckeditor/ckeditor.js')}}"></script>--}}

    <!-- Ckfinder js -->
    <script src="{{asset('dboard/plugins/ckfinder/ckfinder.js')}}"></script>

    <!-- Dropzone js -->
    <link href='https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/dropzone.css' type='text/css' rel='stylesheet'>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/dropzone.js' type='text/javascript'></script>

    <!-- Custom Theme style -->
    <link rel="stylesheet" href="{{asset('dboard/css/custom_error.css')}}">
    <link rel="stylesheet" href="{{asset('dboard/css/style_custom_btl.css')}}">

    <link rel="stylesheet" href="{{asset('dboard/plugins/woocommerce/custom_style.css')}}">

    <!-- Flag Icons -->
    <link rel="stylesheet" href="{{asset('dboard/plugins/flag/css/flag-icon.css')}}">
</head>

<body class="hold-transition sidebar-mini sidebar-collapse">
    <div class="wrapper">
        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
                </li>
                <li class="nav-item d-none d-sm-inline-block">
                    <a href="{{route('dashboard')}}" class="nav-link">Dashboard</a>
                </li>
                <li class="nav-item d-none d-sm-inline-block">
                    <a href="{{route('home')}}" class="nav-link">Visit site</a>
                </li>
                <li class="nav-item d-none d-sm-inline-block">
                    <a href="{{route('dashboard.logout')}}" class="nav-link">Logout</a>
                </li>
            </ul>

            <!-- Right navbar links -->
            <ul class="navbar-nav ml-auto">
                <span class="badge badge-info"
                    style="text-transform: uppercase; font-size:medium; margin: 5px;">{{Auth::user()->nicename}}</span>
            </ul>
        </nav>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-dark-primary elevation-4">
            <!-- Brand Logo -->
            <a href="{{route('dashboard')}}" class="brand-link">
                <img src="{{asset('dboard/images/icons/Asset3.png')}}" alt="{{config('app.name', 'AYA WORKSHOP')}}"
                    style="width:100%; height:100%; opacity: .8">
            </a>

            <!-- Sidebar -->
            <div class="sidebar">
                <!-- Sidebar user panel (optional) -->
                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                        data-accordion="false">
                        <!-- Add icons to the links using the .nav-icon class with font-awesome or any other icon font library -->
                        <li class="nav-item has-treeview">
                            <a href="{{route('dashboard')}}"
                                class="nav-link {{Request::is('dashboard') ? 'active' : ''}}">
                                <i class="nav-icon fal fa-tachometer"></i>
                                <p>Dashboard</p>
                            </a>
                        </li>
                        <li class="nav-item has-treeview">
                            <a href="{{route('dashboard.users')}}"
                                class="nav-link {{Request::is('*users') ? 'active' : ''}}">
                                <i class="nav-icon fal fa-user"></i>
                                <p>Users</p>
                            </a>
                        </li>

                        <li class="nav-item has-treeview">
                            <a href="{{route('dashboard.posts')}}"
                                class="nav-link {{Request::is('*posts') ? 'active' : ''}} {{Request::is('*post*') ? 'active' : ''}}">
                                <i class="nav-icon fal fa-thumbtack"></i>
                                <p>Posts <i class="fas fa-angle-left right"></i></p>
                            </a>

                            <ul class="nav nav-treeview" style="display: none;">
                                <li class="nav-item">
                                    <a href="{{route('dashboard.posts')}}"
                                       class="nav-link {{Request::is('*posts') ? 'active' : ''}}">
                                        <i class="nav-icon fal fa-thumbtack"></i>
                                        <p>Posts</p>
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a href="{{route('dashboard.post.category')}}"
                                       class="nav-link {{Request::is('*post/category') ? 'active' : ''}}">
                                        <i class="nav-icon fal fa-box"></i>
                                        <p>Categories</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{route('dashboard.post.tags')}}"
                                       class="nav-link {{Request::is('*post/tags') ? 'active' : ''}}">
                                        <i class="nav-icon fal fa-tags"></i>
                                        <p>Tags</p>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li class="nav-item has-treeview">
                            <a href="{{route('dashboard.products')}}"
                                class="nav-link {{Request::is('*products') ? 'active' : ''}} {{Request::is('*product*') ? 'active' : ''}} {{Request::is('*attributes') ? 'active' : ''}}">
                                <i class="nav-icon fal fa-box-usd"></i>
                                <p>Products <i class="fas fa-angle-left right"></i></p>
                            </a>

                            <ul class="nav nav-treeview" style="display: none;">
                                <li class="nav-item">
                                    <a href="{{route('dashboard.products')}}"
                                        class="nav-link {{Request::is('*products') ? 'active' : ''}}">
                                        <i class="nav-icon fal fa-box-usd"></i>
                                        <p>Products</p>
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a href="{{route('dashboard.product.category')}}"
                                       class="nav-link {{Request::is('*product/category') ? 'active' : ''}}">
                                        <i class="nav-icon fal fa-box"></i>
                                        <p>Categories</p>
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a href="{{route('dashboard.product.tags')}}"
                                       class="nav-link {{Request::is('*product/tags') ? 'active' : ''}}">
                                        <i class="nav-icon fal fa-tags"></i>
                                        <p>Tags</p>
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a href="{{route('dashboard.attributes')}}"
                                        class="nav-link {{Request::is('*attributes') ? 'active' : ''}}">
                                        <i class="nav-icon fal fa-boxes"></i>
                                        <p>Attributes</p>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li class="nav-item has-treeview">
                            <a href="{{route('dashboard.products')}}"
                               class="nav-link {{Request::is('*orders') ? 'active' : ''}} {{Request::is('*coupons') ? 'active' : ''}}">
                                <i class="nav-icon menu-icon-generic"></i>
                                <p> WooCommerce <i class="fas fa-angle-left right"></i></p>
                            </a>

                            <ul class="nav nav-treeview" style="display: none;">
                                <li class="nav-item has-treeview">
                                    <a href="{{route('dashboard.orders')}}"
                                        class="nav-link {{Request::is('*orders') ? 'active' : ''}}">
                                        <i class="nav-icon fal fa-shopping-cart"></i>
                                        <p>Orders</p>
                                    </a>
                                </li>

                                <li class="nav-item has-treeview">
                                    <a href="{{route('dashboard.coupons')}}"
                                        class="nav-link {{Request::is('*coupons') ? 'active' : ''}}">
                                        <i class="nav-icon fal fa-percent"></i>
                                        <p>Coupons</p>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li class="nav-item has-treeview">
                            <a href="{{route('dashboard.media')}}"
                                class="nav-link {{Request::is('*media') ? 'active' : ''}}">
                                <i class="nav-icon fal fa-camera-alt"></i>
                                <p>Media</p>
                            </a>
                        </li>

                        <li class="nav-item has-treeview">
                            <a href="#"
                                class="nav-link {{Request::is('*menus') ? 'active' : ''}} {{Request::is('*menu') ? 'active' : ''}} {{Request::is('*options') ? 'active' : ''}}">
                                <i class="nav-icon fal fa-cog"></i>
                                <p>Settings <i class="fas fa-angle-left right"></i></p>
                            </a>

                            <ul class="nav nav-treeview" style="display: none;">
                                <li class="nav-item">
                                    <a href="{{route('menu')}}"
                                        class="nav-link {{Request::is('*menus') ? 'active' : ''}} {{Request::is('*menu') ? 'active' : ''}}">
                                        <i class="nav-icon fal fa-bars"></i>
                                        <p>Menus</p>
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a href="{{route('dashboard.options')}}"
                                        class="nav-link {{Request::is('*options') ? 'active' : ''}}">
                                        <i class="nav-icon fal fa-tools"></i>
                                        <p>Options</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </nav>
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    @yield('page-header')
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->
            <!-- Main content -->
            <div class="content">
                <div class="container-fluid">
                    @if(Session::has('alert'))
                    <div class="alert alert-{{Session::get('alert')}} alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <i class="fal fa-check"></i> {{Session::get('message')}}
                    </div>
                    @endif

                    @if($errors->any())
                    <div class="alert alert-error alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <ol class='sweet2'>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ol>
                    </div>
                    @endif

                    <!-- .row -->
                    @yield('content')
                    <!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content -->

        </div>
        <!-- /.content-wrapper -->

        <!-- Main Footer -->
        <footer class="main-footer">
            <!-- To the right -->
            <div class="float-right d-none d-sm-inline">
                version 2.0.0
            </div>
            <!-- Default to the left -->
            <strong>Copyright &copy; {{date('Y')}} <a
                    href="{{route('home')}}">{{config('app.name', 'AYA WORKSHOP')}}</a>.</strong> All rights reserved.
        </footer>
    </div>
    <!-- ./wrapper -->
    <!-- Bootstrap 4 -->
    <script src="{{asset('dboard/js/bootstrap.bundle.min.js')}}"></script>
    <!-- DataTables bootstrap 4-->
    <script src="{{asset('dboard/plugins/datatables/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('dboard/plugins/datatables/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('dboard/plugins/datatables/js/dataTables.select.min.js')}}"></script>
    <script src="{{asset('dboard/plugins/datatables/js/dataTables.checkboxes.min.js')}}"></script>

    <!-- AdminLTE App -->
    <script src="{{asset('dboard/js/adminlte.min.js')}}"></script>

    <!-- sweetalert2 js -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8.8.5/dist/sweetalert2.all.min.js"></script>

    <!-- Toastr js -->
    <script src="{{asset('dboard/plugins/toastr/toastr.min.js')}}"></script>

    <!-- Jquery Validate js -->
    <script src="{{asset('js/jquery.validate.min.js')}}"></script>

    <!-- dropify js upload -->
    <script type="application/javascript" src="{{asset('dboard/plugins/dropify/dist/js/dropify.js')}}"></script>

    <!-- Input Mask JS -->
    <script src="{{asset('dboard/js/jquery.inputmask.bundle.js')}}"></script>

    <!-- Date time range picker JS -->
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

    <!-- Show notification using toastr-->
    @yield('script-footer')

    <script>
    @if(Session::has('alert'))
    toastr["{{Session::get('alert')}}"]("{{Session::get('message')}}");
    @endif

    $(".alert").fadeTo(2000, 500).slideUp(500, function() {
        $(".alert").slideUp(500);
    });

    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": true,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "2000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }
    </script>
</body>

</html>
