<html>

<body style="padding:0; margin:0; display:block; background:#ffffff; -webkit-text-size-adjust:none">
    <style type="text/css">
        /* ----- Client Fixes ----- */

        /* Force Outlook to provide a "view in browser" message */
        #outlook a {
            padding: 0;
        }

        /* Force Hotmail to display emails at full width */
        .ReadMsgBody {
            width: 100%;
        }

        .ExternalClass {
            width: 100%;
        }

        /* Force Hotmail to display normal line spacing */
        .ExternalClass,
        .ExternalClass p,
        .ExternalClass span,
        .ExternalClass font,
        .ExternalClass td,
        .ExternalClass div {
            line-height: 100%;
        }


        /* Prevent WebKit and Windows mobile changing default text sizes */
        body,
        table,
        td,
        p,
        a,
        li,
        blockquote {
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
        }

        /* Remove spacing between tables in Outlook 2007 and up */
        table,
        td {
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
        }

        /* Allow smoother rendering of resized image in Internet Explorer */
        img {
            -ms-interpolation-mode: bicubic;
        }

        /* ----- Reset ----- */

        html,
        body,
        .body-wrap,
        .body-wrap-cell {
            margin: 0;
            padding: 0;
            background: #ffffff;
            font-family: Arial, Helvetica, sans-serif;
            font-size: 14px;
            color: #BA55D3;
            text-align: left;
        }


        img {
            border: 0;
            line-height: 100%;
            outline: none;
            text-decoration: none;
        }

        table {
            border-collapse: collapse !important;
        }

        td,
        th {
            text-align: center;
            font-family: Arial, Helvetica, sans-serif;
            font-size: 14px;
            color: #BA55D3;
            line-height: 1.5em;
        }

        b a,
        .footer a {
            text-decoration: none;
            color: #464646;
        }

        a {
            color: #BA55D3;
            text-decoration: none;
        }

        /* ----- General ----- */

        td.center {
            text-align: center;
        }

        .left {
            text-align: left;
        }

        .center {
            text-align: center;
        }

        .body-padding {
            padding: 24px 40px 40px;
        }

        .border-bottom {
            border-bottom: 2px solid #BA55D3;
        }

        table.full-width-gmail-android {
            width: 100% !important;
        }


        /* ----- Header ----- */
        .header {
            font-weight: bold;
            font-size: 16px;
            line-height: 16px;
            height: 16px;
            padding-top: 19px;
            padding-bottom: 7px;
        }

        .header a {
            color: #464646;
            text-decoration: none;
        }

        /* ----- Footer ----- */

        .footer a {
            font-size: 12px;
        }
    </style>

    <style type="text/css" media="only screen and (max-width: 650px)">
        @media only screen and (max-width: 650px) {
            * {
                font-size: 16px !important;
            }

            table[class*="w320"] {
                width: 320px !important;
            }

            td[class="mobile-center"],
            div[class="mobile-center"] {
                text-align: center !important;
            }

            td[class*="body-padding"] {
                padding: 20px !important;
            }

            td[class="mobile"] {
                text-align: right;
                vertical-align: top;
            }
        }
    </style>
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td valign="top" align="left" width="100%" style="background:repeat-x url(https://www.filepicker.io/api/file/al80sTOMSEi5bKdmCgp2) #ffffff">
                <center>
                    <table class="w320 full-width-gmail-android" bgcolor="#ffffff" style="background-color:transparent" cellpadding="0" cellspacing="0" border="0" width="100%">
                        <tr>
                            <td width="100%" height="48" valign="top">
                                <table class="full-width-gmail-android" cellspacing="0" cellpadding="0" border="0" width="100%">
                                    <tr>
                                        <td class="header center" width="100%">
                                            <img src="https://ayaworkshop.com/wp-content/uploads/2019/03/Artboard-1-copy-2.png" style="margin-top: 20px;" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>

                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr>
                            <td align="center">
                                <center>
                                    <table class="w320" cellspacing="0" cellpadding="0" width="500">
                                        <tr>
                                            <td class="body-padding mobile-padding">

                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td style="padding-bottom:20px;">
                                                            Xin chào Admin, <br><br>
                                                            {{$your_message}}<br><br>
                                                            <h3 style="text-align:center;">Thông tin</h3>
                                                        </td>
                                                    </tr>
                                                </table>

                                                <table cellspacing="0" cellpadding="0" width="100%">
                                                    <tr>
                                                        <td>
                                                            <b>NGƯỜI GỬI</b>
                                                        </td>
                                                        <td>
                                                            <b>EMAIL</b>
                                                        </td>
                                                        <td>
                                                            <b>NGÀY GỬI</b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="border-bottom" height="10"></td>
                                                        <td class="border-bottom" height="10"></td>
                                                        <td class="border-bottom" height="10"></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding-top:10px; vertical-align:top;">
                                                        {{$your_name}}
                                                        </td>
                                                        <td style="padding-top:10px;" class="mobile">
                                                            <a href="mailto:{{$your_email}}">{{$your_email}}</a>
                                                        </td>
                                                        <td style="padding-top:10px;" class="mobile">
                                                        {{Carbon\Carbon::now()->format('d-m-Y')}}
                                                        </td>
                                                    </tr>
                                                </table>

                                                <table cellspacing="0" cellpadding="0" width="100%">
                                                    <tr>
                                                        <td class="mobile-center" align="left" style="padding:20px 0;">
                                                            <div class="mobile-center" align="left">
                                                        </td>
                                                    </tr>
                                                </table>

                                                <table cellspacing="0" cellpadding="0" width="100%">
                                                    <tr>
                                                        <td class="left" style="padding-top:10px; text-align:left;">
                                                            <img class="left" width="150" height="40" src="https://ayaworkshop.com/wp-content/uploads/2019/04/signature5ca45788243e1.gif">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </center>
                            </td>
                        </tr>
                    </table>
                </center>
            </td>
        </tr>
    </table>
</body>

</html> 