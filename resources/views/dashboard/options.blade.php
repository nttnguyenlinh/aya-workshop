@extends('layouts.dboard')

@section('page-header')
<div class="row mb-2">
    <div class="col-sm-6">
        <h1 class="m-0 text-dark">Options</h1>
    </div>
</div>
@endsection


@section('content')
<div class="content-header">
    <div class="container-fluid">
        <div class="color-palette-set panel-btn-add-line">
            <div class="bg-success disabled color-palette">
                <div class="row">
                    <div class="col-sm-2 col-md-2"></div>

                    <div class="col-sm-2 col-md-2">
                        <span>Select the field to add</span>
                    </div>

                    <div class="col-sm-3 col-md-3">
                        <select class="form-control" id="selectField">
                            <option value="single">Single line</option>
                            <option value="multi">Multi line</option>
                        </select>
                    </div>

                    <div class="col-sm-1 col-md-1">
                        <button type="button" id="btnGenerate" class="btn btn-block btn-primary">Add
                            new</button>
                    </div>

                    <div class="col-sm-3 col-md-3"></div>
                </div>
            </div>
        </div>

        <form id="frmOptions" method="post" action="{{route('dashboard.options.store')}}" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-sm-12">
                    <div class="bg-info disabled color-palette">
                        <span>To use a option on your site just call
                            <strong>"\App\Helpers\Helpers::getOption('key');"</strong>
                        </span>

                        <button type="submit" class="btn btn-primary float-right">Save</button>
                    </div>
                </div>
            </div>

            <div id="pannelOptions">
                @if($options)
                @foreach($options as $option)
                <div class="items">
                    <input type="hidden" name="type[]" value="{{$option->type}}">
                    <div class="item-optons pt-3 ml-5 mr-5">
                        <div class="row">
                            <div class="col-sm-12 col-md-3">
                                <input type="text" name="key[]" class="form-control" value="{{$option->key}}"
                                    placeholder="Enter the key">
                            </div>

                            @if($option->type == 0)
                            <div class="col-sm-12 col-md-8">
                                <input type="text" name="value[]" class="form-control"
                                    value="{!!htmlentities($option->value)!!}" placeholder="Enter the value">
                            </div>
                            @else
                            <div class="col-sm-12 col-md-8">
                                <textarea class="form-control" name="value[]" rows="7" placeholder="Enter the value">{{html_entity_decode($option->value)}}</textarea>
                            </div>
                            @endif

                            <div class="col-sm-1 col-md-1">
                                <button type="button" id="delete" class="btn btn-block btn-danger"
                                    value="{{$option->key}}"><i class="fas fa-trash"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                @endif
            </div>
        </form>
    </div>
</div>

@endsection

@section('script-footer')
<script>
$(document).ready(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(document).on('click', '#btnGenerate', function() {
        var type = $('#selectField').val();
        var rand_id = Math.floor((Math.random() * 1000) + 1);
        console.log(type);
        var prepend = '';
        if (type == "single") {
            prepend =
                "<div class='items'><input type='hidden' name='type[]' value='single'><div class='item-optons pt-3 ml-5 mr-5'><div class='row'><div class='col-sm-12 col-md-3'><input type='text' id='" +
                rand_id +
                "'name='key[]' class='form-control' placeholder='Enter the key'></div><div class='col-sm-12 col-md-8'><input type='text' name='value[]' class='form-control' placeholder='Enter the value'></div><div class='col-sm-1 col-md-1'><button type='button' id='delete' class='btn btn-block btn-danger' value='" +
                rand_id + "'><i class='fas fa-trash'></i></button></div></div></div></div>";
        } else {
            prepend =
                "<div class='items'><input type='hidden' name='type[]' value='multi'><div class='item-optons pt-3 ml-5 mr-5'><div class='row'><div class='col-sm-12 col-md-3'><input type='text' id='" +
                rand_id +
                "'name='key[]' class='form-control' placeholder='Enter the key'></div><div class='col-sm-12 col-md-8'><textarea class='form-control' name='value[]' rows='5' placeholder='Enter the value'></textarea></div><div class='col-sm-1 col-md-1'><button type='button' id='delete' class='btn btn-block btn-danger' value='" +
                rand_id + "'><i class='fas fa-trash'></i></button></div></div></div></div>";
        }


        $("#pannelOptions").prepend(prepend);
    });

    $(document).on('click', '#delete', function() {
        var key = $(this).val();
        var parents = $(this).parents('.items');

        //console.log(key);
        $.ajax({
            url: 'options/delete',
            type: 'delete',
            dataType: 'json',
            data: {
                key: key,
                _method: 'delete'
            },
            success: function(data) {
                parents.remove();
                Swal.fire('Deleted!', 'The field has been deleted.', 'success');
            }
        });
    });
});
</script>
@endsection