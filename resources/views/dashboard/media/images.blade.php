<!DOCTYPE html>
<html>

<head>
    <title>Load Images</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="https://colorlib.com/etc/tb/Table_Responsive_v1/css/main.css">
    <!--===============================================================================================-->
</head>

<body>
    <div class="limiter">
        <div class="container-table100">
            <td class="wrap-table100">
            <td class="table100">
                <table>
                    <thead>
                        <tr class="table100-head">
                            <th class="column1">Name</th>
                            <th class="column2">Extension</th>
                            <th class="column3">Size</th>
                            <th class="column4">Path</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($files as $file)
                        <tr>
                            <td class="column1">{{$file['name']}}</td>
                            <td class="column2">{{$file['extension']}}</td>
                            <td class="column3">{{$file['size']}}</td>
                            <td class="column4">{{$file['path']}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </td>
        </div>
    </div>
    </div>
</body>

</html>