@extends('layouts.dboard')

@section('page-header')
<div class="row mb-2">
    <div class="col-sm-6">
        <h1 class="m-0 text-dark">Media Library</h1>
    </div><!-- /.col -->
</div><!-- /.row -->
@endsection


@section('content')
<div class="row">
    <div class="col-sm-12">
        <div id="media"></div>
    </div><!-- /.col -->
</div><!-- /.row -->

@endsection

@section('script-footer')
<script>
    CKFinder.widget('media', {
        width: '100%',
        height: 800,
        plugins: ['samples/plugins/StatusBarInfo/StatusBarInfo']
    });
</script>

@endsection