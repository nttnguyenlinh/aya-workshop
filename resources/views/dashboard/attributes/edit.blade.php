@extends('layouts.dboard')

@section('page-header')
<div class="row mb-2">
    <div class="col-sm-6">
        <h1 class="m-0 text-dark">Edit attribute</h1>
    </div>
</div>
@endsection


@section('content')

<div class="row">
    <div class="col-md-8 offset-md-2">
        <div class="col-wrap">
            <div class="form-wrap">
                <form action="{{route('dashboard.attribute.update')}}" method="post" class="form-horizontal">
                    @csrf
                    <input type="hidden" name="attribute_id" value="{{$attribute->id}}" />
                    <div class="form-group row">
                        <label for="attribute_name" class="col-sm-4">Name</label>
                        <div class="col-sm-8">
                            <input name="attribute_name" id="attribute_name" type="text" class="form-control"
                                value="{{$attribute->name}}" maxlength="28" required autofocus>
                            <p class="description">Name for the attribute (shown on the front-end). Unique
                                name/reference
                                for the attribute; must be no more than 28
                                characters.</p>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="attribute_slug" class="col-sm-4">Slug</label>
                        <div class="col-sm-8">
                            <input name="attribute_slug" id="attribute_slug" type="text" class="form-control"
                                value="{{$attribute->slug}}" maxlength="28">
                            <p class="description">Unique slug/reference for the attribute; must be no more than 28
                                characters.</p>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="attribute_position" class="col-sm-4">Default sort order</label>

                        <div class="col-sm-8">
                            <select name="attribute_position" id="attribute_position" class="form-control">
                                @php
                                $orderby = array("none" => "Default ordering", "name" => "Term Name", "id" => "Term
                                ID");
                                foreach ($orderby as $key => $val) {
                                if ($attribute->orderby == $key) {
                                echo "<option value='" . $key . "' selected>" . $val . "</option>";
                                } else {
                                echo "<option value='" . $key . "'>" . $val . "</option>";
                                }
                                }
                                @endphp
                            </select>
                            <p class="description">Determines the sort order of the terms on the frontend shop product
                                pages. If using default ordering, will sort by system data.</p>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-4 offset-md-4">
                            <button type="submit" class="btn btn-primary">Update</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
