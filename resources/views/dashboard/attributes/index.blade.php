@extends('layouts.dboard')

@section('page-header')
<div class="row mb-2">
    <div class="col-sm-6">
        <h1 class="m-0 text-dark">Attributes</h1>
    </div>
</div>
@endsection


@section('content')

<div class="row">
    <div class="col-md-5">
        <div class="col-wrap">
            <div class="form-wrap">
                <h2>Add new attribute</h2>
                <p>Attributes let you define extra product data, such as size or color.</p>
                <form action="{{route('dashboard.attributes.store')}}" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="attribute_name">Name</label>
                        <input name="attribute_name" id="attribute_name" type="text" class="form-control" value=""
                            maxlength="28" required autofocus>
                        <p class="description">Name for the attribute (shown on the front-end). Unique name/reference
                            for the attribute; must be no more than 28
                            characters.</p>
                    </div>

                    <div class="form-group">
                        <label for="attribute_slug">Slug</label>
                        <input name="attribute_slug" id="attribute_slug" type="text" class="form-control" value=""
                            maxlength="28">
                        <p class="description">Unique slug/reference for the attribute; must be no more than 28
                            characters.</p>
                    </div>

                    <div class="form-group">
                        <label for="attribute_position">Default sort order</label>
                        <select name="attribute_position" id="attribute_position" class="form-control">
                            <option value="none">Default ordering</option>
                            <option value="name">Term Name</option>
                            <option value="id">Term ID</option>
                        </select>
                        <p class="description">Determines the sort order of the terms on the frontend shop product
                            pages. If using default ordering, will sort by system data.</p>
                    </div>

                    <button type="submit" class="btn btn-primary">Add attribute</button>
                </form>
            </div>
        </div>
    </div>

    <div class="col-md-7">
        <div class="card">
            <div class="card-body p-0">
                <table class="table table-sm attributes-table">
                    <thead>
                        <tr>
                            <th style="width:120px;">Name</th>
                            <th style="width:100px;">Slug</th>
                            <th style="width:100px;">Position</th>
                            <th>Terms</th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach($attributes as $attribute)
                        <tr class="alternate">
                            <td>
                                <strong>
                                    <a href="{{route('dashboard.attribute.terms', $attribute->id)}}">{{$attribute->name}}</a>
                                </strong>
                                <div class="row-actions">
                                    <span class="edit">
                                        <a href="{{route('dashboard.attribute.edit', $attribute->id)}}">Edit</a>
                                    </span> |
                                    <span class="delete">
                                        <a class="delete" href="javascript:void(0)" id="delete" data-id="{{$attribute->id}}">Delete</a>
                                    </span>
                                </div>
                            </td>

                            <td>{{$attribute->slug}}</td>
                            <td>{{$attribute->orderby}}</td>

                            @php
                                $terms = \App\Term::where('attribute_id', $attribute->id);
                                if($attribute->position != "none")
                                    $terms = $terms->orderBy($attribute->position, 'asc');
                                $terms = $terms->get();
                            @endphp

                            <td class="attribute-terms">
                                @if($terms->count() == 0)
                                    <span>-</span>
                                @elseif($terms->count() > 0)
                                    @foreach($terms as $term)
                                        <span class="badge badge-info" style="margin:0 1px;">{{$term->name}}</span>
                                    @endforeach
                                @endif
                                <br>
                                <a href="{{route('dashboard.attribute.terms', $attribute->id)}}" class="configure-terms">Configure terms</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
    </div>
</div>
@endsection

@section('script-footer')
    <script>
        $(document).ready(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            //remove
            $(document).on('click', '#delete', function(e) {
                e.preventDefault();
                var id = $(this).attr('data-id');
                var parents = $(this).parents('.alternate');
                Swal.fire({
                    title: 'Are you sure?',
                    text: 'You will not be able to recover!',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#d33',
                    cancelButtonColor: '#3085d6',
                    cancelButtonText: 'No, cancel it!',
                    confirmButtonText: 'Yes, I am sure!',
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            url: 'attribute/delete',
                            type: 'delete',
                            dataType: 'json',
                            data: {
                                id: id,
                                _method: 'delete'
                            },
                            success: function(data) {
                                if(data === 'success') {
                                    parents.remove();
                                    Swal.fire('Deleted!', 'The Attribute has been deleted.', 'success');
                                }
                                else
                                    Swal.fire('Deleted!', 'Oh no...!, Can not delete', 'error');
                            }
                        });
                    } else if (result.dismiss === Swal.DismissReason.cancel) {
                        Swal.fire('Canceled!', 'The Attribute have been canceled', 'info')
                    }
                })
            });
        });
    </script>
@endsection
