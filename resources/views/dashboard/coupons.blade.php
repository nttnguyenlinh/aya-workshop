@extends('layouts.dboard')

@section('page-header')

<div class="row mb-2">
    <div class="col-sm-6">
        <h1 class="m-0 text-dark">Coupons</h1>
    </div><!-- /.col -->
    <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item">
                <a href="" class="btn btn-default" data-toggle="modal" data-target="#div_add">
                    <i class="fal fa-plus"></i> Add
                </a>
            </li>
        </ol>
    </div><!-- /.col -->
</div><!-- /.row -->
@endsection


@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title">
                    <i class="fal fa-percent"></i>
                    Data Tables
                </h3>
            </div>
            <div class="card-body pad table-responsive">
                <table id="tb_coupons" class="table table-striped table-hover table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th>Coupon Code</th>
                            <th>Discount type</th>
                            <th>Coupon Amount</th>
                            <th>Description</th>
                            <th>Usage/Limit</th>
                            <th>Expiry Date</th>
                            <th width="100">Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
            <!-- /.card -->
        </div>
    </div>
</div>

<div class="modal fade" id="div_add" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="card card-info">
                <div class="card-header">
                    <h3 class="card-title">Add new coupon
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </h3>
                </div>
                <div class="card-body">
                    <form method="post" id="frm_add_coupons" action="{{route('dashboard.postCoupons')}}">
                        @csrf
                        <div class="input-group mb-3">
                            <input type="text" name="coupon_code" id="coupon_code" value="{{old('coupon_code')}}"
                                class="form-control" placeholder="Coupon Code" required autofocus>
                        </div>

                        <div class="input-group mb-3">
                            <input type="text" name="coupon_description" id="coupon_description"
                                value="{{old('coupon_description')}}" class="form-control" placeholder="Description">
                        </div>

                        <div class="input-group mb-3">
                            <input type="text" name="coupon_amount" id="coupon_amount" value="{{old('coupon_amount')}}"
                                class="form-control" placeholder="Coupon Amount" required>
                        </div>

                        <label>Type</label>
                        <div class="input-group mb-3">
                            <select name="discount_type" id="discount_type" class="form-control">
                                <option value="Fixed">Fixed</option>
                                <option value="Percentage">Percentage</option>
                            </select>
                        </div>

                        <label>Coupon expiry date</label>
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" id="expiry_date" name="expiry_date"
                                placeholder="Pick a date" value="{{date('d-m-Y')}}" required>
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                            </div>
                        </div>

                        <div class="input-group mb-3">
                            <input type="text" name="coupon_limit" id="coupon_limit" value="{{old('coupon_limit')}}"
                                class="form-control" placeholder="Limit" required>
                        </div>

                        <div class="input-group justify-content-center">
                            <button class="btn btn-danger" data-dismiss="modal"
                                style="margin-right:20px;">Close</button>
                            <button type="submit" class="btn btn-success" id="btn_add">Add</button>
                        </div>
                    </form>
                </div>
                <!-- /.card-body -->
            </div>
        </div>
    </div>
</div>

@endsection

@section('script-footer')
<script>
$(document).ready(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    // $(document).on('click', '#div_add', function() {
    //$('#expiry_date').val(moment().format("DD-MM-YYYY"));

    // $('#expiry_date').daterangepicker({
    //     parentEl: '.card-body',
    //     singleDatePicker: true,
    //     showDropdowns: true,
    //     minDate: '01-01-2020',
    //     maxDate: '01-01-2036',
    //     locale: {
    //         format: 'DD-MM-YYYY'
    //     },
    // });
    // });

    var tb_coupons = $('#tb_coupons').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: "{!!route('dashboard.anyCoupons')!!}",
            type: 'GET',
        },
        rowId: 'id',
        columns: [{
                data: 'coupon_code',
                name: 'coupon_code',
                orderable: false,
                searchable: false,
            },
            {
                data: 'coupon_discount_type',
                name: 'coupon_discount_type',
                orderable: false,
                searchable: false,
            },
            {
                data: 'coupon_amount',
                name: 'coupon_amount',
                orderable: false,
                searchable: false,
            },
            {
                data: 'coupon_description',
                name: 'coupon_description',
                orderable: false,
                searchable: false,
            },
            {
                data: 'coupon_usage',
                name: 'coupon_usage',
                orderable: false,
                searchable: false,
            },
            {
                data: 'coupon_expiry_date',
                name: 'coupon_expiry_date',
                orderable: false,
                searchable: false,
            },
            {
                data: 'action',
                name: 'action',
                orderable: false,
                searchable: false,
            }
        ]
    });

    $(document).on('click', '#delete', function() {
        var id = $(this).val();

        $.ajax({
            url: 'coupons/delete',
            type: 'delete',
            dataType: 'json',
            data: {
                id: id,
                _method: 'delete'
            },
            success: function(data) {
                if (data === 'success') {
                    Swal.fire('Deleted!', 'Coupon has been deleted.', 'success');
                    $('#tb_coupons').DataTable().ajax.reload();
                } else {
                    Swal.fire('Oh no...!', 'Can not delete', 'error');
                }
            }
        });
    });
});
</script>
@endsection
