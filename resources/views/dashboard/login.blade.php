<!DOCTYPE html>
<html lang="{{app()->getLocale()}}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Login - {{config('app.name')}}</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="icon" href="{{asset('dboard/images/icons/ICO-32x32.png')}}" sizes="32x32" />
    <link rel="icon" href="{{asset('dboard/images/icons/ICO-192x192.png')}}" sizes="192x192" />
    <link rel="apple-touch-icon-precomposed" href="{{asset('dboard/images/icons/ICO-180x180.png')}}" />
    <meta name="msapplication-TileImage" content="{{asset('dboard/images/icons/ICO-270x270.png')}}" />

    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="{{asset('dboard/plugins/font-awesome/css/all.min.css')}}">

    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('dboard/css/adminlte.min.css')}}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{asset('dboard/plugins/iCheck/square/blue.css')}}">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <script src='https://www.google.com/recaptcha/api.js?hl={{app()->getLocale()}}'></script>
</head>

<body class="hold-transition login-page">
    <div class="login-box">
        <div class="login-logo">
            <img src="{{asset('dboard/images/icons/LOGO.png')}}" width="100%" height="100%"
                alt="{{ config('app.name', 'AYA WORKSHOP') }}" />
        </div>
        <!-- /.login-logo -->
        <div class="card">
            <div class="card-body login-card-body">
                <p class="login-box-msg">Login to start your session</p>

                @if(Session::has('alert'))
                <div class="callout callout-danger">
                    <p class="text-danger">
                        {!!Session::get('message')!!}
                    </p>
                </div>
                @endif

                <form action="{{ route('dashboard.login') }}" method="post" role="form">
                    @csrf
                    <div class="input-group mb-3">
                        <input type="email" name="email"
                            class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Email"
                            value="{{ old('email') }}" required autofocus>
                        <div class="input-group-append">
                            <span class="fal fa-envelope input-group-text" {!! $errors->has('email') ?
                                'style="color:red; border:1px solid red;"' : '' !!}></span>
                        </div>
                        @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert" style="display:block;">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="input-group mb-3">
                        <input type="password" name="password"
                            class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}"
                            placeholder="Password" required>
                        <div class="input-group-append">
                            <span class="fal fa-lock input-group-text" {!! $errors->has('password') ? 'style="color:red;
                                border:1px solid red;"' : '' !!}></span>
                        </div>
                        @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert" style="display:block;">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <center>
                                <div style="margin-bottom:10px;" class="g-recaptcha"
                                    data-sitekey="{{config('app.recaptcha_site_key')}}"></div>
                            </center>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-8">
                            <div class="checkbox icheck">
                                <label>
                                    <input type="checkbox" name="remember" id="remember"
                                        {{ old('remember') ? 'checked' : '' }}> Remember Me
                                </label>
                            </div>
                        </div>
                        <!-- /.col -->
                        <div class="col-4">
                            <button type="submit" class="btn btn-primary btn-block btn-flat">Login</button>
                        </div>
                        <!-- /.col -->
                    </div>
                </form>
                <p class="mb-1">
                    <a href="{{route('password.request') }}">{{ __('Forgot Your Password?') }}</a>
                </p>
            </div>
            <!-- /.login-card-body -->
        </div>
    </div>
    <!-- /.login-box -->

    <!-- jQuery -->
    <script src="{{asset('dboard/js/jquery.min.js')}}"></script>
    <!-- Bootstrap 4 -->
    <script src="{{asset('dboard/js/bootstrap.bundle.min.js')}}"></script>
    <!-- iCheck -->
    <script src="{{asset('dboard/plugins/iCheck/icheck.min.js')}}"></script>
    <script>
    $(function() {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        })
    })
    </script>
</body>

</html>
