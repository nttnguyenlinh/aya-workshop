@extends('layouts.dboard')

@section('page-header')
<div class="row mb-2">
    <div class="col-sm-6">
        <h1 class="m-0 text-dark">Posts</h1>
    </div><!-- /.col -->

    <div class="col-sm-6" id="containerAdd">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item">
                <a href="{{route('dashboard.post.create')}}" class="btn btn-default">
                    <i class="fal fa-plus-octagon"></i> Add new
                </a>
            </li>
        </ol>
    </div><!-- /.col -->
</div><!-- /.row -->
@endsection


@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card card-primary card-outline">
            <div class="card-header">
                <button type="button" id="btnDelete" class="btn btn-danger mb-1">Delete</button> | <button type="button" id="btnTrash" class="btn btn-secondary mb-1">Move to trash</button>
                <span class="panel-filter-right float-right"><a href="javascript:void(0)" id="btnAll" data-status="100">All ({{Helpers::get_count_post_via_status(100)}})</a> | <a href="javascript:void(0)" id="btnPublished" data-status="1">Published ({{Helpers::get_count_post_via_status(1)}})</a> | <a href="javascript:void(0)" id="btnDraft" data-status="-1">Draft ({{Helpers::get_count_post_via_status(-1)}})</a> | <a href="javascript:void(0)" id="btnPending" data-status="0">Pending ({{Helpers::get_count_post_via_status(0)}})</a> </span>
            </div>

            <div class="card-body pad table-responsive">
                <table id="tb_posts" class="table table-striped table-hover table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Title</th>
                            <th>Author</th>
                            <th>Categories</th>
                            <th>Tags</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script-footer')
    <script type="text/javascript">
        $(document).ready(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            loadData("");

            //Load All
            $(document).on('click', '#btnAll', function(e) {
                loadData(100);
            });

            //Load Published
            $(document).on('click', '#btnPublished', function(e) {
                loadData($(this).attr('data-status'));
            });

            //Load Draft
            $(document).on('click', '#btnDraft', function(e) {
                loadData($(this).attr('data-status'));
            });

            //Load Pending
            $(document).on('click', '#btnPending', function(e) {
                loadData($(this).attr('data-status'));
            });

            //Load data
            function loadData(status){
                $('#tb_posts').DataTable().destroy();
                $('#tb_posts').DataTable({
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    ordering: true,
                    ajax: {
                        url: "{{route('dashboard.posts.list')}}",
                        type: 'GET',
                        data: {
                            status: status,
                        },
                    },
                    columnDefs: [
                        {
                            targets: 0,
                            checkboxes: {
                                selectRow: true
                            }
                        }
                    ],
                    select: {
                        style: 'multi'
                    },
                    columns: [
                        {
                            data: 'id',
                            name: 'id',
                        },
                        {
                            data: 'name',
                            name: 'name',
                        },
                        {
                            data: 'author',
                            name: 'author',
                        },
                        {
                            data: 'categories',
                            name: 'categories',
                        },
                        {
                            data: 'tags',
                            name: 'tags',
                        },
                        {
                            data: 'date',
                            name: 'date',
                        }
                    ],
                    order: []
                });
            }

            //multi delete
            $(document).on('click', '#btnDelete', function(e) {
                var rows_selected = $('#tb_posts').DataTable().column(0).checkboxes.selected();
                var posts_id = [];
                // Iterate over all selected checkboxes
                $.each(rows_selected, function(index, rowId){
                    posts_id.push(rowId);
                });

                Swal.fire({
                    title: 'Are you sure?',
                    text: 'You will not be able to recover!',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#d33',
                    cancelButtonColor: '#3085d6',
                    cancelButtonText: 'No, cancel it!',
                    confirmButtonText: 'Yes, I am sure!',
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            url: '/dashboard/post/delete',
                            type: 'delete',
                            dataType: 'json',
                            data: {
                                posts_id: posts_id,
                                _method: 'delete'
                            },
                            success: function(data) {
                                $('#tb_posts').DataTable().ajax.reload();
                                $('#tb_posts').DataTable().rows().deselect();
                                if(data.errors > 0)
                                    Swal.fire({
                                        title: "Deleted!",
                                        html: "<p class='text-success'>" + data.success + " items has been deleted! </p><p class='text-danger'>" + data.errors + " items Can not delete!</p>",
                                        type: "error",
                                    });
                                else
                                    Swal.fire({
                                        title: "Deleted!",
                                        html: "<p class='text-success'>" + data.success + " items has been deleted! </p>",
                                        type: "success",
                                    });
                            }
                        });
                    } else if (result.dismiss === Swal.DismissReason.cancel) {
                        $('#tb_posts').DataTable().ajax.reload();
                        $('#tb_posts').DataTable().rows().deselect();
                        Swal.fire('Canceled!', 'Have been canceled', 'info')
                    }
                });
            });

            //multi trash
            $(document).on('click', '#btnTrash', function(e) {
                var rows_selected = $('#tb_posts').DataTable().column(0).checkboxes.selected();
                var posts_id = [];
                // Iterate over all selected checkboxes
                $.each(rows_selected, function(index, rowId){
                    posts_id.push(rowId);
                });

                Swal.fire({
                    title: 'Are you sure?',
                    text: 'This is a replacement delete function, the data will be reused if you want!',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#d33',
                    cancelButtonColor: '#3085d6',
                    cancelButtonText: 'No, cancel it!',
                    confirmButtonText: 'Yes, I am sure!',
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            url: '/dashboard/post/trash',
                            type: 'post',
                            dataType: 'json',
                            data: {
                                posts_id: posts_id,
                            },
                            success: function(data) {
                                $('#tb_posts').DataTable().ajax.reload();
                                $('#tb_posts').DataTable().rows().deselect();
                                if(data.errors > 0)
                                    Swal.fire({
                                        title: "Move to trash!",
                                        html: "<p class='text-success'>" + data.success + " items has been move to trash! </p><p class='text-danger'>" + data.errors + " items Can not move to trash!</p>",
                                        type: "error",
                                    });
                                else
                                    Swal.fire({
                                        title: "Move to trash!",
                                        html: "<p class='text-success'>" + data.success + " items has been move to trash! </p>",
                                        type: "success",
                                    });
                            }
                        });
                    } else if (result.dismiss === Swal.DismissReason.cancel) {
                        $('#tb_posts').DataTable().ajax.reload();
                        $('#tb_posts').DataTable().rows().deselect();
                        Swal.fire('Canceled!', 'Have been canceled', 'info')
                    }
                });
            });
            //move to trash
            $(document).on('click', '#trash', function(e) {
                e.preventDefault();
                var posts_id = [];
                posts_id.push($(this).attr('data-id'));

                Swal.fire({
                    title: 'Are you sure?',
                    text: 'This is a replacement delete function, the data will be reused if you want!',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#d33',
                    cancelButtonColor: '#3085d6',
                    cancelButtonText: 'No, cancel it!',
                    confirmButtonText: 'Yes, I am sure!',
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            url: '/dashboard/post/trash',
                            type: 'post',
                            dataType: 'json',
                            data: {
                                posts_id: posts_id,
                            },
                            success: function(data) {
                                $('#tb_posts').DataTable().ajax.reload();
                                $('#tb_posts').DataTable().rows().deselect();
                                if(data.errors > 0)
                                    Swal.fire({
                                        title: "Move to trash!",
                                        html: "<p class='text-success'>" + data.success + " items has been move to trash! </p><p class='text-danger'>" + data.errors + " items Can not move to trash!</p>",
                                        type: "error",
                                    });
                                else
                                    Swal.fire({
                                        title: "Move to trash!",
                                        html: "<p class='text-success'>" + data.success + " items has been move to trash! </p>",
                                        type: "success",
                                    });
                            }
                        });
                    } else if (result.dismiss === Swal.DismissReason.cancel) {
                        $('#tb_posts').DataTable().ajax.reload();
                        $('#tb_posts').DataTable().rows().deselect();
                        Swal.fire('Canceled!', 'Have been canceled', 'info')
                    }
                })
            });
        });
    </script>
@endsection
