@extends('layouts.dboard')

@section('page-header')
    <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0 text-dark">Tags</h1>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-5">
            <div class="col-wrap">
                <div class="form-wrap">
                    <h2>Add New Tag</h2>
                    <form action="{{route('dashboard.tag.store')}}" method="post">
                        @csrf
                        <input type="hidden" name="tag_type" value="post"/>
                        <div class="form-group">
                            <label for="tag_name">Name</label>
                            <input name="tag_name" id="tag_name" type="text" class="form-control" value="" required autofocus>
                            <p class="description">The name is how it appears on your site.</p>
                        </div>

                        <div class="form-group">
                            <label for="tag_slug">Slug</label>
                            <input name="tag_slug" id="tag_slug" type="text" class="form-control" value="">
                            <p class="description">The 'slug' is the URL-friendly version of the name. It is usually all lowercase and contains only letters, numbers, and hyphens.</p>
                        </div>

                        <button type="submit" class="btn btn-primary">Add new Tag</button>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-md-7">
            <button type="button" id="btnDelete" class="btn btn-danger mb-1">Delete</button>
            <div class="card card-outline card-primary">
                <div class="card-body p-0">
                    <table id="tbl_tags" class="table widefat table-sm">
                        <thead>
                        <tr>
                            <th></th>
                            <th>Name</th>
                            <th>Slug</th>
                            <th>Count</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script-footer')
    <script>
        $(document).ready(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            //Load data
            var tbl_tags = $('#tbl_tags').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: {
                    url: '/dashboard/post/tag/list',
                    type: 'get',
                },
                columnDefs: [
                    {
                        targets: 0,
                        checkboxes: {
                            selectRow: true
                        }
                    }
                ],
                select: {
                    style: 'multi'
                },
                columns: [
                    {
                        data: 'id',
                        name: 'id',

                    },
                    {
                        data: 'name',
                        name: 'name',
                    },
                    {
                        data: 'slug',
                        name: 'slug',
                    },
                    {
                        data: 'count',
                        name: 'count',
                    }
                ],
                order: [1, 'asc'],
            });
            $('#tbl_tags tbody').on( 'click', 'tr', function () {
                $(this).toggleClass('selected');
            });

            //multi delete
            $(document).on('click', '#btnDelete', function(e) {
                var rows_selected = tbl_tags.column(0).checkboxes.selected();
                var tags_id = [];
                // Iterate over all selected checkboxes
                $.each(rows_selected, function(index, rowId){
                    tags_id.push(rowId);
                });

                Swal.fire({
                    title: 'Are you sure?',
                    text: 'You will not be able to recover!',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#d33',
                    cancelButtonColor: '#3085d6',
                    cancelButtonText: 'No, cancel it!',
                    confirmButtonText: 'Yes, I am sure!',
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            url: '/dashboard/tag/delete',
                            type: 'delete',
                            dataType: 'json',
                            data: {
                                tags_id: tags_id,
                                _method: 'delete'
                            },
                            success: function(data) {
                                $('#tbl_tags').DataTable().ajax.reload();
                                if(data.errors > 0)
                                    Swal.fire({
                                        title: "Deleted!",
                                        html: "<p class='text-success'>" + data.success + " items has been deleted! </p><p class='text-danger'>" + data.errors + " items Can not delete!</p>",
                                        type: "error",
                                    });
                                else
                                    Swal.fire({
                                        title: "Deleted!",
                                        html: "<p class='text-success'>" + data.success + " items has been deleted! </p>",
                                        type: "success",
                                    });
                            }
                        });
                    } else if (result.dismiss === Swal.DismissReason.cancel) {
                        $('#tbl_tags').DataTable().ajax.reload();
                        Swal.fire('Canceled!', 'Have been canceled', 'info')
                    }
                    tbl_tags.rows().deselect();
                });
            });
        });
    </script>
@endsection
