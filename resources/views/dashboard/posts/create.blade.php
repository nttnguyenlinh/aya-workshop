@extends('layouts.dboard')

@section('page-header')
<div class="row mb-2">
    <div class="col-sm-6">
        <h1 class="m-0 text-dark">Add new post</h1>
    </div><!-- /.col -->
</div><!-- /.row -->
@endsection


@section('content')
    <link rel="stylesheet" href="{{asset('dboard/plugins/tinymce/custom_css.css')}}">
    <script type="text/javascript" src="https://cdn.tiny.cloud/1/k9y1yuun52fw46xsxp3l1pdost7wyn7kc1jxwc866jgweclj/tinymce/5/tinymce.min.js"></script>
    <link rel="stylesheet" href="{{asset('dboard/css/bootstrap-tagsinput.css')}}">

<section class="content">
    <div class="container-fluid">
        <form method="post" id="myform" action="{{route('dashboard.post.store')}}" role="form" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-9">
                    <div class="card card-primary card-outline">
                        <div class="card-body">
                            <div class="form-group">
                                <input type="text" class="form-control" name="title" placeholder="Title..." required>
                            </div>
                            <div class="form-group">
                                <textarea id="contents" name="contents" class="form-control" placeholder="Write content here ..."></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="card card-primary card-outline">
                                <div class="card-header">
                                    <h3 class="card-title">Modify SEO</h3>
                                </div>
                                <div class="card-body">
                                    <p class="bolder" style="padding-top: 20px; font-weight: 600;">SEO title (<span class="jWwadS">Title</span> <span class="jWwadS">-</span> <span class="jWwadS">Site title</span>)</p>
                                    <input class="form-control" style="border: #057bbe 1px solid; color: #057bbe;" id="seo_title" name="seo_title" placeholder="Modify your SEO title by editing it right here"/>

                                    <p class="bolder" style="padding-top: 20px; font-weight: 600;">SEO keyword</p>
                                    <textarea class="form-control" style="border: #057bbe 1px solid; height:100px; color: #057bbe; resize: none;" id="seo_keyword" name="seo_keyword" placeholder="SEO keyword"></textarea>

                                    <p class="bolder" style="padding-top: 20px; font-weight: 600;">Meta description</p>
                                    <textarea class="form-control" style="border: #057bbe 1px solid; height:100px; color: #057bbe; resize: none;" id="meta_description" name="meta_description" placeholder="Modify your meta description by editing it right here"></textarea>

                                    <div id="the-count" style="font-weight: normal;">
                                        <span id="current" style="color: rgb(102, 102, 102);"></span>
                                        <span id="maximum" style="color: rgb(102, 102, 102);">/ 150</span>
                                    </div>

                                    <div style="margin-top: 20px; font-weight: 600;">
                                        <span class="bolder">Facebook Image</span>
                                        <input type="hidden" name="facebook_image" id="facebook_image" value="">
                                        <center>
                                            <img src="https://dummyimage.com/1920x768/000/fff" style="width:70%; height:50%;" id="fb_image">
                                        </center>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Status & Visibility</h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-widget="collapse"><i
                                        class="fa fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body p-0">
                            <fieldset class="visibility-fieldset">
                                <div class="visibility-choice">
                                    <input type="radio" name="status" id="status_1" class="visibility-radio" value="1" checked>
                                    <label for="status_1" class="visibility-label">Public</label>
                                    <p class="visibility-info">Visible to everyone.</p>
                                </div>

                                <div class="visibility-choice">
                                    <input type="radio" name="status" id="status_0" class="visibility-radio" value="0">
                                    <label for="status_0" class="visibility-label">Private</label>
                                    <p class="visibility-info">Only visible to site admins and editors.</p>
                                </div>

                                <div class="visibility-choice">
                                    <input type="radio" name="status" id="status_-1" class="visibility-radio" value="-1">
                                    <label for="status_-1" class="visibility-label">Draft</label>
                                    <p class="visibility-info">Move to trash</p>
                                </div>
                            </fieldset>

                            <div class="components-panel__row">
                                <label>Author</label>
                                <select id="author" name="author" class="author_select">
                                    @foreach($users as $author)
                                        <option value="{{$author->id}}">{{mb_strtoupper($author->nicename, 'UTF-8')}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="card-footer">
                            <div class="float-right">
                                <button type="submit" class="btn btn-success"><i class="fal fa-arrow-square-right"></i> Public...</button>
                            </div>
                            <button id="reset" class="btn btn-default"><i class="fa fa-times"></i> Clear</button>
                        </div>

                    </div>

                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Categories</h3>
                            <div class="card-tools">
                                <button class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body p-0 categories_choice">
                            <?php $currents = array(); ?>
                            {!! Helpers::checkbox_categories_hierarchy($categories, $currents, 0, '') !!}
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Tags</h3>
                            <div class="card-tools">
                                <button class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body p-0">
                            <p class="tag-info">Add New Tag</p>
                            <input id="post_tag" name="post_tag" type="text" class="form-control tag-input" data-role="tagsinput" value="">
                            <p class="tag-info">Separate with commas or the Enter key.</p>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Featured Image</h3>
                            <div class="card-tools">
                                <button class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body p-0">
                            <input type="hidden" id="thumb" name="thumbnail" value="" />
                            <input type="button" id="thumbnail" class="dropify" data-show-loader="true"
                                data-default-file="" value="" data-height="90" />
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Excerpt</h3>
                            <div class="card-tools">
                                <button class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body p-0">
                            <textarea id="excerpt" name="excerpt" class="form-control" placeholder="Write an excerpt (optional)" style="width:98%; margin:3px;" rows="4" required></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>
@endsection

@section('script-footer')
    <script src="{{asset('dboard/js/bootstrap-tagsinput.min.js')}}"></script>
    <script src="{{asset('dboard/js/typeahead.bundle.min.js')}}"></script>
    <script src="{{asset('dboard/js/bloodhound.min.js')}}"></script>

    <script type="text/javascript">
        var elt = $("#post_tag");
        var path = "{{ route('dashboard.post.tag.autocomplete') }}";

        var tags = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('id'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                url: path + '?query=%QUERY%',
            wildcard: '%QUERY%',
            },
        });
        tags.initialize();

        elt.tagsinput({
            itemValue: 'id',
            itemText  : 'name',
            maxTags: 5,
            trimValue: true,
            freeInput: true,
            focusClass: 'form-control',
            typeaheadjs: {
                name: 'tags',
                itemValue: 'id',
                displayKey: 'name',
                source: tags.ttAdapter()
            },
        });

        elt.on('beforeItemAdd', function(event) {
            //alert(event.item.id);
        });

        elt.on('itemAdded', function(event) {
            setTimeout(function(){
                $(">input[type=text]",".bootstrap-tagsinput").val("");
            }, 1);
        });

        elt.on('itemRemoved', function(event) {
            //alert(event.item.id);
            var tags_id = [];
            tags_id.push(event.item.id);

            $(document).ready(function() {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    url: '/dashboard/post-tag/delete',
                    type: 'delete',
                    dataType: 'json',
                    data: {
                        tags_id: tags_id,
                        _method: 'delete'
                    },
                    success: function(data) {
                        console.log(data);
                    }
                });
            });
        });
    </script>

<script type="text/javascript">
    tinymce.init({
        selector: '#contents',
        language : 'en',
        menubar: false,
        toolbar_sticky: true,
        toolbar_drawer: 'floating',
        height: 300,
        convert_urls: false,
        image_title: true,
        image_caption: true,
        image_advtab: true,
        image_class_list: [
            {title: 'None', value: ''},
        ],
        external_filemanager_path: "/dboard/plugins/filemanager/",
        filemanager_title: 'File Manager',
        plugins: 'save formatpainter wordcount toc lists fullscreen preview image imagetools link media code paste table tabfocus quickbars hr pagebreak searchreplace autolink filemanager responsivefilemanager',
        toolbar: 'fullscreen code preview toc | bold italic underline | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent | numlist bullist | forecolor backcolor formatpainter removeformat | responsivefilemanager image media | link table pagebreak hr',

        external_plugins: {
            "responsivefilemanager": "{{asset('dboard/plugins/tinymce/plugins/responsivefilemanager/plugin.min.js')}}",
            "filemanager": "{{asset('dboard/plugins/filemanager/plugin.min.js')}}"
        },

        audio_template_callback: function(data) {
            return '<audio controls>' + '\n<source src="' + data.source1 + '"' + (data.source1mime ? ' type="' + data.source1mime + '"' : '') + ' />\n' + '</audio>';
        },
        video_template_callback: function(data) {
            return '<video width="' + data.width + '" height="' + data.height + '"' + (data.poster ? ' poster="' + data.poster + '"' : '') + ' controls="controls">\n' + '<source src="' + data.source1 + '"' + (data.source1mime ? ' type="' + data.source1mime + '"' : '') + ' />\n' + (data.source2 ? '<source src="' + data.source2 + '"' + (data.source2mime ? ' type="' + data.source2mime + '"' : '') + ' />\n' : '') + '</video>';
        },
        setup: function (editor) {
            editor.on('change', function (e) {
                editor.save();
            });
        }
    });

    $(document).ready(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        // Scroll to the bottom, in case we're in a tall textarea
        $.fn.putCursorAtEnd = function() {
            return this.each(function() {
                // Cache references
                var $el = $(this), el = this;
                // Only focus if input isn't already
                if (!$el.is(":focus")) {$el.focus();}
                // If this function exists... (IE 9+)
                if (el.setSelectionRange) {
                    // Double the length because Opera is inconsistent about whether a carriage return is one character or two.
                    var len = $el.val().length * 2;
                    // Timeout seems to be required for Blink
                    setTimeout(function() {el.setSelectionRange(len, len);}, 1);
                } else {
                    // As a fallback, replace the contents with itself
                    // Doesn't work in Chrome, but Chrome supports setSelectionRange
                    $el.val($el.val());
                }
                // Scroll to the bottom, in case we're in a tall textarea
                // (Necessary for Firefox and Chrome)
                this.scrollTop = 999999;
            });
        };
        var meta_description = $('#meta_description');
        meta_description.putCursorAtEnd() // should be chainable
            .on("focus", function() { // could be on any event
                meta_description.putCursorAtEnd()
            });

        $('#meta_description').bind('mousedown keyup', function() {
            var characterCount = $(this).val().length,
                current = $('#current'),
                maximum = $('#maximum'),
                theCount = $('#the-count');
            current.text(characterCount);
            if (characterCount >= 150) {
                maximum.css('color', 'red');
                current.css('color', 'red');
                theCount.css('font-weight','bold');
            } else {
                current.css('color', '#666');
                maximum.css('color','#666');
                theCount.css('font-weight','normal');
            }
        });

        $('#reset').click(function(e) {
            $('textarea').each(function(k, v) {
                tinyMCE.get(k).setContent('');
            });
        });

        $('#thumbnail').click(function() {
            selectFile('thumbnail');
        });

        function selectFile(elementId) {
            CKFinder.modal({
                skin: "neko",
                resourceType: 'Images',
                chooseFiles: true,
                chooseFilesOnDblClick: true,
                width: 800,
                height: 600,
                onInit: function(finder) {
                    finder.on('files:choose', function(evt) {
                        var file = evt.data.files.first();
                        var output = document.getElementById(elementId);
                        output.value = file.getUrl();
                        updateFile(file.getUrl(), file.get('name'));
                        $(".dropify-clear").attr('style', 'display:block');
                    });

                    finder.on('file:choose:resizedImage', function(evt) {
                        var output = document.getElementById(elementId);
                        output.value = evt.data.resizedUrl;
                        updateFile(evt.data.resizedUrl, file.get('name'));
                        $(".dropify-clear").attr('style', 'display:block');
                    });
                }
            });
        };

        function updateFile(url, name) {
            $('#thumb').val(url);
            $('#thumbnail').attr('data-default-file', url);
            $(".dropify-preview").attr('style', 'display:block');
            $('.dropify-render img').attr('src', url);
            $('.dropify-filename-inner').text(name);
        };

        $('.dropify').dropify({
            messages: {
                'default': '',
                'replace': '',
                'remove': 'Remove',
                'error': ''
            },
        });

        $('.dropify').dropify().on('dropify.afterClear', function(event, element) {
            $('#thumb').val('');
            $('#thumbnail').attr('data-default-file', '');
            $('.dropify-render').append('<img src="">');
            $('.dropify-filename-inner').text('');
            $(".dropify-clear").attr('style', 'display:none');
        });

        if ($('#thumb').val().length <= 0) {
            $(".dropify-clear").attr('style', 'display:none');
            $('.dropify-render').append('<img src="">');
        }

        $('#fb_image').click(function() {
            selectFile2('fb_image');
        });

        function selectFile2(elementId) {
            CKFinder.modal({
                skin: "neko",
                resourceType: 'Images',
                chooseFiles: true,
                chooseFilesOnDblClick: true,
                width: 800,
                height: 600,
                onInit: function(finder) {
                    finder.on('files:choose', function(evt) {
                        var file = evt.data.files.first();
                        var output = document.getElementById(elementId);
                        output.value = file.getUrl();
                        updateFile2(file.getUrl(), file.get('name'));
                    });

                    finder.on('file:choose:resizedImage', function(evt) {
                        var output = document.getElementById(elementId);
                        output.value = evt.data.resizedUrl;
                        updateFile2(evt.data.resizedUrl, file.get('name'));
                    });
                }
            });
        };
        function updateFile2(url, name) {
            $('#facebook_image').val(url);
            $('#fb_image').attr('src', url);
        };
    });
</script>
@endsection
