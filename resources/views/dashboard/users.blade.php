@extends('layouts.dboard')

@section('page-header')
<div class="row mb-2">
    <div class="col-sm-6">
        <h1 class="m-0 text-dark">Users</h1>
    </div><!-- /.col -->
    <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item">
                <a href="" class="btn btn-default" data-toggle="modal" data-target="#div_add">
                    <i class="fal fa-user-plus"></i> Add
                </a>
            </li>
        </ol>
    </div><!-- /.col -->
</div><!-- /.row -->
@endsection


@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title">
                    <i class="fal fa-users"></i>
                    Data Tables
                </h3>
            </div>
            <div class="card-body pad table-responsive">
                <table id="tb_users" class="table table-striped table-hover table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th width="20">ID</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th width="40">Confirmed</th>
                            <th width="50">Roles</th>
                            <th width="40">Status</th>
                            <th width="120">Created</th>
                            <th width="160">Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
            <!-- /.card -->
        </div>
    </div>
</div>

<div class="row" id="tb_customer">
    <div class="col-md-12">
        <div class="card card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title">
                    <i class="fal fa-id-badge"></i>
                    Customer Information
                    <strong><span class="btn btn-info" id="span-user-id"></span></strong>
                </h3>
            </div>
            <div class="card-body pad table-responsive">
                <table id="tb_customer_info" class="table table-striped table-hover table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th width="20">CID</th>
                            <th>Address</th>
                            <th>County</th>
                            <th>City</th>
                            <th>ZipCode</th>
                            <th>Country</th>
                            <th>Phone</th>
                        </tr>
                    </thead>
                </table>
            </div>
            <!-- /.card -->
        </div>
    </div>
</div>

<div class="modal fade" id="div_add" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="card card-info">
                <div class="card-header">
                    <h3 class="card-title">Add new user
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </h3>
                </div>
                <div class="card-body">
                    <form method="post" id="frm_add_users" action="{{route('dashboard.postAddUsers')}}">
                        @csrf
                        <h5 class='text-warning'><i class="fal fa-info"></i> Note: Customer accounts are not allowed to be add.</h5><br>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fal fa-user"></i></span>
                            </div>
                            <input type="text" name="name" id="name" value="{{old('name')}}" class="form-control" placeholder="Name" required autofocus>
                        </div>

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fal fa-envelope"></i></span>
                            </div>
                            <input type="email" name="email" id="email" value="{{old('email')}}" class="form-control" placeholder="Email" required>
                        </div>

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fal fa-lock-alt"></i></span>
                            </div>
                            <input type="password" name="password" id="password" value="" class="form-control" placeholder="Password" required>
                        </div>

                        <div class="input-group justify-content-center">
                            <button class="btn btn-danger" data-dismiss="modal" style="margin-right:20px;">Close</button> 
                            <button type="submit" class="btn btn-success" id="btn_add">Add</button>
                        </div>
                    </form>
                </div>
                <!-- /.card-body -->
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="div_edit" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="card card-info">
                <div class="card-header">
                    <h3 class="card-title">Edit User <strong><span class="btn btn-info" style="color:white;" id="span-edit-user-id"></span></strong>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </h3>
                </div>
                <div class="card-body">
                    <form method="post" id="frm_edit_users" action="{{route('dashboard.postEditUsers')}}">
                        @csrf
                        <input type="hidden" name="id" id="id" value=""/>
                        <div class="form-control">
                            <label>Roles</label>
                            <select name="level" id="level" class="form-control">
                                <option value="1">Admin</option>
                                <option value="0">Customer</option>
                            </select>
                            <br>
                            <label>Status</label>
                            <select name="status" id="status" class="form-control">
                                <option value="1">Active</option>
                                <option value="0">Lock</option>
                            </select>
                            <br>
                            <center>
                                <button class="btn btn-danger" data-dismiss="modal" style="margin-right:20px;">Close</button> 
                                <button type="submit" class="btn btn-success" id="btn_edit">Update</button>
                            </center>
                        </div>
                    </form>
                </div>
                <!-- /.card-body -->
            </div>
        </div>
    </div>
</div>
@endsection

@section('script-footer')
<script>
    $(document).ready(function() {
        $("#tb_customer").addClass("tb_display_none");

        var tb_users = $('#tb_users').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: "{!!route('dashboard.anyUsers')!!}",
                type: 'GET',
            },
            rowId: 'id',
            columns: [{
                    data: 'id',
                    name: 'id',
                },
                {
                    data: 'nicename',
                    name: 'nicename',
                },
                {
                    data: 'email',
                    name: 'email',
                },
                {
                    data: 'email_verified_at',
                    name: 'email_verified_at',
                },
                {
                    data: 'level',
                    name: 'level',
                },
                {
                    data: 'status',
                    name: 'status',
                },
                {
                    data: 'created_at',
                    name: 'created_at',
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false,
                }
            ]
        });

        // $('#tb_users tbody').on('click', 'tr', function() {
        //     if ($(this).hasClass('selected')) {
        //         $(this).removeClass('selected');

        //     } else {
        //         tb_users.$('tr.selected').removeClass('selected');
        //         $(this).addClass('selected');
        //     }
        // });

        $(document).on('click', '#view', function() {
            var id = $(this).val();

            $("#tb_customer").removeClass("tb_display_none").addClass("tb_display");
            $("#span-user-id").text("User ID: " + id);

            $('#tb_customer_info').DataTable().clear().draw();
            $('#tb_customer_info').DataTable().destroy();

            var tb_customer_info = $('#tb_customer_info').DataTable({
                searching: false,
                ordering: false,
                paging: false,
                info: false,
                ajax: {
                    url: 'findCustomer/' + id,
                    type: "get",
                },
                columns: [{
                        data: 'id',
                        name: 'id'
                    },
                    {
                        data: 'address',
                        name: 'address'
                    },
                    {
                        data: 'county',
                        name: 'county'
                    },
                    {
                        data: 'city',
                        name: 'city'
                    },
                    {
                        data: 'zipcode',
                        name: 'zipcode'
                    },
                    {
                        data: 'country',
                        name: 'country'
                    },
                    {
                        data: 'phone',
                        name: 'phone'
                    }
                ]
            });
        });

        //pass_regex
        $.validator.addMethod("pass_regex", function (value) {
            return /(?!^[0-9]*$)(?!^[a-zA-Z]*$)(?!^[0-9]{1})^([a-zA-Z0-9]{6,})$/.test(value);
        });

        $("#frm_add_users").validate({
            rules: {
                name: {
                    required: true,
                    maxlength: 50
                },
                password: {
                    required: true,
                    pass_regex: true
                },

                email: {
                    required: true,
                    email: true
                }
            },

            messages: {
                name: {
                    required: 'Please enter your name',
                    maxlength: 'Name has not more than 50 chars'
                },

                password: {
                    required: 'Please provide a password',
                    pass_regex: 'The password should be at least six characters long. To make it stronger, use upper and lower case letters and numbers.'
                },

                email: {
                    required: 'Please enter a valid email address',
                    email: 'Please enter a valid email address'
                }
            }
        });

        $(document).on('click', '#edit', function() {
            var id = $(this).val();

            $.ajax({
                url: 'users/UserByID/' + id,
                type: 'get',
                dataType: 'json',
                success: function(data){
                    $('#id').val(data.id);
                    $("#level").val(data.level).change();
                    $('#status').val(data.status).change();
                
                    $("#div_edit").modal();
                    $("#span-edit-user-id").text("User: " + data.email);
                }
            });
        });
    });

</script>
@endsection