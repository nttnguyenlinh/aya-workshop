@extends('layouts.dboard')

@section('page-header')
<div class="row mb-2">
    <div class="col-sm-6">
        <h1 class="m-0 text-dark">Orders</h1>
    </div><!-- /.col -->
</div><!-- /.row -->
@endsection


@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title">
                    <i class="fal fa-users"></i>
                    Data Tables
                </h3>
            </div>
            <div class="card-body pad table-responsive">
                <table id="tb_bills" class="table table-striped table-hover table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th>Order</th>
                            <th>Date</th>
                            <th>Status</th>
                            <th>Payment</th>
                            <th>Total</th>
                        </tr>
                    </thead>
                </table>
            </div>
            <!-- /.card -->
        </div>
    </div>
</div>

<div class="modal fade" id="detail">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><strong>Order #<span id="orderId"></span></strong></h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <h3>Billing details</h3>
                    <div class="row">
                        <div class="col-6 col-sm">
                            <p id="shipping_name"></p>
                            <p id="shipping_address"></p>
                            <p id="shipping_county"></p>
                            <p id="shipping_city"></p>
                            <p id="shipping_country"></p>
                        </div>

                        <div class="col-6 col-sm">
                            <strong>Phone</strong>
                            <p id="shipping_phone"></p>
                            <strong>Email</strong>
                            <p id="shipping_email"></p>
                            <strong>Payment method</strong>
                            <p id="shipping_payment"></p>
                            <strong>Payment info</strong>
                            <p id="shipping_payment_info"></p>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="card-body p-0">
                        <table class="table" id="table_products">
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>Product</th>
                                <th>Quantity</th>
                                <th>Price</th>
                                <th>Total</th>
                            </tr>
                        </table>
                    </div>
                </div>

                <!-- textarea -->
                <div class="form-group">
                    <label>Notes</label>
                    <textarea class="form-control" id="shipping_notes" rows="3" placeholder="Notes..."></textarea>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="change_status">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Change order status</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form method="post" action="{{route('dashboard.orderChange')}}">
                    @csrf
                    <input type="hidden" name="orderID" id="orderID" value="" />
                    <div class="form-group">
                        <label>Select a status</label>
                        <select name="status" id="status" class="form-control"></select>
                        <br>
                        <center><input type="submit" class="btn btn-sm btn-primary" value="Change"></center>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script-footer')
<script>
$(document).ready(function() {
    var tb_bills = $('#tb_bills').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: "{!!route('dashboard.anyOrders')!!}",
            type: 'GET',
        },
        rowId: 'id',
        columns: [{
                data: 'id',
                name: 'id',
            },
            {
                data: 'created_at',
                name: 'created_at',
            },
            {
                data: 'title',
                name: 'title',
                orderable: false,
                searchable: false,
            },
            {
                data: 'payment',
                name: 'payment',
                orderable: false,
                searchable: false,
            },
            {
                data: 'total_amounts',
                name: 'total_amounts',
            }
        ]
    });

    Number.prototype.formatMoney = function(decPlaces, thouSeparator, decSeparator) {
        var n = this,
            decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces,
            decSeparator = decSeparator == undefined ? "." : decSeparator,
            thouSeparator = thouSeparator == undefined ? "," : thouSeparator,
            sign = n < 0 ? "-" : "",
            i = parseInt(n = Math.abs(+n || 0).toFixed(decPlaces)) + "",
            j = (j = i.length) > 3 ? j % 3 : 0;
        return sign + (j ? i.substr(0, j) + thouSeparator : "") + i.substr(j).replace(/(\d{3})(?=\d)/g,
            "$1" + thouSeparator) + (decPlaces ? decSeparator + Math.abs(n - i).toFixed(decPlaces)
            .slice(2) : "");
    };

    $(document).on('click', '#view', function(e) {
        e.preventDefault();
        var id = $(this).attr('data-bill');
        $('#orderId').text('');
        $('#shipping_name').text('');
        $('#shipping_address').text('');
        $('#shipping_county').text('');
        $('#shipping_city').text('');
        $('#shipping_country').text('');
        $('#shipping_phone').text('');
        $('#shipping_email').text('');
        $('#shipping_notes').text('');
        $('#shipping_payment').text('');
        $('#shipping_payment_info').text('');

        $('#detail').modal('show');
        $('#orderId').text(id);

        $.ajax({
            url: 'order/getCustomer/' + id,
            type: 'get',
            success: function(data) {
                $('#shipping_name').text(data.nicename);
                $('#shipping_address').text(data.address);
                $('#shipping_county').text(data.county);
                $('#shipping_city').text(data.city);
                $('#shipping_country').text(data.country);

                $('#shipping_phone').append("<a href='tel:" + data.phone + "'>" + data
                    .phone + "</a>");
                $('#shipping_email').append("<a href='mailto:" + data.email + "'>" + data
                    .email + "</a>");

                if (data.payment > 0)
                    $('#shipping_payment').text("VNPAY");
                else
                    $('#shipping_payment').text("Cash on delivery");
                $('#shipping_payment_info').text(data.payment_info);
                $('#shipping_notes').text(data.notes);
            }
        });

        $.ajax({
            url: 'order/getDetails/' + id,
            type: 'get',
            success: function(details) {
                $("#table_products tbody").find('td[name="record"]').each(function() {
                    $(this).parents("tr").remove();
                });

                $.each(details, function(key, detail) {
                    $('#table_products').append('<tr><td name="record">' + (key +
                            1) + '</td><td><a href="/products/' + detail.slug +
                        '"> ' + detail.name + '</a></td><td>' + detail
                        .quantity + '</td><td>$' + detail.price.formatMoney(2,
                            ',', '.') + '</td><td>$' + (detail.quantity * detail
                            .price).formatMoney(2, ',', '.') + '</td></tr>');
                });
            }
        });

    });

    $(document).on('click', '#change', function(e) {
        var order_id = $(this).attr('data-bill');
        var status = $(this).attr('data-status');
        $('#orderID').val('');
        $('#change_status').modal('show');
        $('#orderID').val(order_id);

        $.ajax({
            url: 'anyStatus',
            type: 'get',
            success: function(data) {
                $("#status option").remove();
                $.each(data, function(key, value) {
                    if (value.id == status)
                        $('#status').append($('<option value="' + value.id +
                            '" selected>' + value.title + '</option>'));
                    else
                        $('#status').append($('<option value="' + value.id + '">' +
                            value.title + '</option>'));
                });
            }
        });
    });
});
</script>
@endsection