@extends('layouts.dboard')

@section('page-header')
    <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0 text-dark">Edit Category</h1>
        </div>
    </div>
@endsection


@section('content')
    <link rel="stylesheet" href="{{asset('dboard/plugins/tinymce/custom_css.css')}}">
    <script type="text/javascript" src="https://cdn.tiny.cloud/1/k9y1yuun52fw46xsxp3l1pdost7wyn7kc1jxwc866jgweclj/tinymce/5/tinymce.min.js"></script>

    <div class="row">
        <div class="col-md-8 offset-md-2">d
            <div class="col-wrap">
                <div class="form-wrap">
                    <form action="{{route('dashboard.category.update')}}" method="post" class="form-horizontal">
                        @csrf
                        <input type="hidden" name="category_type" value="{{$category->type}}"/>
                        <input type="hidden" name="category_id" value="{{$category->id}}" />
                        <input type="hidden" name="edit_type" value="edit" />
                        <div class="form-group row">
                            <label for="category_name" class="col-sm-4">Name</label>
                            <div class="col-sm-8">
                                <input name="category_name" id="category_name" type="text" class="form-control"
                                       value="{{$category->name}}" maxlength="50" required autofocus>
                                <p class="description">The name is how it appears on your site.</p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="category_slug" class="col-sm-4">Slug</label>
                            <div class="col-sm-8">
                                <input name="category_slug" id="category_slug" type="text" class="form-control"
                                       value="{{$category->slug}}" maxlength="50">
                                <p class="description">The 'slug' is the URL-friendly version of the name. It is usually all lowercase and contains only letters, numbers, and hyphens.</p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="category_parent" class="col-sm-4">Parent Category</label>
                            <div class="col-sm-8">
                                <select name="category_parent" id="category_parent" class="form-control">
                                    <option value="0">None</option>
                                    {!! Helpers::select_categories_hierarchy($categories, $category->parent, 0, 0, 'edit') !!}
                                </select>
                                <p class="description">Categories can have a hierarchy.</p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="category_description" class="col-sm-4">Description</label>
                            <div class="col-sm-8">
                                <textarea name="category_description" id="category_description" class="form-control" rows="5">{{$category->description}}</textarea>
                                <p class="description">The description is not prominent by default; however, some themes may show it.</p>
                            </div>
                        </div>

                        @if($category->type == "product")
                            <div class="form-group row">
                                <label for="category_top_content" class="col-sm-4">Top Content</label>
                                <div class="col-sm-8">
                                    <textarea name="category_top_content" id="category_top_content" class="form-control" rows="5">{{htmlspecialchars_decode($category->top_content)}}</textarea>
                                    <p class="description">Enter a value for this field. This will be displayed at top of the category.</p>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="category_bottom_content" class="col-sm-4">Bottom Content</label>
                                <div class="col-sm-8">
                                    <textarea name="category_bottom_content" id="category_bottom_content" class="form-control" rows="5">{{htmlspecialchars_decode($category->bottom_content)}}</textarea>
                                    <p class="description">Enter a value for this field. This will be displayed at bottom of the category.</p>
                                </div>
                            </div>
                        @endif

                        <div class="form-group row">
                            <label for="position" class="col-sm-4">Sorting</label>
                            <div class="col-sm-8">
                                <input name="position" id="position" type="text" class="form-control" value="{{$category->orderby}}">
                                <p class="description">Sort by descending value. That is, the higher the value will be arranged first.</p>
                            </div>
                        </div>

                        @if($category->type == "product")
                            <div class="form-group">
                                <label for="category_thumbnail" class="col-sm-4">Thumbnail</label>
                                <input type="hidden" id="category_thumbnail" name="category_thumbnail" value="{{!empty($category->thumbnail) ? asset('storage/' . $category->thumbnail) : asset('storage/placeholder.png')}}" />
                                <div class="col-sm-8">
                                    <div id="product_cat_thumbnail" style="margin-right: 10px; float: right;">
                                        <img src="{{!empty($category->thumbnail) ? asset('storage/' . $category->thumbnail) : asset('storage/placeholder.png')}}" width="60px" height="60px">
                                        <button type="button" id="upload_image_button">Upload/Add image</button>
                                        <button type="button" id="remove_image_button" style="display: none;">Remove</button>
                                    </div>
                                </div>
                            </div>
                        @endif
                        <div class="form-group row clearfix">
                            <div class="col-sm-4 offset-md-4" style="margin: 100px auto 0;">
                                <button type="submit" class="btn btn-primary">Update</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script-footer')
    <script type="text/javascript">
        tinymce.init({
            selector: '#category_top_content',
            language : 'en',
            menubar: false,
            toolbar_sticky: true,
            toolbar_drawer: 'floating',
            height: 300,
            convert_urls: false,
            image_title: true,
            image_caption: true,
            image_advtab: true,
            image_class_list: [
                {title: 'None', value: ''},
            ],
            external_filemanager_path: "/dboard/plugins/filemanager/",
            filemanager_title: 'File Manager',
            plugins: 'lists fullscreen preview image imagetools link media code paste table quickbars hr pagebreak searchreplace autolink filemanager responsivefilemanager',
            toolbar: 'fullscreen code preview | bold italic underline | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent | numlist bullist | forecolor backcolor formatpainter removeformat | responsivefilemanager image media | link table pagebreak hr',

            external_plugins: {
                "responsivefilemanager": "{{asset('dboard/plugins/tinymce/plugins/responsivefilemanager/plugin.min.js')}}",
                "filemanager": "{{asset('dboard/plugins/filemanager/plugin.min.js')}}"
            },

            audio_template_callback: function(data) {
                return '<audio controls>' + '\n<source src="' + data.source1 + '"' + (data.source1mime ? ' type="' + data.source1mime + '"' : '') + ' />\n' + '</audio>';
            },
            video_template_callback: function(data) {
                return '<video width="' + data.width + '" height="' + data.height + '"' + (data.poster ? ' poster="' + data.poster + '"' : '') + ' controls="controls">\n' + '<source src="' + data.source1 + '"' + (data.source1mime ? ' type="' + data.source1mime + '"' : '') + ' />\n' + (data.source2 ? '<source src="' + data.source2 + '"' + (data.source2mime ? ' type="' + data.source2mime + '"' : '') + ' />\n' : '') + '</video>';
            },
        });

        tinymce.init({
            selector: '#category_bottom_content',
            language : 'en',
            menubar: false,
            toolbar_sticky: true,
            toolbar_drawer: 'floating',
            height: 300,
            convert_urls: false,
            image_title: true,
            image_caption: true,
            image_advtab: true,
            image_class_list: [
                {title: 'None', value: ''},
            ],
            external_filemanager_path: "/dboard/plugins/filemanager/",
            filemanager_title: 'File Manager',
            plugins: 'lists fullscreen preview image imagetools link media code paste table quickbars hr pagebreak searchreplace autolink filemanager responsivefilemanager',
            toolbar: 'fullscreen code preview | bold italic underline | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent | numlist bullist | forecolor backcolor formatpainter removeformat | responsivefilemanager image media | link table pagebreak hr',

            external_plugins: {
                "responsivefilemanager": "{{asset('dboard/plugins/tinymce/plugins/responsivefilemanager/plugin.min.js')}}",
                "filemanager": "{{asset('dboard/plugins/filemanager/plugin.min.js')}}"
            },

            audio_template_callback: function(data) {
                return '<audio controls>' + '\n<source src="' + data.source1 + '"' + (data.source1mime ? ' type="' + data.source1mime + '"' : '') + ' />\n' + '</audio>';
            },
            video_template_callback: function(data) {
                return '<video width="' + data.width + '" height="' + data.height + '"' + (data.poster ? ' poster="' + data.poster + '"' : '') + ' controls="controls">\n' + '<source src="' + data.source1 + '"' + (data.source1mime ? ' type="' + data.source1mime + '"' : '') + ' />\n' + (data.source2 ? '<source src="' + data.source2 + '"' + (data.source2mime ? ' type="' + data.source2mime + '"' : '') + ' />\n' : '') + '</video>';
            },
        });

        $(document).ready(function() {
            $('#upload_image_button').click(function() {
                selectFile('upload_image_button');
            });

            $('#remove_image_button').click(function() {
                $(this).attr('style', 'display: none');
                $('#category_thumbnail').val('/storage/placeholder.png');
                $('#upload_image_button').val('/storage/placeholder.png');
                $('#product_cat_thumbnail img').attr('src', '/storage/placeholder.png');
            });

            function selectFile(elementId) {
                CKFinder.modal({
                    skin: "neko",
                    resourceType: 'Images',
                    chooseFiles: true,
                    chooseFilesOnDblClick: true,
                    width: 800,
                    height: 600,
                    onInit: function(finder) {
                        finder.on('files:choose', function(evt) {
                            var file = evt.data.files.first();
                            var output = document.getElementById(elementId);
                            output.value = file.getUrl();
                            updateFile(file.getUrl(), file.get('name'));
                            $("#remove_image_button").attr('style', 'display: inline-block');
                        });

                        finder.on('file:choose:resizedImage', function(evt) {
                            var output = document.getElementById(elementId);
                            output.value = evt.data.resizedUrl;
                            updateFile(evt.data.resizedUrl, file.get('name'));
                            $("#remove_image_button").attr('style', 'display: inline-block');
                        });
                    }
                });
            }
            function updateFile(url, name) {
                $('#category_thumbnail').val(url);
                $('#product_cat_thumbnail img').attr('src', url);
            }
        });
    </script>
@endsection

