@extends('layouts.dboard')

@section('page-header')
<div class="row mb-2">
    <div class="col-sm-6">
        <h1 class="m-0 text-dark">Edit {{$term->name}}</h1>
    </div>
</div>
@endsection


@section('content')

<div class="row">
    <div class="col-md-8 offset-md-2">
        <div class="col-wrap">
            <div class="form-wrap">
                <form action="{{route('dashboard.term.update')}}" method="post" class="form-horizontal">
                    @csrf
                    <input type="hidden" name="edit_type" id="edit_type" value="edit">
                    <input type="hidden" name="term_id" id="term_id_edit" value="{{$term->id}}">
                    <div class="form-group row">
                        <label for="term_name" class="col-sm-4">Name</label>
                        <div class="col-sm-8">
                            <input name="term_name" id="term_name" type="text" class="form-control"
                                value="{{$term->name}}" maxlength="28" required autofocus>
                            <p class="description">The name is how it appears on your site.</p>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="attribute_slug" class="col-sm-4">Slug</label>
                        <div class="col-sm-8">
                            <input name="term_slug" id="term_slug" type="text" class="form-control"
                                value="{{$term->slug}}" maxlength="28">
                            <p class="description">The “slug” is the URL-friendly version of the name. It is usually all lowercase and contains only letters, numbers, and hyphens. Unique slug/reference for the attribute; must be no more than 28
                                characters.</p>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="attribute_slug" class="col-sm-4">Description</label>
                        <div class="col-sm-8">
                            <textarea name="term_description" id="term_description" class="form-control" rows="5">{{$term->description}}</textarea>
                            <p class="description">The description is not prominent by default; however, some themes may show it.</p>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-4 offset-md-4">
                            <button type="submit" class="btn btn-primary">Update</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
@endsection
