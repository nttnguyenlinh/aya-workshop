@extends('layouts.dboard')

@section('page-header')
<div class="row mb-2">
    <div class="col-sm-6">
        <h1 class="m-0 text-dark">Product {{$attribute->name}}</h1>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-md-5">
        <div class="col-wrap">
            <div class="form-wrap">
                <p>Attribute terms can be assigned to products and variations.</p>
                <p class="text-danger" style="font-size:15px; font-weight:600;">Note: Deleting a term will remove it from all products and variations to which it has been assigned. Recreating a term will not automatically assign it back to products.</p>
                <h2>Add new {{$attribute->name}}</h2>
                <form action="{{route('dashboard.terms.store')}}" method="post">
                    @csrf
                    <input type="hidden" name="attribute_id" id="attribute_id" value="{{$attribute->id}}">
                    <div class="form-group">
                        <label for="term_name">Name</label>
                        <input name="term_name" id="term_name" type="text" class="form-control" value="" maxlength="28" required autofocus>
                        <p class="description">The name is how it appears on your site.</p>
                    </div>

                    <div class="form-group">
                        <label for="term_slug">Slug</label>
                        <input name="term_slug" id="term_slug" type="text" class="form-control" value="" maxlength="28">
                        <p class="description">The “slug” is the URL-friendly version of the name. It is usually all lowercase and contains only letters, numbers, and hyphens. Unique slug/reference for the attribute; must be no more than 28
                            characters.</p>
                    </div>

                    <div class="form-group">
                        <label for="term_description">Description</label>
                        <textarea name="term_description" id="term_description" class="form-control" rows="5"></textarea>
                        <p class="description">The description is not prominent by default; however, some themes may show it.</p>
                    </div>

                    <button type="submit" class="btn btn-primary">Add new {{$attribute->name}}</button>
                </form>
            </div>
        </div>
    </div>

    <div class="col-md-7">
        <button type="button" id="btnDelete" class="btn btn-danger mb-1">Delete</button>
        <div class="card card-outline card-primary">
            <div class="card-body p-0">
                <table id="tbl_terms" class="table widefat table-sm terms-table">
                    <thead>
                        <tr>
                            <th></th>
                            <th style="width: 170px !important;">Name</th>
                            <th>Description</th>
                            <th>Slug</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_term_edit" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="card card-info">
                <div class="card-header">
                    <h3 class="card-title">Quick Edit for <strong><span class="btn btn-info" style="color:white;" id="span_term_name"></span></strong>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </h3>
                </div>
                <div class="card-body">
                    <form>
                        <input type="hidden" name="term_id" id="term_id_edit" value="">
                        <div class="form-group">
                            <label for="term_name">Name</label>
                            <input name="term_name" id="term_name_edit" type="text" class="form-control" value="" maxlength="28" required autofocus>
                        </div>

                        <div class="form-group">
                            <label for="term_slug">Slug</label>
                            <input name="term_slug" id="term_slug_edit" type="text" class="form-control" value="" maxlength="28">
                        </div>
                        <input type="hidden" name="term_description" id="term_description_edit" value="">
                        <center>
                            <button class="btn btn-danger" data-dismiss="modal" style="margin-right:20px;">Close</button>
                            <button type="button" class="btn btn-success" id="btn_term_edit">Update</button>
                        </center>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script-footer')
    <script>
        $(document).ready(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var attribute_id = $('#attribute_id').val();

            //Load data
            var tbl_terms = $('#tbl_terms').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: {
                    url: '/dashboard/terms/list/' + attribute_id,
                    type: 'get',
                },
                columnDefs: [
                    {
                        targets: 0,
                        checkboxes: {
                            selectRow: true
                        }
                    }
                ],
                select: {
                    style: 'multi'
                },
                columns: [
                    {
                        data: 'id',
                        name: 'id',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'name',
                        name: 'name',
                    },
                    {
                        data: 'description',
                        name: 'description',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'slug',
                        name: 'slug',
                        orderable: false,
                        searchable: false
                    }
                ],
                order: [1, 'asc'],
            });

            $('#tbl_terms tbody').on( 'click', 'tr', function () {
                $(this).toggleClass('selected');
            });
            //Quick edit
            $(document).on('click', '#quickEdit', function() {
                var term_id = $(this).attr('data-id');

                $.ajax({
                    url: '/dashboard/term/' + term_id,
                    type: 'get',
                    dataType: 'json',
                    success: function(data){
                        $('#span_term_name').text('Term: ' + data.name);
                        $('#term_id_edit').val(data.id);
                        $('#term_name_edit').val(data.name);
                        $('#term_slug_edit').val(data.slug);
                        $('#term_description_edit').val(data.description);
                        $('#modal_term_edit').modal();
                    }
                });
            });
            //Update Quick edit
            $(document).on('click', '#btn_term_edit', function() {
                var term_id =  $('#term_id_edit').val();
                var term_name =  $('#term_name_edit').val();
                var term_slug =  $('#term_slug_edit').val();
                var term_description =  $('#term_description_edit').val();

                $.ajax({
                    url: '/dashboard/term',
                    type: 'post',
                    dataType: 'json',
                    data: {edit_type: 'quick', term_id: term_id, term_name: term_name, term_slug: term_slug, term_description: term_description},
                    success: function(data){
                        $('#tbl_terms').DataTable().ajax.reload();
                        switch(data)
                        {
                            case 'success':
                                Swal.fire({
                                    title: "Updated!",
                                    html: "<p class='text-success'> The term has been updated!</p>",
                                    type: "success",
                                });
                                break;
                            case 'unique':
                                Swal.fire('Oh no...!', 'The Term is already in use. Change it, please.', 'error');
                                break;
                            default:
                                Swal.fire('Oh no...!', 'Can not update!', 'error');
                        }
                        $('#modal_term_edit').modal('toggle');
                    }
                });
            });

            //multi delete
            $(document).on('click', '#btnDelete', function(e) {
                var rows_selected = tbl_terms.column(0).checkboxes.selected();
                var terms_id = [];
                // Iterate over all selected checkboxes
                $.each(rows_selected, function(index, rowId){
                    terms_id.push(rowId);
                });

                Swal.fire({
                    title: 'Are you sure?',
                    text: 'You will not be able to recover!',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#d33',
                    cancelButtonColor: '#3085d6',
                    cancelButtonText: 'No, cancel it!',
                    confirmButtonText: 'Yes, I am sure!',
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            url: '/dashboard/terms/delete',
                            type: 'delete',
                            dataType: 'json',
                            data: {
                                terms_id: terms_id,
                                _method: 'delete'
                            },
                            success: function(data) {
                                $('#tbl_terms').DataTable().ajax.reload();
                                if(data.errors > 0)
                                    Swal.fire({
                                        title: "Deleted!",
                                        html: "<p class='text-success'>" + data.success + " items has been deleted! </p><p class='text-danger'>" + data.errors + " items Can not delete!</p>",
                                        type: "error",
                                    });
                                else
                                    Swal.fire({
                                        title: "Deleted!",
                                        html: "<p class='text-success'>" + data.success + " items has been deleted! </p>",
                                        type: "success",
                                    });
                            }
                        });
                    } else if (result.dismiss === Swal.DismissReason.cancel) {
                        Swal.fire('Canceled!', 'Have been canceled', 'info')
                    }
                });
            });

            //delete
            $(document).on('click', '#delete', function(e) {
                e.preventDefault();
                var terms_id = [];
                terms_id.push($(this).attr('data-id'));

                Swal.fire({
                    title: 'Are you sure?',
                    text: 'You will not be able to recover!',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#d33',
                    cancelButtonColor: '#3085d6',
                    cancelButtonText: 'No, cancel it!',
                    confirmButtonText: 'Yes, I am sure!',
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            url: '/dashboard/terms/delete',
                            type: 'delete',
                            dataType: 'json',
                            data: {
                                terms_id: terms_id,
                                _method: 'delete'
                            },
                            success: function(data) {
                                $('#tbl_terms').DataTable().ajax.reload();
                                if(data.errors > 0)
                                    Swal.fire({
                                        title: "Deleted!",
                                        html: "<p class='text-danger'> The term can not delete!</p>",
                                        type: "error",
                                    });
                                else
                                    Swal.fire({
                                        title: "Deleted!",
                                        html: "<p class='text-success'> The term has been deleted! </p>",
                                        type: "success",
                                    });
                            }
                        });
                    } else if (result.dismiss === Swal.DismissReason.cancel) {
                        Swal.fire('Canceled!', 'Have been canceled', 'info')
                    }
                })
            });
        });
    </script>
@endsection
