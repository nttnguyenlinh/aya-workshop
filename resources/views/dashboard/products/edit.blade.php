@extends('layouts.dboard')

@section('page-header')
<div class="row mb-2">
    <div class="col-sm-6">
        <h1 class="m-0 text-dark">Edit Product</h1>
    </div><!-- /.col -->
</div><!-- /.row -->
@endsection

@section('content')
<section class="content">
    <div class="container-fluid">
        <form id="myform" method="post" action="{{route('dashboard.product.update')}}" role="form" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="product_id" id="product_id" value="{{$product->id}}" />
            <div class="row">
                <div class="col-md-3">
                    <a href="{{route('dashboard.products')}}" class="btn btn-primary btn-block mb-3">Back to Products</a>
                    <div class="card card-primary card-outline collapsed-card">
                        <div class="card-header" data-widget="collapse">
                            <h3 class="card-title">Public</h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fal fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body p-0">
                            <div class="row" style="margin:3px;">
                                <div class="form-group" style="width:100%; padding-top:10px;">
                                    <div class="input-group">
                                        <div class="col-lg-6">
                                            <input type="radio" name="status" id="status" value="1" {{($product->status == 1)?'checked':''}}> Public
                                        </div>

                                        <div class="col-lg-6">
                                            <input type="radio" name="status" id="status" value="0" {{($product->status == 0)?'checked':''}}> Pending Review
                                        </div>
                                    </div>
                                    <!-- /.input group -->
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /. box -->

                    <div class="card card-primary card-outline collapsed-card">
                        <div class="card-header" data-widget="collapse">
                            <h3 class="card-title">Category</h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fal fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body p-0">
                            <select name="category" id="category" class="form-control" style="width:98%; height:50px; margin:3px;" required>
                                @foreach($category as $item)
                                <option value="{{$item->id}}" {{($product->category_id == $item->id)?'selected':''}}>{{$item->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <!-- /.card-body -->
                    </div>

                    <div class="card card-primary card-outline">
                        <div class="card-header" data-widget="collapse">
                            <h3 class="card-title">Thumbnail</h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fal fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body p-0">
                            <input type="hidden" id="thumb" name="thumbnail" value="{{!empty($product->thumbnail) ? asset('storage/' . $product->thumbnail) : ''}}"/>

                            <input type="button" id="thumbnail"  class="dropify"
                                data-show-loader="true" data-show-errors="false"
                                data-default-file="{{!empty($product->thumbnail) ? asset('storage/' . $product->thumbnail) : ''}}"
                                value="{{!empty($product->thumbnail) ? asset('storage/' . $product->thumbnail) : ''}}" data-height="90" />
                        </div>
                        <!-- /.card-body -->
                    </div>

                    <div class="card card-primary card-outline">
                        <div class="card-header" data-widget="collapse">
                            <h3 class="card-title">Price</h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fal fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body p-0">
                            <div class="row" style="margin:3px;">
                                <div class="form-group" style="width:100%; padding-top:10px;">
                                    <label>Price / Sale:</label>
                                    <div class="input-group">
                                        <div class="col-lg-6">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"><i class="fal fa-dollar-sign"></i></span>
                                                </div>
                                                <input name="price" id="price" value="{{$product->price}}" required class="form-control mask">
                                            </div>
                                        </div>

                                        <div class="col-lg-6">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"><i class="fal fa-dollar-sign"></i></span>
                                                </div>
                                                <input name="sale" id="sale" value="{{$product->sale}}" class="form-control mask">
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.input group -->
                                </div>

                                <div id="discount_datetime" class="form-group" style="width:100%; margin:3px; padding-top:10px; display: none;">
                                    <label>Discount time:</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fal fa-calendar"></i></span>
                                        </div>
                                        <input type="text" name="discounttime" id="discounttime" value="{{$product->sale_date_start}} - {{$product->sale_date_end}}" class="form-control float-right">
                                    </div>
                                    <!-- /.input group -->
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>

                    <div class="card card-primary card-outline">
                        <div class="card-header" data-widget="collapse">
                            <h3 class="card-title">Description</h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            </div>
                        </div>
                        <div class="card-body p-0">
                            <textarea id="description" name="description" class="form-control" placeholder="Write description here..." style="width:98%; height:200px; margin:3px;" required>{{$product->description}}</textarea>
                        </div>
                        <!-- /.card-body -->
                    </div>

                    <div class="card card-primary card-outline collapsed-card">
                        <div class="card-header" data-widget="collapse">
                            <h3 class="card-title">Option</h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fal fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body p-0">
                            <div class="row" style="margin:3px;">
                                <div class="form-group" style="width:100%; padding-top:10px;">
                                    <div class="input-group">
                                        <div class="col-lg-6">
                                            <label>Featured:</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" style="margin:1px;">
                                                        <input type="radio" name="featured" id="featured" value="1" {{($product->featured == 1)?'checked':''}}> Yes
                                                    </span>
                                                    <span class="input-group-text" style="margin:1px;">
                                                        <input type="radio" name="featured" id="featured" value="0" {{($product->featured == 0)?'checked':''}}> No
                                                    </span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-6">
                                            <label>In-stock:</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" style="margin:1px;">
                                                        <input type="radio" name="stock" id="stock" value="1" {{($product->stock == 1)?'checked':''}}> Yes
                                                    </span>
                                                    <span class="input-group-text" style="margin:1px;">
                                                        <input type="radio" name="stock" id="stock" value="0" {{($product->stock == 0)?'checked':''}}> No
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.input group -->
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /. box -->
                    <!-- /.card -->
                </div>
                <!-- /.col -->
                <div class="col-md-9">
                    <div class="card card-primary card-outline">
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="form-group">
                                <input type="text" class="form-control" name="title" id="title" placeholder="Enter product name..." value="{{$product->name}}" required autofocus>
                            </div>
                            <div class="form-group">
                                <textarea id="details" name="details" class="form-control" placeholder="Write details here ..." required>{{$product->details}}</textarea>
                                <script>
                                    CKEDITOR.replace('details', {height: 575});
                                </script>
                            </div>

                            <div class="form-group">
                                <div class="dropzone" id="myDropzone">
                                    <!-- @foreach($images as $image)
                                    <div class="dz-preview dz-image-preview">
                                        <div class="dz-image">
                                            <img alt="{{$image->title}}" src="{{asset($image->path)}}" style="width:120px; height:120px;">
                                        </div>
                                        <div class="dz-details">
                                            <div class="dz-filesize">
                                                <span>{{$image->size}}</span>
                                            </div>
                                            <div class="dz-filename">
                                                <span>{{$image->title}}</span>
                                            </div>
                                        </div>
                                        <a class="dz-remove" href="javascript:undefined;" id="img-remove" data-title="{{$image->title}}">x</a>
                                    </div>
                                    @endforeach -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <div class="float-right">
                            <button id="uploads_images" class="btn btn-info"><i class="fal fa-arrow-square-up"></i>Upload</button>
                            <button type="submit" class="btn btn-success" id="public"><i class="fal fa-arrow-square-right"></i>Public...</button>
                        </div>
                        <button id="reset" class="btn btn-default"><i class="fa fa-times"></i> Clear</button>
                    </div>
                </div>
            </div>
    </form>
    </div>
</section>
@endsection

@section('script-footer')
<script>
    $(document).ready(function() {
        $('#reset').click(function(e) {
            $('#myform').find('input, textarea').not(':button, :submit').val('');
            if ($(CKEDITOR.instances).length)
                for (var key in CKEDITOR.instances) {
                    var instance = CKEDITOR.instances[key];
                    if ($(instance.element.$).closest('form').attr('name') == $(e.target).attr('name'))
                        instance.setData(instance.element.$.defaultValue);
                }
        });

        $(".mask").inputmask({
            'alias': 'decimal',
            'rightAlign': true,
            'groupSeparator': ',',
            'digits': 2,
            'digitsOptional': false,
            'placeholder': '0',
            'autoGroup': true
        });

        $('input[name=sale]').bind("keyup change", function() {
            //$('#discount_datetime').toggle($(this).val().length);
            if ($.trim($(this).val()) != '0.00')
                $('#discount_datetime').show();
            else
                $('#discount_datetime').hide();
        });
        $('input[name="discounttime"]').daterangepicker({
            timePicker: true,
            startDate: moment().startOf('hour'),
            endDate: moment().startOf('hour').add(32, 'hour'),
            minDate: '2019-01-01',
            maxDate: '2036-01-01',
            dateLimit: {days: 60},
            showDropdowns: true,
            showWeekNumbers: true,
            timePicker24Hour: true,
            locale: {format: 'YYYY-MM-DD hh:mm:ss'}
        });

        $('#thumbnail').click(function() {
            selectFile('thumbnail');
        });

        function selectFile(elementId) {
            CKFinder.modal({
                skin: "neko",
                resourceType: 'Images',
                chooseFiles: true,
                chooseFilesOnDblClick: true,
                width: 800,
                height: 600,
                onInit: function(finder) {
                    finder.on('files:choose', function(evt) {
                        var file = evt.data.files.first();
                        var output = document.getElementById(elementId);
                        output.value = file.getUrl();
                        updateFile(file.getUrl(), file.get('name'));
                        $(".dropify-clear").attr('style', 'display:block;');
                    });

                    finder.on('file:choose:resizedImage', function(evt) {
                        var output = document.getElementById(elementId);
                        output.value = evt.data.resizedUrl;
                        updateFile(evt.data.resizedUrl, file.get('name'));
                        $(".dropify-clear").attr('style', 'display:block;');
                    });
                }
            });
        };

        function updateFile(url, name) {
            $('#thumb').val(url);
            $('#thumbnail').attr('data-default-file', url);
            $(".dropify-preview").attr('style', 'display:block;');
            $('.dropify-render img').attr('src', url);
            $('.dropify-filename-inner').text(name);
            //$(".dropify-infos").remove();
        };

        $('.dropify').dropify({
            messages: {
                'default': '',
                'replace': '',
                'remove': 'Remove',
                'error': ''
            },
        });

        $('.dropify').dropify().on('dropify.afterClear', function(event, element){
            $('#thumb').val('');
            $('#thumbnail').attr('data-default-file', '');
            $('.dropify-render').append('<img src="">');
            $('.dropify-filename-inner').text('');
            $(".dropify-clear").attr('style', 'display:none;');
        });

        if($('#thumb').val().length <= 0){
            $(".dropify-clear").attr('style', 'display:none');
            $('.dropify-render').append('<img src="">');
        }
    });

    Dropzone.options.myDropzone = {
        url: "{{route('dashboard.images.upload')}}",
        headers: {'X-CSRF-TOKEN': '{!! csrf_token() !!}'},
        autoProcessQueue: false,
        uploadMultiple: true,
        parallelUploads: 5,
        maxFiles: 5,
        maxFilesize: 3,
        acceptedFiles: 'image/*',
        addRemoveLinks: true,
        dictRemoveFile: "x",
        dictCancelUpload: "",
        dictFileTooBig: "Image is bigger than 3MB",
        dictDefaultMessage: "Drop images here to upload",
        dictMaxFilesExceeded: "Can't upload any more images",
        dictInvalidFileType: "Can't upload files of this type",
        init: function() {
            myDropzone = this;
            var product_id = $("#product_id").val();
            var submitButton = document.querySelector("#uploads_images");

            $.ajax({
                url: "/dashboard/products/images/" + product_id,
                type: "get",
                success: function(data){
                    if(Object.keys(data).length > 0)
                    {
                        $.each(data, function(i, item) {
                            var mockFile = {name: item.path, size: item.bytes_size};
                            myDropzone.emit("addedfile", mockFile);
                            myDropzone.emit("thumbnail", mockFile, "/storage/" + item.path);
                            myDropzone.emit("complete", mockFile);
                            myDropzone.files.push(mockFile);
                        });
                    }
                }
            });

            submitButton.addEventListener("click", function(e) {
                e.preventDefault();
                myDropzone.processQueue();
            });

            this.on('sendingmultiple', function(data, xhr, formData) {
                formData.append("product_id", $("#product_id").val());
            });

            this.on("success", function(file) {
                toastr["success"]("Upload completed!");
            });

            this.on("removedfile", function(file) {
                $.ajax({
                    url: "{{route('dashboard.images.remove')}}",
                    type: 'post',
                    //headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
                    headers: {'X-CSRF-TOKEN': '{!! csrf_token() !!}'},
                    data: {name: file.name},
                    success: function(data) {
                        switch(data) {
                            case 'success':
                                toastr["success"]("Image has been removed!");
                                myDropzone.removeFile(file);
                                //location.reload().delay(1000);
                                break;
                            case 'error':
                                toastr["error"]("Oh no...! Can't remove!");
                                break;
                            default:
                                myDropzone.removeFile(file);
                        }
                    }
                });
            });
        },
    };
</script>
@endsection
