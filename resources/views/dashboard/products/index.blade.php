@extends('layouts.dboard')

@section('page-header')
<div class="row mb-2">
    <div class="col-sm-6">
        <h1 class="m-0 text-dark">Products</h1>
    </div>
    <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item">
                <a href="{{route('dashboard.product.create')}}" class="btn btn-default">
                    <i class="fal fa-plus-octagon"></i> Add new
                </a>
            </li>
        </ol>
    </div>
</div>
@endsection


@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card card-primary card-outline">
            <div class="card-header">
                <button type="button" id="btnDelete" class="btn btn-danger mb-1">Delete</button> | <button type="button" id="btnTrash" class="btn btn-secondary mb-1">Move to trash</button>
                <span class="panel-filter-right float-right"><a href="javascript:void(0)" id="btnAll" data-visibility="100">All ({{Helpers::get_count_product_via_visibility(100)}})</a> | <a href="javascript:void(0)" id="btnPublished" data-visibility="1">Published ({{Helpers::get_count_product_via_visibility(1)}})</a> | <a href="javascript:void(0)" id="btnDraft" data-visibility="-1">Draft ({{Helpers::get_count_product_via_visibility(-1)}})</a> | <a href="javascript:void(0)" id="btnPending" data-visibility="0">Pending ({{Helpers::get_count_product_via_visibility(0)}})</a> </span>
            </div>

            <div class="card-body pad table-responsive">
                <table id="tb_products" class="table table-striped table-hover table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th></th>
                            <th width="50px"><i class="fal fa-image"></i></th>
                            <th>Name</th>
                            <th>SKU</th>
                            <th>Price</th>
                            <th>Categories</th>
                            <th>Tags</th>
                            <th width="20"><i class="fas fa-star"></i></th>
                            <th>Date</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script-footer')
<script>
        $(document).ready(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            //Load default
            loadData("");

            //Load All
            $(document).on('click', '#btnAll', function(e) {
                loadData(100);
            });

            //Load Published
            $(document).on('click', '#btnPublished', function(e) {
                loadData($(this).attr('data-visibility'));
            });

            //Load Draft
            $(document).on('click', '#btnDraft', function(e) {
                loadData($(this).attr('data-visibility'));
            });

            //Load Pending
            $(document).on('click', '#btnPending', function(e) {
                loadData($(this).attr('data-visibility'));
            });

            //Load data
            function loadData(visibility){
                //Destroy - renew tables
                $('#tb_products').DataTable().destroy();

                $('#tb_products').DataTable({
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    ordering: true,
                    ajax: {
                        url: "{{route('dashboard.products.list')}}",
                        type: 'GET',
                        data: {
                            visibility: visibility,
                        },
                    },
                    columnDefs: [
                        {
                            targets: 0,
                            checkboxes: {
                                selectRow: true
                            }
                        }
                    ],
                    select: {
                        style: 'multi'
                    },
                    columns: [
                        {
                            data: 'id',
                            name: 'id',
                        },
                        {
                            data: 'thumbnail',
                            name: 'thumbnail',
                            orderable: false,
                            searchable: false
                        },
                        {
                            data: 'name',
                            name: 'name',
                        },
                        {
                            data: 'sku',
                            name: 'sku',
                        },
                        {
                            data: 'price',
                            name: 'price',
                        },
                        {
                            data: 'categories',
                            name: 'categories',
                            orderable: false
                        },
                        {
                            data: 'tags',
                            name: 'tags',
                            orderable: false
                        },
                        {
                            data: 'featured',
                            name: 'featured',
                            orderable: false,
                            searchable: false
                        },
                        {
                            data: 'date',
                            name: 'date',
                            orderable: false,
                            searchable: false
                        }
                    ],
                    order: []
                });
            }

            //multi delete
            $(document).on('click', '#btnDelete', function(e) {
                var rows_selected = $('#tb_products').DataTable().column(0).checkboxes.selected();
                var products_id = [];
                // Iterate over all selected checkboxes
                $.each(rows_selected, function(index, rowId){
                    products_id.push(rowId);
                });

                Swal.fire({
                    title: 'Are you sure?',
                    text: 'You will not be able to recover!',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#d33',
                    cancelButtonColor: '#3085d6',
                    cancelButtonText: 'No, cancel it!',
                    confirmButtonText: 'Yes, I am sure!',
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            url: '/dashboard/product/delete',
                            type: 'delete',
                            dataType: 'json',
                            data: {
                                products_id: products_id,
                                _method: 'delete'
                            },
                            success: function(data) {
                                $('#tb_products').DataTable().ajax.reload();
                                $('#tb_products').DataTable().rows().deselect();
                                if(data.errors > 0)
                                    Swal.fire({
                                        title: "Deleted!",
                                        html: "<p class='text-success'>" + data.success + " items has been deleted! </p><p class='text-danger'>" + data.errors + " items Can not delete!</p>",
                                        type: "error",
                                    });
                                else
                                    Swal.fire({
                                        title: "Deleted!",
                                        html: "<p class='text-success'>" + data.success + " items has been deleted! </p>",
                                        type: "success",
                                    });
                            }
                        });
                    } else if (result.dismiss === Swal.DismissReason.cancel) {
                        $('#tb_products').DataTable().ajax.reload();
                        $('#tb_products').DataTable().rows().deselect();
                        Swal.fire('Canceled!', 'Have been canceled', 'info')
                    }
                });
            });

            //multi trash
            $(document).on('click', '#btnTrash', function(e) {
                var rows_selected = $('#tb_products').DataTable().column(0).checkboxes.selected();
                var products_id = [];
                // Iterate over all selected checkboxes
                $.each(rows_selected, function(index, rowId){
                    products_id.push(rowId);
                });

                Swal.fire({
                    title: 'Are you sure?',
                    text: 'This is a replacement delete function, the data will be reused if you want!',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#d33',
                    cancelButtonColor: '#3085d6',
                    cancelButtonText: 'No, cancel it!',
                    confirmButtonText: 'Yes, I am sure!',
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            url: '/dashboard/product/trash',
                            type: 'post',
                            dataType: 'json',
                            data: {
                                products_id: products_id,
                            },
                            success: function(data) {
                                $('#tb_products').DataTable().ajax.reload();
                                $('#tb_products').DataTable().rows().deselect();
                                if(data.errors > 0)
                                    Swal.fire({
                                        title: "Move to trash!",
                                        html: "<p class='text-success'>" + data.success + " items has been move to trash! </p><p class='text-danger'>" + data.errors + " items Can not move to trash!</p>",
                                        type: "error",
                                    });
                                else
                                    Swal.fire({
                                        title: "Move to trash!",
                                        html: "<p class='text-success'>" + data.success + " items has been move to trash! </p>",
                                        type: "success",
                                    });
                            }
                        });
                    } else if (result.dismiss === Swal.DismissReason.cancel) {
                        $('#tb_products').DataTable().ajax.reload();
                        $('#tb_products').DataTable().rows().deselect();
                        Swal.fire('Canceled!', 'Have been canceled', 'info')
                    }
                });
            });
            //move to trash
            $(document).on('click', '#trash', function(e) {
                e.preventDefault();
                var products_id = [];
                products_id.push($(this).attr('data-id'));

                Swal.fire({
                    title: 'Are you sure?',
                    text: 'This is a replacement delete function, the data will be reused if you want!',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#d33',
                    cancelButtonColor: '#3085d6',
                    cancelButtonText: 'No, cancel it!',
                    confirmButtonText: 'Yes, I am sure!',
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            url: '/dashboard/product/trash',
                            type: 'post',
                            dataType: 'json',
                            data: {
                                products_id: products_id,
                            },
                            success: function(data) {
                                $('#tb_products').DataTable().ajax.reload();
                                $('#tb_products').DataTable().rows().deselect();
                                if(data.errors > 0)
                                    Swal.fire({
                                        title: "Move to trash!",
                                        html: "<p class='text-success'>" + data.success + " items has been move to trash! </p><p class='text-danger'>" + data.errors + " items Can not move to trash!</p>",
                                        type: "error",
                                    });
                                else
                                    Swal.fire({
                                        title: "Move to trash!",
                                        html: "<p class='text-success'>" + data.success + " items has been move to trash! </p>",
                                        type: "success",
                                    });
                            }
                        });
                    } else if (result.dismiss === Swal.DismissReason.cancel) {
                        $('#tb_products').DataTable().ajax.reload();
                        $('#tb_products').DataTable().rows().deselect();
                        Swal.fire('Canceled!', 'Have been canceled', 'info')
                    }
                })
            });

            //convert trash to public
            $(document).on('click', '#publish', function(e) {
                e.preventDefault();
                var products_id = [];
                products_id.push($(this).attr('data-id'));

                $.ajax({
                    url: '/dashboard/product/publish',
                    type: 'post',
                    dataType: 'json',
                    data: {
                        products_id: products_id,
                    },
                    success: function(data) {
                        $('#tb_products').DataTable().ajax.reload();
                        $('#tb_products').DataTable().rows().deselect();
                        if(data.errors > 0)
                            Swal.fire({
                                title: "Published!",
                                html: "<p class='text-success'>" + data.success + " items has been convert to public! </p><p class='text-danger'>" + data.errors + " items Can not convert to public!</p>",
                                type: "error",
                            });
                        else
                            Swal.fire({
                                title: "Published!",
                                html: "<p class='text-success'>" + data.success + " items has been convert to public! </p>",
                                type: "success",
                            });
                    }
                });
            });

            //featured
            $(document).on('click', '#featured', function(e) {
                e.preventDefault();
                var id = $(this).attr('data-id');

                Swal.fire({
                    title: 'Change featured products',
                    text: 'You want to change featured for this product!',
                    type: 'info',
                    showCancelButton: true,
                    confirmButtonColor: '#00ff80',
                    cancelButtonColor: '#80d4ff',
                    cancelButtonText: 'No, cancel it!',
                    confirmButtonText: 'Yes, I want to change it!',
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            url: '/dashboard/product/featured/' + id,
                            type: 'get',
                            success: function (data)
                            {
                                $('#tb_products').DataTable().ajax.reload();
                                $('#tb_products').DataTable().rows().deselect();
                                if(data == 'success')
                                    Swal.fire('Changed!', 'Product has been Changed.','success');
                                else
                                    Swal.fire('Oh no...!', 'Can not change', 'error');
                            }
                        })
                    } else if (result.dismiss === Swal.DismissReason.cancel) {
                        $('#tb_products').DataTable().ajax.reload();
                        $('#tb_products').DataTable().rows().deselect();
                        Swal.fire('Canceled!', 'Have been canceled', 'info')
                    }
                })
            });
        });
    </script>

@endsection
