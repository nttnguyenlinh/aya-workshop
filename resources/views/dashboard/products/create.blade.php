@extends('layouts.dboard')

@section('content')
    <link rel="stylesheet" href="{{asset('dboard/plugins/tinymce/custom_css.css')}}">
    <link rel="stylesheet" href="{{asset('dboard/css/bootstrap-tagsinput.css')}}">
    <link rel="stylesheet" href="{{asset('dboard/plugins/selectize/dist/css/selectize.default.css')}}">

    <script type="text/javascript" src="https://cdn.tiny.cloud/1/k9y1yuun52fw46xsxp3l1pdost7wyn7kc1jxwc866jgweclj/tinymce/5/tinymce.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
    <script src="{{asset('dboard/plugins/selectize/dist/js/standalone/selectize.js')}}"></script>
{{--    <script src="https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@2.1.6/dist/loadingoverlay.min.js"></script>--}}

    <section class="content">
        <div class="container-fluid">
            <form id="myform" method="post" action="{{route('dashboard.product.store')}}" role="form" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-md-9">
                        <div class="card card-primary card-outline">
                            <div class="card-body">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="title" name="title" placeholder="Enter product name..." required autofocus>
                                    <p id="p_title" class="text-danger mt-1"></p>
                                </div>
                                <div class="form-group">
                                    <textarea id="contents" name="contents" class="form-control"></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="card card-primary card-outline">
                            <div class="card-header">
                                <h3 class="card-title">Product Gallery</h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fal fa-minus"></i>
                                    </button>
                                </div>
                            </div>

                            <div class="card-body">
                                <div class="form-group">
                                    <div class="dropzone" id="myDropzone"></div>
                                </div>
                            </div>

                            <div class="card-footer">
                                <button id="uploads_images" class="btn btn-info float-right"><i class="fal fa-arrow-square-up"></i>Upload</button>
                            </div>
                        </div>

                        <div class="card card-primary card-outline">
                            <div class="card-header">
                                <h3 class="card-title">Product data -
                                    <span class="type_box hidden">
                                        <label for="product-type">
                                            <select id="product_type" name="product_type">
                                                <optgroup label="Product Type">
                                                    <option value="simple" selected="selected">Simple product</option>
                                                    <option value="variable">Variable product</option>
                                                </optgroup>
                                            </select>
                                        </label>
                                    </span>
                                </h3>

                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fal fa-minus"></i>
                                    </button>
                                </div>
                            </div>

                            <div class="card-body mt-2 p-2" id="product_varible_container">
                                <div class="row">
                                    <div class="col-md-2 mb-3">
                                        <ul class="nav nav-pills flex-column" id="product_varible_tabs_sidebar" role="tablist">
                                            <li class="nav-item">
                                                <a class="nav-link active" id="general_tab" data-toggle="tab" href="#general" role="tab" aria-controls="general" aria-selected="true"><i class="fas fa-wrench"></i> <span class="pl-2">General</span></a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" id="inventory_tab" data-toggle="tab" href="#inventory" role="tab" aria-controls="inventory" aria-selected="false"><i class="fas fa-file-invoice-dollar"></i> <span class="pl-2">Inventory</span></a>
                                            </li>
                                        </ul>
                                    </div>

                                    <div class="col-md-10">
                                        <div class="tab-content" id="product_varible_tabs_content">
                                            <div id="overlay"><div class="cv-spinner"><span class="spinner"></span></div></div>
                                            <div class="tab-pane fade show active" id="general" role="tabpanel" aria-labelledby="general_tab">
                                                <div class="row" style="margin:3px;">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-5">
                                                                <label>Regular price ($)</label>
                                                            </div>

                                                            <div class="col-md-7">
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend">
                                                                        <span class="input-group-text"><i class="fal fa-dollar-sign"></i></span>
                                                                    </div>
                                                                    <input name="max_price" class="form-control mask">
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-5">
                                                                <label>Sale price ($)</label>
                                                            </div>

                                                            <div class="col-md-7">
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend">
                                                                        <span class="input-group-text"><i class="fal fa-dollar-sign"></i></span>
                                                                    </div>
                                                                    <input name="min_price" class="form-control mask">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div id="discount_datetime" class="form-group" style="display: none;">
                                                        <div class="row">
                                                            <div class="col-md-5">
                                                                <label>Discount time</label>
                                                            </div>

                                                            <div class="col-md-7">
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend">
                                                                        <span class="input-group-text"><i class="fal fa-calendar"></i></span>
                                                                    </div>
                                                                    <input type="text" class="form-control float-right" name="discounttime" id="discounttime">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="inventory" role="tabpanel" aria-labelledby="inventory_tab">
                                                <div class="row" style="margin:3px;">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-5">
                                                                <label>SKU</label>
                                                            </div>

                                                            <div class="col-md-7">
                                                                <div class="input-group">
                                                                    <input name="sku" class="form-control" text="text">
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-5">
                                                                <label>Stock status</label>
                                                            </div>

                                                            <div class="col-md-7">
                                                                <div class="input-group">
                                                                    <select name="stock" class="form-control">
                                                                        <option value="1" selected>In stock</option>
                                                                        <option value="0">Out of stock</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-5">
                                                                <label>Stock quantity</label>
                                                            </div>

                                                            <div class="col-md-7">
                                                                <div class="input-group">
                                                                    <input name="stock_quantity" class="form-control" type="text" id="stock_quantity">
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-5">
                                                                <label>Featured products</label>
                                                            </div>

                                                            <div class="col-md-7">
                                                                <div class="input-group">
                                                                    <select name=featured" class="form-control">
                                                                        <option value="0" selected>Do not choose</option>
                                                                        <option value="1">Choose as featured products</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="card card-primary card-outline">
                                    <div class="card-header">
                                        <h3 class="card-title">Modify SEO</h3>
                                    </div>
                                    <div class="card-body">
                                        <p class="bolder" style="padding-top: 20px; font-weight: 600;">SEO title (<span class="jWwadS">Title</span> <span class="jWwadS">-</span> <span class="jWwadS">Site title</span>)</p>
                                        <input class="form-control" style="border: #057bbe 1px solid; color: #057bbe;" id="seo_title" name="seo_title" placeholder="Modify your SEO title by editing it right here"/>

                                        <p class="bolder" style="padding-top: 20px; font-weight: 600;">SEO keyword</p>
                                        <textarea class="form-control" style="border: #057bbe 1px solid; height:100px; color: #057bbe; resize: none;" id="seo_keyword" name="seo_keyword" placeholder="SEO keyword"></textarea>

                                        <p class="bolder" style="padding-top: 20px; font-weight: 600;">Meta description</p>
                                        <textarea class="form-control" style="border: #057bbe 1px solid; height:100px; color: #057bbe; resize: none;" id="meta_description" name="meta_description" placeholder="Modify your meta description by editing it right here"></textarea>

                                        <div id="the-count" style="font-weight: normal;">
                                            <span id="current" style="color: rgb(102, 102, 102);"></span>
                                            <span id="maximum" style="color: rgb(102, 102, 102);">/ 150</span>
                                        </div>

                                        <div style="margin-top: 20px; font-weight: 600;">
                                            <span class="bolder">Facebook Image</span>
                                            <input type="hidden" name="facebook_image" id="facebook_image" value="">
                                            <center>
                                                <img src="https://dummyimage.com/1920x768/000/fff" style="width:70%; height:50%;" id="fb_image">
                                            </center>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card card-primary card-outline">
                            <div class="card-header" data-widget="collapse">
                                <h3 class="card-title">Status & Visibility</h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fal fa-minus"></i></button>
                                </div>
                            </div>

                            <div class="card-body p-0">
                                <fieldset class="visibility-fieldset">
                                    <div class="visibility-choice">
                                        <input type="radio" name="visibility" id="status_1" class="visibility-radio" value="1" checked>
                                        <label for="status_1" class="visibility-label">Public</label>
                                        <p class="visibility-info">Visible to everyone.</p>
                                    </div>

                                    <div class="visibility-choice">
                                        <input type="radio" name="status" id="status_0" class="visibility-radio" value="0">
                                        <label for="status_0" class="visibility-label">Private</label>
                                        <p class="visibility-info">Only visible to site admins and editors.</p>
                                    </div>

                                    <div class="visibility-choice">
                                        <input type="radio" name="status" id="status_-1" class="visibility-radio" value="-1">
                                        <label for="status_-1" class="visibility-label">Draft</label>
                                        <p class="visibility-info">Move to trash</p>
                                    </div>
                                </fieldset>
                            </div>

                            <div class="card-footer">
                                <div class="float-right">
                                    <button type="submit" class="btn btn-success" id="public"><i class="fal fa-arrow-square-right"></i> Public...</button>
                                </div>
                                <button id="reset" class="btn btn-default"><i class="fa fa-times"></i> Clear</button>
                            </div>
                        </div>

                        <div class="card card-primary card-outline">
                            <div class="card-header">
                                <h3 class="card-title">Categories</h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-widget="collapse"><i
                                            class="fal fa-minus"></i>
                                    </button>
                                </div>
                            </div>

                            <div class="card-body p-0 categories_choice">
                                <?php $currents = array(); ?>
                                {!! Helpers::checkbox_categories_hierarchy($categories, $currents, 0, '') !!}
                            </div>
                        </div>

                        <div class="card card-primary card-outline">
                            <div class="card-header">
                                <h3 class="card-title">Tags</h3>
                                <div class="card-tools">
                                    <button class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body p-0">
                                <p class="tag-info">Add New Tag</p>
                                <input id="product_tag" name="product_tag" type="text" class="form-control tag-input" data-role="tagsinput" value="">
                                <p class="tag-info">Separate with commas or the Enter key.</p>
                            </div>
                        </div>

                        <div class="card card-primary card-outline">
                            <div class="card-header" data-widget="collapse">
                                <h3 class="card-title">Product Image</h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-widget="collapse"><i
                                            class="fal fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body p-0">
                                <input type="hidden" id="thumb" name="thumbnail" value="" />
                                <input type="button" id="thumbnail" class="dropify" data-show-loader="true"
                                    data-default-file="" value="" data-height="90" />
                            </div>
                            <!-- /.card-body -->
                        </div>

                        <div class="card card-primary card-outline">
                            <div class="card-header" data-widget="collapse">
                                <h3 class="card-title">Product short description</h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                </div>
                            </div>
                            <div class="card-body p-0">
                                <textarea id="description" name="description" class="form-control"
                                    placeholder="Write short description here..." style="width:98%; height:200px; margin:3px;"
                                    required></textarea>
                            </div>
                        </div>

                    </div>
                </div>
            </form>
        </div>
    </section>
@endsection

@section('script-footer')
    <script src="{{asset('dboard/js/bootstrap-tagsinput.min.js')}}"></script>
    <script src="{{asset('dboard/js/typeahead.bundle.min.js')}}"></script>
    <script src="{{asset('dboard/js/bloodhound.min.js')}}"></script>

    <script type="text/javascript">
        var elt = $("#product_tag");
        var path = "{{ route('dashboard.product.tag.autocomplete') }}";

        var tags = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('id'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                url: path + '?query=%QUERY%',
                wildcard: '%QUERY%',
            },
        });
        tags.initialize();

        elt.tagsinput({
            itemValue: 'id',
            itemText  : 'name',
            maxTags: 5,
            trimValue: true,
            freeInput: true,
            focusClass: 'form-control',
            typeaheadjs: {
                name: 'tags',
                itemValue: 'id',
                displayKey: 'name',
                source: tags.ttAdapter()
            },
        });

        elt.on('beforeItemAdd', function(event) {
            //alert(event.item.id);
        });

        elt.on('itemAdded', function(event) {
            setTimeout(function(){
                $(">input[type=text]",".bootstrap-tagsinput").val("");
            }, 1);
        });

        elt.on('itemRemoved', function(event) {
            //alert(event.item.id);
            var tags_id = [];
            tags_id.push(event.item.id);

            $(document).ready(function() {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    url: '{{route('dashboard.product.tag.delete')}}',
                    type: 'delete',
                    dataType: 'json',
                    data: {
                        tags_id: tags_id,
                        _method: 'delete'
                    },
                    success: function(data) {
                        console.log(data);
                    }
                });
            });
        });
    </script>

    <script type="text/javascript">
        tinymce.init({
            selector: '#contents',
            language: 'en',
            menubar: false,
            toolbar_sticky: true,
            toolbar_drawer: 'floating',
            height: 300,
            convert_urls: false,
            image_title: true,
            image_caption: true,
            image_advtab: true,
            image_class_list: [
                {title: 'None', value: ''},
            ],
            external_filemanager_path: "/dboard/plugins/filemanager/",
            filemanager_title: 'File Manager',
            plugins: 'save formatpainter wordcount toc lists fullscreen preview image imagetools link media code paste table tabfocus quickbars hr pagebreak searchreplace autolink filemanager responsivefilemanager',
            toolbar: 'fullscreen code preview toc | bold italic underline | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent | numlist bullist | forecolor backcolor formatpainter removeformat | responsivefilemanager image media | link table pagebreak hr',

            external_plugins: {
                "responsivefilemanager": "{{asset('dboard/plugins/tinymce/plugins/responsivefilemanager/plugin.min.js')}}",
                "filemanager": "{{asset('dboard/plugins/filemanager/plugin.min.js')}}"
            },

            audio_template_callback: function (data) {
                return '<audio controls>' + '\n<source src="' + data.source1 + '"' + (data.source1mime ? ' type="' + data.source1mime + '"' : '') + ' />\n' + '</audio>';
            },
            video_template_callback: function (data) {
                return '<video width="' + data.width + '" height="' + data.height + '"' + (data.poster ? ' poster="' + data.poster + '"' : '') + ' controls="controls">\n' + '<source src="' + data.source1 + '"' + (data.source1mime ? ' type="' + data.source1mime + '"' : '') + ' />\n' + (data.source2 ? '<source src="' + data.source2 + '"' + (data.source2mime ? ' type="' + data.source2mime + '"' : '') + ' />\n' : '') + '</video>';
            },
            setup: function (editor) {
                editor.on('change', function (e) {
                    editor.save();
                });
            }
        });

        Dropzone.options.myDropzone = {
            url: "{{route('dashboard.product.images.upload')}}",
            headers: {'X-CSRF-TOKEN': '{!! csrf_token() !!}'},
            autoProcessQueue: false,
            uploadMultiple: true,
            parallelUploads: 5,
            maxFiles: 5,
            maxFilesize: 3,
            acceptedFiles: 'image/*',
            addRemoveLinks: true,
            dictRemoveFile: "x",
            dictCancelUpload: "",
            dictFileTooBig: "Image is bigger than 3MB",
            dictDefaultMessage: "Drop images here to upload",
            dictMaxFilesExceeded: "Can't upload any more images",
            dictInvalidFileType: "Can't upload files of this type",
            init: function() {
                myDropzone = this;
                var product_title = $("#title").val();
                var submitButton = document.querySelector("#uploads_images");

                $.ajax({
                    url: "{{route('dashboard.product.images')}}",
                    type: "POST",
                    data: {
                        type: "create",
                        product_title: product_title
                    },
                    success: function(data){
                        if(Object.keys(data).length > 0)
                        {
                            $.each(data, function(i, item) {
                                var mockFile = {name: item.path, size: item.bytes_size};
                                myDropzone.emit("addedfile", mockFile);
                                myDropzone.emit("thumbnail", mockFile, "/storage/" + item.path);
                                myDropzone.emit("complete", mockFile);
                                myDropzone.files.push(mockFile);
                            });
                        }
                    }
                });

                submitButton.addEventListener("click", function(e) {
                    e.preventDefault();
                    myDropzone.processQueue();
                });

                this.on('sendingmultiple', function(data, xhr, formData) {
                    formData.append("product_id", $("#product_id").val());
                });

                this.on("success", function(file) {
                    toastr["success"]("Upload completed!");
                });

                this.on("removedfile", function(file) {
                    $.ajax({
                        url: "{{route('dashboard.product.images.remove')}}",
                        type: 'post',
                        //headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
                        headers: {'X-CSRF-TOKEN': '{!! csrf_token() !!}'},
                        data: {name: file.name},
                        success: function(data) {
                            switch(data) {
                                case 'success':
                                    toastr["success"]("Image has been removed!");
                                    myDropzone.removeFile(file);
                                    //location.reload().delay(1000);
                                    break;
                                case 'error':
                                    toastr["error"]("Oh no...! Can't remove!");
                                    break;
                                default:
                                    myDropzone.removeFile(file);
                            }
                        }
                    });
                });
            },
        };

        function event_select_variant_thumbnail(elm){
            event_choose_file_variant(elm.getElementsByTagName("input")[0].getAttribute("id"), elm.getElementsByTagName("img")[0].getAttribute("id"));
        }

        function event_choose_file_variant(elementId, img) {
            CKFinder.modal({
                skin: "neko",
                resourceType: 'Images',
                chooseFiles: true,
                chooseFilesOnDblClick: true,
                width: 800,
                height: 600,
                onInit: function(finder) {
                    finder.on('files:choose', function(evt) {
                        var file = evt.data.files.first();
                        var output = document.getElementById(elementId);
                        output.value = file.getUrl();
                        event_upload_file_variant(file.getUrl(), file.get('name'), img);
                    });

                    finder.on('file:choose:resizedImage', function(evt) {
                        var output = document.getElementById(elementId);
                        output.value = evt.data.resizedUrl;
                        event_upload_file_variant(evt.data.resizedUrl, file.get('name'), img);
                    });
                }
            });
        }

        function event_upload_file_variant(url, name, img) {
            document.getElementById(img).parentElement.getElementsByTagName("i")[0].remove();
            document.getElementById(img).src = url;
            document.getElementById(img).style.display = 'block';
        }

        $(document).ready(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $(document).on({
                click: function() {
                    $('#variationRemove').parent().parent().parent().remove();
                },
            }, '#variationRemove');

            $(document).on({
                click: function() {
                    $('#attRemove').parent().parent().parent().remove();
                },
            }, '#attRemove');

            $(document).on({
                click: function() {
                    $("#overlay").fadeIn(300);

                    setTimeout(function(){
                        $("#overlay").fadeOut(300);
                    },1000);
                },
            }, '#attributes_tab');

            $(document).on({
                click: function() {
                    $("#overlay").fadeIn(300);

                    setTimeout(function(){
                        $("#overlay").fadeOut(300);
                    },1000);

                },
            }, '#variations_tab');

            $(document).on({
                click: function() {
                    $("#overlay").fadeIn(300);

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    var frmData = $('#variantions_frm').serializeArray();
                    var title = $("#title").val();
                    $.ajax({
                        url: "{{ route('dashboard.product.variant.update') }}",
                        type: 'POST',
                        dataType: 'json',
                        data: {
                            frmData: frmData,
                            product_title: title
                        },
                        success: function (data) {
                            if(data.errors > 0)
                                toastr["error"](data.success + " items has been added! AND " + data.errors + " items Can not add!");
                            else
                                toastr["success"](data.success + " items has been added!");
                        }
                    }).done(function() {
                        setTimeout(function(){
                            $("#overlay").fadeOut(300);
                        },500);
                    });
                },
            }, '#saveVariation');

            $(document).on({
                click: function() {
                    $("#overlay").fadeIn(300);

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    var title = $("#title").val();
                    $.ajax({
                        url: "{{ route('dashboard.product.variant.load') }}",
                        type: 'POST',
                        dataType: 'json',
                        data: {
                            title: title
                        },
                        success: function(data) {
                            console.log(data);
                            var variations_table = '<div id="variations_table"></div>';
                            $("#variations").html(variations_table);
                            $("#variations_table").append('<form id="variantions_frm"></form>');

                            var variations_content = '';
                            $.each(data.product_variant, function(pv, variant) {
                                variations_content += '<div class="card"><div class="card-header"><input type="hidden" name="variant_id" value="' + variant.id + '"><h4 class="card-title" style="display: inline-block;">#' + variant.id + '</h4>';
                                for(var c = 0; c < data.attr_count; c++)
                                {
                                    var variations_select = '';
                                    $.each(data.attributes, function(a, attr) {
                                        variations_select += '<select name="pa_term">';
                                        var variations_option = '';
                                        $.each(data.product_term, function(i, item) {
                                            if(item.attribute_id === attr.id)
                                            {
                                                var variant_code = variant.variant_code;
                                                var array_code = variant_code.split('-');
                                                $.each(array_code, function(ac, arr_code) {
                                                    if(item.slug == arr_code)
                                                    {
                                                        variations_option += '<option value="' + item.term_id + '" selected>' + item.name + '</option>';
                                                        item = $.grep(item, function(data, index) {
                                                            return data.id != id;
                                                        });
                                                    }
                                                });
                                                variations_option += '<option value="' + item.term_id + '">' + item.name + '</option>';
                                            }
                                        });
                                        variations_select += variations_option + '</select>';
                                    });
                                }

                                variations_content += variations_select + '<div class="card-tools"><a href="#" id="variationRemove" class="remove_row delete">Remove</a><button class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button></div></div>';
                                variations_content += '<div class="card-body p-0" style="">' +
                                    '<div class="row" style="margin: 5px auto;width: 95%;"><div class="col-md-5">' +
                                    '<a href="javascript:void(0);" class="thumbnail_variant_button" onclick="event_select_variant_thumbnail(this)"><i class="far fa-image"></i><input type="hidden" name="thumbnail_variant" id="upload_image_' +  variant.id + '_" value=""><img id="img_thumbnail_variant_' +  variant.id + '_" src="" style=""></a>' +
                                    '</div><div class="col-md-7"><div class="form-group"><div class="row"><div class="col-md-2"><label style="line-height: 2.5;">SKU</label></div>'+
                                    '<div class="col-md-7"><div class="input-group"><input name="variant_sku" class="form-control" value="' + variant.sku + '"></div></div></div></div></div></div>'+
                                    '<div class="row" style="margin: 5px auto;width: 95%;"><div class="col-md-6"><div class="form-group"><div class="row"><div class="col-md-5"><label>Regular price ($)</label></div><div class="col-md-7"><div class="input-group"><div class="input-group-prepend"><span class="input-group-text"><i class="fal fa-dollar-sign"></i></span></div><input name="variant_max_price" class="form-control mask" style="text-align: right;"></div></div></div></div></div>'+
                                    '<div class="col-md-6"><div class="form-group"><div class="row"><div class="col-md-5"><label>Sale price ($)</label></div><div class="col-md-7"><div class="input-group"><div class="input-group-prepend"><span class="input-group-text"><i class="fal fa-dollar-sign"></i></span></div><input name="variant_min_price" class="form-control mask" style="text-align: right;"></div></div></div></div></div>' +
                                    '<div id="variant_discount_datetime" class="form-group" style="display: none;"><div class="row"><div class="col-md-5"><label>Discount time</label></div><div class="col-md-7"><div class="input-group"><div class="input-group-prepend"><span class="input-group-text"><i class="fal fa-calendar"></i></span></div><input type="text" class="form-control" name="variant_discounttime"></div></div></div></div>' +
                                    '<div class="row" style="margin: 5px auto;width: 95%;"><div class="col-md-6"><div class="form-group"><div class="row"><div class="col-md-5"><label>Stock status</label></div><div class="col-md-7"><div class="input-group"><select name="variant_stock" class="form-control"><option value="1" selected="">In stock</option><option value="0">Out of stock</option></select></div></div></div></div></div><div class="col-md-6"><div class="form-group"><div class="row"><div class="col-md-5"><label>Stock quantity</label></div><div class="col-md-7"><div class="input-group"><input name="variant_stock_quantity" class="form-control" type="text" style="text-align: right;"></div></div></div></div></div></div></div></div></div>';

                            });

                            variations_content += '<div class="toolbar toolbar-bottom"><button type="button" class="button saveVariation" id="saveVariation">Save variation</button></div>';
                            $("#variantions_frm").html(variations_content);
                        }
                    }).done(function() {
                        setTimeout(function(){
                            $("#overlay").fadeOut(300);
                        },500);

                        $('select option').filter(function() {
                            return $(this).text() == 'undefined';
                        }).remove();

                        //Mask decimal
                        $(".mask").inputmask({
                            'alias': 'decimal',
                            'rightAlign': true,
                            'groupSeparator': ',',
                            'digits': 2,
                            'digitsOptional': false,
                            'placeholder': '0',
                            'autoGroup': true
                        });

                        //mask numeric for stock_quantity
                        $('input[name=variant_stock_quantity]').inputmask({
                            alias: 'numeric',
                            allowMinus: false,
                            digits: 0,
                            max: 1000
                        });

                        //set discount datetime
                        $('input[name=variant_min_price]').bind("keyup change", function() {
                            //$('#discount_datetime').toggle($(this).val().length);
                            if ($.trim($(this).val()) != '0.00')
                                $('#variant_discount_datetime').show();
                            else
                                $('#variant_discount_datetime').hide();
                        });

                        //choice discount datetime
                        $('input[name="variant_discounttime"]').daterangepicker({
                            timePicker: true,
                            startDate: moment().startOf('hour'),
                            endDate: moment().startOf('hour').add(32, 'hour'),
                            minDate: '2020-02-01',
                            maxDate: '2036-01-01',
                            dateLimit: {
                                days: 60
                            },
                            showDropdowns: true,
                            showWeekNumbers: true,
                            timePicker24Hour: true,
                            locale: {
                                format: 'YYYY-MM-DD hh:mm:ss'
                            }
                        });
                    });
                },
            }, '#addVariation');

            //Create draft support for Attributes and Variations
            $("#title").change(function(){
                var title = $("#title").val();

                $.ajax({
                    url: '/dashboard/product/create-draft',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        title: title
                    },
                    success: function(data) {
                        if(data == "error")
                        {
                            $("#title").css({
                                'border': '2px solid #c23321',
                                'color': '#c23321'
                            });
                            $("#p_title").html("The Product already exists. Please, change it.");
                        }

                        else
                        {
                            $("#title").css({
                                'border': '2px solid #28a745',
                                'color': '#28a745'
                            });
                            $("#p_title").html("");
                        }
                    }
                });
            });

            $("#product_type").change(function(){
                $(document).ajaxSend(function() {
                    $("#overlay").fadeIn(300);
                });

                var title = $(this).val();
                if(title == "simple")
                {
                    if ($("#nav-item-attributes").length){
                        $("#nav-item-attributes").remove();
                    }

                    if ($("#nav-item-variations").length){
                        $("#nav-item-variations").remove();
                    }

                    if ($("#attributes").length){
                        $("#attributes").remove();
                    }

                    if ($("#variations").length){
                        $("#variations").remove();
                    }

                    //remove product_ attribute and term and variant for product ID
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    $.ajax({
                        url: '{{route('dashboard.product.simple.variant.delete')}}',
                        type: 'delete',
                        dataType: 'json',
                        data: {
                            product_name: $('#title').val(),
                            _method: 'delete'
                        },
                        success: function(data) {
                            console.log(data);
                        }
                    });
                }
                else
                {
                    var nav_item_attributes = '<li class="nav-item" id="nav-item-attributes"><a class="nav-link" id="attributes_tab" data-toggle="tab" href="#attributes" role="tab" aria-controls="attributes" aria-selected="false"><i class="fas fa-th-list"></i> <span class="pl-2">Attributes</span></a></li>';
                    var nav_item_variations = '<li class="nav-item" id="nav-item-variations"><a class="nav-link" id="variations_tab" data-toggle="tab" href="#variations" role="tab" aria-controls="variations" aria-selected="false"><i class="fas fa-table"></i> <span class="pl-2">Variations</span></a></li>';
                    $("#product_varible_tabs_sidebar").append(nav_item_attributes);
                    $("#product_varible_tabs_sidebar").append(nav_item_variations);

                    var options = '';
                    $.ajax({
                        url: '/dashboard/product/attributes-load',
                        type: 'get',
                        dataType: 'json',
                        success: function(data) {
                            $.each(data, function(i, item) {
                                options = options + '<option value="pa_' + item.id + '">'+item.name+'</option>';
                            });
                            var attributes_tab_contents = '<div class="tab-pane fade" id="attributes" role="tabpanel" aria-labelledby="attributes_tab">' +
                                '<div class="toolbar toolbar-top"><select id="attribute_taxonomy"><option value="" disabled selected>Select product attribute</option>' +
                                options + '</select><button type="button" class="button add_attribute" id="addAttribute">Add</button></div><form id="frmAttributes"><div id="attribute_terms_table"></div></form><div class="toolbar">' +
                                '<button type="button" class="button save_attributes" id="save_attributes">Save attributes</button></div></div>';
                            $("#product_varible_tabs_content").append(attributes_tab_contents);
                        }
                    });

                    var variations_tab_contents = '<div class="tab-pane fade" id="variations" role="tabpanel" aria-labelledby="variations_tab"><div class="toolbar toolbar-top"><button type="button" class="button addVariation" id="addVariation">Add variation</button></div></div>';
                    $("#product_varible_tabs_content").append(variations_tab_contents);

                    setTimeout(function(){
                        $("#overlay").fadeOut(300);
                    },500);
                }
            });

            $(document).on({
                change: function() {
                    $('#attribute_taxonomy option:selected').attr('disabled','disabled');
                },
            }, '#attribute_taxonomy');

            $(document).on({
                click: function() {
                    $(document).ajaxSend(function() {
                        $("#overlay").fadeIn(300);
                    });

                    var frmData = $('#frmAttributes').serializeArray();
                    var product_name = $('#title').val();
                    console.log(frmData);

                    $.ajax({
                        url: '{{route('dashboard.product.attribute.add')}}',
                        type: 'POST',
                        dataType: 'json',
                        data: {frmData: frmData, product_name: product_name},
                        success: function(data){

                        }
                    }).done(function() {
                        setTimeout(function(){
                            $("#overlay").fadeOut(300);
                        },500);
                    });
                }
            }, '#save_attributes');

            $('#product_varible_tabs_content').on('click', '#addAttribute', function() {
                $(document).ajaxSend(function() {
                    $("#overlay").fadeIn(300);
                });

                var selected = $('#attribute_taxonomy option:selected');
                if(selected.val() != "")
                {
                    var selectedText = $("#attribute_taxonomy option:selected").html();
                    var att_contents = '<div class="card"><div class="card-header"><h4 class="card-title">' + selectedText + '</h4><div class="card-tools"><a href="#" id="attRemove" class="remove_row delete">Remove</a> <button class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button></div></div>' + '<div class="card-body p-0"><table cellpadding="0" cellspacing="0"><tbody><tr><td class="attribute_name"><label>Name: </label><strong>' + selectedText + '</strong><input type="hidden" name="attributes" value="' + selected.val() + '">' + '</td><td rowspan="3" class="pb-1"><label>Value(s):</label> <select id="select_terms_' + selected.val() + '" name="terms" multiple class="demo-default" style="width:50%" placeholder="Select a value..."></td></tr></tbody></table>';
                    $("#attribute_terms_table").append(att_contents);

                    var evt = $('#select_terms_'+ selected.val());
                    var path = "{{ route('dashboard.product.variant.get') }}";
                    //path + '?attribute=' + selected.val()

                    var options = '';
                    $.ajax({
                        url: path + '?attribute=' + selected.val(),
                        type: 'get',
                        dataType: 'json',
                        success: function(data) {
                            $.each(data, function(i, item) {
                                options = options + '<option value="' + item.id + '">'+item.name+'</option>';
                            });
                            evt.append(options);
                        }
                    }).done(function() {
                        evt.selectize({
                            plugins: ['remove_button', 'drag_drop'],
                            valueField: 'id',
                            labelField: 'name',
                            maxItems: 7,
                            persist: false,
                            create: false,
                            render: {
                                item: function(data, escape) {
                                    return '<div>' + escape(data.name) + '</div>';
                                }
                            },
                            onDelete: function(values) {
                                return confirm(values.length > 1 ? 'Are you sure you want to remove these ' + values.length + ' items?' : 'Are you sure you want to remove "' + values[0] + '"?');
                            }
                        });

                        setTimeout(function(){
                            $("#overlay").fadeOut(300);
                        },500);
                    });
                }
            });

            //Mask decimal
            $(".mask").inputmask({
                'alias': 'decimal',
                'rightAlign': true,
                'groupSeparator': ',',
                'digits': 2,
                'digitsOptional': false,
                'placeholder': '0',
                'autoGroup': true
            });

            //mask numeric for stock_quantity
            $('#stock_quantity').inputmask({
                alias: 'numeric',
                allowMinus: false,
                digits: 0,
                max: 1000
            });

            //set discount datetime
            $('input[name=min_price]').bind("keyup change", function() {
                //$('#discount_datetime').toggle($(this).val().length);
                if ($.trim($(this).val()) != '0.00')
                    $('#discount_datetime').show();
                else
                    $('#discount_datetime').hide();
            });

            //choice discount datetime
            $('input[name="discounttime"]').daterangepicker({
                timePicker: true,
                startDate: moment().startOf('hour'),
                endDate: moment().startOf('hour').add(32, 'hour'),
                minDate: '2020-02-01',
                maxDate: '2036-01-01',
                dateLimit: {
                    days: 60
                },
                showDropdowns: true,
                showWeekNumbers: true,
                timePicker24Hour: true,
                locale: {
                    format: 'YYYY-MM-DD hh:mm:ss'
                }
            });

            // Scroll to the bottom, in case we're in a tall textarea
            $.fn.putCursorAtEnd = function() {
                return this.each(function() {
                    // Cache references
                    var $el = $(this), el = this;
                    // Only focus if input isn't already
                    if (!$el.is(":focus")) {$el.focus();}
                    // If this function exists... (IE 9+)
                    if (el.setSelectionRange) {
                        // Double the length because Opera is inconsistent about whether a carriage return is one character or two.
                        var len = $el.val().length * 2;
                        // Timeout seems to be required for Blink
                        setTimeout(function() {el.setSelectionRange(len, len);}, 1);
                    } else {
                        // As a fallback, replace the contents with itself
                        // Doesn't work in Chrome, but Chrome supports setSelectionRange
                        $el.val($el.val());
                    }
                    // Scroll to the bottom, in case we're in a tall textarea
                    // (Necessary for Firefox and Chrome)
                    this.scrollTop = 999999;
                });
            };
            var meta_description = $('#meta_description');
            meta_description.putCursorAtEnd() // should be chainable
                .on("focus", function() { // could be on any event
                    meta_description.putCursorAtEnd()
                });

            $('#meta_description').bind('mousedown keyup', function() {
                var characterCount = $(this).val().length,
                    current = $('#current'),
                    maximum = $('#maximum'),
                    theCount = $('#the-count');
                current.text(characterCount);
                if (characterCount >= 150) {
                    maximum.css('color', 'red');
                    current.css('color', 'red');
                    theCount.css('font-weight','bold');
                } else {
                    current.css('color', '#666');
                    maximum.css('color','#666');
                    theCount.css('font-weight','normal');
                }
            });

            $('#reset').click(function(e) {
                $('textarea').each(function(k, v) {
                    tinyMCE.get(k).setContent('');
                });
            });

            $('#thumbnail').click(function() {
                selectFile('thumbnail');
            });

            function selectFile(elementId) {
                CKFinder.modal({
                    skin: "neko",
                    resourceType: 'Images',
                    chooseFiles: true,
                    chooseFilesOnDblClick: true,
                    width: 800,
                    height: 600,
                    onInit: function(finder) {
                        finder.on('files:choose', function(evt) {
                            var file = evt.data.files.first();
                            var output = document.getElementById(elementId);
                            output.value = file.getUrl();
                            updateFile(file.getUrl(), file.get('name'));
                            $(".dropify-clear").attr('style', 'display:block');
                        });

                        finder.on('file:choose:resizedImage', function(evt) {
                            var output = document.getElementById(elementId);
                            output.value = evt.data.resizedUrl;
                            updateFile(evt.data.resizedUrl, file.get('name'));
                            $(".dropify-clear").attr('style', 'display:block');
                        });
                    }
                });
            };

            function updateFile(url, name) {
                $('#thumb').val(url);
                $('#thumbnail').attr('data-default-file', url);
                $(".dropify-preview").attr('style', 'display:block');
                $('.dropify-render img').attr('src', url);
                $('.dropify-filename-inner').text(name);
            };

            $('.dropify').dropify({
                messages: {
                    'default': '',
                    'replace': '',
                    'remove': 'Remove',
                    'error': ''
                },
            });

            $('.dropify').dropify().on('dropify.afterClear', function(event, element) {
                $('#thumb').val('');
                $('#thumbnail').attr('data-default-file', '');
                $('.dropify-render').append('<img src="">');
                $('.dropify-filename-inner').text('');
                $(".dropify-clear").attr('style', 'display:none');
            });

            if ($('#thumb').val().length <= 0) {
                $(".dropify-clear").attr('style', 'display:none');
                $('.dropify-render').append('<img src="">');
            }

            $('#fb_image').click(function() {
                selectFile2('fb_image');
            });

            function selectFile2(elementId) {
                CKFinder.modal({
                    skin: "neko",
                    resourceType: 'Images',
                    chooseFiles: true,
                    chooseFilesOnDblClick: true,
                    width: 800,
                    height: 600,
                    onInit: function(finder) {
                        finder.on('files:choose', function(evt) {
                            var file = evt.data.files.first();
                            var output = document.getElementById(elementId);
                            output.value = file.getUrl();
                            updateFile2(file.getUrl(), file.get('name'));
                        });

                        finder.on('file:choose:resizedImage', function(evt) {
                            var output = document.getElementById(elementId);
                            output.value = evt.data.resizedUrl;
                            updateFile2(evt.data.resizedUrl, file.get('name'));
                        });
                    }
                });
            };
            function updateFile2(url, name) {
                $('#facebook_image').val(url);
                $('#fb_image').attr('src', url);
            };
        });
    </script>
@endsection
