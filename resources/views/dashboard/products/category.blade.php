@extends('layouts.dboard')

@section('page-header')
    <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0 text-dark">Product categories</h1>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-5">
            <div class="col-wrap">
                <div class="form-wrap">
                    <p>Product categories for your store can be managed here. Sort by descending value. That is, the higher the value will be arranged first.</p>
                    <h2>Add new category</h2>
                    <form action="{{route('dashboard.category.store')}}" method="post">
                        @csrf
                        <input type="hidden" name="category_type" value="product"/>
                        <div class="form-group">
                            <label for="category_name">Name</label>
                            <input name="category_name" id="category_name" type="text" class="form-control" value="" required autofocus>
                            <p class="description">The name is how it appears on your site.</p>
                        </div>

                        <div class="form-group">
                            <label for="category_slug">Slug</label>
                            <input name="category_slug" id="category_slug" type="text" class="form-control" value="">
                            <p class="description">The 'slug' is the URL-friendly version of the name. It is usually all lowercase and contains only letters, numbers, and hyphens.</p>
                        </div>

                        <div class="form-group">
                            <label for="category_parent">Parent Category</label>
                            <select name="category_parent" id="category_parent" class="form-control">
                                <option value="0">None</option>
                                {!! Helpers::select_categories_hierarchy($categories) !!}
                            </select>
                            <p class="description">Categories can have a hierarchy.</p>
                        </div>

                        <div class="form-group">
                            <label for="category_description">Description</label>
                            <textarea name="category_description" id="category_description" class="form-control" rows="5"></textarea>
                            <p class="description">The description is not prominent by default; however, some themes may show it.</p>
                        </div>

                        <div class="form-group">
                            <label for="position">Position</label>
                            <input name="position" id="position" type="text" class="form-control" value="0">
                            <p class="description">Sort by descending value. That is, the higher the value will be arranged first.</p>
                        </div>

                        <div class="form-group">
                            <label for="category_thumbnail">Thumbnail</label>
                            <input type="hidden" id="category_thumbnail" name="category_thumbnail" value="" />
                            <div id="product_cat_thumbnail" style="margin-right: 10px;">
                                <img src="{{asset('storage/placeholder.png')}}" width="60px" height="60px">
                                <button type="button" id="upload_image_button">Upload/Add image</button>
                                <button type="button" id="remove_image_button" style="display: none;">Remove</button>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-primary">Add new Category</button>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-md-7">
            <button type="button" id="btnDelete" class="btn btn-danger mb-1">Delete</button>
            <div class="card card-outline card-primary">
                <div class="card-body p-0">
                    <table id="tbl_categories" class="table widefat table-sm">
                        <thead>
                        <tr>
                            <th></th>
                            <th>Image</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Slug</th>
                            <th>Position</th>
                            <th>Count</th>
                        </tr>
                        </thead>
                    </table>
                </div>
                <p class="text-danger p-2" style="font-size:13px;">Deleting a category does not delete the posts in that category. Instead, posts that were only assigned to the deleted category are set to the default category <strong>Uncategorized</strong>. The default category cannot be deleted.</p>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal_category_edit" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">Quick Edit for <strong><span class="btn btn-info" style="color:white;" id="span_category_name"></span></strong>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </h3>
                    </div>
                    <div class="card-body">
                        <form>
                            <input type="hidden" name="category_type" id="category_type_edit" value="" />
                            <input type="hidden" name="category_id" id="category_id_edit" value="">
                            <div class="form-group">
                                <label for="category_name">Name</label>
                                <input name="category_name" id="category_name_edit" type="text" class="form-control" value="" maxlength="28" required autofocus>
                            </div>

                            <div class="form-group">
                                <label for="category_slug">Slug</label>
                                <input name="category_slug" id="category_slug_edit" type="text" class="form-control" value="" maxlength="28">
                            </div>

                            <div class="form-group">
                                <label for="position_edit">Position</label>
                                <input name="position_edit" id="position_edit" type="text" class="form-control" value="0">
                                <p class="description">Sort by descending value. That is, the higher the value will be arranged first.</p>
                            </div>

                            <center>
                                <button class="btn btn-danger" data-dismiss="modal" style="margin-right:20px;">Close</button>
                                <button type="button" class="btn btn-success" id="btn_category_edit">Update</button>
                            </center>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script-footer')
    <script>
        $(document).ready(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            //Load data
            var tbl_categories = $('#tbl_categories').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: {
                    url: '/dashboard/product/category/list',
                    type: 'get',
                },
                columnDefs: [
                    {
                        targets: 0,
                        checkboxes: {
                            selectRow: true
                        }
                    }
                ],
                select: {
                    style: 'multi'
                },
                columns: [
                    {
                        data: 'id',
                        name: 'id',

                    },
                    {
                        data: 'thumbnail',
                        name: 'thumbnail',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'name',
                        name: 'name',
                    },
                    {
                        data: 'description',
                        name: 'description',
                    },
                    {
                        data: 'slug',
                        name: 'slug',
                    },
                    {
                        data: 'position',
                        name: 'position',
                    },
                    {
                        data: 'count',
                        name: 'count',
                    }
                ],
                order: [5, 'desc'],
            });
            $('#tbl_categories tbody').on( 'click', 'tr', function () {
                $(this).toggleClass('selected');
            });
            //Quick edit
            $(document).on('click', '#quickEdit', function() {
                var id = $(this).attr('data-id');

                $.ajax({
                    url: '/dashboard/category/' +id,
                    type: 'get',
                    dataType: 'json',
                    success: function(data){
                        $('#span_category_name').text('Category: ' + data.name);
                        $('#category_type_edit').val(data.type);
                        $('#category_id_edit').val(data.id);
                        $('#category_name_edit').val(data.name);
                        $('#category_slug_edit').val(data.slug);
                        $('#position_edit').val(data.orderby);
                        $('#modal_category_edit').modal();
                    }
                });
            });
            //Update Quick edit
            $(document).on('click', '#btn_category_edit', function() {
                var category_type =  $('#category_type_edit').val();
                var category_id =  $('#category_id_edit').val();
                var category_name =  $('#category_name_edit').val();
                var category_slug =  $('#category_slug_edit').val();
                var position =  $('#position_edit').val();

                $.ajax({
                    url: '/dashboard/category/update',
                    type: 'post',
                    dataType: 'json',
                    data: {category_type: category_type, edit_type: 'quick', category_id: category_id, category_name: category_name, category_slug: category_slug, position: position},
                    success: function(data){
                        $('#tbl_categories').DataTable().ajax.reload();
                        switch(data)
                        {
                            case 'success':
                                Swal.fire({
                                    title: "Updated!",
                                    html: "<p class='text-success'> The Category has been updated!</p>",
                                    type: "success",
                                });
                                break;
                            case 'unique':
                                Swal.fire('Oh no...!', 'The Category is already in use. Change it, please.', 'error');
                                break;
                            default:
                                Swal.fire('Oh no...!', 'Can not update!', 'error');
                        }
                        $('#modal_category_edit').modal('toggle');
                    }
                });
            });
            //multi delete
            $(document).on('click', '#btnDelete', function(e) {
                var rows_selected = tbl_categories.column(0).checkboxes.selected();
                var categories_id = [];
                // Iterate over all selected checkboxes
                $.each(rows_selected, function(index, rowId){
                    categories_id.push(rowId);
                });

                Swal.fire({
                    title: 'Are you sure?',
                    text: 'You will not be able to recover!',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#d33',
                    cancelButtonColor: '#3085d6',
                    cancelButtonText: 'No, cancel it!',
                    confirmButtonText: 'Yes, I am sure!',
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            url: '/dashboard/category/delete',
                            type: 'delete',
                            dataType: 'json',
                            data: {
                                categories_id: categories_id,
                                _method: 'delete'
                            },
                            success: function(data) {
                                $('#tbl_categories').DataTable().ajax.reload();
                                if(data.errors > 0)
                                    Swal.fire({
                                        title: "Deleted!",
                                        html: "<p class='text-success'>" + data.success + " items has been deleted! </p><p class='text-danger'>" + data.errors + " items Can not delete!</p>",
                                        type: "error",
                                    });
                                else
                                    Swal.fire({
                                        title: "Deleted!",
                                        html: "<p class='text-success'>" + data.success + " items has been deleted! </p>",
                                        type: "success",
                                    });
                            }
                        });
                    } else if (result.dismiss === Swal.DismissReason.cancel) {
                        Swal.fire('Canceled!', 'Have been canceled', 'info')
                    }
                });
            });
            //delete
            $(document).on('click', '#delete', function(e) {
                e.preventDefault();
                var categories_id = [];
                categories_id.push($(this).attr('data-id'));

                Swal.fire({
                    title: 'Are you sure?',
                    text: 'You will not be able to recover!',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#d33',
                    cancelButtonColor: '#3085d6',
                    cancelButtonText: 'No, cancel it!',
                    confirmButtonText: 'Yes, I am sure!',
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            url: '/dashboard/category/delete',
                            type: 'delete',
                            dataType: 'json',
                            data: {
                                categories_id: categories_id,
                                _method: 'delete'
                            },
                            success: function(data) {
                                $('#tbl_categories').DataTable().ajax.reload();
                                if(data.errors > 0)
                                    Swal.fire({
                                        title: "Deleted!",
                                        html: "<p class='text-danger'> The Category can not delete!</p>",
                                        type: "error",
                                    });
                                else
                                    Swal.fire({
                                        title: "Deleted!",
                                        html: "<p class='text-success'> The Category has been deleted! </p>",
                                        type: "success",
                                    });
                            }
                        });
                    } else if (result.dismiss === Swal.DismissReason.cancel) {
                        Swal.fire('Canceled!', 'Have been canceled', 'info')
                    }
                })
            });

            $('#upload_image_button').click(function() {
                selectFile('upload_image_button');
            });

            $('#remove_image_button').click(function() {
                $(this).attr('style', 'display: none');
                $('#category_thumbnail').val('/storage/placeholder.png');
                $('#upload_image_button').val('/storage/placeholder.png');
                $('#product_cat_thumbnail img').attr('src', '/storage/placeholder.png');
            });

            function selectFile(elementId) {
                CKFinder.modal({
                    skin: "neko",
                    resourceType: 'Images',
                    chooseFiles: true,
                    chooseFilesOnDblClick: true,
                    width: 800,
                    height: 600,
                    onInit: function(finder) {
                        finder.on('files:choose', function(evt) {
                            var file = evt.data.files.first();
                            var output = document.getElementById(elementId);
                            output.value = file.getUrl();
                            updateFile(file.getUrl(), file.get('name'));
                            $("#remove_image_button").attr('style', 'display: inline-block');
                        });

                        finder.on('file:choose:resizedImage', function(evt) {
                            var output = document.getElementById(elementId);
                            output.value = evt.data.resizedUrl;
                            updateFile(evt.data.resizedUrl, file.get('name'));
                            $("#remove_image_button").attr('style', 'display: inline-block');
                        });
                    }
                });
            }
            function updateFile(url, name) {
                $('#category_thumbnail').val(url);
                $('#product_cat_thumbnail img').attr('src', url);
            }
        });
    </script>
@endsection
