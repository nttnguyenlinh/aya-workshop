@extends('layouts.home')

@section('title', @Auth::user()->nicename . ' - AYA WORKSHOP')

@section('content')
<main id="main" style="height:800px;">
    <div class="my-account-header page-title normal-title">
        <div class="page-title-inner flex-row  container">
            <div class="flex-col flex-grow medium-text-center">
                <h1 class="uppercase mb-0" style="text-align:center;">My Account</h1>
                <p class="uppercase" style="text-align:center;">Dashboard</p>
            </div><!-- .flex-left -->
        </div><!-- flex-row -->
    </div><!-- .page-title -->

    <div class="page-wrapper my-account mb">
        <div class="container" role="main">
            <div class="row vertical-tabs">
                <div class="large-3 col col-border">
                    <div class="account-user circle">
                        <span class="image mr-half inline-block">
                            <img alt='AYA WORKSHOP' src="https://1.gravatar.com/avatar/1d0a073380ba3fc704e7aba68b250918"
                                class='avatar avatar-70 photo' height='70' width='70' />
                        </span>
                        <span class="user-name inline-block" style="text-transform: uppercase;">
                            {{Auth::user()->nicename}} <em class="user-id op-5">#{{Auth::user()->id}}</em>
                        </span>
                    </div>
                    <ul id="my-account-nav" class="account-nav nav nav-line nav-uppercase nav-vertical mt-half">
                        <li class="woocommerce-MyAccount-navigation-link">
                            <a href="{{route('myaccount')}}">Dashboard</a>
                            <!-- empty -->
                        </li>
                        <li class="woocommerce-MyAccount-navigation-link is-active active">
                            <a href="{{route('account.orders')}}">Orders</a>
                        </li>
                        <li class="woocommerce-MyAccount-navigation-link">
                            <a href="{{route('editAccount')}}">Account details</a>
                        </li>
                        <li class="woocommerce-MyAccount-navigation-link">
                            <a href="{{route('account.logout')}}">Logout</a>
                        </li>
                    </ul><!-- .account-nav -->
                </div><!-- .large-3 -->

                <div class="large-9 col">
                    <div class="woocommerce">
                        <div class="woocommerce-MyAccount-content">
                            <div class="woocommerce-notices-wrapper"></div>
                            @if(!empty($orders))
                            <div class="touch-scroll-table">
                                <table
                                    class="woocommerce-orders-table woocommerce-MyAccount-orders shop_table shop_table_responsive my_account_orders account-orders-table">
                                    <thead>
                                        <tr>
                                            <th
                                                class="woocommerce-orders-table__header woocommerce-orders-table__header-order-number">
                                                <span class="nobr">Order</span></th>
                                            <th
                                                class="woocommerce-orders-table__header woocommerce-orders-table__header-order-date">
                                                <span class="nobr">Date</span></th>
                                            <th
                                                class="woocommerce-orders-table__header woocommerce-orders-table__header-order-status">
                                                <span class="nobr">Status</span></th>
                                            <th
                                                class="woocommerce-orders-table__header woocommerce-orders-table__header-order-total">
                                                <span class="nobr">Total</span></th>
                                            <th
                                                class="woocommerce-orders-table__header woocommerce-orders-table__header-order-actions">
                                                <span class="nobr">Actions</span></th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @foreach($orders as $item)
                                        <tr
                                            class="woocommerce-orders-table__row woocommerce-orders-table__row--status-processing order">
                                            <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-number"
                                                data-title="Order">
                                                <a href="{{route('account.viewOrder', $item->id)}}">#{{$item->id}} </a>

                                            </td>
                                            <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-date"
                                                data-title="Date">
                                                <time
                                                    datetime="{{$item->created_at}}">{{Date_format($item->created_at, "Y-m-d")}}</time>

                                            </td>
                                            <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-status"
                                                data-title="Status">
                                                {{$item->title}}
                                            </td>
                                            <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-total"
                                                data-title="Total">
                                                <span class="woocommerce-Price-amount amount"><span
                                                        class="woocommerce-Price-currencySymbol">$</span>{{$item->total_amounts}}</span>
                                                for {{$item->total_item}} @if($item->total_item > 1) items @else item
                                                @endif
                                            </td>
                                            <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-actions"
                                                data-title="Actions">
                                                <a href="{{route('account.viewOrder', $item->id)}}"
                                                    class="woocommerce-button button view">View</a> </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            @else
                            <div
                                class="woocommerce-message woocommerce-message--info woocommerce-Message woocommerce-Message--info woocommerce-info">
                                <a class="woocommerce-Button button" href="{{route('products')}}">Go products</a>
                                No order has been made yet.
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div><!-- .row .vertical-tabs -->
        </div><!-- .container -->
    </div><!-- .page-wrapper.my-account  -->
</main><!-- #main -->
@endsection