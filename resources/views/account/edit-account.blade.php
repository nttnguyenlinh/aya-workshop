@extends('layouts.home')

@section('title', @Auth::user()->nicename . ' - AYA WORKSHOP')

@section('content')
<main id="main">
    <div class="my-account-header page-title normal-title">
        <div class="page-title-inner flex-row  container">
            <div class="flex-col flex-grow medium-text-center">
                <h1 class="uppercase mb-0" style="text-align:center;">My Account</h1>
                <p class="uppercase" style="text-align:center;">Account details</p>
            </div><!-- .flex-left -->
        </div><!-- flex-row -->
    </div><!-- .page-title -->

    <div class="page-wrapper my-account mb">
        <div class="container" role="main">
            <form action="{{route('editAccountSubmit')}}" method="post">
                @csrf
                <input type="hidden" name="id" value="{{Auth::user()->id}}" />
                <input type="hidden" name="customer_id" value="{{$customer->id}}"/>
                <div class="row vertical-tabs">
                    <div class="large-3 col col-border">
                        <div class="account-user circle">
                            <span class="image mr-half inline-block">
                                <img alt='AYA WORKSHOP' src="https://1.gravatar.com/avatar/1d0a073380ba3fc704e7aba68b250918" class='avatar avatar-70 photo' height='70' width='70' />
                            </span>
                            <span class="user-name inline-block" style="text-transform: uppercase;">
                                {{Auth::user()->nicename}} <em class="user-id op-5">#{{Auth::user()->id}}</em>
                            </span>
                        </div>
                        <ul id="my-account-nav" class="account-nav nav nav-line nav-uppercase nav-vertical mt-half">
                            <li class="woocommerce-MyAccount-navigation-link">
                                <a href="{{route('myaccount')}}">Dashboard</a>
                                <!-- empty -->
                            </li>
                            <li class="woocommerce-MyAccount-navigation-link">
                                <a href="{{route('account.orders')}}">Orders</a>
                            </li>
                            <li class="woocommerce-MyAccount-navigation-link is-active active">
                                <a href="{{route('editAccount')}}">Account details</a>
                            </li>
                            <li class="woocommerce-MyAccount-navigation-link">
                                <a href="{{route('account.logout')}}">Logout</a>
                            </li>
                        </ul><!-- .account-nav -->
                    </div><!-- .large-3 -->

                    <div class="large-4 col">
                        <div class="woocommerce">
                            <div class="woocommerce-MyAccount-content">
                                <div class="woocommerce-notices-wrapper"></div>
                                <p>
                                    <label>Full name <span class="required" style="color:red;">(*)</span></label>
                                    <input type="text" name="nicename" id="nicename" value="{{Auth::user()->nicename}}" required>
                                </p>
                                <div class="clear"></div>

                                <p>
                                    <label>Email address <span class="required" style="color:red;">(*)</span></label>
                                    <input type="email" name="email_address" id="email_address" autocomplete="email" value="{{Auth::user()->email}}" required readonly>
                                </p>

                                <legend>PASSWORD CHANGE</legend>

                                <span><em style="color:red;">Password must have at least 6 characters. Start with a character, containing at least 1 number</em></span>

                                <p>
                                    <label>Current password (leave blank to leave unchanged)</label>
                                    <input type="password" name="password_current" id="password_current" autocomplete=off readonly onfocus="this.removeAttribute('readonly');">
                                </p>
                                <div id="div_password_new" style="display:none;">
                                    <p>
                                        <label>New password (leave blank to leave unchanged)</label>
                                        <input type="password" name="password_1" id="password_1" value="" autocomplete=off>
                                    </p>
                                    <p>
                                        <label>Confirm new password</label>
                                        <input type="password" name="password_2" id="password_2" value="" autocomplete=off>
                                    </p>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>

                    <div class="large-5 col">
                        <div class="woocommerce">
                            <div class="woocommerce-MyAccount-content">
                                <div class="woocommerce-notices-wrapper"></div>
                                <h3>Billing address</h3>
                                <div class="woocommerce-address-fields">
                                    <div class="woocommerce-address-fields__field-wrapper">
                                        <p>
                                            <?php
                                                $countries = array(
                                                    "AF" => "Afghanistan",
                                                    "AX" => "&Aring;land Islands",
                                                    "AL" => "Albania",
                                                    "DZ" => "Algeria",
                                                    "AS" => "American Samoa",
                                                    "AD" => "Andorra",
                                                    "AO" => "Angola",
                                                    "AI" => "Anguilla",
                                                    "AQ" => "Antarctica",
                                                    "AG" => "Antigua and Barbuda",
                                                    "AR" => "Argentina",
                                                    "AM" => "Armenia",
                                                    "AW" => "Aruba",
                                                    "AU" => "Australia",
                                                    "AT" => "Austria",
                                                    "AZ" => "Azerbaijan",
                                                    "BS" => "Bahamas",
                                                    "BH" => "Bahrain",
                                                    "BD" => "Bangladesh",
                                                    "BB" => "Barbados",
                                                    "BY" => "Belarus",
                                                    "BE" => "Belgium",
                                                    "BZ" => "Belize",
                                                    "BJ" => "Benin",
                                                    "BM" => "Bermuda",
                                                    "BT" => "Bhutan",
                                                    "BO" => "Bolivia, Plurinational State of",
                                                    "BA" => "Bosnia and Herzegovina",
                                                    "BW" => "Botswana",
                                                    "BV" => "Bouvet Island",
                                                    "BR" => "Brazil",
                                                    "IO" => "British Indian Ocean Territory",
                                                    "BN" => "Brunei Darussalam",
                                                    "BG" => "Bulgaria",
                                                    "BF" => "Burkina Faso",
                                                    "BI" => "Burundi",
                                                    "KH" => "Cambodia",
                                                    "CM" => "Cameroon",
                                                    "CA" => "Canada",
                                                    "CV" => "Cape Verde",
                                                    "KY" => "Cayman Islands",
                                                    "CF" => "Central African Republic",
                                                    "TD" => "Chad",
                                                    "CL" => "Chile",
                                                    "CN" => "China",
                                                    "CX" => "Christmas Island",
                                                    "CC" => "Cocos (Keeling) Islands",
                                                    "CO" => "Colombia",
                                                    "KM" => "Comoros",
                                                    "CG" => "Congo",
                                                    "CD" => "Congo, the Democratic Republic of the",
                                                    "CK" => "Cook Islands",
                                                    "CR" => "Costa Rica",
                                                    "CI" => "C&ocirc;te d'Ivoire",
                                                    "HR" => "Croatia",
                                                    "CU" => "Cuba",
                                                    "CY" => "Cyprus",
                                                    "CZ" => "Czech Republic",
                                                    "DK" => "Denmark",
                                                    "DJ" => "Djibouti",
                                                    "DM" => "Dominica",
                                                    "DO" => "Dominican Republic",
                                                    "EC" => "Ecuador",
                                                    "EG" => "Egypt",
                                                    "SV" => "El Salvador",
                                                    "GQ" => "Equatorial Guinea",
                                                    "ER" => "Eritrea",
                                                    "EE" => "Estonia",
                                                    "ET" => "Ethiopia",
                                                    "FK" => "Falkland Islands (Malvinas)",
                                                    "FO" => "Faroe Islands",
                                                    "FJ" => "Fiji",
                                                    "FI" => "Finland",
                                                    "FR" => "France",
                                                    "GF" => "French Guiana",
                                                    "PF" => "French Polynesia",
                                                    "TF" => "French Southern Territories",
                                                    "GA" => "Gabon",
                                                    "GM" => "Gambia",
                                                    "GE" => "Georgia",
                                                    "DE" => "Germany",
                                                    "GH" => "Ghana",
                                                    "GI" => "Gibraltar",
                                                    "GR" => "Greece",
                                                    "GL" => "Greenland",
                                                    "GD" => "Grenada",
                                                    "GP" => "Guadeloupe",
                                                    "GU" => "Guam",
                                                    "GT" => "Guatemala",
                                                    "GG" => "Guernsey",
                                                    "GN" => "Guinea",
                                                    "GW" => "Guinea-Bissau",
                                                    "GY" => "Guyana",
                                                    "HT" => "Haiti",
                                                    "HM" => "Heard Island and McDonald Islands",
                                                    "VA" => "Holy See (Vatican City State)",
                                                    "HN" => "Honduras",
                                                    "HK" => "Hong Kong",
                                                    "HU" => "Hungary",
                                                    "IS" => "Iceland",
                                                    "IN" => "India",
                                                    "ID" => "Indonesia",
                                                    "IR" => "Iran, Islamic Republic of",
                                                    "IQ" => "Iraq",
                                                    "IE" => "Ireland",
                                                    "IM" => "Isle of Man",
                                                    "IL" => "Israel",
                                                    "IT" => "Italy",
                                                    "JM" => "Jamaica",
                                                    "JP" => "Japan",
                                                    "JE" => "Jersey",
                                                    "JO" => "Jordan",
                                                    "KZ" => "Kazakhstan",
                                                    "KE" => "Kenya",
                                                    "KI" => "Kiribati",
                                                    "KP" => "Korea, Democratic People's Republic of",
                                                    "KR" => "Korea, Republic of",
                                                    "KW" => "Kuwait",
                                                    "KG" => "Kyrgyzstan",
                                                    "LA" => "Lao People's Democratic Republic",
                                                    "LV" => "Latvia",
                                                    "LB" => "Lebanon",
                                                    "LS" => "Lesotho",
                                                    "LR" => "Liberia",
                                                    "LY" => "Libyan Arab Jamahiriya",
                                                    "LI" => "Liechtenstein",
                                                    "LT" => "Lithuania",
                                                    "LU" => "Luxembourg",
                                                    "MO" => "Macao",
                                                    "MK" => "Macedonia, the former Yugoslav Republic of",
                                                    "MG" => "Madagascar",
                                                    "MW" => "Malawi",
                                                    "MY" => "Malaysia",
                                                    "MV" => "Maldives",
                                                    "ML" => "Mali",
                                                    "MT" => "Malta",
                                                    "MH" => "Marshall Islands",
                                                    "MQ" => "Martinique",
                                                    "MR" => "Mauritania",
                                                    "MU" => "Mauritius",
                                                    "YT" => "Mayotte",
                                                    "MX" => "Mexico",
                                                    "FM" => "Micronesia, Federated States of",
                                                    "MD" => "Moldova, Republic of",
                                                    "MC" => "Monaco",
                                                    "MN" => "Mongolia",
                                                    "ME" => "Montenegro",
                                                    "MS" => "Montserrat",
                                                    "MA" => "Morocco",
                                                    "MZ" => "Mozambique",
                                                    "MM" => "Myanmar",
                                                    "NA" => "Namibia",
                                                    "NR" => "Nauru",
                                                    "NP" => "Nepal",
                                                    "NL" => "Netherlands",
                                                    "AN" => "Netherlands Antilles",
                                                    "NC" => "New Caledonia",
                                                    "NZ" => "New Zealand",
                                                    "NI" => "Nicaragua",
                                                    "NE" => "Niger",
                                                    "NG" => "Nigeria",
                                                    "NU" => "Niue",
                                                    "NF" => "Norfolk Island",
                                                    "MP" => "Northern Mariana Islands",
                                                    "NO" => "Norway",
                                                    "OM" => "Oman",
                                                    "PK" => "Pakistan",
                                                    "PW" => "Palau",
                                                    "PS" => "Palestinian Territory, Occupied",
                                                    "PA" => "Panama",
                                                    "PG" => "Papua New Guinea",
                                                    "PY" => "Paraguay",
                                                    "PE" => "Peru",
                                                    "PH" => "Philippines",
                                                    "PN" => "Pitcairn",
                                                    "PL" => "Poland",
                                                    "PT" => "Portugal",
                                                    "PR" => "Puerto Rico",
                                                    "QA" => "Qatar",
                                                    "RE" => "R&eacute;union",
                                                    "RO" => "Romania",
                                                    "RU" => "Russian Federation",
                                                    "RW" => "Rwanda",
                                                    "BL" => "Saint Barth&eacute;lemy",
                                                    "SH" => "Saint Helena, Ascension and Tristan da Cunha",
                                                    "KN" => "Saint Kitts and Nevis",
                                                    "LC" => "Saint Lucia",
                                                    "MF" => "Saint Martin (French part)",
                                                    "PM" => "Saint Pierre and Miquelon",
                                                    "VC" => "Saint Vincent and the Grenadines",
                                                    "WS" => "Samoa",
                                                    "SM" => "San Marino",
                                                    "ST" => "Sao Tome and Principe",
                                                    "SA" => "Saudi Arabia",
                                                    "SN" => "Senegal",
                                                    "RS" => "Serbia",
                                                    "SC" => "Seychelles",
                                                    "SL" => "Sierra Leone",
                                                    "SG" => "Singapore",
                                                    "SK" => "Slovakia",
                                                    "SI" => "Slovenia",
                                                    "SB" => "Solomon Islands",
                                                    "SO" => "Somalia",
                                                    "ZA" => "South Africa",
                                                    "GS" => "South Georgia and the South Sandwich Islands",
                                                    "ES" => "Spain",
                                                    "LK" => "Sri Lanka",
                                                    "SD" => "Sudan",
                                                    "SR" => "Suriname",
                                                    "SJ" => "Svalbard and Jan Mayen",
                                                    "SZ" => "Swaziland",
                                                    "SE" => "Sweden",
                                                    "CH" => "Switzerland",
                                                    "SY" => "Syrian Arab Republic",
                                                    "TW" => "Taiwan, Province of China",
                                                    "TJ" => "Tajikistan",
                                                    "TZ" => "Tanzania, United Republic of",
                                                    "TH" => "Thailand",
                                                    "TL" => "Timor-Leste",
                                                    "TG" => "Togo",
                                                    "TK" => "Tokelau",
                                                    "TO" => "Tonga",
                                                    "TT" => "Trinidad and Tobago",
                                                    "TN" => "Tunisia",
                                                    "TR" => "Turkey",
                                                    "TM" => "Turkmenistan",
                                                    "TC" => "Turks and Caicos Islands",
                                                    "TV" => "Tuvalu",
                                                    "UG" => "Uganda",
                                                    "UA" => "Ukraine",
                                                    "AE" => "United Arab Emirates",
                                                    "GB" => "United Kingdom",
                                                    "US" => "United States",
                                                    "UM" => "United States Minor Outlying Islands",
                                                    "UY" => "Uruguay",
                                                    "UZ" => "Uzbekistan",
                                                    "VU" => "Vanuatu",
                                                    "VE" => "Venezuela, Bolivarian Republic of",
                                                    "VN" => "Viet Nam",
                                                    "VG" => "Virgin Islands, British",
                                                    "VI" => "Virgin Islands, U.S.",
                                                    "WF" => "Wallis and Futuna",
                                                    "EH" => "Western Sahara",
                                                    "YE" => "Yemen",
                                                    "ZM" => "Zambia",
                                                    "ZW" => "Zimbabwe",
                                                );
                                            ?>
                                            <label>Country <span class="required" style="color:red;">(*)</span></label>
                                            <select required name="country" id="select-country" class="chosen-select" data-placeholder="Choose a country...">
                                                @foreach($countries as $key => $value)
                                                    @if($key == $customer->country)
                                                        <option value="{{$key}}" selected>{{$value}}</option>
                                                    @else
                                                        <option value="{{$key}}">{{$value}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </p>

                                        <p>
                                            <label>Address <span class="required" style="color:red;">(*)</span></label>
                                            <input type="text" name="address" id="address" placeholder="House number and street address" value="{{$customer->address}}" autocomplete required>
                                        </p>

                                        <p>
                                            <label>Town / City <span class="required" style="color:red;">(*)</span></label>
                                            <input type="text" name="city" id="city" placeholder="" value="{{$customer->city}}" autocomplete required>
                                        </p>

                                        <p>
                                            <label>State / County (optional)</label>
                                            <input type="text" name="county" id="county" placeholder="" value="{{$customer->county}}" autocomplete>
                                        </p>

                                        <p>
                                            <label>Postcode / ZIP (optional)</label>
                                            <input type="text" name="zipcode" id="zipcode" placeholder="" value="{{$customer->zipcode}}" autocomplete>
                                        </p>

                                        <p>
                                            <label>Phone <span class="required" style="color:red;">(*)</span></label>
                                            <input type="text" name="phone" id="phone" placeholder="" value="{{$customer->phone}}" autocomplete required>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- .row .vertical-tabs -->

                <center>
                    <button type="submit" class="button btn-block" name="save_account_details">Save changes</button>
                </center>
            </form>
        </div><!-- .container -->
    </div><!-- .page-wrapper.my-account  -->
</main><!-- #main -->

<script>
    jQuery(document).ready(function($) {
        $(".chosen-select").chosen();

        $('input[name=password_current]').bind("keyup change", function() {
            //$('#discount_datetime').toggle($(this).val().length);
            if($(this).val().length > 0)
                $('#div_password_new').show();
            else
                $('#div_password_new').hide();
        });
    });
</script>

@endsection