@extends("layouts.home")

@section("title", __('my_account'))

@section("content")

<main id="main" style="min-height:800px;">
    <div class="my-account-header page-title normal-title">
        <div class="page-title-inner flex-row  container">
            <div class="flex-col flex-grow medium-text-center">
                <div class="text-center social-login">
                    <h1 class="uppercase mb-0">{{__('my_account')}}</h1>
                </div>
            </div><!-- .flex-left -->
        </div><!-- flex-row -->
    </div><!-- .page-title -->
    <div class="page-wrapper my-account mb">
        <div class="container" role="main">
            <div class="woocommerce">
                <div class="woocommerce-notices-wrapper"></div>
                <div class="account-container lightbox-inner">
                    <div class="col2-set row row-divided row-large" id="customer_login">
                        <div class="col-1 large-6 col pb-0">
                            <div class="account-login-inner">
                                <h3 class="uppercase">{{__('login')}}</h3>
                                <form class="woocommerce-form woocommerce-form-login login" method="post"
                                    action="{{route('account.login')}}">
                                    @csrf
                                    <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                        <label>{{__('email')}}<span class="required"
                                                style="color:red;">(*)</span></label>
                                        <input type="email" class="woocommerce-Input woocommerce-Input--text input-text"
                                            name="email" value="{{old('email')}}" maxlength="50" required autofocus />
                                    </p>
                                    <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                        <label for="password">{{__('password')}}<span class="required"
                                                style="color:red;">(*)</span></label>
                                        <input class="woocommerce-Input woocommerce-Input--text input-text"
                                            type="password" name="password" required />
                                    </p>
                                    <p class="form-row">
                                        <div class="g-recaptcha"
                                            style="display: flex; align-items: center; justify-content: center; margin-bottom: 10px;"
                                            data-sitekey="{{config('app.recaptcha_site_key')}}"></div>
                                    </p>

                                    <p class="form-row">
                                        <label
                                            class="woocommerce-form__label woocommerce-form__label-for-checkbox inline">
                                            <input class="woocommerce-form__input woocommerce-form__input-checkbox"
                                                name="remember" type="checkbox"
                                                checked="{{old('remember') ? 'checked' : ''}}" />
                                            <span>{{__('remember')}}</span>
                                        </label>
                                    </p>

                                    <p class="form-row" style="text-align:center;">
                                        <button type="submit"
                                            class="woocommerce-Button button">{{__('login')}}</button><br>
                                    </p>

                                    <p class="form-row"></p>
                                    <a href="{{route('password.request') }}">{{__('lost_password')}}</a>
                                    </p>
                                </form>
                            </div><!-- .login-inner -->
                        </div>

                        <div class="col-2 large-6 col pb-0">
                            <div class="account-register-inner">
                                <h3 class="uppercase">{{__('register')}}</h3>
                                <form class="woocommerce-form woocommerce-form-login login" method="post"
                                    action="{{route('account.register')}}">
                                    @csrf
                                    <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                        <label>{{__('name')}}<span class="required"
                                                style="color:red;">(*)</span></label>
                                        <input type="text" class="woocommerce-Input woocommerce-Input--text input-text"
                                            name="name" value="{{old('name')}}" maxlength="50" required />
                                    </p>

                                    <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                        <label>{{__('email')}}<span class="required"
                                                style="color:red;">(*)</span></label>
                                        <input type="email" class="woocommerce-Input woocommerce-Input--text input-text"
                                            name="email" value="{{old('email')}}" maxlength="50" required /> </p>
                                    <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                        <label for="password">{{__('password')}}<span class="required"
                                                style="color:red;">(*)</span></label>
                                        <input class="woocommerce-Input woocommerce-Input--text input-text"
                                            type="password" name="password" required />
                                    </p>

                                    <p class="form-row">
                                        <div class="g-recaptcha"
                                            style="display: flex; align-items: center; justify-content: center; margin-bottom: 10px;"
                                            data-sitekey="{{config('app.recaptcha_site_key')}}"></div>
                                    </p>

                                    <p class="form-row" style="text-align:center;">
                                        <button type="submit"
                                            class="woocommerce-Button button">{{__('register')}}</button>
                                    </p>
                                </form>
                            </div><!-- .register-inner -->
                        </div><!-- .large-6 -->
                    </div> <!-- .row -->
                    <br><br>
                </div><!-- .account-login-container -->
            </div>
            <div style="width:20%; float:right;">
                <div class="custom-sel">
                    @if(\App::isLocale('en'))
                    <a class="selected" href="#" data-lang="en">
                        <span class="flag-icon flag-icon-us"></span>{{__("en")}}
                    </a>
                    <a class="hidden" href="#" data-lang="vi">
                        <span class="flag-icon flag-icon-vn"></span>{{__("vi")}}
                    </a>
                    @elseif(\App::isLocale('vi'))
                    <a class="selected" href="#" data-lang="vi">
                        <span class="flag-icon flag-icon-vn"></span>{{__("vi")}}
                    </a>
                    <a class="hidden" href="#" data-lang="en">
                        <span class="flag-icon flag-icon-us"></span>{{__("en")}}
                    </a>
                    @endif
                </div>
            </div>
        </div><!-- .page-wrapper.my-account  -->
    </div><!-- .container -->
</main><!-- #main -->

<script>
jQuery(function($) {

    $('.custom-sel .selected').mouseenter(function() {
        $('.custom-sel').addClass('show-sel');
        $('.custom-sel a').removeClass('hidden');
    });

    $('.custom-sel .hidden').on('click', function(e) {
        var lang = $(this).attr('data-lang');
        $.ajax({
            url: "/locale/" + lang,
            type: "get",
            success: function(data) {
                location.reload();
            }
        });
    });

    $('.custom-sel').mouseleave(function() {
        $('.custom-sel').removeClass('show-sel');
        $('.custom-sel a:not(:first)').addClass('hidden');
    });


});
</script>
@endsection