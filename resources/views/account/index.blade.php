@extends('layouts.home')

@section('title', @Auth::user()->nicename . ' - AYA WORKSHOP')

@section('content')
<main id="main" style="height:800px;">
    <div class="my-account-header page-title normal-title">
        <div class="page-title-inner flex-row  container">
            <div class="flex-col flex-grow medium-text-center">
                <h1 class="uppercase mb-0" style="text-align:center;">My Account</h1>
                <p class="uppercase" style="text-align:center;">Dashboard</p>
            </div><!-- .flex-left -->
        </div><!-- flex-row -->
    </div><!-- .page-title -->

    <div class="page-wrapper my-account mb">
        <div class="container" role="main">
            <div class="row vertical-tabs">
                <div class="large-3 col col-border">
                    <div class="account-user circle">
                        <span class="image mr-half inline-block">
                            <img alt='AYA WORKSHOP' src="https://1.gravatar.com/avatar/1d0a073380ba3fc704e7aba68b250918" class='avatar avatar-70 photo' height='70' width='70' />
                        </span>
                        <span class="user-name inline-block" style="text-transform: uppercase;">
                        {{Auth::user()->nicename}} <em class="user-id op-5">#{{Auth::user()->id}}</em>
                        </span>
                    </div>
                    <ul id="my-account-nav" class="account-nav nav nav-line nav-uppercase nav-vertical mt-half">
                        <li class="woocommerce-MyAccount-navigation-link is-active active">
                            <a href="{{route('myaccount')}}">Dashboard</a>
                            <!-- empty -->
                        </li>
                        <li class="woocommerce-MyAccount-navigation-link">
                            <a href="{{route('account.orders')}}">Orders</a>
                        </li>
                        <li class="woocommerce-MyAccount-navigation-link">
                            <a href="{{route('editAccount')}}">Account details</a>
                        </li>
                        <li class="woocommerce-MyAccount-navigation-link">
                            <a href="{{route('account.logout')}}">Logout</a>
                        </li>
                    </ul><!-- .account-nav -->
                </div><!-- .large-3 -->

                <div class="large-9 col">
                    <div class="woocommerce">
                        <div class="woocommerce-MyAccount-content">
                            <div class="woocommerce-notices-wrapper"></div>
                            <p>Hello <strong style="text-transform: uppercase;">{{Auth::user()->nicename}}</strong> (not <strong style="text-transform: uppercase;">{{Auth::user()->nicename}}</strong>? <a href="{{route('account.logout')}}">Log out</a>)</p>
                            <p>From your account dashboard you can view your 
                                <a href="{{route('account.orders')}}">recent orders</a>
                                and <a href="{{route('editAccount')}}">change your password and account details</a>.</p>
                            <ul class="dashboard-links">
                                <li class="woocommerce-MyAccount-navigation-link is-active active">
                                    <a href="{{route('myaccount')}}">Dashboard</a>
                                </li>
                                <li class="woocommerce-MyAccount-navigation-link">
                                    <a href="{{route('account.orders')}}">Orders</a>
                                </li>

                                <li class="woocommerce-MyAccount-navigation-link">
                                    <a href="{{route('editAccount')}}">Account details</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div><!-- .large-9 -->
            </div><!-- .row .vertical-tabs -->
        </div><!-- .container -->
    </div><!-- .page-wrapper.my-account  -->
</main><!-- #main -->
@endsection