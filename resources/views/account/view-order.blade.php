@extends('layouts.home')

@section('title', @Auth::user()->nicename . ' - AYA WORKSHOP')

@section('content')
<main id="main" style="height:800px;">
    <div class="my-account-header page-title normal-title">
        <div class="page-title-inner flex-row  container">
            <div class="flex-col flex-grow medium-text-center">
                <h1 class="uppercase mb-0" style="text-align:center;">My Account</h1>
                <p class="uppercase" style="text-align:center;">Dashboard</p>
            </div><!-- .flex-left -->
        </div><!-- flex-row -->
    </div><!-- .page-title -->

    <div class="page-wrapper my-account mb">
        <div class="container" role="main">
            <div class="row vertical-tabs">
                <div class="large-3 col col-border">
                    <div class="account-user circle">
                        <span class="image mr-half inline-block">
                            <img alt='AYA WORKSHOP' src="https://1.gravatar.com/avatar/1d0a073380ba3fc704e7aba68b250918"
                                class='avatar avatar-70 photo' height='70' width='70' />
                        </span>
                        <span class="user-name inline-block" style="text-transform: uppercase;">
                            {{Auth::user()->nicename}} <em class="user-id op-5">#{{Auth::user()->id}}</em>
                        </span>
                    </div>
                    <ul id="my-account-nav" class="account-nav nav nav-line nav-uppercase nav-vertical mt-half">
                        <li class="woocommerce-MyAccount-navigation-link">
                            <a href="{{route('myaccount')}}">Dashboard</a>
                            <!-- empty -->
                        </li>
                        <li class="woocommerce-MyAccount-navigation-link is-active active">
                            <a href="{{route('account.orders')}}">Orders</a>
                        </li>
                        <li class="woocommerce-MyAccount-navigation-link">
                            <a href="{{route('editAccount')}}">Account details</a>
                        </li>
                        <li class="woocommerce-MyAccount-navigation-link">
                            <a href="{{route('account.logout')}}">Logout</a>
                        </li>
                    </ul><!-- .account-nav -->
                </div><!-- .large-3 -->

                <div class="large-9 col">
                    <div class="woocommerce">
                        <div class="woocommerce-MyAccount-content">
                            <div class="woocommerce-notices-wrapper"></div>
                            <p>Order <mark class="order-number">#{{$bill->id}}</mark> was placed on <mark
                                    class="order-date">{{Date_format($bill->created_at, "Y-m-d")}}</mark> and is
                                currently <mark class="order-status">{{$bill->title}}</mark>.</p>
                            <section class="woocommerce-order-details">
                                <h2 class="woocommerce-order-details__title">Order details</h2>
                                <table
                                    class="woocommerce-table woocommerce-table--order-details shop_table order_details">
                                    <thead>
                                        <tr>
                                            <th class="woocommerce-table__product-name product-name">Product</th>
                                            <th class="woocommerce-table__product-table product-total">Total</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @foreach($bill_details as $item)
                                        <tr class="woocommerce-table__line-item order_item">
                                            <td class="woocommerce-table__product-name product-name">
                                                <a href="{{route('productDetails', $item->slug)}}">{{$item->name}}</a>
                                                <strong class="product-quantity">× {{$item->quantity}}</strong><strong
                                                    class="product-quantity">× <span
                                                        class="woocommerce-Price-currencySymbol">$</span>{{$item->price}}</strong>
                                            </td>

                                            <td class="woocommerce-table__product-total product-total">
                                                <span class="woocommerce-Price-amount amount"><span
                                                        class="woocommerce-Price-currencySymbol">$</span>{{$item->quantity * $item->price}}</span>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th scope="row">Payment method:</th>
                                            <td>{{$bill->payment > 0 ? "VNPAY" : "Cash on delivery"}}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Payment info:</th>
                                            <td>{{$bill->payment_info}}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Total:</th>
                                            <td><span class="woocommerce-Price-amount amount"><span
                                                        class="woocommerce-Price-currencySymbol">$</span>{{$bill->total_amounts}}</span>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>

                            </section>
                            <section class="woocommerce-customer-details">
                                <h2 class="woocommerce-column__title">Billing address</h2>
                                <address>
                                    {{$user->nicename}}<br>{{$customer->address}}<br>{{$customer->city}}<br>{{$customer->county}}<br>{{$customer->country}}
                                    <p class="woocommerce-customer-details--phone">{{$customer->phone}}</p>

                                    <p class="woocommerce-customer-details--email">{{$user->email}}</p>
                                </address>
                            </section>
                        </div>
                    </div>
                </div>
            </div><!-- .row .vertical-tabs -->
        </div><!-- .container -->
    </div><!-- .page-wrapper.my-account  -->
</main><!-- #main -->
@endsection