@extends('layouts.home')
@section('title', 'Checkout - AYA WORKSHOP')
@section('content')

<main id="main" style="min-height:800px;">
    <div class="checkout-page-title page-title">
        <div class="page-title-inner flex-row medium-flex-wrap container">
            <div class="flex-col flex-grow medium-text-center">
                <nav class="breadcrumbs heading-font checkout-breadcrumbs text-center h2 strong">
                    <a href="{{route('cart')}}" class="hide-for-small">Shopping Cart</a>
                    <span class="divider hide-for-small"><i class="icon-angle-right"></i></span>
                    <a href="{{route('checkout')}}" class="hide-for-small">Checkout details</a>
                    <span class="divider hide-for-small"><i class="icon-angle-right"></i></span>
                    <a href="#" class="no-click current">Order Complete</a>
                </nav>
            </div><!-- .flex-left -->
        </div><!-- flex-row -->
    </div><!-- .page-title -->

    <div class="cart-container container page-wrapper page-checkout">
        <div class="woocommerce">
            <div class="row">
                @if(Session::has('received'))
                <div class="large-7 col">
                    <section class="woocommerce-order-details">
                        <h2 class="woocommerce-order-details__title">Order details</h2>
                        <table class="woocommerce-table woocommerce-table--order-details shop_table order_details">
                            <thead>
                                <tr>
                                    <th class="woocommerce-table__product-name product-name">Product</th>
                                    <th class="woocommerce-table__product-table product-total">Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach(Session::get('received')[0]['cart'] as $item)
                                <tr class="woocommerce-table__line-item order_item">
                                    <td class="woocommerce-table__product-name product-name">
                                        <a
                                            href="{{route('productDetails', [$item->options->slug])}}">{{$item->name}}</a>
                                        <strong class="product-quantity">× {{$item->qty}}</strong> &nbsp; <strong
                                            class="product-quantity">× <span
                                                class="woocommerce-Price-currencySymbol">$</span>{{$item->price}}</strong>
                                    </td>

                                    <td class="woocommerce-table__product-total product-total">
                                        <span class="woocommerce-Price-amount amount"><span
                                                class="woocommerce-Price-currencySymbol">$</span>{{number_format($item->qty * $item->price, 2)}}</span>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th scope="row">Payment method:</th>
                                    <td>{{Session::get('received')[0]['payment'] > 0 ? "VNPAY" : "Cash on delivery"}}
                                    </td>
                                </tr>

                                <tr>
                                    <th scope="row">Subtotal:</th>
                                    <td><span class="woocommerce-Price-amount amount"><span
                                                class="woocommerce-Price-currencySymbol">$</span>{{Session::get('received')[0]['subtotal_amount']}}</span>
                                    </td>
                                </tr>

                                <tr>
                                    <th scope="row">Discount:</th>
                                    <td>-<span class="woocommerce-Price-amount amount"><span
                                                class="woocommerce-Price-currencySymbol">$</span>{{Session::get('received')[0]['coupon_amount']}}</span>
                                    </td>
                                </tr>

                                <tr>
                                    <th scope="row">Total:</th>
                                    <td><strong><span class="woocommerce-Price-amount amount"><span
                                                    class="woocommerce-Price-currencySymbol">$</span>{{Session::get('received')[0]['total']}}</span></strong>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>

                    </section>

                    <section class="woocommerce-customer-details">
                        <h2 class="woocommerce-column__title">Billing address</h2>

                        <address>

                            {{Session::get('received')[0]['info']['name']}}<br>{{Session::get('received')[0]['info']['address']}}<br>
                            <p class="woocommerce-customer-details--phone">
                                {{Session::get('received')[0]['info']['phone']}}</p>
                            <p class="woocommerce-customer-details--email">
                                {{Session::get('received')[0]['info']['email']}}</p>

                        </address>
                    </section>
                </div>

                <div class="large-5 col">
                    <div class="is-well col-inner entry-content">
                        @if(Session::get('paymentInfo')[0]['payment_code'] == 0)
                        <p class="success-color woocommerce-notice">
                            <strong>{{Session::get('paymentInfo')[0]['payment_message']}} Thank you! Your order has
                                been received.</strong>
                        </p>
                        @elseif(Session::get('paymentInfo')[0]['payment_code'] > 0)
                        <p class="danger-color woocommerce-notice">
                            <strong>{{Session::get('paymentInfo')[0]['payment_message']}}</strong>
                        </p>
                        @elseif(Session::get('paymentInfo')[0]['payment_code'] < 0) <p
                            class="danger-color woocommerce-notice">
                            <strong>{{Session::get('paymentInfo')[0]['payment_message']}}</strong>
                            </p>
                            @endif

                            <ul class="woocommerce-order-overview order_details">
                                <li class="woocommerce-order-overview__order order">
                                    Order number: <strong>{{Session::get('received')[0]['bill_id']}}</strong>
                                </li>

                                <li class="woocommerce-order-overview__date date">
                                    Date: <strong>{{Carbon\Carbon::now()->format('Y-m-d')}}</strong>
                                </li>

                                <li class="woocommerce-order-overview__total total">
                                    Email: <strong>{{Session::get('received')[0]['info']['email']}}</strong>
                                </li>

                                <li class="woocommerce-order-overview__total total">
                                    Total: <strong><span class="woocommerce-Price-amount amount"><span
                                                class="woocommerce-Price-currencySymbol">$</span>{{Session::get('received')[0]['total']}}</span></strong>
                                </li>

                                <li class="woocommerce-order-overview__payment-method method">
                                    Payment method:
                                    <strong>{{Session::get('received')[0]['payment'] > 0 ? "VNPAY" : "Cash on delivery"}}</strong>
                                </li>

                            </ul>

                            @if(Session::get('paymentInfo')[0]['payment_code'] == 0)
                            {{Session::get('paymentInfo')[0]['payment_message']}} Thank you! Your order has been
                            received.
                            @elseif(Session::get('paymentInfo')[0]['payment_code'] > 0)
                            {{Session::get('paymentInfo')[0]['payment_message']}}
                            @elseif(Session::get('paymentInfo')[0]['payment_code'] < 0)
                                {{Session::get('paymentInfo')[0]['payment_message']}} @endif <div class="clear">
                    </div>
                </div>
            </div>

            @php
            Session::forget('received'); Session::forget('paymentInfo'); Session::forget('Coupon');
            @endphp
            @else
            <p class="danger-color woocommerce-notice">Sorry! Payment information does not exist, the session has
                expired.</p>
            @endif
        </div>
    </div>
    </div>
</main>

@endsection