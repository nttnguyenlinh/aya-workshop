@extends('layouts.home')

@section('title', "Products - AYA WORKSHOP")

@section('content')


<div class="shop-page-title category-page-title page-title ">
    <div class="page-title-inner flex-row  medium-flex-wrap container">
        <div class="flex-col flex-grow medium-text-center">
            <div class="is-large">
                <nav class="woocommerce-breadcrumb breadcrumbs"><a href="{{url('/')}}">Home</a> <span class="divider">/</span> <a href="{{route('products')}}">Products</a> <span class="divider">/</span> {{$category_name->name}}</nav>
            </div>
            <div class="category-filtering category-filter-row show-for-medium">
                <a href="#" data-open="#shop-sidebar" data-visible-after="true" data-pos="left" class="filter-button uppercase plain">
                    <i class="icon-menu"></i>
                    <strong>Filter</strong>
                </a>
                <div class="inline-block">
                </div>
            </div>
        </div><!-- .flex-left -->
        <div class="flex-col medium-text-center">
            <p class="woocommerce-result-count hide-for-medium">
                Showing all {{$products->count()}} results</p>
            <form class="woocommerce-ordering" method="get">
                <select name="orderby" class="orderby">
                    <option value="" selected>Default sorting</option>
                    <option value="oldest">Sort by oldest</option>
                    <option value="latest">Sort by latest</option>
                    <option value="price">Sort by price: low to high</option>
                    <option value="price-desc">Sort by price: high to low</option>
                </select>
            </form>
        </div><!-- .flex-right -->

    </div><!-- flex-row -->
</div>

<main id="main" style="min-height:800px;">
    <div class="row category-page-row">
        <div class="col large-9">
            <div class="shop-container">    
                <div class="products row row-small large-columns-4 medium-columns-3 small-columns-2 has-equal-box-heights">
                    @if($products->count() > 0)
                        @foreach($products as $product)
                            <div class="product-small col has-hover">
                                <div class="col-inner">
                                    <div class="badge-container absolute left top z-1"></div>
                                    <div class="product-small box ">
                                        <div class="box-image">
                                            <div class="image-zoom-fade">
                                                <a href="{{route('productDetails', $product->slug)}}">
                                                    <img width="300" height="300" src="{{asset('storage/'.$product->thumbnail)}}" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="{{$product->name}}" /></a>
                                            </div>

                                            @if(!empty($product->sale) && $product->sale_start <= Carbon\Carbon::now() && Carbon\Carbon::now() <= $product->sale_end)
                                            <div class="image-tools top right show-on-hover">
                                                <div class="callout badge badge-circle">
                                                    <div class="badge-inner secondary on-sale"><span class="onsale">-{{round(100 - (($product->sale * 100)/$product->price))}}%</span></div>
                                                </div>
                                            </div>
                                            @endif

                                        </div><!-- box-image -->

                                        <div class="box-text box-text-products text-center grid-style-2">
                                            <div class="title-wrapper">
                                                <p class="category uppercase is-smaller no-text-overflow product-cat op-7">{{$product->category_name }}</p>
                                                <p class="name product-title"><a href="{{route('productDetails', $product->slug)}}">{{$product->name}}</a></p>
                                            </div>
                                            <div class="price-wrapper">
                                            @if(!empty($product->sale) && $product->sale_start <= Carbon\Carbon::now() && Carbon\Carbon::now() <= $product->sale_end)
                                                <span class="price">
                                                    <del>
                                                        <span class="woocommerce-Price-amount amount">
                                                            <span class="woocommerce-Price-currencySymbol">$</span>{{number_format($product->price, 2)}}
                                                        </span>
                                                    </del>
                                                    <ins>
                                                        <span class="woocommerce-Price-amount amount">
                                                            <span class="woocommerce-Price-currencySymbol">$</span>{{number_format($product->sale, 2)}}
                                                        </span>
                                                    </ins>
                                                </span>
                                                @else
                                                <span class="price">
                                                    <ins>
                                                        <span class="woocommerce-Price-amount amount">
                                                            <span class="woocommerce-Price-currencySymbol">$</span>{{number_format($product->price, 2)}}
                                                        </span>
                                                    </ins>
                                                </span>
                                                @endif
                                            </div>
                                        </div><!-- box-text -->
                                    </div><!-- box -->
                                </div><!-- .col-inner -->
                            </div><!-- col -->
                        @endforeach 
                    @else
                        No products were found matching your selection.
                    @endif
                </div><!-- row -->
                <div class="container">
                    <ul class="pagination links text-center">
                        {{$products->links()}}
                    </ul>
                </div>

            </div><!-- shop container -->
        </div><!-- col-fit  -->

        <div class="large-3 col hide-for-medium ">
            <div id="shop-sidebar" class="sidebar-inner">
                <aside class="widget woocommerce"><span class="widget-title shop-sidebar">Categories</span>
                    <div class="is-divider small"></div>
                    <ul>
                        @foreach($categories as $category)
                        <li class="cat-item">
                            <a href="{{route('product-category', $category->slug)}}">{{$category->name}}</a>
                        </li>
                        @endforeach
                    </ul>
                </aside>
            </div><!-- .sidebar-inner -->
        </div><!-- large-3 -->
    </div>

</main>

@endsection