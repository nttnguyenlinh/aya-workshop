@extends('layouts.home')

@section('title', 'Track your order - AYA WORKSHOP')

@section('content')

<main id="main" style="min-height:800px;">
    <div class="my-account-header page-title normal-title">
        <div class="page-title-inner flex-row  container">
            <div class="flex-col flex-grow medium-text-center">
                <div class="text-center social-login">
                    <h1 class="uppercase mb-0">Track your order</h1>
                </div>
            </div><!-- .flex-left -->
        </div><!-- flex-row -->
    </div><!-- .page-title -->

    <div class="page-wrapper my-account mb">
        <div class="container" role="main">
            <div class="woocommerce">
                <form action="" method="post" class="track_order">
                    @csrf
                    <p>To track your order please enter your Order ID in the box below and press the "Track" button. This was given to you on your receipt and in the confirmation email you should have received.</p>
                    <p class="form-row form-row-first"><label>Order ID</label> <input class="input-text" type="text" name="order_id" id="order_id" value="" placeholder="Found in your order confirmation email."></p>
                    <p class="form-row form-row-last"><label>Billing email</label> <input class="input-text" type="text" name="order_email" id="order_email" value="" placeholder="Email you used during checkout."></p>
                    <div class="clear"></div>

                    <p class="form-row text-center"><button type="submit" class="button" value="Track" disabled>Coming Soon...</button></p>
                </form>
            </div>
        </div><!-- .container -->
    </div><!-- .page-wrapper.my-account  -->
</main>
@endsection