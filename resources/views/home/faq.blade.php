@extends('layouts.home')

@section('title', __('faq') . " - " . config('app.name'))

@section('content')
<main id="main">
    <div id="content" role="main" class="content-area">
        <div class="row align-equal align-center">
            <div class="col large-12">
                <div class="col-inner" style="padding:50px 0px 0px 0px;">
                    <div class="tabbed-content">
                        <h1 class="uppercase text-center">{{__('faqs')}}</h1>
                    </div>
                </div>
            </div>
            <div class="col large-12">
                <div class="col-inner">
                    <div class="tabbed-content faq-tabbed-content">
                        <ul class="nav nav-outline nav-vertical nav-uppercase nav-size-xlarge nav-left">
                            <li class="tab active has-icon">
                                <a href="#{{$data[2]['title']}}"><span>{{__($data[0]['title'])}}</span></a>
                            </li>
                            <li class="tab has-icon">
                                <a href="#{{$data[2]['title']}}"><span>{{__($data[1]['title'])}}</span></a>
                            </li>
                            <li class="tab has-icon">
                                <a href="#{{$data[2]['title']}}"><span>{{__($data[2]['title'])}}</span></a>
                            </li>
                            <li class="tab has-icon">
                                <a href="#contact"><span>{{__('contact')}}</span></a>
                            </li>
                        </ul>
                        <div class="tab-panels">
                            <div class="panel active entry-content" id="delivery">
                                <h2 class="uppercase text-center" style="color:#A7D6D1;">
                                    {{__($data[0]['title'])}}
                                </h2>
                                {!!$data[0]['contents']!!}
                            </div>

                            <div class="panel entry-content" id="warranty">
                                <h2 class="uppercase text-center" style="color:#A7D6D1;">
                                    {{__($data[1]['title'])}}
                                </h2>
                                {!!$data[1]['contents']!!}
                            </div>
                            <div class="panel entry-content" id="quality">
                                <h2 class="uppercase text-center" style="color:#A7D6D1;">
                                    {{__($data[2]['title'])}}
                                </h2>
                                {!!$data[2]['contents']!!}
                            </div>

                            <div class="panel entry-content" id="contact">
                                <h2 class="uppercase text-center" style="color:#A7D6D1;">
                                    {{__('contact_us')}}
                                </h2>
                                <div role="form" class="wpcf7">
                                    {{ Form::open(['route' => 'faq.store', 'method' => 'POST', 'class' => 'wpcf7-form']) }}
                                    <p>
                                        {!!Html::decode(Form::label('name', __('name') . ' (<span style="color:red;">' .
                                            __('required_label') . '</span>)'))!!}
                                        <span class="wpcf7-form-control-wrap">
                                            <input type="text" name="your_name" value="{{old('your_name')}}"
                                                class="wpcf7-form-control wpcf7-text" maxlength="20" required
                                                autofocus />
                                        </span>
                                    </p>
                                    <p>
                                        {!!Html::decode(Form::label('email', __('email') . ' (<span style="color:red;">'
                                            . __('required_label') . '</span>)'))!!}
                                        <span class="wpcf7-form-control-wrap">
                                            <input type="email" name="your_email" value="{{old('your_email')}}"
                                                class="wpcf7-form-control wpcf7-email" maxlength="35" required />
                                        </span>
                                    </p>
                                    <p>
                                        {!!Html::decode(Form::label('message', __('message') . ' (<span
                                            style="color:red;">' . __('required_label') . '</span>)'))!!}
                                        <span class="wpcf7-form-control-wrap your-message">
                                            <textarea class="wpcf7-form-control wpcf7-textarea" cols="40" rows="10"
                                                maxlength="150" required
                                                name="your_message">{{old('your_message')}}</textarea>
                                        </span>
                                    </p>
                                    <div class="g-recaptcha"
                                        style="display: flex; align-items: center; justify-content: center; margin-bottom: 10px;"
                                        data-sitekey="{{config('app.recaptcha_site_key')}}"></div>
                                    <p class="form-row" style="text-align:center;">
                                        {{Form::submit(__('send'), ['name' => 'submit', 'class' =>
                                        'wpcf7-form-control wpcf7-submit button'])}}
                                    </p>
                                    {{ Form::close() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<!-- #main -->
@endsection
