@extends('layouts.home')

@section('title', config('app.name') . ' - ' . config('app.subname'))

@section('content')
    @menu('primary-menu');
@endsection


{{--                            <ul class="nav nav-right nav-uppercase">--}}
                                <li class="menu-item">
                                    <a href="{{url('/')}}" class="nav-top-link">{{__('home')}}</a>
                                </li>

{{--                                <?php--}}
{{--                                    $cate_parents = \App\Category::where('parent', 0)->get();--}}
{{--                                    $cate_childs = \App\Category::where('parent', '!=', 0)->get();--}}
{{--                                ?>--}}
{{--                                @foreach($cate_parents as $parent)--}}
{{--                                    <?php--}}
{{--                                        $class = '';--}}
{{--                                        $icon = '';--}}
{{--                                        $count = \App\Category::where('parent', $parent->id)->count();--}}
{{--                                        if ($count > 0) {--}}
{{--                                            $class = 'has-dropdown';--}}
{{--                                            $icon = '<i class="icon-angle-down"></i>';--}}
{{--                                        } else {--}}
{{--                                            $class = '';--}}
{{--                                            $icon = '';--}}
{{--                                        }--}}
{{--                                    ?>--}}
{{--                                <li class="menu-item {{$class}}">--}}
{{--                                    <a href="{{url('/', $parent->slug)}}" class="nav-top-link">{{__($parent->name)}}--}}
{{--                                        {!!Html::decode($icon)!!}</a>--}}

{{--                                    @if( $count > 0)--}}
{{--                                    <ul class='nav-dropdown nav-dropdown-simple dropdown-uppercase'>--}}
{{--                                        @foreach($cate_childs as $child)--}}
{{--                                        @if($parent->id == $child->parent)--}}
{{--                                            @if($parent->type == 'product')--}}
{{--                                            <li class="menu-item">--}}
{{--                                                <a href="{{route('product-category', $child->slug)}}">{{__($child->name)}}</a>--}}
{{--                                            </li>--}}
{{--                                            @else--}}
{{--                                            <li class="menu-item">--}}
{{--                                                <a href="{{route('post-category', $child->slug)}}">{{__($child->name)}}</a>--}}
{{--                                            </li>--}}
{{--                                            @endif--}}
{{--                                        @endif--}}
{{--                                        @endforeach--}}
{{--                                    </ul>--}}
{{--                                    @endif--}}
{{--                                </li>--}}
{{--                                @endforeach--}}

                                <li class="menu-item">
                                    <a href="{{route('contact.create')}}" class="nav-top-link">{{__('contact')}}</a>
                                </li>
                                <li class="menu-item">
                                    <a href="{{route('track.order')}}" class="nav-top-link">{{__('track_orders')}}</a>
                                </li>
                                <li class="has-icon has-dropdown">
                                    <a href="#" class="is-small"><i class="icon-search"></i></a>
                                    <ul class="nav-dropdown nav-dropdown-simple dropdown-uppercase">
                                        <li class="header-search-form search-form html relative has-icon">
                                            <div class="header-search-form-wrapper">
                                                <div class="searchform-wrapper ux-search-box relative is-normal">
                                                    <form role="search" method="get" class="searchform" action="/">
                                                        <div class="flex-row relative">
                                                            <div class="flex-col flex-grow">
                                                                <input type="search" class="search-field mb-0" name="s"
                                                                    value="" placeholder="{{__('coming')}}" />
                                                            </div><!-- .flex-col -->
                                                            <div class="flex-col">
                                                                <button type="submit"
                                                                    class="ux-search-submit submit-button secondary button icon mb-0"
                                                                    disabled>
                                                                    <i class="icon-search"></i> </button>
                                                            </div><!-- .flex-col -->
                                                        </div><!-- .flex-row -->
                                                        <div class="live-search-results text-left z-top"></div>
                                                    </form>
                                                </div>
                                            </div>
                                        </li>
{{--                                    </ul><!-- .nav-dropdown -->--}}
{{--                                </li>--}}

                                @if(\Auth::check())
                                <li class="account-item has-icon has-dropdown">
                                    <a href="{{route('myaccount')}}" class="account-link account-login"
                                        title="My account">
                                        <span class="header-account-title"
                                            style="text-transform: uppercase;">{{Auth::user()->nicename}}</span>
                                        <i class="image-icon circle">
                                            <img alt="{{config('app.name')}}"
                                                src="https://1.gravatar.com/avatar/1d0a073380ba3fc704e7aba68b250918"
                                                class="avatar avatar-96 photo" height="96" width="96">
                                        </i>
                                    </a>

                                    <ul class="nav-dropdown nav-dropdown-simple dropdown-uppercase">
                                        <li class="woocommerce-MyAccount-navigation-link is-active active">
                                            <a href="{{route('myaccount')}}">{{__('dashboard')}}</a>
                                        </li>
                                        <li class="woocommerce-MyAccount-navigation-link">
                                            <a href="{{route('account.orders')}}">{{__('orders')}}</a>
                                        </li>

                                        <li class="woocommerce-MyAccount-navigation-link">
                                            <a href="{{route('editAccount')}}">{{__('account_detail')}}</a>
                                        </li>
                                        <li class="woocommerce-MyAccount-navigation-link">
                                            <a href="{{route('account.logout')}}">{{__('logout')}}</a>
                                        </li>
                                    </ul>
                                </li>
                                @else
                                <li class="account-item has-icon">
                                    <div class="header-button">
                                        <a href="{{route('account.getLogin')}}"
                                            class="nav-top-link nav-top-not-logged-in icon button circle is-outline is-small"
                                            data-open="#login-form-popup">
                                            <span>{{__('login')}} / {{__('register')}} </span>
                                        </a>
                                    </div>
                                </li>
                                @endif

                                @if(Session::has('cart') && Cart::content()->count() > 0)
                                <li class="cart-item has-icon has-dropdown">
                                    <a href="{{route('cart')}}" title="{{__('cart')}}"
                                        class="header-cart-link is-small">
                                        <span class="header-cart-title">
                                            {{__('cart')}} /
                                            <span class="cart-price">
                                                <span class="woocommerce-Price-amount amount"><span
                                                        class="woocommerce-Price-currencySymbol">$</span>{{Cart::subtotal()}}</span>
                                            </span>
                                            <span class="cart-icon image-icon"><strong>{{Cart::count()}}</strong></span>
                                        </span>
                                    </a>
                                    <ul class="nav-dropdown nav-dropdown-simple dropdown-uppercase">
                                        <li class="html widget_shopping_cart">
                                            <div class="widget_shopping_cart_content">
                                                <ul class="woocommerce-mini-cart cart_list product_list_widget">
                                                    @foreach(\Cart::content() as $item)
                                                    <li class="woocommerce-mini-cart-item mini_cart_item">
                                                        <a href="#" class="remove" data-row="{{$item->rowId}}"
                                                            style="color:red;">×</a>
                                                        <a href="{{route('productDetails', [$item->options->slug])}}"
                                                            class="item_cart">
                                                            <img src="{{asset('storage/'.$item->options->thumbnail)}}"
                                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail">{{$item->name}}
                                                        </a>
                                                        <span class="quantity">{{$item->qty}} × <span
                                                                class="woocommerce-Price-amount amount">
                                                                <span
                                                                    class="woocommerce-Price-currencySymbol">$</span>{{number_format($item->price,2)}}</span></span>
                                                    </li>
                                                    @endforeach
                                                    <p class="woocommerce-mini-cart__total total">
                                                        <strong>{{__('total')}}:</strong> <span
                                                            class="woocommerce-Price-amount amount"><span
                                                                class="woocommerce-Price-currencySymbol">$</span>{{Cart::subtotal()}}</span>
                                                    </p>
                                                    <p class="woocommerce-mini-cart__buttons buttons"><a
                                                            href="{{route('cart')}}"
                                                            class="button wc-forward">{{__('view_cart')}}</a>
                                                        <a href="{{route('checkout')}}"
                                                            class="button checkout wc-forward">{{__('checkout')}}</a>
                                                    </p>
                                                </ul>
                                            </div>
                                        </li>
                                    </ul><!-- .nav-dropdown -->
                                </li>
                                @else
                                <li class="cart-item has-icon has-dropdown">
                                    <a href="{{route('cart')}}" title="{{__('cart')}}"
                                        class="header-cart-link is-small">
                                        <span class="header-cart-title">{{__('cart')}} /
                                            <span class="cart-price">
                                                <span class="woocommerce-Price-amount amount">
                                                    <span class="woocommerce-Price-currencySymbol">$</span>0</span>
                                            </span>
                                            <span class="cart-icon image-icon"><strong>0</strong></span>
                                        </span>
                                    </a>
                                    <ul class="nav-dropdown nav-dropdown-simple dropdown-uppercase">
                                        <li class="html widget_shopping_cart">
                                            <div class="widget_shopping_cart_content">
                                                <p class="woocommerce-mini-cart__empty-message">
                                                    {{__('no_products_in_cart')}}</p>
                                            </div>
                                        </li>
                                    </ul><!-- .nav-dropdown -->
                                </li>
                                @endif
{{--                            </ul>--}}
