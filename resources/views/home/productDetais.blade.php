@extends('layouts.home')

@section('title', $product->name . ' - AYA WORKSHOP')

@section('content')
<main id="main" style="min-height:800px;">
    <div class="shop-container">
        <div class="product-container">
            <div class="product-main">
                <div class="row mb-0 content-row">
                    <div class="product-gallery large-5 col">
                        <div class="product-gallery-default has-hover relative">
                            <div id="append_images" class="woocommerce-product-gallery woocommerce-product-gallery--with-images woocommerce-product-gallery--columns-4 images" style="opacity: 1; transition: opacity 0.25s ease-in-out 0s;">
                                <div class="flex-viewport" style="overflow: hidden; position: relative; height: 300px;">
                                    <figure id="figure_append" class="woocommerce-product-gallery__wrapper" style="width: 1000%; transition-duration: 0s; transform: translate3d(0px, 0px, 0px);">
                                        <div class="woocommerce-product-gallery__image slide first flex-active-slide" style="width: 420px; height:300px; margin-right: 0px; float: left; display: block;">
                                            <a id="a_image" href="/storage/{{$product->thumbnail}}" data-lightbox="i-set" style="display:block;">
                                                <img id="change_image" class="wp-post-image skip-lazy" src="/storage/{{$product->thumbnail}}" style="min-height:300px;">
                                            </a>
                                        </div>
                                    </figure>
                                </div>
                                <ol class="flex-control-nav flex-control-thumbs">
                                    <li><img class="i_item" src="/storage/{{$product->thumbnail}}"></li>
                                </ol>
                            </div>
                        </div>
                    </div>

                    <div class="product-info summary col-fit col-divided col entry-summary product-summary text-left form-flat">
{{--                        <nav class="woocommerce-breadcrumb breadcrumbs">--}}
{{--                            <a href="{{url('/')}}">Home</a>--}}
{{--                            <span class="divider">/</span>--}}
{{--                            <a href="{{url('/') . '/' . $parent_category->slug}}">{{$parent_category->name}}</a>--}}
{{--                            <span class="divider">/</span>--}}
{{--                            <a href="{{route('product-category', $product->category_slug)}}">{{$product->category_name}}</a>--}}
{{--                        </nav>--}}
                        <h1 class="product-title product_title entry-title">{{$product->name}}</h1>

                        <div class="price-wrapper">
                            <div class="product-short-description">
                                {{Str::substr($product->description, 0, 230)}}
                            </div>
                            <p class="price product-page-price ">
                                @if(!empty($product->min_price) && $product->sale_start <= Carbon\Carbon::now() && Carbon\Carbon::now() <= $product->sale_end)
                                    <p class="price product-page-price price-on-sale">
                                        <del>
                                            <span class="woocommerce-Price-amount amount">
                                                <span
                                                    class="woocommerce-Price-currencySymbol">$</span>{{number_format($product->max_price, 2)}}
                                            </span>
                                        </del>
                                        <ins>
                                            <span class="woocommerce-Price-amount amount">
                                                <span
                                                    class="woocommerce-Price-currencySymbol">$</span>{{number_format($product->min_price, 2)}}</span>
                                        </ins>
                                    </p>
                                    @else
                                    <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>{{number_format($product->max_price, 2)}}</span>
                                    @endif
                        </div>


                        <form class="cart" action="{{route('addCart')}}" method="post">
                            @csrf
                            <input type="hidden" name="product_id" id="product_id" value="{{$product->id}}" />
                            <div class="quantity buttons_added">
                                <input type="button" value="-" class="minus button is-form"> <label
                                    class="screen-reader-text">Quantity</label>
                                <input type="number" id="quantity" class="input-text qty text" step="1" min="1" max="10"
                                    name="quantity" value="1" title="Qty" size="4" pattern="[0-9]*" inputmode="numeric">
                                <input type="button" value="+" class="plus button is-form">
                            </div>
                            <button type="submit" class="single_add_to_cart_button button alt">Add to cart</button>
                        </form>

                        <div class="social-icons share-icons share-row relative">
                            <a href="whatsapp://send?text={{$product->name}} - {{route('productDetails', $product->slug)}}" data-action="share/whatsapp/share" class="icon button circle is-outline tooltip whatsapp show-for-medium tooltipstered">
                                <i class="icon-phone"></i>
                            </a>
                            <a href="//www.facebook.com/sharer.php?u={{route('productDetails', $product->slug)}}" data-label="Facebook" onclick="window.open(this.href,this.title,'width=500,height=500,top=300px,left=300px');  return false;" rel="noopener noreferrer nofollow" target="_blank" class="icon button circle is-outline tooltip facebook tooltipstered">
                                <i class="icon-facebook"></i>
                            </a>
                            <a href="//twitter.com/share?url={{route('productDetails', $product->slug)}}" onclick="window.open(this.href,this.title,'width=500,height=500,top=300px,left=300px');  return false;" rel="noopener noreferrer nofollow" target="_blank" class="icon button circle is-outline tooltip twitter tooltipstered">
                                <i class="icon-twitter"></i>
                            </a>
                            <a href="mailto:enteryour@addresshere.com?subject={{$product->name}}&amp;body=Check%20this%20out:%20{{route('productDetails', $product->slug)}}" rel="nofollow" class="icon button circle is-outline tooltip email tooltipstered">
                                <i class="icon-envelop"></i>
                            </a>
                            <a href="//pinterest.com/pin/create/button/?url={{route('productDetails', $product->slug)}}&amp;media={{asset($product->thumbnail)}}&amp;description={{$product->name}}" onclick="window.open(this.href,this.title,'width=500,height=500,top=300px,left=300px');  return false;" rel="noopener noreferrer nofollow" target="_blank" class="icon button circle is-outline tooltip pinterest tooltipstered">
                                <i class="icon-pinterest"></i>
                            </a>
                            <a href="//plus.google.com/share?url={{route('productDetails', $product->slug)}}" target="_blank" class="icon button circle is-outline tooltip google-plus tooltipstered" onclick="window.open(this.href,this.title,'width=500,height=500,top=300px,left=300px');  return false;" rel="noopener noreferrer nofollow">
                                <i class="icon-google-plus"></i>
                            </a>
                            <a
                                href="//www.linkedin.com/shareArticle?mini=true&amp;url={{route('productDetails', $product->slug)}}&amp;title={{$product->name}}" onclick="window.open(this.href,this.title,'width=500,height=500,top=300px,left=300px');  return false;" rel="noopener noreferrer nofollow" target="_blank" class="icon button circle is-outline tooltip linkedin tooltipstered">
                                <i class="icon-linkedin"></i>
                            </a>
                        </div>
                    </div><!-- .summary -->

                    <div id="product-sidebar" class="col large-3 hide-for-medium ">
                        <aside class="widget woocommerce"><span class="widget-title shop-sidebar">Categories</span>
                            <div class="is-divider small"></div>
                            <ul>
                                @foreach($categories as $category)
                                <li class="cat-item">
                                    <a href="{{route('product-category', $category->slug)}}">{{$category->name}}</a>
                                </li>
                                @endforeach
                            </ul>
                        </aside>
                    </div>

                </div><!-- .row -->
            </div><!-- .product-main -->

            <div class="product-footer">
                <div class="container">
                    <div class="woocommerce-tabs container tabbed-content">
                        <ul class="product-tabs small-nav-collapse tabs nav nav-uppercase nav-line-grow nav-left">
                            <li class="description_tab active">
                                <a href="#tab-description">Description</a>
                            </li>

                            <li class="reviews_tab">
                                <a href="#tab-reviews">Reviews</a>
                            </li>
                        </ul>
                        <div class="tab-panels">
                            <div class="panel entry-content active" id="tab-description">
                                {!!$product->details!!}
                            </div>
                            <div class="panel entry-content " id="tab-reviews">
                                <div class="row" id="reviews">
                                    <div class="col large-12" id="comments">
                                        <div class="fb-comments" data-mobile="Auto-detected" data-order-by="time"
                                            data-href="{{route('productDetails', $product->slug)}}" data-width="100%"
                                            data-numposts="30"></div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- .tab-panels -->
                    </div><!-- .tabbed-content -->

                    <div class="related related-products-wrapper product-section">
                        <h3
                            class="product-section-title container-width product-section-title-related pt-half pb-half uppercase">
                            Related products </h3>
                        <div class="row large-columns-3 medium-columns-1 small-columns-1 carousel"
                            data-flickity='{ "autoPlay": 3000, "pageDots": false, "imagesLoaded": true, "prevNextButtons": false, "dragThreshold": 5, "groupCells": "100%", "cellAlign": "left"}'>
                            @foreach($related as $product)
                            <div class="col large-3 carousel-cell">
                                <div class="col-inner text-center">
                                    <div class="row row-small">
                                        <div class="col-inner text-center">
                                            <div class="product-small box has-hover box-normal box-text-bottom">
                                                <div class="box-image" style="width:80%;">
                                                    <div class="image-cover"
                                                        style="border-radius:5%;width:85%; padding-top:80%;">
                                                        <a href="{{route('productDetails', $product->slug)}}">
                                                            <img src="{{asset('storage/'.$product->thumbnail)}}" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail">
                                                        </a>
                                                    </div>

                                                    @if(!empty($product->sale) && $product->sale_start <= Carbon\Carbon::now() && Carbon\Carbon::now() <= $product->sale_end)
                                                        <div class="image-tools top right show-on-hover">
                                                            <div class="callout badge badge-circle">
                                                                <div class="badge-inner secondary on-sale">
                                                                    <span class="onsale">-{{round(100 - (($product->sale * 100)/$product->price))}}%</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @endif
                                                </div><!-- box-image -->

                                                <div class="box-text text-center is-large">
                                                    <div class="title-wrapper">
                                                        <p
                                                            class="category uppercase is-smaller no-text-overflow product-cat op-7">
                                                            {{$product->category_name}}</p>
                                                        <p class="name product-title"><a href="{{route('productDetails', $product->slug)}}">{{$product->name}}</a>
                                                        </p>
                                                    </div>
                                                    <div class="price-wrapper">
                                                        @if(!empty($product->sale) && $product->sale_start <= Carbon\Carbon::now() && Carbon\Carbon::now() <=$product->sale_end)
                                                            <span class="price">
                                                                <del>
                                                                    <span class="woocommerce-Price-amount amount">
                                                                        <span
                                                                            class="woocommerce-Price-currencySymbol">$</span>{{number_format($product->price, 2)}}
                                                                    </span>
                                                                </del>
                                                                <ins>
                                                                    <span class="woocommerce-Price-amount amount">
                                                                        <span
                                                                            class="woocommerce-Price-currencySymbol">$</span>{{number_format($product->sale, 2)}}
                                                                    </span>
                                                                </ins>
                                                            </span>
                                                            @else
                                                            <span class="price">
                                                                <ins>
                                                                    <span class="woocommerce-Price-amount amount">
                                                                        <span
                                                                            class="woocommerce-Price-currencySymbol">$</span>{{number_format($product->price, 2)}}
                                                                    </span>
                                                                </ins>
                                                            </span>
                                                            @endif
                                                    </div>
                                                </div><!-- box-text -->
                                            </div><!-- box -->
                                        </div><!-- .col-inner -->
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div><!-- container -->
            </div><!-- product-footer -->
        </div><!-- .product-container -->
    </div><!-- shop container -->
</main>

<script type="text/javascript">
    jQuery(document).ready(function($)
    {
        var product_id = $("#product_id").val();
        $.ajax({
            url: "/products/images/" + product_id,
            type: "get",
            success: function(data)
            {
                if(Object.keys(data).length > 0)
                {
                    $.each(data, function(i, item) {
                        $("#figure_append div").append("<a data-lightbox='i-set' href='/storage/" +item.path + "' style='display:none;'><img src='/storage/" +item.path + "'></a>");
                        $("#append_images ol").append("<li><img class='i_item' src='/storage/" +item.path + "'></li>");

                    });
                }
            }
        });

        $('.buttons_added').on('click', '.plus', function() {
            var value = document.getElementById("quantity").value;
            if (value < 10)
                document.getElementById("quantity").value = parseInt(value, 10) + 1;
            else
                document.getElementById("quantity").value = 10;
        });

        $('.buttons_added').on('click', '.minus', function() {
            var value = document.getElementById("quantity").value;
            if (value > 1)
                document.getElementById("quantity").value = parseInt(value, 10) - 1;
            else
                document.getElementById("quantity").value = 1;

            ;
        });

        $('.flex-control-thumbs').on('click', '.i_item', function(){
            var src = $(this).attr('src');
            var url_old = $("#figure_append div a").attr('href');

			if(src != url_old) {
				//remove a current
				$("#figure_append div a[href='" + src + "']").remove();

				//Add new with new src_old
				$("#figure_append div").append("<a data-lightbox='i-set' href='" + url_old + "' style='display:none;'><img src='" + url_old + "'></a>");

				//$("#figure_append div a").css("display", "none");

				//Change value show
				//$('#a_image').css("display", "block");
				$('#a_image').attr('href', src);
				$('#change_image').attr('src', src);
			}
        });

        lightbox.option({
            'resizeDuration': 700,
            'wrapAround': true,
            'alwaysShowNavOnTouchDevices': true,
            'disableScrolling': false,
        });
    });
</script>
@endsection
