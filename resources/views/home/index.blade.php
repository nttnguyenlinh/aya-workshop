@extends('layouts.home')

@section('title', config('app.name') . ' - ' . config('app.subname'))

@section('content')
<main style="min-height:800px;">
    <div role="main" class="content-area">
        <div class="slider-wrapper relative" style="background-color:rgb(255,255,255);">
            <div class="slider slider-nav-circle slider-nav-large slider-nav-light slider-style-normal"
                data-flickity-options='{"cellAlign": "center","imagesLoaded": true,"lazyLoad": 1,"freeScroll": false,"wrapAround": true,"autoPlay": 6000,"pauseAutoPlayOnHover" : true,"prevNextButtons": true,"contain" : true,"adaptiveHeight" : true,"dragThreshold" : 10,"percentPosition": true,"pageDots": true,"rightToLeft": false,"draggable": true,"selectedAttraction": 0.1,"parallax" : 0,"friction": 0.6}'>
                <div class="banner has-hover has-video"
                    style=" padding-top: 50%; background-color: rgb(255, 255, 255);">
                    <div class="banner-inner fill">
                        <div class="banner-bg fill">
                            <div class="bg fill bg-fill bg-loaded"></div>
                            <div class="video-overlay no-click fill visible"></div>
                            <video class="video-bg fill visible" preload playsinline autoplay muted loop>
                                <source src="{{asset('uploads/images/AYA-SIlver-Earphones.mp4')}}" type="video/mp4">
                            </video>
                        </div><!-- bg-layers -->
                    </div><!-- .banner-inner -->
                </div><!-- .banner -->
            </div>
        </div><!-- .ux-slider-wrapper -->

        <section class="section" style="padding-top: 30px; padding-bottom: 30px; background-color: rgb(255, 255, 255);">
            <div class="bg section-bg fill bg-fill bg-loaded bg-loaded"></div><!-- .section-bg -->
            <div class="section-content relative">
                <div class="row row-large align-center">
                    <div class="col medium-3 large-3">
                        <div class="col-inner text-center">
                            <a class="plain" href="{{route('faq.index')}}#{{$faq[0]['title']}}" target="_self">
                                <div class="icon-box featured-box icon-box-center text-center is-large"
                                    style="margin:0px 0px 0px 0px;">
                                    <div class="icon-box-img has-icon-bg" style="width: 60px">
                                        <div class="icon">
                                            <div class="icon-inner" style="border-width:2px;">
                                                <img width="50" height="50"
                                                    src="{{asset('uploads/icons/Marker_50px.png')}}"
                                                    class="attachment-medium size-medium" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="icon-box-text last-reset">
                                        <h5 class="uppercase">{{__($faq[0]['title'])}}</h5>
                                    </div>
                                </div><!-- .icon-box -->
                            </a>
                        </div>
                    </div>
                    <div class="col medium-3 large-3">
                        <div class="col-inner text-center">
                            <a class="plain" href="{{route('faq.index')}}#{{$faq[1]['title']}}" target="_self">
                                <div class="icon-box featured-box icon-box-center text-center is-large"
                                    style="margin:0px 0px 0px 0px;">
                                    <div class="icon-box-img has-icon-bg" style="width: 60px">
                                        <div class="icon">
                                            <div class="icon-inner" style="border-width:2px;">
                                                <img width="50" height="50"
                                                    src="{{asset('uploads/icons/Synchronize_50px.png')}}"
                                                    class="attachment-medium size-medium" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="icon-box-text last-reset">
                                        <h5 class="uppercase">{{__($faq[1]['title'])}}</h5>
                                    </div>
                                </div><!-- .icon-box -->
                            </a>
                        </div>
                    </div>
                    <div class="col medium-3 large-3">
                        <div class="col-inner text-center">
                            <a class="plain" href="{{route('faq.index')}}#{{$faq[2]['title']}}" target="_self">
                                <div class="icon-box featured-box icon-box-center text-center is-large"
                                    style="margin:0px 0px 0px 0px;">
                                    <div class="icon-box-img has-icon-bg" style="width: 60px">
                                        <div class="icon">
                                            <div class="icon-inner" style="border-width:2px;">
                                                <img width="50" height="50"
                                                    src="{{asset('uploads/icons/Prize_50px.png')}}"
                                                    class="attachment-medium size-medium" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="icon-box-text last-reset">
                                        <h5 class="uppercase">{{__($faq[2]['title'])}}</h5>
                                    </div>
                                </div><!-- .icon-box -->
                            </a>
                        </div>
                    </div>
                    <div class="col medium-3 large-3">
                        <div class="col-inner text-center">
                            <a class="plain" href="{{route('faq.index')}}#contact" target="_self">
                                <div class="icon-box featured-box icon-box-center text-center is-large"
                                    style="margin:0px 0px 0px 0px;">
                                    <div class="icon-box-img has-icon-bg" style="width: 60px">
                                        <div class="icon">
                                            <div class="icon-inner" style="border-width:2px;">
                                                <img width="50" height="50"
                                                    src="{{asset('uploads/icons/Online-Support_50px.png')}}"
                                                    class="attachment-medium size-medium" /> </div>
                                        </div>
                                    </div>
                                    <div class="icon-box-text last-reset">
                                        <h5 class="uppercase">{{__('contact')}}</h5>
                                    </div>
                                </div><!-- .icon-box -->
                            </a>
                        </div>
                    </div>
                </div>
            </div><!-- .section-content -->
        </section>

        <section class="section" id="section_548763340">
            <div class="bg section-bg fill bg-fill  bg-loaded">
                <div class="section-bg-overlay absolute fill"></div>
                <div class="is-border" style="border-width:1px 0px 1px 0px;"></div>
            </div>
            <div class="section-content relative">
                <div class="row row-large align-middle" id="row-1295483861">
                    <div class="col small-12 large-12">
                        <div class="col-inner">
                            <div class="banner-grid-wrapper">
                                <div id="banner-grid-1249417164" class="banner-grid row row-grid row-small"
                                    data-packery-options="" style="position: relative; height: 600px;">
                                    <div class="col grid-col large-7 grid-col-1" data-animate="bounceIn"
                                        style="position: absolute; left: 0px; top: 0px;" data-animated="true">
                                        <div class="col-inner">
                                            <div class="banner has-hover bg-zoom" id="banner-890732116">
                                                <div class="banner-inner fill">
                                                    <div class="banner-bg fill">
                                                        <div class="bg fill bg-fill  bg-loaded"></div>
                                                        <div class="overlay"></div>
                                                        <div class="effect-glass bg-effect fill no-click"></div>
                                                    </div>
                                                    <div class="banner-layers container">
                                                        <div class="fill banner-link"></div>
                                                        <div id="text-box-1628817860"
                                                            class="text-box banner-layer x50 md-x50 lg-x50 y95 md-y95 lg-y95 res-text">
                                                            <div class="hover-bounce">
                                                                <div data-animate="bounceIn" data-animated="true">
                                                                    <div class="text dark">
                                                                        <div class="text-inner text-center">
                                                                            <a href="{{route('product-category', 'earphones')}}"
                                                                                target="_self"
                                                                                class="button white is-outline is-xxlarge"
                                                                                style="border-radius:40px;">
                                                                                <span>{{__('earphones')}}</span>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <style scope="scope">
                                                            #text-box-1628817860 {
                                                                width: 60%
                                                            }

                                                            #text-box-1628817860 .text {
                                                                font-size: 100%
                                                            }
                                                            </style>
                                                        </div>
                                                    </div>
                                                </div>
                                                <style scope="scope">
                                                #banner-890732116 {
                                                    padding-top: 500px
                                                }

                                                #banner-890732116 .bg.bg-loaded {
                                                    background-image: url(/uploads/images/13.jpg)
                                                }

                                                #banner-890732116 .overlay {
                                                    background-color: rgba(0, 0, 0, .17)
                                                }
                                                </style>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col grid-col large-5 grid-col-1-2" data-animate="bounceIn"
                                        style="position: absolute; left: 621px; top: 0px;" data-animated="true">
                                        <div class="col-inner">
                                            <div class="banner has-hover bg-zoom bg-zoom" id="banner-1161077347">
                                                <div class="banner-inner fill">
                                                    <div class="banner-bg fill">
                                                        <div class="bg fill bg-fill  bg-loaded"></div>
                                                        <div class="overlay"></div>
                                                        <div class="effect-glass bg-effect fill no-click"></div>
                                                    </div>
                                                    <div class="banner-layers container">
                                                        <div class="fill banner-link"></div>
                                                        <div id="text-box-1356841949"
                                                            class="text-box banner-layer x20 md-x20 lg-x20 y95 md-y95 lg-y95 res-text">
                                                            <div class="hover-bounce">
                                                                <div data-animate="bounceIn" data-animated="true">
                                                                    <div class="text dark">
                                                                        <div class="text-inner text-center">
                                                                            <a data-animate="blurIn"
                                                                                href="{{route('product-category', 'accessories')}}"
                                                                                target="_self"
                                                                                class="button white is-outline is-xxlarge box-shadow-3"
                                                                                style="border-radius:50px;"
                                                                                data-animated="true">
                                                                                <span>{{__('accessories')}}</span>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <style scope="scope">
                                                            #text-box-1356841949 {
                                                                width: 60%
                                                            }

                                                            #text-box-1356841949 .text {
                                                                font-size: 90%
                                                            }
                                                            </style>
                                                        </div>
                                                    </div>
                                                </div>
                                                <style scope="scope">
                                                #banner-1161077347 {
                                                    padding-top: 500px
                                                }

                                                #banner-1161077347 .bg.bg-loaded {
                                                    background-image: url(/uploads/images/14.jpg)
                                                }

                                                #banner-1161077347 .overlay {
                                                    background-color: rgba(0, 0, 0, .17)
                                                }
                                                </style>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col grid-col large-5 grid-col-1-2" data-animate="bounceIn"
                                        style="position: absolute; left: 621px; top: 300px;" data-animated="true">
                                        <div class="col-inner">
                                            <div class="banner has-hover bg-zoom bg-zoom" id="banner-492849278">
                                                <div class="banner-inner fill">
                                                    <div class="banner-bg fill">
                                                        <div class="bg fill bg-fill  bg-loaded"></div>
                                                        <div class="overlay"></div>
                                                        <div class="effect-glass bg-effect fill no-click"></div>
                                                    </div>
                                                    <div class="banner-layers container">
                                                        <div class="fill banner-link"></div>
                                                        <div id="text-box-1178938716"
                                                            class="text-box banner-layer x20 md-x20 lg-x20 y95 md-y95 lg-y95 res-text">
                                                            <div class="hover-bounce">
                                                                <div data-animate="blurIn" data-animated="true">
                                                                    <div class="text dark">
                                                                        <div class="text-inner text-center">
                                                                            <a data-animate="bounceIn"
                                                                                href="{{route('posts')}}" target="_self"
                                                                                class="button white is-outline is-xxlarge"
                                                                                style="border-radius:50px;"
                                                                                data-animated="true">
                                                                                <span>{{__('posts')}}</span>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <style scope="scope">
                                                            #text-box-1178938716 {
                                                                width: 60%
                                                            }

                                                            #text-box-1178938716 .text {
                                                                font-size: 100%
                                                            }
                                                            </style>
                                                        </div>
                                                    </div>
                                                </div>
                                                <style scope="scope">
                                                #banner-492849278 {
                                                    padding-top: 500px;
                                                    background-color: rgba(171, 216, 211, .19)
                                                }

                                                #banner-492849278 .bg.bg-loaded {
                                                    background-image: url(/uploads/images/24.jpg)
                                                }

                                                #banner-492849278 .overlay {
                                                    background-color: rgba(0, 0, 0, .17)
                                                }
                                                </style>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <style scope="scope">
                                #banner-grid-1249417164 .grid-col-1 {
                                    height: 600px
                                }

                                #banner-grid-1249417164 .grid-col-1-2 {
                                    height: 300px
                                }

                                #banner-grid-1249417164 .grid-col-1-3 {
                                    height: 200px
                                }

                                #banner-grid-1249417164 .grid-col-2-3 {
                                    height: 400px
                                }

                                #banner-grid-1249417164 .grid-col-1-4 {
                                    height: 150px
                                }

                                #banner-grid-1249417164 .grid-col-3-4 {
                                    height: 450px
                                }
                                </style>
                            </div>
                        </div>
                    </div>
                    <style scope="scope"></style>
                </div>
            </div>
            <style scope="scope">
            #section_548763340 {
                padding-top: 60px;
                padding-bottom: 60px;
                background-color: rgb(193, 193, 193)
            }

            #section_548763340 .section-bg-overlay {
                background-color: rgba(255, 255, 255, .85)
            }
            </style>
        </section>
        <section class="section" style="padding: 10px 0;">
            <div class="section-content relative">
                <h2 class="uppercase" style="text-align:center;">༺<sub>{{__('featured_products')}}</sub>༻</h2>
                <div class="row large-columns-3 medium-columns-1 small-columns-1 carousel"
                    data-flickity='{ "autoPlay": 3000, "pageDots": false, "imagesLoaded": true, "prevNextButtons": false, "dragThreshold": 5, "groupCells": "100%", "cellAlign": "left"}'>
                    @foreach($featured as $product)
                    <div class="col large-3 carousel-cell">
                        <div class="col-inner text-center">
                            <div class="row row-small">
                                <div class="col-inner text-center">
                                    <div class="product-small box has-hover box-normal box-text-bottom">
                                        <div class="box-image" style="width:90%;">
                                            <div class="image-cover"
                                                style="border-radius:5%;padding-top:80%;">
                                                <a href="{{route('productDetails', $product->slug)}}">
                                                    <img src="{{($product->thumbnail) ? asset('storage/'.$product->thumbnail) : asset('uploads/images/no_image.jpg')}}"
                                                        class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                        alt="{{$product->name}}"> </a>
                                            </div>

                                            @if(!empty($product->sale) && $product->sale_start <= Carbon\Carbon::now()
                                                && Carbon\Carbon::now() <=$product->sale_end)
                                                <div class="image-tools top right show-on-hover">
                                                    <div class="callout badge badge-circle">
                                                        <div class="badge-inner secondary on-sale"><span
                                                                class="onsale">-{{round(100 - (($product->sale * 100)/$product->price))}}%</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endif
                                        </div><!-- box-image -->

                                        <div class="box-text text-center is-large">
                                            <div class="title-wrapper">
                                                <p
                                                    class="category uppercase is-smaller no-text-overflow product-cat op-7">
                                                    {{$product->category_name}}</p>
                                                <p class="name product-title"><a
                                                        href="{{route('productDetails', $product->slug)}}">{{$product->name}}</a>
                                                </p>
                                            </div>
                                            <div class="price-wrapper">
                                                @if(!empty($product->sale) && $product->sale_start <=
                                                    Carbon\Carbon::now() && Carbon\Carbon::now() <=$product->sale_end)
                                                    <span class="price">
                                                        <del>
                                                            <span class="woocommerce-Price-amount amount">
                                                                <span
                                                                    class="woocommerce-Price-currencySymbol">$</span>{{number_format($product->price, 2)}}
                                                            </span>
                                                        </del>
                                                        <ins>
                                                            <span class="woocommerce-Price-amount amount">
                                                                <span
                                                                    class="woocommerce-Price-currencySymbol">$</span>{{number_format($product->sale, 2)}}</span>
                                                        </ins>
                                                    </span>
                                                    @else
                                                    <span class="price">
                                                        <ins>
                                                            <span class="woocommerce-Price-amount amount">
                                                                <span
                                                                    class="woocommerce-Price-currencySymbol">$</span>{{number_format($product->price, 2)}}</span>
                                                        </ins>
                                                    </span>
                                                    @endif
                                            </div>
                                        </div><!-- box-text -->
                                    </div><!-- box -->
                                </div><!-- .col-inner -->
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </section>

        <section class="section" style="padding: 10px 0;">
            <div class="section-content relative">
                <h2 class="uppercase" style="text-align:center;">༺<sub>{{__('latest_products')}}</sub>༻</h2>
                <div class="row large-columns-3 medium-columns-1 small-columns-1 carousel"
                    data-flickity='{ "autoPlay": 3000, "pageDots": false, "imagesLoaded": true, "prevNextButtons": false, "dragThreshold": 5, "groupCells": "100%", "cellAlign": "left"}'>
                    @foreach($products as $product)
                    <div class="col large-3 carousel-cell">
                        <div class="col-inner text-center">
                            <div class="row row-small">
                                <div class="col-inner text-center">
                                    <div class="product-small box has-hover box-normal box-text-bottom">
                                        <div class="box-image" style="width:90%;">
                                            <div class="image-cover"
                                                style="border-radius:5%;padding-top:80%;">
                                                <a href="{{route('productDetails', $product->slug)}}">
                                                    <img src="{{($product->thumbnail) ? asset('storage/'.$product->thumbnail) : asset('uploads/images/no_image.jpg')}}"
                                                        class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                        alt="{{$product->name}}"> </a>
                                            </div>

                                            @if(!empty($product->sale) && $product->sale_start <= Carbon\Carbon::now()
                                                && Carbon\Carbon::now() <=$product->sale_end)
                                                <div class="image-tools top right show-on-hover">
                                                    <div class="callout badge badge-circle">
                                                        <div class="badge-inner secondary on-sale"><span
                                                                class="onsale">-{{round(100 - (($product->sale * 100)/$product->price))}}%</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endif
                                        </div><!-- box-image -->

                                        <div class="box-text text-center is-large">
                                            <div class="title-wrapper">
                                                <p
                                                    class="category uppercase is-smaller no-text-overflow product-cat op-7">
                                                    {{$product->category_name}}</p>
                                                <p class="name product-title"><a
                                                        href="{{route('productDetails', $product->slug)}}">{{$product->name}}</a>
                                                </p>
                                            </div>
                                            <div class="price-wrapper">
                                                @if(!empty($product->sale) && $product->sale_start <=
                                                    Carbon\Carbon::now() && Carbon\Carbon::now() <=$product->sale_end)
                                                    <span class="price">
                                                        <del>
                                                            <span class="woocommerce-Price-amount amount">
                                                                <span
                                                                    class="woocommerce-Price-currencySymbol">$</span>{{number_format($product->price, 2)}}</span>
                                                        </del>
                                                        <ins>
                                                            <span class="woocommerce-Price-amount amount">
                                                                <span
                                                                    class="woocommerce-Price-currencySymbol">$</span>{{number_format($product->sale, 2)}}</span>
                                                        </ins>
                                                    </span>
                                                    @else
                                                    <span class="price">
                                                        <ins>
                                                            <span class="woocommerce-Price-amount amount">
                                                                <span
                                                                    class="woocommerce-Price-currencySymbol">$</span>{{number_format($product->price, 2)}}</span>
                                                        </ins>
                                                    </span>
                                                    @endif
                                            </div>
                                        </div><!-- box-text -->
                                    </div><!-- box -->
                                </div><!-- .col-inner -->
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </section>


        <section class="section" style="padding: 10px 0;">
            <div class="section-content relative">
                <h2 class="uppercase" style="text-align:center;">༺<sub>{{__('latest_news')}}</sub>༻</h2>

                <div class="row large-columns-3 medium-columns-1 small-columns-1 carousel"
                    data-flickity='{ "autoPlay": 2000, "pageDots": false, "imagesLoaded": true, "prevNextButtons": false, "dragThreshold": 5, "groupCells": "100%", "cellAlign": "left"}'>
                    @foreach($posts as $post)
                    <div class="col large-3 carousel-cell-news">
                        <div class="col-inner text-center">
                            <a href="{{route('postDetails', $post->slug)}}" class="plain">
                                <div class="box box-shade dark box-text-middle box-blog-post has-hover">
                                    <div class="box-image">
                                        <div class="image-cover" style="padding-top:50%;">
                                            <img src="{{($post->thumbnail) ? asset('storage/'.$post->thumbnail) : asset('uploads/images/no_image.jpg')}}"
                                                class="attachment-medium size-medium wp-post-image"
                                                alt="{{$post->name}}">
                                            <div class="shade"></div>
                                        </div>
                                    </div><!-- .box-image -->

                                    <div class="box-text text-center">
                                        <div class="box-text-inner blog-post-inner">
                                            <p class="cat-label tag-label is-xxsmall op-7 uppercase">
                                                {{$post->categories_name}}</p>
                                            <h5 class="post-title is-large ">{{$post->name}}</h5>
                                            <div class="is-divider"></div>
                                            <p class="from_the_blog_excerpt ">{{substr($post->excerpt,0,65)}} [...] </p>
                                        </div><!-- .box-text-inner -->
                                    </div><!-- .box-text -->

                                    <div class="badge absolute top post-date badge-square">
                                        <div class="badge-inner">
                                            <span
                                                class="post-date-day">{{Date_format($post->created_at, "d")}}</span><br>
                                            <span
                                                class="post-date-month is-xsmall">{{Date_format($post->created_at, "m")}}</span>
                                        </div>
                                    </div>
                                </div><!-- .box -->
                            </a><!-- .link -->
                        </div>
                    </div>
                    @endforeach
                </div>
            </div><!-- .section-content -->
        </section>
    </div>
</main><!-- #main -->
@endsection