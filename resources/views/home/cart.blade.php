@extends('layouts.home')

@section('title', __('cart') . ' - ' . config('app.name'))

@section('content')


<main id="main" style="min-height:800px;">
    <div class="checkout-page-title page-title">
        <div class="page-title-inner flex-row medium-flex-wrap container">
            <div class="flex-col flex-grow medium-text-center">
                <nav class="breadcrumbs heading-font checkout-breadcrumbs text-center h2 strong">
                    <a href="{{route('cart')}}" class="current">{{__('shopping_cart')}}</a>
                    <span class="divider hide-for-small"><i class="icon-angle-right"></i></span>
                    <a href="{{route('checkout')}}" class="hide-for-small">{{__('checkout_details')}}</a>
                    <span class="divider hide-for-small"><i class="icon-angle-right"></i></span>
                    <a href="#" class="no-click hide-for-small">{{__('order_complete')}}</a>
                </nav>
            </div><!-- .flex-left -->
        </div><!-- flex-row -->
    </div><!-- .page-title -->
    <div class="cart-container container page-wrapper page-checkout">
        <div class="woocommerce">
            <div class="woocommerce row row-large row-divided">
                @if(Session::has('cart') && Cart::content()->count() > 0)
                <div class="col large-7 pb-0 cart-auto-refresh">
                    <form action="{{route('cart')}}" method="post">
                        @csrf
                        <div class="cart-wrapper sm-touch-scroll">
                            <table class="shop_table shop_table_responsive cart" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th class="product-name" colspan="3">{{__('product')}}</th>
                                        <th class="product-price">{{__('price')}}</th>
                                        <th class="product-quantity">{{__('quantity')}}</th>
                                        <th class="product-subtotal">{{__('amount')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach(\Cart::content() as $item)
                                    <tr class="woocommerce-cart-form__cart-item cart_item">
                                        <td class="product-remove">
                                            <a href="#" class="remove" data-row="{{$item->rowId}}"
                                                style="color:red;">×</a>
                                        </td>

                                        <td class="product-thumbnail">
                                            <a href="{{route('productDetails', [$item->options->slug])}}">
                                                <img src="{{asset('storage/'.$item->options->thumbnail)}}"
                                                    class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail">
                                            </a>
                                        </td>

                                        <td class="product-name" data-title="Product">
                                            <a
                                                href="{{route('productDetails', [$item->options->slug])}}">{{$item->name}}</a>
                                        </td>

                                        <td class="product-price" data-title="Price">
                                            <span class="woocommerce-Price-amount amount"><span
                                                    class="woocommerce-Price-currencySymbol">$</span>{{number_format($item->price,2)}}</span>
                                        </td>

                                        <td class="product-quantity" data-title="Quantity">
                                            <div class="quantity button_qty" data-row="{{$item->rowId}}"
                                                data-id="qty_{{$item->rowId}}">
                                                <input readonly type="number" step="1" min="1" max="10" name="quantity"
                                                    id="qty_{{$item->rowId}}" value="{{$item->qty}}" title="Qty"
                                                    size="4">
                                            </div>
                                        </td>

                                        <td class="product-subtotal" data-title="Total">
                                            <span class="woocommerce-Price-amount amount"><span
                                                    class="woocommerce-Price-currencySymbol">$</span>{{number_format($item->qty * $item->price, 2)}}</span>
                                        </td>
                                    </tr>
                                    @endforeach

                                    <tr>
                                        <td colspan="6" class="actions clear">
                                            <div class="continue-shopping pull-left text-left">
                                                <a class="button-continue-shopping button primary is-outline"
                                                    href="{{route('products')}}">← {{__('continue_shopping')}} </a>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </form>
                </div>

                <div class="cart-collaterals large-5 col pb-0">
                    <div class="cart-sidebar col-inner ">
                        <div class="cart_totals ">
                            <table cellspacing="0">
                                <thead>
                                    <tr>
                                        <th class="product-name" colspan="2" style="border-width:3px;">
                                            {{__('cart_totals')}}</th>
                                    </tr>
                                </thead>
                            </table>

                            <h2>{{__('cart_totals')}}</h2>
                            <table cellspacing="0" class="shop_table shop_table_responsive">
                                <tbody>
                                    <tr class="order-total">
                                        <th>{{__('total')}}</th>
                                        <td data-title="Total"><strong><span
                                                    class="woocommerce-Price-amount amount"><span
                                                        class="woocommerce-Price-currencySymbol">$</span>{{Cart::subtotal()}}</span></strong>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                            <div class="wc-proceed-to-checkout">
                                <a href="{{route('checkout')}}"
                                    class="checkout-button button alt wc-forward">{{__('proceed_to_checkout')}}</a>
                            </div>
                        </div>
                    </div>
                </div>
                @else
                <div class="text-center pt pb">
                    <p class="cart-empty">{{__('proceed_to_checkout')}}</p>
                    <p class="return-to-shop">
                        <a class="button primary wc-backward"
                            href="{{route('products')}}">{{__('return_to_products')}}</a>
                    </p>
                </div>
                @endif
                <div class="cart-footer-content after-cart-content relative"></div>
            </div>
        </div>
</main>

<script>
jQuery(document).ready(function($) {
    $('input[type="number"]').each(function() {
        $(this).number();
    });

    // $('#qty').bind("keyup change", function() {
    //     var qty = $('#qty').val();
    //     var rowid =  $('#qty').attr('data-row');

    //     console.log(qty);
    // });

    $('.number-style').on('click', '.number-plus, .number-minus', function() {

        var id = $(this).parent().parent().attr('data-id');
        var rowId = $(this).parent().parent().attr('data-row');
        var qty = $("#" + id).val();

        $.ajax({
            url: '/cart/update/' + rowId,
            type: 'get',
            data: {
                qty: qty
            },
            success: function(data) {
                toastr.clear();
                toastr["success"](
                    <?php echo "'" . __('updated', ['attribute' => __('cart')]) . "'";?>
                );
                setTimeout(function() {
                    location.reload();
                }, 1000);
            }
        });
    });

    $('.remove').on('click', function() {
        var rowId = $(this).attr('data-row');

        $.ajax({
            url: '/cart/remove/' + rowId,
            type: 'get',

            success: function(data) {
                toastr.clear();
                toastr["success"](
                    <?php echo "'" . __('deleted', ['attribute' => __('product')]) . "'";?>
                );
                setTimeout(function() {
                    location.reload();
                }, 1000);
            }
        });
    });
});
</script>

@endsection