@extends('layouts.home')

@section('title', "Posts - AYA WORKSHOP")

@section('content')

<main id="main" class="">
    <div id="content" class="blog-wrapper blog-archive page-wrapper">
        <div class="row row-large row-divided ">
            <div class="large-9 col">
                <div>
                    @if($posts->count() > 0)
                        @foreach($posts as $post)
                            <article>
                                <div class="article-inner">
                                    <header class="entry-header">
                                        <div class="entry-header-text text-left">
                                            <h2 class="entry-title"><a href="{{route('postDetails', $post->slug)}}" rel="bookmark" class="plain">{{$post->name}}</a></h2>
                                            <div class="entry-divider is-divider small"></div>
                                            <div class="entry-meta uppercase is-xsmall">
                                                <span class="posted-on">Posted on <a href="{{route('postDetails', $post->slug)}}" rel="bookmark"><time class="entry-date published" datetime="{{$post->created_at}}">{{Date_format($post->created_at, "Y-m-d")}}</time></a></span></div><!-- .entry-meta -->
                                        </div><!-- .entry-header -->
                                    </header><!-- post-header -->
                                    <div class="entry-image-float">
                                        <a href="{{route('postDetails', $post->slug)}}"><img width="660" height="495" src="{{asset('storage/'.$post->thumbnail)}}" </a>
                                        <div class="badge absolute top post-date badge-square">
                                            <div class="badge-inner">
                                                <span class="post-date-day">{{Date_format($post->created_at, "d")}}</span><br>
                                                <span class="post-date-month is-small">{{Date_format($post->created_at, "M")}}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="entry-content">
                                        <div class="entry-summary">
                                            <p>{{substr($post->excerpt,200)}}</p>
                                            <div class="text-left">
                                                <a class="more-link button primary is-outline is-smaller" href="{{route('postDetails', $post->slug)}}">Continue reading <span class="meta-nav">→</span></a>
                                            </div>
                                        </div><!-- .entry-summary -->

                                    </div><!-- .entry-content -->
                                    <div class="clearfix"></div>
                                    <footer class="entry-meta clearfix">
                                        <span class="cat-links">
                                            Posted in <a href="{{route('post-category', $post->categories_slug)}}" rel="category tag">{{$post->categories_name}}</a> </span>
                                        <span class="comments-link pull-right"><a href="{{route('postDetails', $post->slug)}}#respond">Leave a comment</a></span>
                                    </footer><!-- .entry-meta -->
                                </div><!-- .article-inner -->
                            </article>
                        @endforeach
                    @else
                        No posts were found matching your selection.
                    @endif
                </div>
                <div class="container" style="margin-top:50px;">
                    <ul class="pagination links text-center">
                        {{$posts->links()}}
                    </ul>
                </div>
            </div> <!-- .large-9 -->

            <div class="post-sidebar large-3 col">
                <div id="secondary" class="widget-area " role="complementary">
                    <aside id="flatsome_recent_posts-17" class="widget flatsome_recent_posts"> <span class="widget-title "><span>Latest Posts</span></span>
                        <div class="is-divider small"></div>
                        <ul>
                            @foreach($latestPosts as $latestPost)
                                    <li class="recent-blog-posts-li">
                                        <div class="flex-row recent-blog-posts align-top pt-half pb-half">
                                            <div class="flex-col mr-half">
                                                <div class="badge post-date  badge-square">
                                                    <div class="badge-inner bg-fill" style="background: linear-gradient( rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.2) ), url({{asset('storage/'.$latestPost->thumbnail)}}); color:#fff; text-shadow:1px 1px 0px rgba(0,0,0,.5); border:0;">
                                                        <span class="post-date-day">{{Date_format($latestPost->created_at, "d")}}</span><br>
                                                        <span class="post-date-month is-xsmall">{{Date_format($latestPost->created_at, "M")}}</span>
                                                    </div>
                                                </div>
                                            </div><!-- .flex-col -->
                                            <div class="flex-col flex-grow">
                                                <a href="{{route('postDetails', $latestPost->slug)}}" title="{{$latestPost->name}}">{{$latestPost->name}}</a>
                                                <span class="post_comments op-7 block is-xsmall"><a href="{{route('postDetails', $latestPost->slug)}}"></a></span>
                                            </div>
                                        </div><!-- .flex-row -->
                                    </li>
                                @endforeach
                        </ul>
                    </aside>
                    
                    <aside class="widget"><span class="widget-title"><span>Categories</span></span>
                        <div class="is-divider small"></div>
                        <ul>
                            @foreach($categories as $category)
                                <li class="cat-item">
                                    <a href="{{route('post-category', $category->slug)}}">{{$category->name}}</a>
                                </li>
                            @endforeach
                        </ul>
                    </aside>
                </div><!-- #secondary -->
            </div><!-- .post-sidebar -->
        </div><!-- .row -->
    </div><!-- .page-wrapper .blog-wrapper -->
</main>
@endsection