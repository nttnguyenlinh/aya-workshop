@extends('layouts.home')

@section('title', 'Contact Us - AYA WORKSHOP')

@section('content')

<main id="main">
    <div id="content" role="main" class="content-area">
        <div class="banner has-hover has-parallax" id="banner-1397637908">
            <div class="banner-inner fill">
                <div class="banner-bg fill" data-parallax="-5" data-parallax-container=".banner" data-parallax-background>
                    <div class="bg fill bg-fill "></div>
                    <div class="overlay"></div>
                </div>
                <!-- bg-layers -->
                <div class="banner-layers container">
                    <div class="fill banner-link"></div>
                    <div id="text-box-924772444" class="text-box banner-layer x50 md-x50 lg-x50 y100 md-y100 lg-y100 res-text">
                        <div class="text ">
                            <div class="text-inner text-center">
                                <h1 class="lead">About Us</h1>
                                <p class="lead">
                                    AYA WORKSHOP is a shop specializing in selling quality Earphone products. We are proud to be the supplier of the best quality products to the users, with the motto of serving customers like our own people. We are always acting with PRESTIGE business extract to survive and grow.
                                </p>
                            </div>
                        </div>
                        <!-- text-box-inner -->

                        <style scope="scope">
                            #text-box-924772444 {
                                margin: 0px 0px 5px 0px;
                                width: 80%;
                            }

                            #text-box-924772444 .text {
                                font-size: 100%;
                            }
                        </style>
                    </div>
                    <!-- text-box -->

                </div>
                <!-- .banner-layers -->
            </div>
            <!-- .banner-inner -->

            <style scope="scope">
                #banner-1397637908 {
                    padding-top: 275px;
                    background-color: rgba(103, 163, 162, 0.5);
                }

                #banner-1397637908 .bg.bg-loaded {
                    background-image: 8333;
                }

                #banner-1397637908 .overlay {
                    background-color: rgba(203, 232, 244, 0.49);
                }
            </style>
        </div>
        <!-- .banner -->

        <div class="row" id="row-644882297">
            <div class="col medium-4 small-12 large-4 small-col-first">
                <div class="col-inner">
                    <a class="plain" href="https://www.google.com/maps/place/Phan%20Xich%20Long%20Street,%20Phu%20Nhuan%20District" target="_blank" rel="noopener noreferrer">
                        <div class="icon-box featured-box icon-box-center text-center">
                            <div class="icon-box-img has-icon-bg" style="width: 60px">
                                <div class="icon">
                                    <div class="icon-inner" style="border-width:2px;">
                                        <img width="50" height="50" src="{{asset('uploads/icons/Address_50px.png')}}" class="attachment-medium size-medium" alt="AYA WORKSHOP" /> </div>
                                </div>
                            </div>
                            <div class="icon-box-text last-reset">
                                <h5 class="uppercase">Address</h5>
                                <h6>Phan Xich Long Street, Phu Nhuan District</h6>
                            </div>
                        </div>
                        <!-- .icon-box -->
                    </a>

                </div>
            </div>
            <div class="col medium-4 small-12 large-4 small-col-first">
                <div class="col-inner">

                    <a class="plain" href="tel:0832068972" target="_self">
                        <div class="icon-box featured-box icon-box-center text-center">
                            <div class="icon-box-img has-icon-bg" style="width: 60px">
                                <div class="icon">
                                    <div class="icon-inner" style="border-width:2px;">
                                        <img width="50" height="50" src="{{asset('uploads/icons/Cell-Phone_50px.png')}}" class="attachment-medium size-medium" alt="AYA WORKSHOP" /> </div>
                                </div>
                            </div>
                            <div class="icon-box-text last-reset">
                                <h5 class="uppercase">Phone</h5>
                                <h6>0832068972</h6>

                            </div>
                        </div>
                        <!-- .icon-box -->
                    </a>

                </div>
            </div>
            <div class="col medium-4 small-12 large-4 small-col-first">
                <div class="col-inner">

                    <a class="plain" href="mailto:support@ayaworkshop.com" target="_blank" rel="noopener noreferrer">
                        <div class="icon-box featured-box icon-box-center text-center">
                            <div class="icon-box-img has-icon-bg" style="width: 60px">
                                <div class="icon">
                                    <div class="icon-inner" style="border-width:2px;">
                                        <img width="50" height="50" src="{{asset('uploads/icons/New-Post_50px.png')}}" class="attachment-medium size-medium" alt="AYA WORKSHOP" /> </div>
                                </div>
                            </div>
                            <div class="icon-box-text last-reset">
                                <h5 class="uppercase">Email</h5>
                                <h6>support@ayaworkshop.com</h6>

                            </div>
                        </div>
                        <!-- .icon-box -->
                    </a>

                </div>
            </div>

        </div>

        <div class="row" id="row-347352327">

            <div class="col medium-6 large-6">
                <div class="col-inner">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3919.161251619337!2d106.68480921480094!3d10.798958992306224!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x317528cfe0e084f7%3A0xef3aa0c7e5e72a20!2zUGhhbiBYw61jaCBMb25nLCBQaMO6IE5odeG6rW4sIEjhu5MgQ2jDrSBNaW5o!5e0!3m2!1svi!2s!4v1554286515314!5m2!1svi!2s" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>
            <div class="col medium-6 large-6">
                <div class="col-inner">
                    <h2 style="text-align: center; color:orange;"><strong>CONTACT US</strong></h2>
                    &nbsp;
                    <div role="form" class="wpcf7" id="wpcf7-f11-p653-o1">
                        <div class="screen-reader-response"></div>

                        {{ Form::open(['route' => 'contact.store', 'method' => 'POST', 'class' => 'wpcf7-form']) }}
                        <p>
                            {!!Html::decode(Form::label('name', 'Your Name (<font color=red>required</font>)'))!!}
                            <span class="wpcf7-form-control-wrap">
                                <input type="text" name="your_name" value="{{ old('your_name') }}" class="wpcf7-form-control wpcf7-text" maxlength="20" required autofocus/>
                            </span>
                        </p>
                        <p>
                            {!!Html::decode(Form::label('email', 'Your Email (<font color=red>required</font>)'))!!}
                            <span class="wpcf7-form-control-wrap">
                                <input type="email" name="your_email" value="{{ old('your_email') }}" class="wpcf7-form-control wpcf7-email" maxlength="35" required/>
                            </span>
                        </p>
                        <p>
                            {!!Html::decode(Form::label('message', 'Your Message (<font color=red>required</font>)'))!!}
                            <span class="wpcf7-form-control-wrap your-message">
                                <textarea class="wpcf7-form-control wpcf7-textarea" cols="40" rows="10" maxlength="150" required name="your_message">{{ old('your_message') }}</textarea>
                            </span>
                        </p>
                        <p>
                            <center>
                                <div class="g-recaptcha" data-sitekey="{{config('app.recaptcha_site_key')}}"></div>
                            </center>
                            <br />
                        </p>
                        <p>
                            <center>
                                {{Form::submit('Send', ['name' => 'submit', 'class' => 'wpcf7-form-control wpcf7-submit button'])}}
                            </center>
                        </p>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>

        </div>

    </div>

</main>
<!-- #main -->
@endsection 