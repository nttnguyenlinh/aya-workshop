@extends('layouts.home')

@section('title', $post->name . ' - AYA WORKSHOP')

@section('content')

<div class="page-title blog-featured-title featured-title no-overflow">
    <div class="page-title-bg fill">
        <div class="title-bg fill bg-fill bg-top parallax-active" style="background-image: url({{asset('storage/'.$post->thumbnail)}}); height: 322.778px; transform: translate3d(0px, -9.22px, 0px); backface-visibility: hidden;" data-parallax-fade="true" data-parallax="-2" data-parallax-background="" data-parallax-container=".page-title"></div>
        <div class="title-overlay fill" style="background-color: rgba(0,0,0,.5)"></div>
    </div>

    <div class="page-title-inner container  flex-row  dark is-large" style="min-height: 300px">
        <div class="flex-col flex-center text-center">
            <h6 class="entry-category is-xsmall">
                <a href="{{route('post-category', $post->category_slug)}}" rel="category tag">{{$post->category_name}}</a>
            </h6>

            <h1 class="entry-title">{{$post->name}}</h1>
            <div class="entry-divider is-divider small"></div>

            <div class="entry-meta uppercase is-xsmall">
                <span class="posted-on">Posted on <a href="{{route('postDetails', $post->slug)}}" rel="bookmark"><time class="entry-date published" datetime="{{$post->created_at}}">{{Date_format($post->created_at, "Y-m-d")}}</time></div><!-- .entry-meta -->
        </div>
    </div><!-- flex-row -->
</div>

<main id="main" style="min-height:800px;">
    <div id="content" class="blog-wrapper blog-single page-wrapper">
        <div class="row row-large row-divided ">
            <div class="large-9 col">
                <article id="post-781" class="post-781 post type-post status-publish format-standard has-post-thumbnail hentry category-reviews tag-aya-nightingale-yk-s">
                    <div class="article-inner ">
                        <div class="entry-content single-page">
                            {!!$post->contents!!}
                            <p></p>
                            <div class="blog-share text-center">
                                <div class="is-divider medium"></div>
                                <div class="social-icons share-icons share-row relative"><a href="whatsapp://send?text={{$post->name}} - {{route('postDetails', $post->slug)}}" data-action="share/whatsapp/share" class="icon button circle is-outline tooltip whatsapp show-for-medium tooltipstered"><i class="icon-phone"></i></a><a href="//www.facebook.com/sharer.php?u={{route('postDetails', $post->slug)}}" data-label="Facebook" onclick="window.open(this.href,this.title,'width=500,height=500,top=300px,left=300px');  return false;" rel="noopener noreferrer nofollow" target="_blank" class="icon button circle is-outline tooltip facebook tooltipstered"><i class="icon-facebook"></i></a><a href="//twitter.com/share?url={{route('postDetails', $post->slug)}}" onclick="window.open(this.href,this.title,'width=500,height=500,top=300px,left=300px');  return false;" rel="noopener noreferrer nofollow" target="_blank" class="icon button circle is-outline tooltip twitter tooltipstered"><i class="icon-twitter"></i></a><a href="mailto:enteryour@addresshere.com?subject={{$post->name}}&amp;body=Check%20this%20out:%20{{route('postDetails', $post->slug)}}" rel="nofollow" class="icon button circle is-outline tooltip email tooltipstered"><i class="icon-envelop"></i></a><a href="//pinterest.com/pin/create/button/?url={{route('postDetails', $post->slug)}}&amp;media={{asset($post->thumbnail)}}&amp;description={{$post->description}}" onclick="window.open(this.href,this.title,'width=500,height=500,top=300px,left=300px');  return false;" rel="noopener noreferrer nofollow" target="_blank" class="icon button circle is-outline tooltip pinterest tooltipstered"><i class="icon-pinterest"></i></a><a href="//plus.google.com/share?url={{route('postDetails', $post->slug)}}" target="_blank" class="icon button circle is-outline tooltip google-plus tooltipstered" onclick="window.open(this.href,this.title,'width=500,height=500,top=300px,left=300px');  return false;" rel="noopener noreferrer nofollow"><i class="icon-google-plus"></i></a><a href="//www.linkedin.com/shareArticle?mini=true&amp;url={{route('postDetails', $post->slug)}}&amp;title={{$post->name}}" onclick="window.open(this.href,this.title,'width=500,height=500,top=300px,left=300px');  return false;" rel="noopener noreferrer nofollow" target="_blank" class="icon button circle is-outline tooltip linkedin tooltipstered"><i class="icon-linkedin"></i></a></div>
                            </div>
                        </div><!-- .entry-content2 -->
                    </div><!-- .article-inner -->
                </article><!-- #-781 -->

                <div class="wpdiscuz_top_clearing"></div>
                <div id="comments" class="comments-area">
                    <div id="respond" style="width: 0;height: 0;clear: both;margin: 0;padding: 0;"></div>
                    <h3 id="wc-comment-header">
                        Leave a Reply </h3>
                    <div id="wpcomm" class="wpdiscuz_unauth wpd-default">
                        <div class="col large-12" id="comments">
                            <div class="fb-comments" data-mobile="Auto-detected" data-order-by="time" data-href="{{route('postDetails', $post->slug)}}" data-width="100%" data-numposts="30"></div>
                        </div>
                    </div>
                </div>
            </div> <!-- .large-9 -->

            <div class="post-sidebar large-3 col">
                <div id="secondary" class="widget-area" role="complementary">
                    <aside class="widget flatsome_recent_posts"><span class="widget-title"><span>Latest Posts</span></span>
                        <div class="is-divider small"></div>
                        <ul>
                            @foreach($latestPosts as $latestPost)
                                <li class="recent-blog-posts-li">
                                    <div class="flex-row recent-blog-posts align-top pt-half pb-half">
                                        <div class="flex-col mr-half">
                                            <div class="badge post-date  badge-square">
                                                <div class="badge-inner bg-fill" style="background: linear-gradient( rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.2) ), url({{asset('storage/'.$latestPost->thumbnail)}}); color:#fff; text-shadow:1px 1px 0px rgba(0,0,0,.5); border:0;">
                                                    <span class="post-date-day">{{Date_format($latestPost->created_at, "d")}}</span><br>
                                                    <span class="post-date-month is-xsmall">{{Date_format($latestPost->created_at, "M")}}</span>
                                                </div>
                                            </div>
                                        </div><!-- .flex-col -->
                                        <div class="flex-col flex-grow">
                                            <a href="{{route('postDetails', $latestPost->slug)}}" title="{{$latestPost->name}}">{{$latestPost->name}}</a>
                                            <span class="post_comments op-7 block is-xsmall"><a href="{{route('postDetails', $latestPost->slug)}}"></a></span>
                                        </div>
                                    </div><!-- .flex-row -->
                                </li>
                            @endforeach
                        </ul>
                    </aside>

                    <aside class="widget"><span class="widget-title"><span>Categories</span></span>
                        <div class="is-divider small"></div>
                        <ul>
                            @foreach($categories as $category)
                                <li class="cat-item">
                                    <a href="{{route('post-category', $category->slug)}}">{{$category->name}}</a>
                                </li>
                            @endforeach
                        </ul>
                    </aside>
                </div><!-- #secondary -->
            </div><!-- .post-sidebar -->
        </div><!-- .row -->
    </div><!-- #content .page-wrapper -->
</main>
@endsection