@extends('layouts.home')
@section('title', __('checkout') . ' - ' . config('app.name'))
@section('content')

<?php
$countries = array(
    "AF" => "Afghanistan",
    "AX" => "&Aring;land Islands",
    "AL" => "Albania",
    "DZ" => "Algeria",
    "AS" => "American Samoa",
    "AD" => "Andorra",
    "AO" => "Angola",
    "AI" => "Anguilla",
    "AQ" => "Antarctica",
    "AG" => "Antigua and Barbuda",
    "AR" => "Argentina",
    "AM" => "Armenia",
    "AW" => "Aruba",
    "AU" => "Australia",
    "AT" => "Austria",
    "AZ" => "Azerbaijan",
    "BS" => "Bahamas",
    "BH" => "Bahrain",
    "BD" => "Bangladesh",
    "BB" => "Barbados",
    "BY" => "Belarus",
    "BE" => "Belgium",
    "BZ" => "Belize",
    "BJ" => "Benin",
    "BM" => "Bermuda",
    "BT" => "Bhutan",
    "BO" => "Bolivia, Plurinational State of",
    "BA" => "Bosnia and Herzegovina",
    "BW" => "Botswana",
    "BV" => "Bouvet Island",
    "BR" => "Brazil",
    "IO" => "British Indian Ocean Territory",
    "BN" => "Brunei Darussalam",
    "BG" => "Bulgaria",
    "BF" => "Burkina Faso",
    "BI" => "Burundi",
    "KH" => "Cambodia",
    "CM" => "Cameroon",
    "CA" => "Canada",
    "CV" => "Cape Verde",
    "KY" => "Cayman Islands",
    "CF" => "Central African Republic",
    "TD" => "Chad",
    "CL" => "Chile",
    "CN" => "China",
    "CX" => "Christmas Island",
    "CC" => "Cocos (Keeling) Islands",
    "CO" => "Colombia",
    "KM" => "Comoros",
    "CG" => "Congo",
    "CD" => "Congo, the Democratic Republic of the",
    "CK" => "Cook Islands",
    "CR" => "Costa Rica",
    "CI" => "C&ocirc;te d'Ivoire",
    "HR" => "Croatia",
    "CU" => "Cuba",
    "CY" => "Cyprus",
    "CZ" => "Czech Republic",
    "DK" => "Denmark",
    "DJ" => "Djibouti",
    "DM" => "Dominica",
    "DO" => "Dominican Republic",
    "EC" => "Ecuador",
    "EG" => "Egypt",
    "SV" => "El Salvador",
    "GQ" => "Equatorial Guinea",
    "ER" => "Eritrea",
    "EE" => "Estonia",
    "ET" => "Ethiopia",
    "FK" => "Falkland Islands (Malvinas)",
    "FO" => "Faroe Islands",
    "FJ" => "Fiji",
    "FI" => "Finland",
    "FR" => "France",
    "GF" => "French Guiana",
    "PF" => "French Polynesia",
    "TF" => "French Southern Territories",
    "GA" => "Gabon",
    "GM" => "Gambia",
    "GE" => "Georgia",
    "DE" => "Germany",
    "GH" => "Ghana",
    "GI" => "Gibraltar",
    "GR" => "Greece",
    "GL" => "Greenland",
    "GD" => "Grenada",
    "GP" => "Guadeloupe",
    "GU" => "Guam",
    "GT" => "Guatemala",
    "GG" => "Guernsey",
    "GN" => "Guinea",
    "GW" => "Guinea-Bissau",
    "GY" => "Guyana",
    "HT" => "Haiti",
    "HM" => "Heard Island and McDonald Islands",
    "VA" => "Holy See (Vatican City State)",
    "HN" => "Honduras",
    "HK" => "Hong Kong",
    "HU" => "Hungary",
    "IS" => "Iceland",
    "IN" => "India",
    "ID" => "Indonesia",
    "IR" => "Iran, Islamic Republic of",
    "IQ" => "Iraq",
    "IE" => "Ireland",
    "IM" => "Isle of Man",
    "IL" => "Israel",
    "IT" => "Italy",
    "JM" => "Jamaica",
    "JP" => "Japan",
    "JE" => "Jersey",
    "JO" => "Jordan",
    "KZ" => "Kazakhstan",
    "KE" => "Kenya",
    "KI" => "Kiribati",
    "KP" => "Korea, Democratic People's Republic of",
    "KR" => "Korea, Republic of",
    "KW" => "Kuwait",
    "KG" => "Kyrgyzstan",
    "LA" => "Lao People's Democratic Republic",
    "LV" => "Latvia",
    "LB" => "Lebanon",
    "LS" => "Lesotho",
    "LR" => "Liberia",
    "LY" => "Libyan Arab Jamahiriya",
    "LI" => "Liechtenstein",
    "LT" => "Lithuania",
    "LU" => "Luxembourg",
    "MO" => "Macao",
    "MK" => "Macedonia, the former Yugoslav Republic of",
    "MG" => "Madagascar",
    "MW" => "Malawi",
    "MY" => "Malaysia",
    "MV" => "Maldives",
    "ML" => "Mali",
    "MT" => "Malta",
    "MH" => "Marshall Islands",
    "MQ" => "Martinique",
    "MR" => "Mauritania",
    "MU" => "Mauritius",
    "YT" => "Mayotte",
    "MX" => "Mexico",
    "FM" => "Micronesia, Federated States of",
    "MD" => "Moldova, Republic of",
    "MC" => "Monaco",
    "MN" => "Mongolia",
    "ME" => "Montenegro",
    "MS" => "Montserrat",
    "MA" => "Morocco",
    "MZ" => "Mozambique",
    "MM" => "Myanmar",
    "NA" => "Namibia",
    "NR" => "Nauru",
    "NP" => "Nepal",
    "NL" => "Netherlands",
    "AN" => "Netherlands Antilles",
    "NC" => "New Caledonia",
    "NZ" => "New Zealand",
    "NI" => "Nicaragua",
    "NE" => "Niger",
    "NG" => "Nigeria",
    "NU" => "Niue",
    "NF" => "Norfolk Island",
    "MP" => "Northern Mariana Islands",
    "NO" => "Norway",
    "OM" => "Oman",
    "PK" => "Pakistan",
    "PW" => "Palau",
    "PS" => "Palestinian Territory, Occupied",
    "PA" => "Panama",
    "PG" => "Papua New Guinea",
    "PY" => "Paraguay",
    "PE" => "Peru",
    "PH" => "Philippines",
    "PN" => "Pitcairn",
    "PL" => "Poland",
    "PT" => "Portugal",
    "PR" => "Puerto Rico",
    "QA" => "Qatar",
    "RE" => "R&eacute;union",
    "RO" => "Romania",
    "RU" => "Russian Federation",
    "RW" => "Rwanda",
    "BL" => "Saint Barth&eacute;lemy",
    "SH" => "Saint Helena, Ascension and Tristan da Cunha",
    "KN" => "Saint Kitts and Nevis",
    "LC" => "Saint Lucia",
    "MF" => "Saint Martin (French part)",
    "PM" => "Saint Pierre and Miquelon",
    "VC" => "Saint Vincent and the Grenadines",
    "WS" => "Samoa",
    "SM" => "San Marino",
    "ST" => "Sao Tome and Principe",
    "SA" => "Saudi Arabia",
    "SN" => "Senegal",
    "RS" => "Serbia",
    "SC" => "Seychelles",
    "SL" => "Sierra Leone",
    "SG" => "Singapore",
    "SK" => "Slovakia",
    "SI" => "Slovenia",
    "SB" => "Solomon Islands",
    "SO" => "Somalia",
    "ZA" => "South Africa",
    "GS" => "South Georgia and the South Sandwich Islands",
    "ES" => "Spain",
    "LK" => "Sri Lanka",
    "SD" => "Sudan",
    "SR" => "Suriname",
    "SJ" => "Svalbard and Jan Mayen",
    "SZ" => "Swaziland",
    "SE" => "Sweden",
    "CH" => "Switzerland",
    "SY" => "Syrian Arab Republic",
    "TW" => "Taiwan, Province of China",
    "TJ" => "Tajikistan",
    "TZ" => "Tanzania, United Republic of",
    "TH" => "Thailand",
    "TL" => "Timor-Leste",
    "TG" => "Togo",
    "TK" => "Tokelau",
    "TO" => "Tonga",
    "TT" => "Trinidad and Tobago",
    "TN" => "Tunisia",
    "TR" => "Turkey",
    "TM" => "Turkmenistan",
    "TC" => "Turks and Caicos Islands",
    "TV" => "Tuvalu",
    "UG" => "Uganda",
    "UA" => "Ukraine",
    "AE" => "United Arab Emirates",
    "GB" => "United Kingdom",
    "US" => "United States",
    "UM" => "United States Minor Outlying Islands",
    "UY" => "Uruguay",
    "UZ" => "Uzbekistan",
    "VU" => "Vanuatu",
    "VE" => "Venezuela, Bolivarian Republic of",
    "VN" => "Việt Nam",
    "VG" => "Virgin Islands, British",
    "VI" => "Virgin Islands, U.S.",
    "WF" => "Wallis and Futuna",
    "EH" => "Western Sahara",
    "YE" => "Yemen",
    "ZM" => "Zambia",
    "ZW" => "Zimbabwe",
);
?>
<main id="main" style="min-height:800px;">
    <div class="checkout-page-title page-title">
        <div class="page-title-inner flex-row medium-flex-wrap container">
            <div class="flex-col flex-grow medium-text-center">
                <nav class="breadcrumbs heading-font checkout-breadcrumbs text-center h2 strong">
                    <a href="{{route('cart')}}" class="hide-for-small">{{__('shopping_cart')}}</a>
                    <span class="divider hide-for-small"><i class="icon-angle-right"></i></span>
                    <a href="{{route('checkout')}}" class="current">{{__('checkout_details')}}</a>
                    <span class="divider hide-for-small"><i class="icon-angle-right"></i></span>
                    <a href="#" class="no-click hide-for-small">{{__('order_complete')}}</a>
                </nav>
            </div><!-- .flex-left -->
        </div><!-- flex-row -->
    </div><!-- .page-title -->

    @if(Session::has('cart') && Cart::content()->count() > 0)
    <div class="cart-container container page-wrapper page-checkout">
        <div class="woocommerce">
            <div class="woocommerce-form-login-toggle">
                <div class="woocommerce-info message-wrapper">
                    @if(!\Auth::check())
                    <div class="message-container container medium-text-center">
                        Returning customer? <a href="{{route('account.getLogin')}}" data-open="#login-form-popup">Click
                            here to login or register</a>
                    </div>
                    <p style="color:#ff8040; font-style: italic;">If you have shopped with us before, <i
                            class="fal fa-hand-point-up"></i> please login <i class="fal fa-hand-point-up"></i>. If you
                        are a new customer, <i class="fal fa-hand-point-down"></i> please proceed to the Billing section
                        <i class="fal fa-hand-point-down"></i>.</p>
                    @endif
                    <p style="color:red; font-style: italic;"><i class="fal fa-do-not-enter"></i> We will not ship if
                        your order information is fake!</p>
                </div>
            </div>
            <div class="woocommerce-notices-wrapper"></div>

            <form method="post" action="{{route('postCheckout')}}" class="checkout woocommerce-checkout"
                enctype="multipart/form-data">
                @csrf
                <div class="row pt-0 ">
                    @if(\Auth::check())
                    <div class="large-7 col">
                        <div id="customer_details">
                            <div class="clear">
                                <div class="woocommerce-billing-fields">
                                    <h3>Billing details</h3>
                                    <div class="woocommerce-billing-fields__field-wrapper">
                                        <p>
                                            <label>Full name <span class="required"
                                                    style="color:red;">(*)</span></label>
                                            <input type="text" name="fullname" id="fullname"
                                                value="{{Auth::user()->nicename}}" maxlength="50" required>
                                        </p>

                                        <p>
                                            <label>Email address <span class="required"
                                                    style="color:red;">(*)</span></label>
                                            <input type="email" name="email_address" id="email_address"
                                                autocomplete="email" value="{{Auth::user()->email}}" maxlength="50"
                                                required readonly>
                                        </p>

                                        <p>

                                            <label>Country <span class="required" style="color:red;">(*)</span></label>
                                            <select required name="country" id="select-country" class="chosen-select"
                                                data-placeholder="Choose a country...">
                                                @foreach($countries as $key => $value)
                                                @if($key == $customer->country)
                                                <option value="{{$key}}" selected>{{$value}}</option>
                                                @else
                                                <option value="{{$key}}">{{$value}}</option>
                                                @endif
                                                @endforeach
                                            </select>
                                        </p>

                                        <p>
                                            <label>Address <span class="required" style="color:red;">(*)</span></label>
                                            <input type="text" name="address" id="address"
                                                placeholder="House number and street address"
                                                value="{{$customer->address}}" autocomplete required>
                                        </p>

                                        <p>
                                            <label>Town / City <span class="required"
                                                    style="color:red;">(*)</span></label>
                                            <input type="text" name="city" id="city" placeholder=""
                                                value="{{$customer->city}}" autocomplete required>
                                        </p>

                                        <p>
                                            <label>State / County (optional)</label>
                                            <input type="text" name="county" id="county" placeholder=""
                                                value="{{$customer->county}}" autocomplete>
                                        </p>

                                        <p>
                                            <label>Postcode / ZIP (optional)</label>
                                            <input type="text" name="zipcode" id="zipcode" placeholder=""
                                                value="{{$customer->zipcode}}" autocomplete>
                                        </p>

                                        <p>
                                            <label>Phone <span class="required" style="color:red;">(*)</span></label>
                                            <input type="text" name="phone" id="phone" placeholder=""
                                                value="{{$customer->phone}}" maxlength="15" autocomplete required>
                                        </p>

                                    </div>
                                </div>
                            </div>

                            <div class="clear">
                                <div class="woocommerce-shipping-fields">
                                </div>
                                <div class="woocommerce-additional-fields">
                                    <h3>Additional information</h3>
                                    <div class="woocommerce-additional-fields__field-wrapper">
                                        <p class="form-row notes">
                                            <label>Order notes<span class="optional">(optional)</span></label>
                                            <span class="woocommerce-input-wrapper">
                                                <textarea name="notes" class="input-text " id="notes"
                                                    placeholder="Notes about your order, e.g. special notes for delivery."
                                                    rows="2" cols="5">{{old('notes')}}</textarea>
                                            </span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- large-7 -->

                    @else
                    <div class="large-7 col">
                        <div id="customer_details">
                            <div class="clear">
                                <div class="woocommerce-billing-fields">
                                    <h3>Billing details</h3>
                                    <div class="woocommerce-billing-fields__field-wrapper">
                                        <p>
                                            <label>Full name <span class="required"
                                                    style="color:red;">(*)</span></label>
                                            <input type="text" name="fullname" id="fullname" value="{{old('fullname')}}"
                                                maxlength="50" required>
                                        </p>

                                        <p>
                                            <label>Email address <span class="required"
                                                    style="color:red;">(*)</span></label>
                                            <input type="email" name="email_address" id="email_address"
                                                autocomplete="email" value="{{old('email_address')}}" maxlength="50"
                                                required>
                                        </p>

                                        <p>
                                            <?php
                                                $countries = array(
                                                    "AF" => "Afghanistan",
                                                    "AX" => "Aring land Islands",
                                                    "AL" => "Albania",
                                                    "DZ" => "Algeria",
                                                    "AS" => "American Samoa",
                                                    "AD" => "Andorra",
                                                    "AO" => "Angola",
                                                    "AI" => "Anguilla",
                                                    "AQ" => "Antarctica",
                                                    "AG" => "Antigua and Barbuda",
                                                    "AR" => "Argentina",
                                                    "AM" => "Armenia",
                                                    "AW" => "Aruba",
                                                    "AU" => "Australia",
                                                    "AT" => "Austria",
                                                    "AZ" => "Azerbaijan",
                                                    "BS" => "Bahamas",
                                                    "BH" => "Bahrain",
                                                    "BD" => "Bangladesh",
                                                    "BB" => "Barbados",
                                                    "BY" => "Belarus",
                                                    "BE" => "Belgium",
                                                    "BZ" => "Belize",
                                                    "BJ" => "Benin",
                                                    "BM" => "Bermuda",
                                                    "BT" => "Bhutan",
                                                    "BO" => "Bolivia, Plurinational State of",
                                                    "BA" => "Bosnia and Herzegovina",
                                                    "BW" => "Botswana",
                                                    "BV" => "Bouvet Island",
                                                    "BR" => "Brazil",
                                                    "IO" => "British Indian Ocean Territory",
                                                    "BN" => "Brunei Darussalam",
                                                    "BG" => "Bulgaria",
                                                    "BF" => "Burkina Faso",
                                                    "BI" => "Burundi",
                                                    "KH" => "Cambodia",
                                                    "CM" => "Cameroon",
                                                    "CA" => "Canada",
                                                    "CV" => "Cape Verde",
                                                    "KY" => "Cayman Islands",
                                                    "CF" => "Central African Republic",
                                                    "TD" => "Chad",
                                                    "CL" => "Chile",
                                                    "CN" => "China",
                                                    "CX" => "Christmas Island",
                                                    "CC" => "Cocos (Keeling) Islands",
                                                    "CO" => "Colombia",
                                                    "KM" => "Comoros",
                                                    "CG" => "Congo",
                                                    "CD" => "Congo, the Democratic Republic of the",
                                                    "CK" => "Cook Islands",
                                                    "CR" => "Costa Rica",
                                                    "CI" => "C&ocirc;te d'Ivoire",
                                                    "HR" => "Croatia",
                                                    "CU" => "Cuba",
                                                    "CY" => "Cyprus",
                                                    "CZ" => "Czech Republic",
                                                    "DK" => "Denmark",
                                                    "DJ" => "Djibouti",
                                                    "DM" => "Dominica",
                                                    "DO" => "Dominican Republic",
                                                    "EC" => "Ecuador",
                                                    "EG" => "Egypt",
                                                    "SV" => "El Salvador",
                                                    "GQ" => "Equatorial Guinea",
                                                    "ER" => "Eritrea",
                                                    "EE" => "Estonia",
                                                    "ET" => "Ethiopia",
                                                    "FK" => "Falkland Islands (Malvinas)",
                                                    "FO" => "Faroe Islands",
                                                    "FJ" => "Fiji",
                                                    "FI" => "Finland",
                                                    "FR" => "France",
                                                    "GF" => "French Guiana",
                                                    "PF" => "French Polynesia",
                                                    "TF" => "French Southern Territories",
                                                    "GA" => "Gabon",
                                                    "GM" => "Gambia",
                                                    "GE" => "Georgia",
                                                    "DE" => "Germany",
                                                    "GH" => "Ghana",
                                                    "GI" => "Gibraltar",
                                                    "GR" => "Greece",
                                                    "GL" => "Greenland",
                                                    "GD" => "Grenada",
                                                    "GP" => "Guadeloupe",
                                                    "GU" => "Guam",
                                                    "GT" => "Guatemala",
                                                    "GG" => "Guernsey",
                                                    "GN" => "Guinea",
                                                    "GW" => "Guinea-Bissau",
                                                    "GY" => "Guyana",
                                                    "HT" => "Haiti",
                                                    "HM" => "Heard Island and McDonald Islands",
                                                    "VA" => "Holy See (Vatican City State)",
                                                    "HN" => "Honduras",
                                                    "HK" => "Hong Kong",
                                                    "HU" => "Hungary",
                                                    "IS" => "Iceland",
                                                    "IN" => "India",
                                                    "ID" => "Indonesia",
                                                    "IR" => "Iran, Islamic Republic of",
                                                    "IQ" => "Iraq",
                                                    "IE" => "Ireland",
                                                    "IM" => "Isle of Man",
                                                    "IL" => "Israel",
                                                    "IT" => "Italy",
                                                    "JM" => "Jamaica",
                                                    "JP" => "Japan",
                                                    "JE" => "Jersey",
                                                    "JO" => "Jordan",
                                                    "KZ" => "Kazakhstan",
                                                    "KE" => "Kenya",
                                                    "KI" => "Kiribati",
                                                    "KP" => "Korea, Democratic People's Republic of",
                                                    "KR" => "Korea, Republic of",
                                                    "KW" => "Kuwait",
                                                    "KG" => "Kyrgyzstan",
                                                    "LA" => "Lao People's Democratic Republic",
                                                    "LV" => "Latvia",
                                                    "LB" => "Lebanon",
                                                    "LS" => "Lesotho",
                                                    "LR" => "Liberia",
                                                    "LY" => "Libyan Arab Jamahiriya",
                                                    "LI" => "Liechtenstein",
                                                    "LT" => "Lithuania",
                                                    "LU" => "Luxembourg",
                                                    "MO" => "Macao",
                                                    "MK" => "Macedonia, the former Yugoslav Republic of",
                                                    "MG" => "Madagascar",
                                                    "MW" => "Malawi",
                                                    "MY" => "Malaysia",
                                                    "MV" => "Maldives",
                                                    "ML" => "Mali",
                                                    "MT" => "Malta",
                                                    "MH" => "Marshall Islands",
                                                    "MQ" => "Martinique",
                                                    "MR" => "Mauritania",
                                                    "MU" => "Mauritius",
                                                    "YT" => "Mayotte",
                                                    "MX" => "Mexico",
                                                    "FM" => "Micronesia, Federated States of",
                                                    "MD" => "Moldova, Republic of",
                                                    "MC" => "Monaco",
                                                    "MN" => "Mongolia",
                                                    "ME" => "Montenegro",
                                                    "MS" => "Montserrat",
                                                    "MA" => "Morocco",
                                                    "MZ" => "Mozambique",
                                                    "MM" => "Myanmar",
                                                    "NA" => "Namibia",
                                                    "NR" => "Nauru",
                                                    "NP" => "Nepal",
                                                    "NL" => "Netherlands",
                                                    "AN" => "Netherlands Antilles",
                                                    "NC" => "New Caledonia",
                                                    "NZ" => "New Zealand",
                                                    "NI" => "Nicaragua",
                                                    "NE" => "Niger",
                                                    "NG" => "Nigeria",
                                                    "NU" => "Niue",
                                                    "NF" => "Norfolk Island",
                                                    "MP" => "Northern Mariana Islands",
                                                    "NO" => "Norway",
                                                    "OM" => "Oman",
                                                    "PK" => "Pakistan",
                                                    "PW" => "Palau",
                                                    "PS" => "Palestinian Territory, Occupied",
                                                    "PA" => "Panama",
                                                    "PG" => "Papua New Guinea",
                                                    "PY" => "Paraguay",
                                                    "PE" => "Peru",
                                                    "PH" => "Philippines",
                                                    "PN" => "Pitcairn",
                                                    "PL" => "Poland",
                                                    "PT" => "Portugal",
                                                    "PR" => "Puerto Rico",
                                                    "QA" => "Qatar",
                                                    "RE" => "R&eacute;union",
                                                    "RO" => "Romania",
                                                    "RU" => "Russian Federation",
                                                    "RW" => "Rwanda",
                                                    "BL" => "Saint Barth&eacute;lemy",
                                                    "SH" => "Saint Helena, Ascension and Tristan da Cunha",
                                                    "KN" => "Saint Kitts and Nevis",
                                                    "LC" => "Saint Lucia",
                                                    "MF" => "Saint Martin (French part)",
                                                    "PM" => "Saint Pierre and Miquelon",
                                                    "VC" => "Saint Vincent and the Grenadines",
                                                    "WS" => "Samoa",
                                                    "SM" => "San Marino",
                                                    "ST" => "Sao Tome and Principe",
                                                    "SA" => "Saudi Arabia",
                                                    "SN" => "Senegal",
                                                    "RS" => "Serbia",
                                                    "SC" => "Seychelles",
                                                    "SL" => "Sierra Leone",
                                                    "SG" => "Singapore",
                                                    "SK" => "Slovakia",
                                                    "SI" => "Slovenia",
                                                    "SB" => "Solomon Islands",
                                                    "SO" => "Somalia",
                                                    "ZA" => "South Africa",
                                                    "GS" => "South Georgia and the South Sandwich Islands",
                                                    "ES" => "Spain",
                                                    "LK" => "Sri Lanka",
                                                    "SD" => "Sudan",
                                                    "SR" => "Suriname",
                                                    "SJ" => "Svalbard and Jan Mayen",
                                                    "SZ" => "Swaziland",
                                                    "SE" => "Sweden",
                                                    "CH" => "Switzerland",
                                                    "SY" => "Syrian Arab Republic",
                                                    "TW" => "Taiwan, Province of China",
                                                    "TJ" => "Tajikistan",
                                                    "TZ" => "Tanzania, United Republic of",
                                                    "TH" => "Thailand",
                                                    "TL" => "Timor-Leste",
                                                    "TG" => "Togo",
                                                    "TK" => "Tokelau",
                                                    "TO" => "Tonga",
                                                    "TT" => "Trinidad and Tobago",
                                                    "TN" => "Tunisia",
                                                    "TR" => "Turkey",
                                                    "TM" => "Turkmenistan",
                                                    "TC" => "Turks and Caicos Islands",
                                                    "TV" => "Tuvalu",
                                                    "UG" => "Uganda",
                                                    "UA" => "Ukraine",
                                                    "AE" => "United Arab Emirates",
                                                    "GB" => "United Kingdom",
                                                    "US" => "United States",
                                                    "UM" => "United States Minor Outlying Islands",
                                                    "UY" => "Uruguay",
                                                    "UZ" => "Uzbekistan",
                                                    "VU" => "Vanuatu",
                                                    "VE" => "Venezuela, Bolivarian Republic of",
                                                    "VN" => "Việt Nam",
                                                    "VG" => "Virgin Islands, British",
                                                    "VI" => "Virgin Islands, U.S.",
                                                    "WF" => "Wallis and Futuna",
                                                    "EH" => "Western Sahara",
                                                    "YE" => "Yemen",
                                                    "ZM" => "Zambia",
                                                    "ZW" => "Zimbabwe",
                                                );
                                            ?>
                                            <label>Country <span class="required" style="color:red;">(*)</span></label>
                                            <select required name="country" id="select-country" class="chosen-select"
                                                data-placeholder="Choose a country...">
                                                @foreach($countries as $k => $v)
                                                <option value="{{$k}}" {{old('country') ? 'checked' : '' }}>{{$v}}
                                                </option>
                                                @endforeach
                                            </select>
                                        </p>

                                        <p>
                                            <label>Address <span class="required" style="color:red;">(*)</span></label>
                                            <input type="text" name="address" id="address"
                                                placeholder="House number and street address" value="{{old('address')}}"
                                                autocomplete required>
                                        </p>

                                        <p>
                                            <label>Town / City <span class="required"
                                                    style="color:red;">(*)</span></label>
                                            <input type="text" name="city" id="city" placeholder=""
                                                value="{{old('city')}}" autocomplete required>
                                        </p>

                                        <p>
                                            <label>State / County (optional)</label>
                                            <input type="text" name="county" id="county" placeholder=""
                                                value="{{old('county')}}" autocomplete>
                                        </p>

                                        <p>
                                            <label>Postcode / ZIP (optional)</label>
                                            <input type="text" name="zipcode" id="zipcode" placeholder=""
                                                value="{{old('zipcode')}}" autocomplete>
                                        </p>

                                        <p>
                                            <label>Phone <span class="required" style="color:red;">(*)</span></label>
                                            <input type="text" name="phone" id="phone" placeholder=""
                                                value="{{old('phone')}}" maxlength="15" autocomplete required>
                                        </p>

                                    </div>
                                </div>
                            </div>

                            <div class="clear">
                                <div class="woocommerce-shipping-fields">
                                </div>
                                <div class="woocommerce-additional-fields">
                                    <h3>Additional information</h3>
                                    <div class="woocommerce-additional-fields__field-wrapper">
                                        <p class="form-row notes">
                                            <label>Order notes<span class="optional">(optional)</span></label>
                                            <span class="woocommerce-input-wrapper">
                                                <textarea name="notes" class="input-text " id="notes"
                                                    placeholder="Notes about your order, e.g. special notes for delivery."
                                                    rows="2" cols="5">{{old('notes')}}</textarea>
                                            </span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- large-7 -->
                    @endif
                    <div class="large-5 col">
                        <div class="col-inner has-border">
                            <div class="checkout-sidebar sm-touch-scroll">
                                <h3 id="order_review_heading">Your order</h3>
                                <div id="order_review" class="woocommerce-checkout-review-order">
                                    <table class="shop_table woocommerce-checkout-review-order-table">
                                        <thead>
                                            <tr>
                                                <th class="product-name">Product</th>
                                                <th class="product-total">Total</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            @foreach(\Cart::content() as $item)
                                            <tr class="cart_item">
                                                <td class="product-name">
                                                    {{$item->name}} &nbsp; <strong class="product-quantity">×
                                                        {{$item->qty}}</strong> &nbsp; <strong
                                                        class="product-quantity">× <span
                                                            class="woocommerce-Price-currencySymbol">$</span>{{$item->price}}</strong>
                                                </td>
                                                <td class="product-total">
                                                    <span class="woocommerce-Price-amount amount"><span
                                                            class="woocommerce-Price-currencySymbol">$</span>{{number_format($item->qty * $item->price, 2)}}</span>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                        <tfoot id="tfoot-total">
                                            <tr class="order-total">
                                                <th>Total</th>
                                                <td><strong><span class="woocommerce-Price-amount amount"><span
                                                                class="woocommerce-Price-currencySymbol">$</span>{{Cart::subtotal()}}</span></strong>
                                                </td>
                                            </tr>
                                        </tfoot>
                                    </table>

                                    <label>Have a coupon?</label>
                                    <div class="woocommerce-info message-wrapper" id="coupon_info">
                                        
                                    </div>

                                    <div style="border: 2px #abd8d3 dashed; padding: 3px; margin-bottom:10px;">
                                        <div class="flex-row medium-flex-wrap">
                                            <div class="flex-col flex-grow">
                                                <input type="text" name="coupon_code" class="input-text"
                                                    placeholder="Coupon code" id="coupon_code" value="">
                                            </div>
                                            <div class="flex-col">
                                                <a id="apply_coupon" class="button expand">Apply</a>
                                            </div>
                                        </div>
                                    </div>


                                    <div id="payment" class="woocommerce-checkout-payment">
                                        <label for="payment_method_cod">Select payment method </label>
                                        <div class="payment_box payment_method_cod">
                                            <input type="radio" id="cod" name="payment-method" value="0" checked>
                                            <label for="cod"><img
                                                    src="{{asset('uploads/icons/cash_on_delivery.png')}}" /></label>
                                        </div>

                                        <div class="payment_box payment_method_cod">
                                            <input type="radio" id="vnpay" name="payment-method" value="1">
                                            <label for="vnpay"><img
                                                    src="{{asset('uploads/icons/vnpay.png')}}" /></label>
                                        </div>

                                        <center>
                                            <div class="g-recaptcha"
                                                data-sitekey="{{config('app.recaptcha_site_key')}}"></div>
                                        </center>

                                        <div class="form-row place-order" style="text-align:center;">
                                            <button type="submit" class="button button-primary"
                                                style="margin-top:10px;">Place order</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- large-5 -->
                </div><!-- row -->
            </form>
        </div>
    </div>
    @else
    <div class="cart-container container page-wrapper page-checkout">
        <div class="woocommerce-info message-wrapper">
            <div class="message-container container medium-text-center">{{__('checkout_not_avalible')}}</div>
        </div>
        <div class="woocommerce">
            <div class="text-center pt pb">
                <div class="woocommerce-notices-wrapper"></div>
                <p class="cart-empty">{{__('no_products_in_cart')}}</p>
                <p class="return-to-shop">
                    <a class="button primary wc-backward" href="{{route('products')}}">{{__('return_to_products')}}</a>
                </p>
            </div>
        </div>
        @endif
</main>

<script>
jQuery(document).ready(function($) {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(".chosen-select").chosen();
    
    $(document).on('click', '#apply_coupon', function() {
        var coupon_code = $('#coupon_code').val();
        console.log(coupon_code);
        $.ajax({
            url: '{{route('applyCoupon')}}',
            type: 'post',
            dataType: 'json',
            data: {
                coupon_code: coupon_code,
            },
            success: function(data) {
                if (data.alert == 'error') {
                    $('#coupon_info').html("<p style='color:red; font-style: italic;'>" + data.message + "</p>"); 
                } else {
                    $('#tfoot-total').html(data.cart_content);
                    $('#coupon_info').html("<p style='color:green; font-style: italic;'><i class='fas fa-check'></i> " + data.message + "</p>"); 
                }
            }
        });
    });
});
</script>
@endsection